# -*- coding: utf-8 -*-

import logging

from odoo import models, fields, api,_
import requests
from odoo.osv import osv
from odoo.tools.translate import _
import json

_logger = logging.getLogger(__name__)


class ParrotNotification(models.Model):
    _name = 'parrot.notification'
    _description = "Parrot Notification"
    _order = "create_date desc"

    name = fields.Char()
    record_id = fields.Integer()
    user_id = fields.Many2one('res.users', 'User', readonly=True)
    model_name = fields.Char()
    function_name = fields.Char()
    channel_name = fields.Char()
    notification_title = fields.Char()
    notification_body = fields.Char()
    message = fields.Text()
    state = fields.Selection([('seen', 'Seen'), ('new', 'New')], 'Status', readonly=True, default='new')
    document_reference_id = fields.Integer(string='Document Reference No.')

    @api.model
    def remove_duplicate(self):
        self.env.cr.execute("""
        DELETE FROM parrot_notification a
            WHERE EXISTS (
                SELECT * FROM parrot_notification b
                    WHERE b.notification_title = a.notification_title
                    AND b.notification_body = a.notification_body
                    AND b.channel_name = a.channel_name
                    AND b.model_name = a.model_name
                    AND b.user_id = a.user_id
                    AND b.record_id = a.record_id
                    AND b.id < a.id);""")
        _logger.info("Duplicate notification removed.")
        return True

class ParrotBroadcastNotification(models.Model):
    _name = 'parrot.broadcast.notification'
    _description = "Parrot Broadcast Notification"
    _order = "create_date desc"

    name = fields.Char()
    user_id = fields.Many2one('res.users', 'Sender Name')
    is_all = fields.Boolean('All')
    users = fields.Many2many('res.users', string='Users')
    region_ids = fields.Many2many('user.region', string='Select Region')
    channel_name = fields.Char()
    notification_title = fields.Char()
    notification_body = fields.Char()
    message = fields.Text()
    state = fields.Selection([('draft', 'Draft'), ('send', 'Send')], 'Status', default='draft')

    @api.one
    def send_broadcast_notification(self):
        self.write({'state': 'send'})
        _logger.info("App Update notification send to all users")
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        region_list = []
        users_list = []
        if self.is_all:
            users = self.env['res.users'].search([])
        elif self.users or self.region_ids:
            if self.users:
                users_list = self.users
            if self.region_ids:
                for region in self.region_ids:
                    region_list.append(region.value)
            region_user_list = self.env['res.users'].search([('region_id', 'in', region_list)])
            if len(users_list) > 0 and len(region_user_list) > 0:
                users = users_list + region_user_list
            elif len(users_list) > 0:
                users = users_list
            elif len(region_user_list) > 0:
                users = region_user_list
            else:
                users = []
        _logger.info("User count = %s" % len(users))
        if self.message:
            message = self.message
        else:
            message = ''
        if self.notification_title:
            title = self.notification_title
        else:
            title = ''
        if self.notification_body:
            body = self.notification_body
        else:
            body = ''
        if self.user_id:
            user_name = self.user_id.name
        else:
            user_name = ''
        for user in users:
            if user.user_tokens:
                for token in self.env['user.token'].browse(user.user_tokens.ids):
                    tokens.append(token.name)
        n = 500
        final = [tokens[i * n:(i + 1) * n] for i in range((len(tokens) + n - 1) // n)]
        for tkn in final:
            data_json = {"channeltype": "newBroadcastMessage",
                         "message": message, 'title': title, 'body': body, 'author': user_name}
            data = {'notification': {'title': title,
                                     'body': body,
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tkn,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                raise osv.except_osv(_('Notification not sent'))
            return True

    @api.one
    def button_draft(self):
        return self.write({'state': 'draft'})
