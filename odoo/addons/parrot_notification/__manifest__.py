# -*- coding: utf-8 -*-
{
    'name': "Parrot Notification",
    'summary': """This module uses to store and view notification.""",
    'description': """
        This module uses to store and view notification using both web and mobile
    """,
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': [],
    'data': [
#         'data/parrot_notification_data.xml',
        'views/parrot_notification_view.xml',
        'views/res_users_view.xml',
        'security/ir.model.access.csv',
    ],
    'auto_install': True,
}