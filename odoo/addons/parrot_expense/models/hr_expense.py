# -*- coding: utf-8 -*-

from odoo import models, fields, api


class HrExpense(models.Model):
    _inherit = "hr.expense"

    product_id = fields.Many2one('product.product', string='Product Test', required=False)

    @api.model
    def create(self, values):
        hr_expense = super(HrExpense, self).create(values)
        hr_expense.write({'state': 'submit'})
        return hr_expense
