# -*- coding: utf-8 -*-
{
    'name': "Parrot Expense",
    'summary': """Customized Expense Calculation for Parrot app""",
    'description': """
        Customized Expense Calculation for Parrot app.
    """,
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': [
        'hr_expense'
    ],
    'data': [
        'views/hr_expense_views.xml',
    ],
}
