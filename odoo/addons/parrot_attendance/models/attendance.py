import logging
import datetime
from datetime import datetime, date, time, timedelta
from odoo import models, fields, api
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.osv import osv
from odoo.tools.translate import _
import json
import requests
import os
import ast

_logger = logging.getLogger(__name__)


class MarkAttendance(models.Model):
    _name = 'mark.attendance'
    _description = "Mark Attendance"
    _order = 'id desc'


    login_image_url = fields.Char(string='Login Image URL')
    logout_image_url = fields.Char(string='Logout Image URL')
    login_latitude = fields.Float('Login Latitude', digits=(6, 10))
    login_longitude = fields.Float('Login Longitude', digits=(6, 10))
    logout_latitude = fields.Float('Logout Latitude', digits=(6, 10))
    logout_longitude = fields.Float('Logout Longitude', digits=(6, 10))
    login_time = fields.Datetime(string='Login Time')
    logout_time = fields.Datetime(string='Logout Time')
    login_status = fields.Char(string='Login Status')
    logout_status = fields.Char(string='Logout Status')

    @api.model
    def update_logout_details(self):
        vals = {}
        logout_obj = self.env['logout.detail'].search([], order='id desc', limit=1)
        logout_time = logout_obj.logout_time
        today_date_strp = datetime.strptime(logout_time, '%Y-%m-%d %H:%M:%S')

        ts = today_date_strp.strftime("%H:%M:%S")
        today_logout = datetime.today().date()
        today_logout_time = str(today_logout) + ' ' + ts
        vals['logout_status'] = 'logout'
        vals['logout_time'] = today_logout_time
        today_start = datetime.now().replace(hour=00, minute=00, second=0, microsecond=0).strftime(
            DEFAULT_SERVER_DATETIME_FORMAT)
        today_end = datetime.now().replace(hour=23, minute=59, second=59, microsecond=59).strftime(
            DEFAULT_SERVER_DATETIME_FORMAT)
        res_obj = self.search([('create_date', '>=', today_start), ('create_date', '<=', today_end)], order='id desc', )
        for res in res_obj:
            if not res.logout_time:
                res.write(vals)
        return True

    @api.multi
    def mobile_create(self, vals):
        user_id = self._uid
        today_start = datetime.now().replace(hour=00, minute=00, second=0, microsecond=0).strftime(
            DEFAULT_SERVER_DATETIME_FORMAT)
        today_end = datetime.now().replace(hour=23, minute=59, second=59, microsecond=59).strftime(
            DEFAULT_SERVER_DATETIME_FORMAT)
        res_obj = self.search(
            [('create_uid', '=', user_id), ('create_date', '>=', today_start), ('create_date', '<=', today_end)],
            order='id desc', limit=1)
        if res_obj:
            if res_obj.login_status:
                login_date = datetime.strptime(res_obj.login_time, DEFAULT_SERVER_DATETIME_FORMAT).strftime("%Y-%m-%d")
                logout_date = datetime.now().strftime("%Y-%m-%d")
                if login_date == logout_date:
                    self.write(vals)
                else:
                    self.create(vals)
        else:
            self.create(vals)
        return True

    @api.multi
    def mobile_create_offline(self, vals):
        values = {}
        result = {}
        today_start = datetime.now().replace(hour=00, minute=00, second=0, microsecond=0).strftime(
            DEFAULT_SERVER_DATETIME_FORMAT)
        today_end = datetime.now().replace(hour=23, minute=59, second=59, microsecond=59).strftime(
            DEFAULT_SERVER_DATETIME_FORMAT)
        att_res_obj = self.search(
            [('create_uid', '=', self.id), ('create_date', '>=', today_start), ('create_date', '<=', today_end)],
            order='id desc', limit=1)
        temp = "/home/parrot/"
        if not os.path.exists(temp + 'Attendance'):
            os.makedirs(temp + 'Attendance')
        temp = temp + 'Attendance/'
        if 'list' in vals and vals['list']:
            if 'loginImage' in vals['list'] and vals['list']['loginImage']:
                if 'fileExeId' in vals['list'] and vals['list']['fileExeId']:
                    img_name = str(vals['list']['fileExeId']) + '_login' + '.jpg'
                else:
                    img_name = str(self.id) + '_login' + '.jpg'
                file_path = os.path.join(temp, img_name)
                fh = open(file_path, "w+")
                fh.write(vals['list']['loginImage'].decode('base64'))
                url = 'https://api.parrot.solutions/parrot/nlp'
                files = {'file': open(file_path, 'rb')}
                headers = {'clientId': 'c1a981fa-5728-4394-b5d0-e3ef42c5a7af'}
                response = requests.post(url, headers=headers, files=files)
                bucket_path = ast.literal_eval(response.text)['answer']['url']
                values.update({'login_image_url': bucket_path})
                values.update({'login_status': 'login'})
            if 'loginLatitude' in vals['list'] and vals['list']['loginLatitude']:
                values.update({'login_latitude': vals['list']['loginLatitude']})
            if 'loginLongitude' in vals['list'] and vals['list']['loginLongitude']:
                values.update({'login_longitude': vals['list']['loginLongitude']})
            if 'createdOn' in vals['list'] and vals['list']['createdOn']:
                values.update({'login_time': vals['list']['createdOn']})
            if 'logoutImage' in vals['list'] and vals['list']['logoutImage']:
                if 'fileExeId' in vals['list'] and vals['list']['fileExeId']:
                    img_name1 = str(vals['list']['fileExeId']) + '_logout' + '.jpg'
                else:
                    img_name1 = str(self.id) + '_logout' + '.jpg'
                file_path1 = os.path.join(temp, img_name1)
                fh = open(file_path1, "w+")
                fh.write(vals['list']['logoutImage'].decode('base64'))
                url1 = 'https://api.parrot.solutions/parrot/nlp'
                files1 = {'file': open(file_path1, 'rb')}
                headers1 = {'clientId': 'c1a981fa-5728-4394-b5d0-e3ef42c5a7af'}
                response1 = requests.post(url1, headers=headers1, files=files1)
                bucket_path1 = ast.literal_eval(response1.text)['answer']['url']
                values.update({'logout_image_url': bucket_path1})
                values.update({'logout_status': 'logout'})
            if 'logoutLongitude' in vals['list'] and vals['list']['logoutLongitude']:
                values.update({'logout_longitude': vals['list']['logoutLongitude']})
            if 'logoutLatitude' in vals['list'] and vals['list']['logoutLatitude']:
                values.update({'logout_latitude': vals['list']['logoutLatitude']})
            if att_res_obj and att_res_obj.id:
                res_obj = self.search([('id', '=', att_res_obj.id)])
                res_obj.write(values)
                result.update({'attendance_id': att_res_obj.id})
            else:
                res_id = self.create(values)
                result.update({'attendance_id': res_id.id})
            if 'fileExeId' in vals['list'] and vals['list']['fileExeId']:
                result.update({'fileExeId': vals['list']['fileExeId']})
        result.update({'syncState': True})
        return result

    @api.model
    def create(self, vals):
        if 'login_time' not in vals:
            if 'login_status' in vals:
                vals['login_time'] = datetime.now()
        attendance_id = super(MarkAttendance, self).create(vals)
        return attendance_id

    @api.multi
    def write(self, vals):
        # res_obj = self.search([('id', '=', self.id)])
        # if 'logout_time' not in vals:
        #     if 'logout_status' in vals:
        #         vals['logout_time'] = datetime.now()
        result = super(MarkAttendance, self).write(vals)
        return result

    @api.multi
    def get_user_staus(self, user_id):
        result = {'status': 'Login'}
        today_start = datetime.now().replace(hour=00, minute=00, second=0, microsecond=0).strftime(
            DEFAULT_SERVER_DATETIME_FORMAT)
        today_end = datetime.now().replace(hour=23, minute=59, second=59, microsecond=59).strftime(
            DEFAULT_SERVER_DATETIME_FORMAT)
        res_obj = self.search(
            [('create_uid', '=', user_id), ('create_date', '>=', today_start), ('create_date', '<=', today_end)],
            order='id desc', limit=1)
        if res_obj.login_status:
            if res_obj.logout_status:
                login_date = datetime.strptime(res_obj.login_time, DEFAULT_SERVER_DATETIME_FORMAT).strftime("%Y-%m-%d")
                logout_date = datetime.now().strftime("%Y-%m-%d")
                if login_date == logout_date:
                    result.update({'status': 'Logout', 'id': res_obj.id})
            else:
                result.update({'status': 'Logout', 'id': res_obj.id})
        return result

    @api.multi
    def get_employee_list(self):
        emp_list = []
        employee_dict = {}
        employee_list_obj = self.env['res.users'].search([('manager_id', '=', self.id)], )
        for employee in employee_list_obj:
            emp_dict = {'emp_id': employee.id, 'emp_name': employee.name}
            emp_list.append(emp_dict)
        employee_dict.update({"result": emp_list})
        return employee_dict

    def get_first_day(self, dt, d_years=0, d_months=0):
        # d_years, d_months are "deltas" to apply to dt
        y, m = dt.year + d_years, dt.month + d_months
        a, m = divmod(m - 1, 12)
        return date(y + a, m + 1, 1)

    def get_last_day(self, dt):
        return self.get_first_day(dt, 0, 1) + timedelta(-1)

    @api.multi
    def get_team_attandance_details(self, user_id, details):
        result_list = []
        total_list = []
        final_dict = {}
        cr = self._cr
        if 'emp_id' in details:
            emp_id = details['emp_id']
        d = date.today()
        if 'dateForm' in details:
            from_date = details['dateForm']
        else:
            from_date = self.get_first_day(d)
        if 'dateTo' in details:
            to_date = details['dateTo']
        else:
            to_date = self.get_last_day(d)

        cr.execute('''SELECT login_date::date
                from generate_series(%s,
                %s, '1 day'::interval) login_date''', (from_date, to_date))
        date_list = cr.fetchall()
        cr.execute('''DROP FUNCTION IF EXISTS get_team_attendance_details()''')
        query = '''CREATE OR REPLACE FUNCTION get_team_attendance_details()
                            RETURNS TABLE (
                            logindate date,
                            logintime text,
                            logouttime text,
                            user_id int,
                            user_login character varying
                            ) AS
                            $func$
                            BEGIN
                            RETURN QUERY
                            SELECT login_time::date as login_date,
                            (CASE
                             WHEN  login_time::time + interval '5:30 hours'> '09:15:00' THEN
                             'False'
                             WHEN  login_time::time + interval '5:30 hours'< '09:15:00' THEN
                             'True'
                             ELSE
                             '-'
                             END)as login_time,
                            (CASE
                             WHEN  logout_time::time > '00:00:00' THEN
                             'False'
                             WHEN  logout_time::time + interval '5:30 hours'>'18:15:00' THEN
                             'True'
                             ELSE
                             '-'
                             END)as logout_time,
                            users.id as user_id,
                            users.login as user_login
                            from mark_attendance att
                            join res_users users on users.id=att.create_uid '''
        if 'emp_id' in details:
            query += '''and att.create_uid in (%s)'''
        else:
            query += '''and att.create_uid in (select id from res_users where manager_id='%s')'''

        query += '''where login_time::date>=%s AND login_time::date<=%s'''

        query += ''' UNION
                SELECT date as login_date,
                'Leave' as login_time,
                'Leave' as logout_time,
                users.id as user_id,
                users.login as user_login
                from leave_application leave
                join res_users users on users.id=leave.create_uid
                and state='approved'
                 '''
        if 'emp_id' in details:
            query += '''and leave.create_uid in (%s)'''
        else:
            query += '''and leave.create_uid in (select id from res_users where manager_id='%s')'''

        query += '''where date>=%s AND date::date<=%s; END
                        $func$ LANGUAGE plpgsql;'''

        if 'emp_id' in details:
            cr.execute(query, (emp_id, from_date, to_date, emp_id, from_date, to_date))
        else:
            cr.execute(query, (user_id, from_date, to_date, user_id, from_date, to_date))
        cr.execute('''Select Distinct logindate,logintime,logouttime,user_id,user_login
                    from get_team_attendance_details() order by logindate''')
        result_dict = cr.dictfetchall()
        if 'emp_id' in details:
            user_search = self.env['res.users'].search([('id', '=', emp_id)])
        else:
            user_search = self.env['res.users'].search([('manager_id', '=', user_id)])
        user_list = []
        datelist = []
        all_records = []
        for user in user_search:
            cr.execute('''SELECT login_date::date as logindate,
                                '-' as logintime,
                                '-' as logouttime,
                                '%s' as user_id
                        from generate_series(%s,
                                                     %s, '1 day'::interval) login_date
                               ''', (user.id, from_date, to_date))
            all_records_temp = cr.dictfetchall()
            all_records.extend(all_records_temp)
        for date_det in date_list:
            datelist.append(date_det[0])
        final_dict.update({'date_list': datelist})
        for rec in all_records:
            for details in result_dict:
                if int(rec['user_id']) == int(details['user_id']) and rec['logindate'] == details['logindate']:
                    rec['logintime'] = details['logintime']
                    rec['logouttime'] = details['logouttime']
        for user in user_search:
            user_dict = {'user_name': '', 'detail': []}
            detail_list = []
            user_dict.update({'user_id': user.id, 'user_name': user.name, 'detail': detail_list})
            for details in all_records:
                if str(details['user_id']) == str(user.id):
                    detail_list.append(details)
            user_dict.update({'detail': detail_list})
            user_list.append(user_dict)
        final_dict.update({'result': user_list})
        return final_dict


class Exception(models.Model):
    _name = 'employee.exception'
    _description = "Exception"
    _order = 'state desc'

    menutype = fields.Selection([('arriving_late', 'Arriving Late'),
                                 ('leaving_early', 'Leaving early'),
                                 ('on_tour', 'On Tour'), ], 'Menu Type')
    date = fields.Date(string='Date')
    arriving_by = fields.Datetime(string='Arriving By')
    leaving_by = fields.Datetime(string='Leaving By')
    reason = fields.Text(string='Reason', size=256)
    to_date = fields.Date(string='To Date')
    location = fields.Char(string='Location')
    state = fields.Selection([('draft', 'Draft'),
                              ('submitted', 'Submitted'),
                              ('approved', 'Approved'),
                              ('rejected', 'Rejected')], 'State', default='draft')
    comments = fields.Text(string='comments')

    @api.model
    def create(self, vals):
        exception_id = super(Exception, self).create(vals)
        send = True
        tokens = []
        user = self.env['res.users'].search([('id', '=', self.id)])
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        if user.manager_id.user_tokens:
            for token in self.env['user.token'].browse(user.manager_id.user_tokens.ids):
                tokens.append(token.name)
        if exception_id.menutype:
            exception_name = exception_id.menutype.replace("_", " ")
            exception = exception_name.title()
        else:
            exception = ""

        data_json = {"channeltype": "CreateException",
                     "name": exception,
                     "message": "%s has been requested by %s" % (
                         exception, user.name),
                     "exception_id": exception_id.id,
                     "from_date": exception_id.date,
                     "to_date": exception_id.to_date}
        data = {'notification': {'title': exception,
                                 'body': "%s has been requested by %s" % (
                                     exception, user.name),
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': self._uid,
                                                'model_name': 'employee.exception',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'create',
                                                'record_id': exception_id.id,
                                                'message': data_json["message"],
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info('Notification not sent')
        return exception_id

    @api.model
    def write(self, vals):
        result = super(Exception, self).write(vals)
        if 'state' in vals:
            fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
            send = True
            tokens = []
            if self.menutype:
                exception_name = self.menutype.replace("_", " ")
                exception = exception_name.title()
            else:
                exception = ""
            user = self.env['res.users'].search([('id', '=', self.create_uid.id)])
            if user.user_tokens:
                for token in self.env['user.token'].browse(user.user_tokens.ids):
                    tokens.append(token.name)
            data_json = {"channeltype": "UpdateException",
                         "name": exception,
                         "message": "%s has been %s by %s" % (
                             exception, self.state, user.manager_id.name),
                         "exception_id": self.id,
                         "from_date": self.date,
                         "to_date": self.to_date}
            data = {'notification': {'title': exception,
                                     'body': "%s has been %s by %s" % (
                                         exception, self.state, user.manager_id.name),
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            self.env['parrot.notification'].create({'name': data_json["name"],
                                                    'user_id': self._uid,
                                                    'model_name': 'employee.exception',
                                                    'channel_name': data_json["channeltype"],
                                                    'function_name': 'write',
                                                    'record_id': self.id,
                                                    'message': data_json["message"],
                                                    'notification_title': data['notification']["title"],
                                                    'notification_body': data['notification']["body"]})
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                _logger.info('Notification not sent')
        return result

    @api.model
    def get_exception_details(self, user_id, type):
        result = {}
        result_list = []
        if type:
            if type.strip() == 'Arriving Late':
                exception_type = 'arriving_late'
            elif type.strip() == 'Leaving Early':
                exception_type = 'leaving_early'
            elif type.strip() == 'On Tour':
                exception_type = 'on_tour'
            else:
                exception_type = False
            if exception_type:
                res_obj = self.search([('create_uid', '=', user_id), ('menutype', '=', exception_type)])
            else:
                res_obj = self.search([('create_uid', '=', user_id)])

            for res in res_obj:
                if res.arriving_by:
                    arriving_by = datetime.strptime(res.arriving_by, DEFAULT_SERVER_DATETIME_FORMAT).strftime(
                        "%H:%M:%S")
                else:
                    arriving_by = "00:00:00"
                if res.leaving_by:
                    leaving_by = datetime.strptime(res.leaving_by, DEFAULT_SERVER_DATETIME_FORMAT).strftime("%H:%M:%S")
                else:
                    leaving_by = "00:00:00"
                if res.state:
                    state = res.state.title()
                else:
                    state = ""
                if res.menutype:
                    exception_name = res.menutype.replace("_", " ")
                    exception = exception_name.title()
                else:
                    exception = ""
                result_dict = {'type': exception, 'date': res.date, 'arriving_by': arriving_by,
                               'leaving_by': leaving_by,
                               'reason': res.reason or "", 'to_date': res.to_date, 'location': res.location,
                               'state': state,
                               'comments': res.comments or ""}
                result_list.append(result_dict)
            result = {'result': result_list}
        return result

    @api.multi
    def get_employee_exception_details(self, user_id):
        user_search = self.env['res.users'].search([('manager_id', '=', user_id)])
        result = {}
        result_list = []
        submit_list = []
        approved_list = []
        rejected_list = []
        for user in user_search:
            res_obj = self.search([('create_uid', '=', user.id)])
            for res in res_obj:
                if res.state:
                    state = res.state.title()
                else:
                    state = ""
                if res.arriving_by:
                    arriving_by = datetime.strptime(res.arriving_by, DEFAULT_SERVER_DATETIME_FORMAT).strftime(
                        "%H:%M:%S")
                else:
                    arriving_by = "00:00:00"
                if res.leaving_by:
                    leaving_by = datetime.strptime(res.leaving_by, DEFAULT_SERVER_DATETIME_FORMAT).strftime("%H:%M:%S")
                else:
                    leaving_by = "00:00:00"
                if res.menutype:
                    exception_name = res.menutype.replace("_", " ")
                    exception = exception_name.title()
                else:
                    exception = ""
                if state == 'Approved':
                    approved_dict = {'exception_type': exception, 'request_date': res.date, 'arriving_by': arriving_by,
                                     'leaving_by': leaving_by, 'location': res.location or '',
                                     'employee_description': res.reason or '',
                                     'emp_name': user.name,
                                     'id': res.id,
                                     'comments': res.comments or '', 'state': state}
                    approved_list.append(approved_dict)
                elif state == 'Rejected':
                    rejected_dict = {'exception_type': exception, 'request_date': res.date, 'arriving_by': arriving_by,
                                     'leaving_by': leaving_by, 'location': res.location or '',
                                     'employee_description': res.reason or '',
                                     'emp_name': user.name,
                                     'id': res.id,
                                     'comments': res.comments or '', 'state': state}
                    rejected_list.append(rejected_dict)
                else:
                    submit_dict = {'exception_type': exception, 'request_date': res.date, 'arriving_by': arriving_by,
                                   'leaving_by': leaving_by, 'location': res.location or '',
                                   'employee_description': res.reason or '',
                                   'emp_name': user.name,
                                   'id': res.id,
                                   'comments': res.comments or '', 'state': state}
                    submit_list.append(submit_dict)
        result_list.extend(approved_list)
        result_list.extend(rejected_list)
        result_list.extend(submit_list)
        result = {'result': result_list}
        return result


class LeaveApplication(models.Model):
    _name = 'leave.application'
    _description = "Leave Application"
    _order = 'id desc'

    date = fields.Date(string='Date')
    from_date = fields.Date(string='From Date')
    to_date = fields.Date(string=' To Date')
    leave_type = fields.Many2one('leave.type', 'Leave Type')
    description = fields.Text(string='Description')
    state = fields.Selection([('draft', 'Draft'),
                              ('submitted', 'Submitted'),
                              ('approved', 'Approved'),
                              ('rejected', 'Rejected')], 'State', default='draft')
    comments = fields.Text(string='comments')

    @api.model
    def create(self, vals):
        leaveApplicationId = super(LeaveApplication, self).create(vals)
        send = True
        tokens = []
        user = self.env['res.users'].search([('id', '=', self.id)])
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        if user.manager_id.user_tokens:
            for token in self.env['user.token'].browse(user.manager_id.user_tokens.ids):
                tokens.append(token.name)
        data_json = {"channeltype": "CreateLeaveApplication",
                     "name": leaveApplicationId.leave_type.name,
                     "message": " %s has been applied by %s" % (
                         leaveApplicationId.leave_type.name, user.name),
                     "leaveApplicationId": leaveApplicationId.id,
                     "from_date": leaveApplicationId.from_date,
                     "to_date": leaveApplicationId.to_date}
        data = {'notification': {'title': leaveApplicationId.leave_type.name,
                                 'body': "%s has been applied by %s" % (
                                     leaveApplicationId.leave_type.name, user.name),
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': self._uid,
                                                'model_name': 'leave.application',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'create',
                                                'record_id': leaveApplicationId.id,
                                                'message': data_json["message"],
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info('Notification not sent')
        return leaveApplicationId

    @api.model
    def write(self, vals):
        result = super(LeaveApplication, self).write(vals)
        if 'state' in vals:
            fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
            send = True
            tokens = []
            user = self.env['res.users'].search([('id', '=', self.create_uid.id)])
            if user.user_tokens:
                for token in self.env['user.token'].browse(user.user_tokens.ids):
                    tokens.append(token.name)
            data_json = {"channeltype": "UpdateLeaveApplication",
                         "name": self.leave_type.name,
                         "message": "%s has been %s by %s" % (
                             self.leave_type.name, self.state, user.manager_id.name),
                         "leaveApplicationId": self.id,
                         "from_date": self.from_date,
                         "to_date": self.to_date}
            data = {'notification': {'title': self.leave_type.name,
                                     'body': "%s has been %s by %s" % (
                                         self.leave_type.name, self.state, user.manager_id.name),
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            self.env['parrot.notification'].create({'name': data_json["name"],
                                                    'user_id': self._uid,
                                                    'model_name': 'leave.application',
                                                    'channel_name': data_json["channeltype"],
                                                    'function_name': 'write',
                                                    'record_id': self.id,
                                                    'message': data_json["message"],
                                                    'notification_title': data['notification']["title"],
                                                    'notification_body': data['notification']["body"]})
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                _logger.info('Notification not sent')
        return result

    @api.multi
    def get_leave_application_details(self, user_id):
        result_list = []
        res_obj = self.search([('create_uid', '=', user_id)])
        for res in res_obj:
            if res.from_date and res.to_date:
                from_date = datetime.strptime(res.from_date, '%Y-%m-%d')
                to_date = datetime.strptime(res.to_date, '%Y-%m-%d')
                difference = to_date - from_date
                no_of_days = difference.days + 1
            else:
                no_of_days = 0
            if res.state:
                state = res.state.title()
            else:
                state = ""
            result_dict = {'date': res.date, 'from_date': res.from_date, 'to_date': res.to_date,
                           'leave_type_id': res.leave_type.id, 'leave_type_name': res.leave_type.name,
                           'description': res.description, 'state': state, 'difference': no_of_days, 'id': res.id,
                           'comments': res.comments or ''}
            result_list.append(result_dict)
        result = {'result': result_list}
        return result

    @api.multi
    def get_employee_leave_application_details(self, user_id):
        result_list = []
        user_search = self.env['res.users'].search([('manager_id', '=', user_id)])
        for user in user_search:
            res_obj = self.search([('create_uid', '=', user.id)])
            for res in res_obj:
                if res.from_date and res.to_date:
                    from_date = datetime.strptime(res.from_date, '%Y-%m-%d')
                    to_date = datetime.strptime(res.to_date, '%Y-%m-%d')
                    difference = to_date - from_date
                    no_of_days = difference.days + 1
                else:
                    no_of_days = 0
                if res.state:
                    state = res.state.title()
                else:
                    state = ""
                result_dict = {'date': res.date, 'from_date': res.from_date, 'to_date': res.to_date,
                               'leave_type_id': res.leave_type.id, 'leave_type_name': res.leave_type.name,
                               'description': res.description, 'state': state, 'difference': no_of_days,
                               'employee_name': user.name, 'id': res.id, 'comments': res.comments or ''}
                result_list.append(result_dict)
        result = {'result': result_list}
        return result

    @api.one
    def button_approve(self):
        return self.write({'state': 'approved'})

    @api.one
    def button_reject(self):
        return self.write({'state': 'rejected'})

    @api.one
    def button_submit(self):
        return self.write({'state': 'submitted'})

    @api.one
    def button_draft(self):
        return self.write({'state': 'draft'})


class LeaveType(models.Model):
    _name = 'leave.type'
    _description = "Leave Type"

    name = fields.Char(string='Name')
    description = fields.Text(string='description')
    active = fields.Boolean(string='Active')

    @api.multi
    def get_leave_type(self):
        result_list = []
        res_obj = self.search([('active', '=', True)])
        for res in res_obj:
            result_dict = {'id': res.id, 'name': res.name}
            result_list.append(result_dict)
        result = {'result': result_list}
        return result


class CompanyDetail(models.Model):
    _name = 'company.detail'
    _description = "Comapany Detail"

    name = fields.Char(string='Name')
    user_id = fields.Many2one('res.users', string="User")
    active = fields.Boolean(string='Active')
    latitude = fields.Float('Latitude', digits=(6, 10))
    longitude = fields.Float('Longitude', digits=(6, 10))

    @api.multi
    def get_company_detail(self, user_id):
        result_list = []
        # res_obj = self.search([('user_id', '=', user_id)])
        # for res in res_obj:
        #     result_dict = {'id': res.id, 'user_id': user_id, 'latitude': res.latitude, 'longitude': res.longitude}
        #     result_list.append(result_dict)
        # result = {'result': result_list}
        result_dict = {'user_id': user_id, 'latitude': '12.9801', 'longitude': '80.2184'}
        result_list.append(result_dict)
        result = {'result': result_list}
        return result


class LogoutDetail(models.Model):
    _name = 'logout.detail'
    _description = "Logout Detail"

    name = fields.Char(string='Name')
    logout_time = fields.Datetime(string='Logout Time')
