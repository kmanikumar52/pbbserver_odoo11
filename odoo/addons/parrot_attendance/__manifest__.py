# -*- coding: utf-8 -*-
{
    'name': "Parrot Attendance",
    'summary': """Parrot Attendance""",
    'description': """Parrot Attendance - This module adds the Empoloyee's attendance""",
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'version': '0.1',
    'depends': ['base','parrot_base'
    ],
    'data': [
         'views/attendance_view.xml',
         'security/attendance_security.xml',
         'security/ir.model.access.csv',

    ],
    'auto_install': True,
}
