# -*- coding: utf-8 -*-
{
    'name': "Parrot Sales",
    'summary': """Customization of Sales for Parrot""",
    'description': """Customization of Sales for Parrot.
        Track module for parrot sales.""",
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'category': 'Sales Management',
    'version': '0.1',
    'sequence': 6,
    'depends': [
        # 'crm_partner_assign',
        # 'project_issue',
        'product',
        'sale',
    ],
    'data': [
        # 'security/ir.model.access.csv',
        'views/crm_lead_view.xml',
        'views/lead_sequence.xml',
        'views/res_partner_view.xml',
        'views/sale_view.xml',
        'views/sale_config_view.xml',
    ],
}
