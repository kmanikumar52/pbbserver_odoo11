import json
import logging
import requests
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from odoo import models, fields, api


_logger = logging.getLogger(__name__)


class CrmLead(models.Model):
    _inherit = "crm.lead"

    client_id = fields.Char(string='Client Reference', required=True, copy=False, readonly=True, index=True,
                            default='New')
    lead_status = fields.Selection([('lead', 'Lead'), ('opportunity', 'Opportunity'), ('park', 'Park'),
                                    ('withdrawn', 'Withdrawn'), ('achieved', 'Achieved')], required=True,
                                   string='Lead Status')
    latitude = fields.Float('Latitude', digits=(6, 10))
    longitude = fields.Float('Longitude', digits=(6, 10))
    state = fields.Char(string='State')
    country = fields.Char(string='Country')
    history_line = fields.One2many('lead.history.line', 'calls_id', string='History Lines')
    product = fields.Many2one('product.product', string='Product')
    lead_value = fields.Many2one('lead.value', string='Lead Value')
    sale_probability = fields.Many2one('sale.probability', string='Sale Probability')
    lead_label = fields.Many2one('label.lead', string='Lead Label')
    next_task = fields.Many2one('next.task', string='Next Task')
    next_date = fields.Date(string='Next Date')
    follow_up_status = fields.Many2one('followup.status', string='Follow Up Status')
    contact_line = fields.One2many('contact.details', 'contact_details', string='Contact Details')
    lead_absolute_value = fields.Char(string='Lead Absolute Value')

    @api.multi
    def write(self, vals):
        uid = self._uid
        prev_status=self.lead_status
        if 'status' in vals and vals['status'] and 'lead_status' not in vals:
            vals['lead_status'] = vals['status'].lower()
        if 'status' in vals and vals['status']:
            history_id = self.env['lead.history.line'].create(vals)
            vals.update({"history_line": [(4, history_id.id)]})
            super(CrmLead, self).write(vals)
        if 'mobile' in vals:
            if 'lead_label' in vals and vals['lead_label']:
                super(CrmLead, self).write({"lead_label": vals['lead_label']})
            if 'product' in vals and vals['product']:
                super(CrmLead, self).write({"product": vals['product']})
            if 'followup_status' in vals and vals['followup_status']:
                super(CrmLead, self).write({"follow_up_status": vals['followup_status']})
                lead_line_sr = self.env['lead.history.line'].search([('calls_id', '=', self.id)], order='id DESC',
                                                                    limit=1)
                followup_status_id = self.env['lead.history.line'].create(
                    {'status': lead_line_sr.status, 'date': lead_line_sr.date, 'comment': lead_line_sr.comment,
                     'sale_probability': lead_line_sr.sale_probability.id, 'next_task': lead_line_sr.next_task.id,
                     'next_date': lead_line_sr.next_date, 'follow_up_status': vals['followup_status'],
                     'calls_id': self.id})
                super(CrmLead, self).write({"history_line": [(4, followup_status_id.id)]})
            if 'contact_line' in vals:
                for cont_id in vals['contact_line']:
                    if 'contact_line_id' in cont_id:
                        cont_sr = self.env['contact.details'].search([('id', '=', cont_id['contact_line_id'])])
                        cont_sr.write(
                            {'contact_name': cont_id['contact_name'], 'phone': cont_id['phone'],
                             'email': cont_id['email'],
                             'designation': cont_id['designation'], 'department': cont_id['department']})
                        super(CrmLead, self).write({"contact_line": [(4, cont_sr.id)]})
                    else:
                        self.env['contact.details'].create(
                            {'contact_name': cont_id['contact_name'], 'phone': cont_id['phone'],
                             'email': cont_id['email'],
                             'designation': cont_id['designation'], 'department': cont_id['department'],
                             'contact_details': self.id})
        else:
            super(CrmLead, self).write(vals)
        if 'from_function' not in vals:
            fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
            send = True
            tokens = []
            users = self.env['res.users'].search([('id', '=', uid)])
            for user_obj in users:
                if user_obj.employee_list:
                    if self.user_id:
                        user = self.env['res.users'].search([('id', '=', self.user_id)])
                        user_id= vals['user_id']
                else:
                    user = user_obj.manager_id
                    user_id=self._uid
                if user.user_tokens:
                    for token in self.env['user.token'].browse(user.user_tokens.ids):
                        tokens.append(token.name)
            write_date = datetime.strptime(self.write_date, "%Y-%m-%d %H:%M:%S")

            data_json = {"channeltype": "UpdateCrmLead",
                         "name": self.name,
                         }
            data = {'notification': {'title': "Update Lead",

                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            if 'status' in vals and vals['status']!='lead':
                if user_obj.employee_list:
                    data_json.update({"message": "%s is converted from %s to %s by %s on %s" % (
                        self.name, prev_status, vals['status'],user_obj.name,write_date.strftime("%d-%m-%Y"))})
                    data['notification'].update({'body':"%s is converted from %s to %s by %s on %s" % (
                        self.name, vals['status'], vals['status'],user_obj.name,write_date.strftime("%d-%m-%Y"))})
                else:
                    data_json.update({"message": "%s is converted from %s to %s by %s on %s" %(
                        self.name, prev_status, vals['status'],user_obj.name,write_date.strftime("%d-%m-%Y"))})
                    data['notification'].update({'body': "%s is converted from %s to %s by %s on %s" % (
                        self.name, vals['status'], vals['status'],user_obj.name,write_date.strftime("%d-%m-%Y"))})

            else:
                if user_obj.employee_list:
                    data_json.update({"message": "%s has been modified by %s on %s and assigned to you" % (
                        self.name, user_obj.name, write_date.strftime("%d-%m-%Y"))})
                    data['notification'].update({'body': "%s has been modified by %s on %s and assigned to you" % (
                        self.name, user_obj.name, write_date.strftime("%d-%m-%Y")),})
                else:
                    data_json.update({"message": "%s has been modified by %s on %s" % (
                        self.name, user_obj.name, write_date.strftime("%d-%m-%Y"))})
                    data['notification'].update({'body': "%s has been modified by %s on %s" % (
                        self.name, user_obj.name, write_date.strftime("%d-%m-%Y"))})
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)

            self.env['parrot.notification'].create({'name': data_json["name"],
                                                    'user_id': user_id,
                                                    'model_name': 'crm.lead',
                                                    'channel_name': data_json["channeltype"],
                                                    'function_name': 'onchange_mute_all',
                                                    'record_id': self.id,
                                                    'message': data_json["message"],
                                                    'notification_title': data['notification']["title"],
                                                    'notification_body': data['notification']["body"]})
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                raise osv.except_osv(_('Notification not sent'))
        return True

    @api.model
    def create(self, vals):
        uid = self._uid
        if vals:
            vals['client_id'] = self.env['ir.sequence'].next_by_code('crm.lead') or 'New'
            if 'mobile' in vals and vals['mobile']:
                temp_values = {'user_id': vals['user_id'], 'name': vals['name'],
                               'partner_name': vals['partner_name'],
                               'street': vals['street'],
                               'active': vals['active'], 'lead_status': vals['lead_status'],
                               'is_existing': vals['is_existing'], 'description': vals['description'],
                               'lead_value': vals['lead_value'], 'next_task': vals['next_task'],
                               'next_date': vals['next_date'], 'lead_label': vals['lead_label'],
                               'product': vals['product'], 'sale_probability': vals['lead_value'],
                               'lead_absolute_value': vals['lead_absolute_value']
                               }
                if 'longitude' in vals and vals['longitude'] and 'latitude' in vals and vals['latitude']:
                    temp_values.update({'longitude': vals['longitude'], 'latitude': vals['latitude'], })
                temp_values['client_id'] = self.env['ir.sequence'].next_by_code('crm.lead') or 'New'
                crm_lead_id = super(CrmLead, self).create(temp_values)
                if 'is_existing' in vals and vals['is_existing'] == 'False':
                    value = {'name': crm_lead_id.partner_name,
                             'street': crm_lead_id.street,
                             'street2': crm_lead_id.street2,
                             'city': crm_lead_id.city,
                             'state': crm_lead_id.state,
                             'country': crm_lead_id.country,
                             'zip': crm_lead_id.zip,
                             }
                    partner_id = self.env['res.partner'].create(value)
                if 'contact_line' in vals and vals['contact_line']:
                    for cont in vals['contact_line']:
                        values = {'contact_name': cont['data_contact'],
                                  'phone': cont['data_phone'],
                                  'email': cont['data_email'],
                                  'department': cont['data_department'],
                                  'designation': cont['data_designation'],
                                  'contact_details': crm_lead_id.id,
                                  }
                        self.env['contact.details'].create(values)
                        partner_values = {'contact_name': cont['data_contact'],
                                          'phone': cont['data_phone'],
                                          'email': cont['data_email'],
                                          'department': cont['data_department'],
                                          'designation': cont['data_designation'],
                                          'contact_customer': partner_id.id,
                                          }
                        self.env['customer.contact'].create(partner_values)
            else:
                crm_lead_id = super(CrmLead, self).create(vals)
            fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
            send = True
            tokens = []
            users = self.env['res.users'].search([('id', '=', uid)])
            for user_obj in users:
                if user_obj.employee_list:
                    if 'user_id' in vals:
                        user = self.env['res.users'].search([('id', '=', vals['user_id'])])
                        user_id= vals['user_id']
                else:
                    user = user_obj.manager_id
                    user_id=self._uid
                if user.user_tokens:
                    for token in self.env['user.token'].browse(user.user_tokens.ids):
                        tokens.append(token.name)
            created_date = datetime.strptime(crm_lead_id.create_date, "%Y-%m-%d %H:%M:%S")

            data_json = {"channeltype": "newCrmLead",
                         "name": crm_lead_id.name,
                         }
            data = {'notification': {'title': "Created New Lead",

                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            if user_obj.employee_list:
                data_json.update({"message": "%s Created by %s on %s and assigned to you" % (
                    crm_lead_id.name, user_obj.name, created_date.strftime("%d-%m-%Y"))})
                data['notification'].update({'body': "%s Created by %s on %s assigned to you" % (
                crm_lead_id.name, user_obj.name, created_date.strftime("%d-%m-%Y")),})
            else:
                data_json.update({"message": "%s Created by %s on %s" % (
                    crm_lead_id.name, user_obj.name, created_date.strftime("%d-%m-%Y"))})
                data['notification'].update({'body': "%s Created by %s on %s" % (
                    crm_lead_id.name, user_obj.name, created_date.strftime("%d-%m-%Y"))})
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            self.env['parrot.notification'].create({'name': data_json["name"],
                                                    'user_id': user_id,
                                                    'model_name': 'crm.lead',
                                                    'channel_name': data_json["channeltype"],
                                                    'function_name': 'onchange_mute_all',
                                                    'record_id': self.id,
                                                    'message': data_json["message"],
                                                    'notification_title': data['notification']["title"],
                                                    'notification_body': data['notification']["body"]})
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                raise osv.except_osv(_('Notification not sent'))
            create_date_format = datetime.today().strftime('%m-%d-%Y')
            values = {'status': vals['lead_status'], 'date': create_date_format, 'comment': vals['description'],
                      'sale_probability': vals['sale_probability'], 'next_task': vals['next_task'],
                      'next_date': vals['next_date'],'from_function':'create'}
            crm_lead_id.write(values)

        return crm_lead_id

    @api.multi
    def lead_history_details(self):
        lead_sr = self.search([('id', '=', self.id)])
        history_list = []
        history_dict = {}
        for history in lead_sr.history_line:
            vals = {'status': history.status, 'date': history.date, 'calls_id': history.calls_id.name,
                    'comment': history.comment, 'sale_probability': history.sale_probability.name,
                    'next_task': history.next_task.name,
                    'follow_up_status': history.follow_up_status.name,
                    'sale_probability_id': history.sale_probability.id,
                    'next_task_id': history.next_task.id,
                    'follow_up_status_id': history.follow_up_status.id}
            history_list.append(vals)
        history_dict.update({'final_res': history_list})
        return history_dict

    @api.multi
    def get_visit_location(self, date):
        visit_detail_list = []
        final_result = {}
        if date['date'] == 'today':
            start_date = datetime.today()
            start_date_strformat = start_date.strftime('%Y-%m-%d')
        elif date['date'] == 'tomorrow':
            start_date = datetime.today() + timedelta(days=1)
            start_date_strformat = start_date.strftime('%Y-%m-%d')
        else:
            start_date_strformat = date['date']
        lead_search = self.search([('next_date', '=', start_date_strformat), ('user_id', '=', date['uid'])])
        for lead_sr in lead_search:
            visit_detail_dict = {'client_id': lead_sr.client_id,
                                 'name': lead_sr.name,
                                 'Latitude': lead_sr.latitude,
                                 'Longitude': lead_sr.longitude,
                                 'lead_label': lead_sr.lead_label.name,
                                 'followup_status': lead_sr.follow_up_status.name,
                                 'next_date': lead_sr.next_date,
                                 'next_task': lead_sr.next_task.name,
                                 'lead_id': self.id,
                                 }
            visit_detail_list.append(visit_detail_dict)
        final_result.update({'result': visit_detail_list})
        return final_result

    @api.multi
    def get_lead_details(self, lead_id):
        lead_sr = self.search([('id', '=', self.id)])
        contact_list = []
        for contact in lead_sr.contact_line:
            contact_dict = {'contact_name': contact.contact_name,
                            'phone': int(contact.phone),
                            'email': contact.email,
                            'designation': contact.designation,
                            'department': contact.department,
                            'contact_line_id': contact.id
                            }
            contact_list.append(contact_dict)
        lead_detail_dict = {'client_id': lead_sr.client_id,
                            'name': lead_sr.name,
                            'product': lead_sr.product.name,
                            'lead_label': lead_sr.lead_label.name,
                            'followup_status': lead_sr.follow_up_status.name,
                            'contact_line': contact_list,
                            'lead_id': self.id,
                            }
        return lead_detail_dict

    @api.multi
    def all_res_partner(self):
        partner_list = []
        partner_dict = {}
        partner_sr = self.env['res.partner'].search([])
        for partner in partner_sr:
            customer_dict = {}
            customer_dict.update({'id': partner.id, 'name': partner.name})
            partner_list.append(customer_dict)
        partner_dict.update({'company_customer': partner_list})
        return partner_dict

    @api.multi
    def existing_partner(self):
        data = {}
        if self.id:
            contact_list = []
            partner_id = self.env['res.partner'].search([('id', '=', self.id)])
            for contact in partner_id.res_contact:
                contact_dict = {'contact_name': contact.contact_name,
                                'phone': int(contact.phone),
                                'email': contact.email,
                                'designation': contact.designation,
                                'department': contact.department,
                                }
                contact_list.append(contact_dict)
            data = {'name': partner_id.id or " ",
                    'street': partner_id.street or " ",
                    'street2': partner_id.street2 or " ",
                    'city': partner_id.city or " ",
                    'state': partner_id.state or " ",
                    'country': partner_id.country or " ",
                    'zip': partner_id.zip or " ",
                    'res_contact': contact_list
                    }
        return data

    @api.multi
    def get_lead_dropdown(self):
        dropdown_dict = {}
        prod_sr = self.env['product.product'].search([])
        prod_list = []
        for prod in prod_sr:
            product_dict = {'id': prod.id, 'name': str(prod.name)}
            prod_list.append(product_dict)
        dropdown_dict['prod_ids'] = prod_list
        lead_sr = self.env['lead.value'].search([])
        lead_list = []
        for lead in lead_sr:
            lead_value_dict = {'id': lead.id, 'name': str(lead.name)}
            lead_list.append(lead_value_dict)
        dropdown_dict['lead_ids'] = lead_list
        prob_sr = self.env['sale.probability'].search([])
        prob_list = []
        for prob in prob_sr:
            sale_prob_dict = {'id': prob.id, 'name': str(prob.name)}
            prob_list.append(sale_prob_dict)
        dropdown_dict['prob_ids'] = prob_list
        task_sr = self.env['next.task'].search([])
        task_list = []
        for task in task_sr:
            next_task_dict = {'id': task.id, 'name': str(task.name)}
            task_list.append(next_task_dict)
        dropdown_dict['task_ids'] = task_list
        label_sr = self.env['label.lead'].search([])
        label_list = []
        for label in label_sr:
            lead_label_dict = {'id': label.id, 'name': str(label.name)}
            label_list.append(lead_label_dict)
        dropdown_dict['label_ids'] = label_list
        follow_up_sr = self.env['followup.status'].search([])
        follow_up_list = []
        for fp_status in follow_up_sr:
            follow_up_dict = {'id': fp_status.id, 'name': str(fp_status.name)}
            follow_up_list.append(follow_up_dict)
        dropdown_dict['follow_up_ids'] = follow_up_list
        return dropdown_dict

    @api.multi
    def search_crm_data(self, status):
        final_dict = {}
        result = []
        cr = self._cr
        manager_id = False
        if 'manager_id' in status and status['manager_id']:
            search_manager = self.env['res.users'].search(([('id', '=', status['manager_id'])]))
            if search_manager.employee_list:
                manager_id = status['manager_id']
        elif self.id:
            search_manager = self.env['res.users'].search(([('id', '=', self.id)]))
            if search_manager.employee_list:
                manager_id = self.id


        cr.execute('''DROP FUNCTION IF EXISTS lead_search_all()''')
        main_function = '''
                CREATE OR REPLACE FUNCTION lead_search_all()
                RETURNS TABLE (
                lead_id int,
                client_id character varying,
                client_name character varying,
                sales_executive_id int,
                sales_executive_name character varying,
                partner_id int,
                partner_name character varying,
                first_call date,
                last_call date,
                lead_value_id int,
                lead_value_name character varying,
                lead_label_id int,
                lead_label_name character varying,
                product_interest_id int,
                product_interest_name character varying,
                sale_probability_id int,
                sale_probability_name character varying,
                next_task_id int,
                next_task_name character varying,
                lead_status character varying
                ) AS
                $func$
                BEGIN
                RETURN QUERY
                select crm.id as lead_id,
                crm.client_id as client_id,crm.name as client_name,
                users.id as sales_executive_id,
                users.login as sales_executive_name,
                crm.partner_id as partner_id,crm.partner_name as partner_name,
                crm.create_date::date as first_call,crm.write_date::date as last_call,
                crm.lead_value as lead_value_id,lv.name as lead_value_name,
                crm.lead_label as lead_label_id,ll.name as lead_label_name,
                crm.product as product_interest_id,prod.name_template as product_interest_name,
                crm.sale_probability as sale_probability_id,sp.name as sale_probability_name,
                crm.next_task as next_task_id,nt.name as next_task_name,
                crm.lead_status
                from crm_lead crm'''
        if manager_id:
            main_function += ''' join res_users users on crm.user_id=users.id and users.id 
                                in (select id from res_users where manager_id=%s)'''
        else:
            main_function += ''' join res_users users on crm.user_id=users.id and users.id=%s'''

        main_function += '''
                left outer join lead_value lv on crm.lead_value=lv.id
                left outer join product_product prod on crm.product=prod.id
                left outer join  sale_probability sp on crm.sale_probability=sp.id
                left outer join  label_lead ll on crm.lead_label=ll.id
                left outer join  next_task nt on crm.next_task=nt.id;
                END
                $func$ LANGUAGE plpgsql;'''
        if manager_id:
            cr.execute(main_function, (manager_id,))
        else:
            if 'emp_id' in status and status['emp_id']:
                cr.execute(main_function, (status['emp_id'],))
            else:
                cr.execute(main_function, (self.id,))
        query = '''select * from lead_search_all() where 1=1 '''
        if 'search_by' in status and status['search_by']:
            query += '''and lead_status=%s '''
            if 'refined_by' in status and status['refined_by'] and 'subfilter' in status and status['subfilter']:
                if status['refined_by'] == 'Sales Probability':
                    query += '''and sale_probability_id=%s'''
                if status['refined_by'] == 'Lead Label':
                    query += '''and lead_label_id=%s'''
                if status['refined_by'] == 'Value of Lead':
                    query += '''and lead_value_id=%s '''
                if status['refined_by'] == 'Product Interest':
                    query += '''and product_interest_id=%s '''
                if status['refined_by'] == 'Next Task':
                    query += '''and next_task_id=%s '''
                cr.execute(query, (status['search_by'], status['subfilter']))
            else:
                cr.execute(query, (status['search_by'],))
            result = cr.dictfetchall()
        elif 'Client_Name' in status and status['Client_Name']:
            status['Client_Name'] = '%' + status['Client_Name'] + '%'
            query += '''and client_name ilike %s '''
            cr.execute(query, (status['Client_Name'],))
            result = cr.dictfetchall()
        elif 'ClientID' in status and status['ClientID']:
            query += '''and client_id=%s '''
            cr.execute(query, (status['ClientID'].upper(),))
            result = cr.dictfetchall()
        else:
            cr.execute(query)
            result = cr.dictfetchall()
        final_dict.update({'result': result})
        return final_dict

    @api.multi
    def get_manager_view_count(self, status):
        today_date = datetime.today()
        start_date = today_date + timedelta(days=-1)
        start_date_strformat = start_date.strftime('%Y-%m-%d')
        end_date = today_date + timedelta(days=-15)
        end_date_strformat = end_date.strftime('%Y-%m-%d')
        self._cr.execute('''
            WITH manager_count AS(select count(crm.id) as count ,
                crm.create_date::date as date,crm.id as crm_id
            from crm_lead crm 
                join res_users users on crm.user_id=users.id and users.id in 
                (select id from res_users where manager_id=%s)
            where crm.lead_status=%s and crm.create_date::date>=%s and
            crm.create_date::date<=%s GROUP BY crm.create_date::date,crm.id
            
            UNION
            
            select count(crm.id) as count ,
                crm.write_date::date as date,crm.id as crm_id
            from crm_lead crm
            join res_users users on crm.user_id=users.id and users.id in 
            (select id from res_users where manager_id=%s)
            where crm.lead_status=%s and crm.write_date::date>=%s and
            crm.write_date::date<=%s GROUP BY crm.write_date::date,crm.id)
            select sum(count) as count, date from manager_count GROUP BY date ''',
                         (
                             self.id, status, end_date_strformat, start_date_strformat, self.id, status,
                             end_date_strformat,
                             start_date_strformat))
        result = self._cr.dictfetchall()
        date_list = []
        for i in range(15):
            date_dict = {}
            i += 1
            week_date = datetime.today() + timedelta(days=-i)
            date_strformat = week_date.strftime('%Y-%m-%d')
            date_dict.update({'date': date_strformat, 'count': 0})
            date_list.append(date_dict)
        fin_dict = {}
        for dat in date_list:
            for res in result:
                if dat['date'] == res['date']:
                    dat.update({'count': res['count']})
        fin_dict.update({'result': date_list})
        return fin_dict

    @api.multi
    def get_employee_mtd_lmtd_count(self, status):
        final_res = {'month_count': 0, 'lmtd_result': 0}
        today_date = datetime.today()
        start_date = today_date + timedelta(days=-1)
        start_date_strformat = start_date.strftime('%Y-%m-%d')
        end_date = start_date.replace(day=1)
        end_date_strformat = end_date.strftime('%Y-%m-%d')
        query = '''WITH employee_count AS(select count(crm.id) as month_count,
                crm.id as crm_id,crm.create_date::date as date
        
                from crm_lead crm
                join res_users users on crm.user_id=users.id and users.id=%s
                where crm.lead_status=%s and crm.create_date::date>=%s and
                crm.create_date::date<=%s  GROUP BY crm.id,crm.create_date::date
                UNION
                select count(crm.id) as month_count,crm.id as crm_id,
                crm.write_date::date as date
                from crm_lead crm 
                join res_users users on crm.user_id=users.id and users.id=%s
                where crm.lead_status=%s and crm.write_date::date>=%s and
                crm.write_date::date<=%s  GROUP BY crm.id,crm.write_date::date)
                
                select sum(month_count) as month_count,date from employee_count GROUP BY date'''
        self._cr.execute(query, (
            self.id, status, end_date_strformat, start_date_strformat, self.id, status, end_date_strformat,
            start_date_strformat))
        mtd_result = self._cr.dictfetchall()
        mon_date = start_date.replace(day=1)
        last_m_first = mon_date - relativedelta(months=1)
        date_strformat = last_m_first.strftime('%Y-%m-%d')
        lastMonth = start_date - relativedelta(months=1)
        lastMonth_strformat = lastMonth.strftime('%Y-%m-%d')
        self._cr.execute(query, (
            self.id, status, date_strformat, lastMonth_strformat, self.id, status, date_strformat, lastMonth_strformat))
        lmtd_result = self._cr.dictfetchall()
        date_list = []
        lmtd_list = []
        for i in range(15):
            date_dict = {}
            lmtd_dict = {}
            i += 1
            week_date = datetime.today() + timedelta(days=-i)
            date_strformat = week_date.strftime('%Y-%m-%d')
            date_dict.update({'date': date_strformat, 'month_count': 0})
            date_list.append(date_dict)
            lmtd_dict.update({'date': date_strformat, 'lmtd_count': 0})
            lmtd_list.append(lmtd_dict)
        fin_dict = {}
        for dat in date_list:
            for res in mtd_result:
                if dat['date'] == res['date']:
                    dat.update({'month_count': res['month_count']})
        fin_dict.update({'month_result': date_list})
        for dat in lmtd_list:
            for res in lmtd_result:
                if dat['date'] == res['date']:
                    dat.update({'lmtd_count': res['month_count']})
        fin_dict.update({'lmtd_result': lmtd_list})
        return fin_dict

    @api.multi
    def get_employee_list(self):
        emp_list = []
        employee_dict = {}
        employee_list_obj = self.env['res.users'].search([('manager_id', '=', self.id)], )
        for employee in employee_list_obj:
            emp_dict = {'emp_id': employee.id, 'emp_name': employee.name}
            emp_list.append(emp_dict)
        employee_dict.update({"result": emp_list})
        return employee_dict

    @api.multi
    def graph_common_query(self, key):
        cr = self._cr
        cr.execute('''Drop function IF EXISTS crm_search_all()''')
        cr.execute('''
                    CREATE OR REPLACE FUNCTION crm_search_all()
                          RETURNS TABLE (
                           lead_id int,
                           client_id character varying,
                           client_name character varying,
                           first_call date,
                           last_call date,
                           lead_value_id int,
                           lead_value_name character varying,
                           lead_label_id int,
                           lead_label_name character varying,
                           product_interest_id int,
                           product_interest_name character varying,
                                       sale_probability_id int,
                                       sale_probability_name character varying, 
                                       lead_status character varying,
                                       user_id int,
                                       lead_absolute_value character varying
                                      ) AS
                                    $func$
                                    BEGIN
                                       RETURN QUERY
                           select crm.id as lead_id,
                            crm.client_id as client_id,
                            crm.name as client_name,
                            --users.id as sales_executive_id,
                            --users.login as sales_executive_name,
                            --crm.partner_id as partner_id,
                            --crm.partner_name as partner_name,
                            crm.create_date::date as first_call,
                            crm.write_date::date as last_call,
                            crm.lead_value as lead_value_id,
                            lv.name as lead_value_name,
                            crm.lead_label as lead_label_id,
                            ll.name as lead_label_name,
                            crm.product as product_interest_id,
                            prod.name_template as product_interest_name,
                            crm.sale_probability as sale_probability_id,
                            sp.name as sale_probability_name,
                            crm.lead_status as lead_status,
                            crm.user_id as user_id,
                            crm.lead_absolute_value as lead_absolute_value
                            from crm_lead crm
                            join res_users users on crm.user_id=users.id and users.id in 
                            (select id from res_users where manager_id=%s)   
                            left outer join lead_value lv on crm.lead_value=lv.id
                            left outer join product_product prod on crm.product=prod.id 
                            left outer join  sale_probability sp on crm.sale_probability=sp.id 
                            left outer join  label_lead ll on crm.lead_label=ll.id;
                        
                         END
                        $func$ LANGUAGE plpgsql;''', (self.id,))

        if 'sale_probability' in key and key['sale_probability'] and 'lead_label' in key and key[
            'lead_label'] and 'lead_value' in key and key['lead_value'] and 'product_interest' in key and key[
            'product_interest'] and 'emp_id' in key and key['emp_id'] and 'Date' in key and key['Date']:
            query1 = '''select count(lead_id),sum(cast(lead_absolute_value as float)),lead_status 
                            from crm_search_all() 
                            where 
                            sale_probability_id=%s and
                            lead_label_id=%s and lead_value_id=%s 
                            and product_interest_id=%s
                            and user_id=%s and (first_call=%s or last_call=%s)
                            group by lead_status '''
            cr.execute(query1, (
                key['sale_probability'], key['lead_label'], key['lead_value'], key['product_interest'], key['emp_id'],
                key['Date'], key['Date']))
        else:
            cr.execute('''select * from crm_search_all()''')
        result_dict = cr.dictfetchall()
        return result_dict

    @api.multi
    def emp_graph_common_query(self, key):
        cr = self._cr
        cr.execute('''Drop function IF EXISTS crm_search_all()''')
        cr.execute('''
                    CREATE OR REPLACE FUNCTION crm_search_all()
                          RETURNS TABLE (
                           lead_id int,
                           client_id character varying,
                           client_name character varying,
                           first_call date,
                           last_call date,
                           lead_value_id int,
                           lead_value_name character varying,
                           lead_label_id int,
                           lead_label_name character varying,
                           product_interest_id int,
                           product_interest_name character varying,
                                       sale_probability_id int,
                                       sale_probability_name character varying, 
                                       lead_status character varying,
                                       user_id int,
                                       lead_absolute_value character varying
                                      ) AS
                                    $func$
                                    BEGIN
                                       RETURN QUERY
                           select crm.id as lead_id,
                            crm.client_id as client_id,
                            crm.name as client_name,
                            --users.id as sales_executive_id,
                            --users.login as sales_executive_name,
                            --crm.partner_id as partner_id,
                            --crm.partner_name as partner_name,
                            crm.create_date::date as first_call,
                            crm.write_date::date as last_call,
                            crm.lead_value as lead_value_id,
                            lv.name as lead_value_name,
                            crm.lead_label as lead_label_id,
                            ll.name as lead_label_name,
                            crm.product as product_interest_id,
                            prod.name_template as product_interest_name,
                            crm.sale_probability as sale_probability_id,
                            sp.name as sale_probability_name,
                            crm.lead_status as lead_status,
                            crm.user_id as user_id,
                            crm.lead_absolute_value as lead_absolute_value
                            from crm_lead crm
                            join res_users users on crm.user_id=users.id and users.id=%s   
                            left outer join lead_value lv on crm.lead_value=lv.id
                            left outer join product_product prod on crm.product=prod.id 
                            left outer join  sale_probability sp on crm.sale_probability=sp.id 
                            left outer join  label_lead ll on crm.lead_label=ll.id;
                        
                         END
                        $func$ LANGUAGE plpgsql;''', (self.id,))

        if 'sale_probability' in key and key['sale_probability'] and 'lead_label' in key and key[
            'lead_label'] and 'lead_value' in key and key['lead_value'] and 'product_interest' in key and key[
            'product_interest'] and 'Date' in key and key['Date']:
            query1 = '''select count(lead_id),sum(cast(lead_absolute_value as float)),lead_status 
                            from crm_search_all() 
                            where 
                            sale_probability_id=%s and
                            lead_label_id=%s and lead_value_id=%s 
                            and product_interest_id=%s
                            and user_id=%s and (first_call=%s or last_call=%s)
                            group by lead_status '''
            cr.execute(query1, (
                key['sale_probability'], key['lead_label'], key['lead_value'], key['product_interest'],
                key['Date'], key['Date']))
        else:
            cr.execute('''select * from crm_search_all()''')
        result_dict = cr.dictfetchall()
        return result_dict

    @api.multi
    def get_manager_analysis_view(self, key):
        cr = self._cr
        result_list = []
        final_result = {}
        sale_prob_list = []
        lead_value_list = []
        lead_label_list = []
        result_prod_list = []
        user_sr = self.env['res.users'].search([('id', '=', self.id)])
        if user_sr.employee_list:
            result_dict = self.graph_common_query(key)
        else:
            result_dict = self.emp_graph_common_query(key)
        if len(key) > 0:
            result_data_list = [{'id': 'sales', 'parent': '', 'name': 'sales'}]
            for lead_status in result_dict:
                if lead_status['lead_status'] == 'lead':
                    res_dict = {'id': 'lead', 'parent': 'sales', 'name': 'Lead',
                                'value':lead_status['count'],'sum':lead_status['sum']}
                    result_list.append(res_dict)
                elif lead_status['lead_status'] == 'opportunity':
                    res_dict = {'id': 'opportunity', 'parent': 'sales', 'name': 'Opporunity',
                                'value': lead_status['count'],'sum':lead_status['sum']}
                    result_list.append(res_dict)
                elif lead_status['lead_status'] == 'park':
                    res_dict = {'id': 'park', 'parent': 'sales', 'name': 'Park',
                                'value': lead_status['count'],'sum':lead_status['sum']}
                    result_list.append(res_dict)
                elif lead_status['lead_status'] == 'withdrawn':
                    res_dict = {'id': 'withdrawn', 'parent': 'sales', 'name': 'Withdrawn',
                                'value': lead_status['count'],'sum':lead_status['sum']}
                    result_list.append(res_dict)
                elif lead_status['lead_status'] == 'achieved':
                    res_dict = {'id': 'achieved', 'parent': 'sales', 'name': 'Achieved',
                                'value': lead_status['count'],'sum':lead_status['sum']}
                    result_list.append(res_dict)
            result_data_list.extend(result_list)
            final_result = {'data': result_data_list}
        else:
            result_data_list = [{'id': 'sales', 'parent': '', 'name': 'sales'},
                                {'id': 'lead', 'parent': 'sales', 'name': 'Lead'},
                                {'id': 'opportunity', 'parent': 'sales', 'name': 'Opporunity'},
                                {'id': 'park', 'parent': 'sales', 'name': 'Park'},
                                {'id': 'withdrawn', 'parent': 'sales', 'name': 'Withdrawn'},
                                {'id': 'achieved', 'parent': 'sales', 'name': 'Achieved'}]
            result_lead_list = [{'id': 'lead_val', 'parent': 'lead', 'name': 'Value of Lead'},
                                {'id': 'lead_prod', 'parent': 'lead', 'name': 'Product Interest'},
                                {'id': 'lead_sale', 'parent': 'lead', 'name': 'sales Probability'},
                                {'id': 'lead_lab', 'parent': 'lead', 'name': 'Lead Label'},
                                {'id': 'opp_val', 'parent': 'opportunity', 'name': 'Value of Lead'},
                                {'id': 'opp_prod', 'parent': 'opportunity', 'name': 'Product Interest'},
                                {'id': 'opp_sale', 'parent': 'opportunity', 'name': 'sales Probability'},
                                {'id': 'opp_lab', 'parent': 'opportunity', 'name': 'Lead Label'},
                                {'id': 'park_val', 'parent': 'park', 'name': 'Value of Lead'},
                                {'id': 'park_prod', 'parent': 'park', 'name': 'Product Interest'},
                                {'id': 'park_sale', 'parent': 'park', 'name': 'sales Probability'},
                                {'id': 'park_lab', 'parent': 'park', 'name': 'Lead Label'},
                                {'id': 'withdrawn_val', 'parent': 'withdrawn', 'name': 'Value of Lead'},
                                {'id': 'withdrawn_prod', 'parent': 'withdrawn', 'name': 'Product Interest'},
                                {'id': 'withdrawn_sale', 'parent': 'withdrawn', 'name': 'sales Probability'},
                                {'id': 'withdrawn_lab', 'parent': 'withdrawn', 'name': 'Lead Label'},
                                {'id': 'achieved_val', 'parent': 'achieved', 'name': 'Value of Lead'},
                                {'id': 'achieved_prod', 'parent': 'achieved', 'name': 'Product Interest'},
                                {'id': 'achieved_sale', 'parent': 'achieved', 'name': 'sales Probability'},
                                {'id': 'achieved_lab', 'parent': 'achieved', 'name': 'Lead Label'},
                                ]
            result_data_list.extend(result_lead_list)
            cr.execute('''select count(lead_id),sum(cast(lead_absolute_value as float)),lead_value_id,
                        lead_value_name,
                        lead_status
                        from crm_search_all() group by lead_value_id,lead_value_name,lead_status
                        order by lead_status''')
            lead_value_dict = cr.dictfetchall()
            for res_val in lead_value_dict:
                if res_val['lead_status'] == 'lead':
                    res_dict = {'id': 'sub_lead_val', 'parent': 'lead_val', 'name': res_val['lead_value_name'],
                                'value': res_val['count'],'sum':res_val['sum']}
                    lead_value_list.append(res_dict)
                elif res_val['lead_status'] == 'opportunity':
                    res_dict = {'id': 'sub_opp_val', 'parent': 'opp_val', 'name': res_val['lead_value_name'],
                                'value': res_val['count'],'sum':res_val['sum']}
                    lead_value_list.append(res_dict)
                elif res_val['lead_status'] == 'park':
                    res_dict = {'id': 'sub_park_val', 'parent': 'park_val', 'name': res_val['lead_value_name'],
                                'value': res_val['count'],'sum':res_val['sum']}
                    lead_value_list.append(res_dict)
                elif res_val['lead_status'] == 'withdrawn':
                    res_dict = {'id': 'sub_withdrawn_val', 'parent': 'withdrawn_val',
                                'name': res_val['lead_value_name'],
                                'value': res_val['count'],'sum':res_val['sum']}
                    lead_value_list.append(res_dict)
                elif res_val['lead_status'] == 'achieved':
                    res_dict = {'id': 'sub_achieved_val', 'parent': 'achieved_val', 'name': res_val['lead_value_name'],
                                'value': res_val['count'],'sum':res_val['sum']}
                    lead_value_list.append(res_dict)
            result_data_list.extend(lead_value_list)
            cr.execute('''select count(lead_id),sum(cast(lead_absolute_value as float)),lead_label_id,
                                lead_label_name,
                                lead_status 
                            from crm_search_all() group by lead_label_id,lead_label_name,lead_status
                            order by lead_status''')
            lead_label_dict = cr.dictfetchall()
            for res_lab in lead_label_dict:
                if res_lab['lead_status'] == 'lead':
                    res_dict = {'id': 'sub_lead_label', 'parent': 'lead_lab', 'name': res_lab['lead_label_name'],
                                'value': res_lab['count'],'sum':res_lab['sum']}
                    lead_label_list.append(res_dict)
                elif res_lab['lead_status'] == 'opportunity':
                    res_dict = {'id': 'sub_opp_label', 'parent': 'opp_lab', 'name': res_lab['lead_label_name'],
                                'value':res_lab['count'],'sum':res_lab['sum']}
                    lead_label_list.append(res_dict)
                elif res_lab['lead_status'] == 'park':
                    res_dict = {'id': 'sub_park_label', 'parent': 'park_lab', 'name': res_lab['lead_label_name'],
                                'value': res_lab['count'],'sum':res_lab['sum']}
                    lead_label_list.append(res_dict)
                elif res_lab['lead_status'] == 'withdrawn':
                    res_dict = {'id': 'sub_withdrawn_label', 'parent': 'withdrawn_lab',
                                'name': res_lab['lead_label_name'],
                                'value': res_lab['count'],'sum':res_lab['sum']}
                    lead_label_list.append(res_dict)
                elif res_lab['lead_status'] == 'achieved':
                    res_dict = {'id': 'sub_achieved_label', 'parent': 'achieved_lab',
                                'name': res_lab['lead_label_name'],
                                'value': res_lab['count'],'sum':res_lab['sum']}
                    lead_label_list.append(res_dict)
            result_data_list.extend(lead_label_list)
            cr.execute('''select count(lead_id),sum(cast(lead_absolute_value as float)),product_interest_id,
                                product_interest_name,
                                lead_status 
                            from crm_search_all() group by product_interest_id,product_interest_name,lead_status
                            order by lead_status''')
            prod_dict = cr.dictfetchall()
            for res_prod in prod_dict:
                if res_prod['lead_status'] == 'lead':
                    res_dict = {'id': 'sub_lead_prod', 'parent': 'lead_prod', 'name': res_prod['product_interest_name'],
                                'value': res_prod['count'],'sum':res_prod['sum']}
                    result_prod_list.append(res_dict)
                elif res_prod['lead_status'] == 'opportunity':
                    res_dict = {'id': 'sub_opp_prod', 'parent': 'opp_prod', 'name': res_prod['product_interest_name'],
                                'value': res_prod['count'],'sum':res_prod['sum']}
                    result_prod_list.append(res_dict)
                elif res_prod['lead_status'] == 'park':
                    res_dict = {'id': 'sub_park_prod', 'parent': 'park_prod', 'name': res_prod['product_interest_name'],
                                'value':res_prod['count'],'sum':res_prod['sum']}
                    result_prod_list.append(res_dict)
                elif res_prod['lead_status'] == 'withdrawn':
                    res_dict = {'id': 'sub_withdrawn_prod', 'parent': 'withdrawn_prod',
                                'name': res_prod['product_interest_name'],
                                'value': res_prod['count'],'sum':res_prod['sum']}
                    result_prod_list.append(res_dict)
                elif res_prod['lead_status'] == 'achieved':
                    res_dict = {'id': 'sub_achieved_prod', 'parent': 'achieved_prod',
                                'name': res_prod['product_interest_name'],
                                'value':res_prod['count'],'sum':res_prod['sum']}
                    result_prod_list.append(res_dict)
            result_data_list.extend(result_prod_list)
            cr.execute('''select count(lead_id),sum(cast(lead_absolute_value as float)),sale_probability_id,
                                sale_probability_name,
                                lead_status 
                            from crm_search_all() group by sale_probability_id,sale_probability_name,lead_status
                            order by lead_status''')
            sale_prob_dict = cr.dictfetchall()

            for res_sale in sale_prob_dict:
                if res_sale['lead_status'] == 'lead':
                    res_dict = {'id': 'sub_lead_sale', 'parent': 'lead_sale', 'name': res_sale['sale_probability_name'],
                                'value': res_sale['count'],'sum':res_sale['sum']}
                    sale_prob_list.append(res_dict)
                elif res_sale['lead_status'] == 'opportunity':
                    res_dict = {'id': 'sub_opp_sale', 'parent': 'opp_sale', 'name': res_sale['sale_probability_name'],
                                'value': res_sale['count'],'sum':res_sale['sum']}
                    sale_prob_list.append(res_dict)
                elif res_sale['lead_status'] == 'park':
                    res_dict = {'id': 'sub_park_sale', 'parent': 'park_sale', 'name': res_sale['sale_probability_name'],
                                'value':res_sale['count'],'sum':res_sale['sum']}
                    sale_prob_list.append(res_dict)
                elif res_sale['lead_status'] == 'withdrawn':
                    res_dict = {'id': 'sub_withdrawn_sale', 'parent': 'withdrawn_sale',
                                'name': res_sale['sale_probability_name'],
                                'value': res_sale['count'],'sum':res_sale['sum']}
                    sale_prob_list.append(res_dict)
                elif res_sale['lead_status'] == 'achieved':
                    res_dict = {'id': 'sub_achieved_sale', 'parent': 'achieved_sale',
                                'name': res_sale['sale_probability_name'],
                                'value': res_sale['count'],'sum':res_sale['sum']}
                    sale_prob_list.append(res_dict)
            result_data_list.extend(sale_prob_list)
            final_result = {'data': result_data_list}
        return final_result

    @api.multi
    def get_activity_track_count(self, no_of_days, emp_id):
        fin_dict = {}
        if no_of_days is None:
            no_of_days = 7
        today_date = datetime.today()
        # start_date = today_date + timedelta(days=-1)
        start_date = today_date
        start_date_strformat = start_date.strftime('%Y-%m-%d')
        end_date = today_date + timedelta(days=-int(no_of_days))
        end_date_strformat = end_date.strftime('%Y-%m-%d')
        self._cr.execute('''DROP FUNCTION IF EXISTS activity_count()''')
        self._cr.execute('''
            CREATE OR REPLACE FUNCTION activity_count()
                          RETURNS TABLE (
                           count bigint,
                           date date,
                           crm_id int,
                           user_id int,
                           create_id int,
                           lead_status character varying
                          ) AS
                        $func$
                        BEGIN
                           RETURN QUERY
                                select count(crm.id) as count,
                                    crm.create_date::date as date,
                                    crm.id as crm_id,
                                    crm.user_id as user_id,
                                    crm.create_uid as create_id,
                                    crm.lead_status as lead_status
                                from crm_lead crm 
                                join res_users users on crm.user_id=users.id and users.id in 
                                (select id from res_users where manager_id=%s)
                                where crm.create_date::date>=%s and
                                      crm.create_date::date<=%s
                                GROUP BY crm.create_date::date,crm.id,crm.user_id,crm.lead_status,crm.create_uid
                                UNION
                                select count(crm.id) as count ,
                                    crm.write_date::date as date,
                                    crm.id as crm_id,
                                    crm.user_id as user_id,
                                    crm.create_uid as create_id,
                                    crm.lead_status as lead_status
                                from crm_lead crm
                                    join res_users users on crm.user_id=users.id and users.id in 
                                    (select id from res_users where manager_id=%s)
                                where crm.write_date::date>=%s and
                                      crm.write_date::date<=%s
                                GROUP BY crm.write_date::date,crm.id,crm.user_id,crm.lead_status,crm.create_uid;
                        
                         END
                        $func$ LANGUAGE plpgsql; ''',
                         (self.id, end_date_strformat, start_date_strformat, self.id,
                          end_date_strformat,
                          start_date_strformat))

        if emp_id:
            self._cr.execute('''select sum(count) as count,'Leads Assigned' as lead_status
            from activity_count()
            where lead_status='lead'
            and create_id !=user_id and user_id=%s
            GROUP BY lead_status''', (emp_id,))
            result = self._cr.dictfetchall()

            self._cr.execute('''select sum(count) as count,'Leads Generated' as lead_status
            from activity_count()
            where lead_status='lead' and user_id=%s
            and create_id=user_id
            GROUP BY lead_status''', (emp_id,))
            leads_generated_result = self._cr.dictfetchall()

            self._cr.execute('''select sum(count) as count,initcap(lead_status) as lead_status
            from activity_count()
            where lead_status!='lead' and user_id=%s
            GROUP BY lead_status''', (emp_id,))
            result_all = self._cr.dictfetchall()

        else:
            self._cr.execute('''select sum(count) as count,'Leads Assigned' as lead_status
            from activity_count()
            where lead_status='lead'
            and create_id !=user_id
            GROUP BY lead_status''')
            result = self._cr.dictfetchall()

            self._cr.execute('''select sum(count) as count,'Leads Generated' as lead_status
            from activity_count()
            where lead_status='lead'
            and create_id=user_id
            GROUP BY lead_status''')
            leads_generated_result = self._cr.dictfetchall()

            self._cr.execute('''select sum(count) as count,initcap(lead_status) as lead_status
            from activity_count()
            where lead_status!='lead'
            GROUP BY lead_status''')
            result_all = self._cr.dictfetchall()

        result.extend(leads_generated_result)
        result.extend(result_all)

        fin_dict.update({'result': result})
        lead_status = []
        lead_value = []
        lead_status_dict = {}
        for data in result:
            lead_status.append(data['lead_status'])
            lead_value.append(data['count'])
        lead_status_dict.update({'lead_status': lead_status, 'lead_value': lead_value})
        final_result = {'data': result}
        return lead_status_dict

    @api.multi
    def get_activity_track_count_emp(self, no_of_days, emp_id):
        fin_dict = {}
        if no_of_days is None:
            no_of_days = 7
        today_date = datetime.today()
        # start_date = today_date + timedelta(days=-1)
        start_date = today_date
        start_date_strformat = start_date.strftime('%Y-%m-%d')
        end_date = today_date + timedelta(days=-int(no_of_days))
        end_date_strformat = end_date.strftime('%Y-%m-%d')
        self._cr.execute('''DROP FUNCTION IF EXISTS activity_count()''')
        self._cr.execute('''
            CREATE OR REPLACE FUNCTION activity_count()
                          RETURNS TABLE (
                           count bigint,
                           date date,
                           crm_id int,
                           user_id int,
                           create_id int,
                           lead_status character varying
                          ) AS
                        $func$
                        BEGIN
                           RETURN QUERY
                                select count(crm.id) as count,
                                    crm.create_date::date as date,
                                    crm.id as crm_id,
                                    crm.user_id as user_id,
                                    crm.create_uid as create_id,
                                    crm.lead_status as lead_status
                                from crm_lead crm 
                                join res_users users on crm.user_id=users.id and users.id=%s 
                                where crm.create_date::date>=%s and
                                      crm.create_date::date<=%s
                                GROUP BY crm.create_date::date,crm.id,crm.user_id,crm.lead_status,crm.create_uid
                                UNION
                                select count(crm.id) as count ,
                                    crm.write_date::date as date,
                                    crm.id as crm_id,
                                    crm.user_id as user_id,
                                    crm.create_uid as create_id,
                                    crm.lead_status as lead_status
                                from crm_lead crm
                                    join res_users users on crm.user_id=users.id and users.id=%s 
                                where crm.write_date::date>=%s and
                                      crm.write_date::date<=%s
                                GROUP BY crm.write_date::date,crm.id,crm.user_id,crm.lead_status,crm.create_uid;
                        
                         END
                        $func$ LANGUAGE plpgsql; ''',
                         (self.id, end_date_strformat, start_date_strformat, self.id,
                          end_date_strformat,
                          start_date_strformat))

        if emp_id:
            self._cr.execute('''select sum(count) as count,'Leads Assigned' as lead_status
            from activity_count()
            where lead_status='lead'
            and create_id !=user_id and user_id=%s
            GROUP BY lead_status''', (emp_id,))
            result = self._cr.dictfetchall()

            self._cr.execute('''select sum(count) as count,'Leads Generated' as lead_status
            from activity_count()
            where lead_status='lead' and user_id=%s
            and create_id=user_id
            GROUP BY lead_status''', (emp_id,))
            leads_generated_result = self._cr.dictfetchall()

            self._cr.execute('''select sum(count) as count,initcap(lead_status) as lead_status
            from activity_count()
            where lead_status!='lead' and user_id=%s
            GROUP BY lead_status''', (emp_id,))
            result_all = self._cr.dictfetchall()

        else:
            self._cr.execute('''select sum(count) as count,'Leads Assigned' as lead_status
            from activity_count()
            where lead_status='lead'
            and create_id !=user_id
            GROUP BY lead_status''')
            result = self._cr.dictfetchall()

            self._cr.execute('''select sum(count) as count,'Leads Generated' as lead_status
            from activity_count()
            where lead_status='lead'
            and create_id=user_id
            GROUP BY lead_status''')
            leads_generated_result = self._cr.dictfetchall()

            self._cr.execute('''select sum(count) as count,initcap(lead_status) as lead_status
            from activity_count()
            where lead_status!='lead'
            GROUP BY lead_status''')
            result_all = self._cr.dictfetchall()

        result.extend(leads_generated_result)
        result.extend(result_all)

        fin_dict.update({'result': result})
        lead_status = []
        lead_value = []
        lead_status_dict = {}
        for data in result:
            lead_status.append(data['lead_status'])
            lead_value.append(data['count'])
        lead_status_dict.update({'lead_status': lead_status, 'lead_value': lead_value})
        final_result = {'data': result}
        return lead_status_dict


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    brochure = fields.Many2many('ir.attachment', 'broucher_rel', 'broucher_id', 'attachment_id', string='Brochure')


class LeadHistoryLine(models.Model):
    _name = 'lead.history.line'

    calls_id = fields.Many2one('crm.lead', string='Calls Reference')
    date = fields.Date(string='Date')
    status = fields.Char(string='Status')
    comment = fields.Text(string='Comment')
    sale_probability = fields.Many2one('sale.probability', string='Sale Probability')
    next_task = fields.Many2one('next.task', string='Next Task')
    next_date = fields.Date(string='Next Date')
    follow_up_status = fields.Many2one('followup.status', string='Follow Up Status')

    @api.model
    def create(self, vals):
        result = super(LeadHistoryLine, self).create(vals)
        return result


class LeadValue(models.Model):
    _name = 'lead.value'

    name = fields.Char(string='Name')
    active = fields.Boolean(string='Active')


class SaleProbability(models.Model):
    _name = 'sale.probability'

    name = fields.Char(string='Name')
    active = fields.Boolean(string='Active')


class LeadLabel(models.Model):
    _name = 'label.lead'

    name = fields.Char(string='Name')
    active = fields.Boolean(string='Active')


class NextTask(models.Model):
    _name = 'next.task'

    name = fields.Char(string='Name')
    active = fields.Boolean(string='Active')


class FollowUpStatus(models.Model):
    _name = 'followup.status'

    name = fields.Char(string='Name')
    active = fields.Boolean(string='Active')


class ContactDetails(models.Model):
    _name = 'contact.details'

    contact_details = fields.Many2one('crm.lead', string='Contacts')
    contact_name = fields.Char(string='Contact Name')
    phone = fields.Char(string='Phone')
    email = fields.Char(string='Email')
    designation = fields.Char(string='Designation')
    department = fields.Char(string='Department')

    @api.model
    def create(self, vals):
        result = super(ContactDetails, self).create(vals)
        return result
