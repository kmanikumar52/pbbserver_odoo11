import logging
import requests
import os
import base64
from odoo import models, fields, api
import ast
# from fpdf import FPDF

_logger = logging.getLogger(__name__)


def FPDF():
    pass


class SaleOrder(models.Model):
    _inherit = "sale.order"
    address = fields.Text(string='Address')
    #    sender_name = fields.Many2one('res.users', 'Sender Name')
    client_name = fields.Char('Client Name')
    sender_name = fields.Char('Sender Name')
    date = fields.Date(string='Date')
    addressed_to = fields.Char(string='Address To')
    email_address = fields.Char(string='Email Address')
    sale_order_line = fields.One2many('product.sale.order.line', 'product_sale_order_line', string='Sale Order Lines',
                                      copy=True)
    email_body = fields.Text("Email Body", help="lead Id")
    email_subject = fields.Char("Email Subject", help="lead Id")
    lead_id = fields.Many2one('crm.lead', 'Lead ID')
    client_state = fields.Char("State")
    country = fields.Char("Country")
    street1 = fields.Char("Street1")
    zip = fields.Char("Zip")
    city = fields.Char("City")

    @api.model
    def mobile_create(self, values):
        if values:
            if 'quo_id' in values and values['quo_id']:
                if 'type' in values and values['type'] == "send":
                    type = 'send'
                else:
                    type = 'draft'
                self._cr.execute('''Update sale_order set state=%s 
                                    ,partner_id=%s , sender_name=%s 
                                    , date=%s ,email_address=%s,email_subject=%s
                                    ,email_body=%s
                                     , addressed_to=%s,city=%s,street1=%s,client_state=%s,zip=%s,country=%s
                                      where id=%s''', (type,
                                                       values['partner_id'],
                                                       values['sender_name'], values['date'], values['email'],
                                                       values['email_subject'], values['email_body'],
                                                       values['addressed_to'],
                                                       values['city'], values['street1'],
                                                       values['client_state'], values['zip'], values['country'],values['quo_id']))
                if 'product_detail' in values:
                    for det in values['product_detail']:
                        if 'detail_id' in det and det['detail_id']:
                            self._cr.execute('''Update product_sale_order_line set
                            product_sale_order_line=%s ,product_name=%s ,ordered_qty=%s ,unit_price=%s where id=%s''',
                                             (values['quo_id'], det['product_name'],
                                              det['ordered_qty'], det['unit_price'], det['detail_id']))
                        else:
                            self._cr.execute('''INSERT INTO "product_sale_order_line"
                            ("product_sale_order_line","product_name", "ordered_qty","unit_price","create_uid", "write_uid","create_date", "write_date")
                            VALUES(%s,%s,%s,%s,%s,%s,(now() at time zone 'UTC'),(now() at time zone 'UTC')) RETURNING id''',
                                             (values['quo_id'], det['product_name'], det['ordered_qty'],
                                              det['unit_price'], self._uid,
                                              self._uid))
                if type == "send":
                    self.send_quotation(values['quo_id'])
            else:
                values['name'] = self.env['ir.sequence'].next_by_code('sale.order') or 'New'
                self._cr.execute('''INSERT INTO "sale_order"
                                                ("id", "invoice_status", "name", "state",
                                                 "sender_name","email_subject","email_body","email_address",
                                                 "date", "addressed_to","date_order",
                                                 "create_uid", "write_uid", "create_date", "write_date",
                                                 "partner_id","pricelist_id","partner_invoice_id","partner_shipping_id","lead_id",
                                                 "city","street1","client_state","zip","country"
                                                 )
                                            VALUES(
                                                nextval('sale_order_id_seq'), 'no', %s,'draft',
                                                %s,%s,%s,%s,
                                                %s,%s,%s,
                                                 %s,%s, (now() at time zone 'UTC'), (now() at time zone 'UTC'),
                                                 %s,1,1,1,%s,%s,%s,%s,%s,%s) RETURNING id''',
                                 (values['name'], values['sender_name'], values['email_subject'], values['email_body'],
                                  values['email'],
                                  values['date'], values['addressed_to'], values['date'],
                                  self._uid, self._uid, values['partner_id'],
                                  values['lead_id'], values['city'], values['street1'], values['client_state'], values['zip'],
                                  values['country']))
                result = self._cr.fetchone()[0]
                if 'product_detail' in values:
                    for det in values['product_detail']:
                        self._cr.execute('''INSERT INTO "product_sale_order_line"
                            ("product_sale_order_line","product_name", "ordered_qty","unit_price","create_uid", "write_uid","create_date", "write_date")
                            VALUES(%s,%s,%s,%s,%s,%s,(now() at time zone 'UTC'),(now() at time zone 'UTC')) RETURNING id''',
                                         (result, det['product_name'], det['ordered_qty'], det['unit_price'], self._uid,
                                          self._uid))
                if 'type' in values and values['type'] == "send":
                    self._cr.execute('''Update sale_order set state='send' where id=%s''', (result,))
                    self.send_quotation(result)
        return True

    @api.model
    def send_quotation(self, quo_id):
        search_obj = self.search(([('id', '=', quo_id)]))
        if search_obj.email_address:
            mail_to = search_obj.email_address
        else:
            mail_to = "parrothelpdesk@googlegroups.com"
        body_html = search_obj.email_body or "PFA for Quotation"
        subject = search_obj.email_subject or "Regarding Quotation"
        Mail = self.env['mail.mail']
        body_html_content = """<html><body><p>Hi,</p>
                                    <p>%s</p>
                                  """ % (body_html)
        body_html_content += """<p>Thank you</p></body></html>"""
        mail_values = {
            'email_from': "parrotsolutionschennai@gmail.com",
            'email_to': mail_to,
            'subject': subject,
            'body_html': body_html_content,
            'notification': True,
        }
        company_obj = self.pool.get('res.users').browse(self._cr, self._uid, self._uid, context=None).company_id
        temp = company_obj.temp_files_path
        if not temp:
            temp = "/home/parrot/"
        if not os.path.exists(temp + 'EmailQuotation'):
            os.makedirs(temp + 'EmailQuotation')
        temp = temp + "EmailQuotation"
        img_name = "quotation.pdf"
        file_path = os.path.join(temp, img_name)
        pdf = FPDF()
        pdf.set_font("Arial", style='B', size=10)
        pdf.add_page()
        col_width = pdf.w / 5.5
        row_height = pdf.font_size
        pdf.ln(7)
        pdf.cell(pdf.w - 4, 5, txt="Quotation", align='C')
        pdf.ln(15)

        pdf.set_font("Arial", style='B', size=10)
        pdf.cell(col_width - 2, 5, txt="Customer        : ", align='L')
        pdf.set_font("Arial", size=10)
        pdf.cell(col_width, 5, txt=search_obj.partner_id.name, align='L')
        pdf.set_font("Arial", style='B', size=10)
        pdf.cell(col_width, 5, txt="Address  : ", align='L')
        pdf.set_font("Arial", size=10)
        pdf.cell(col_width, 5, txt=search_obj.city, align='L')
        pdf.ln(5)

        pdf.set_font("Arial", style='B', size=10)
        pdf.cell(col_width - 2, 5, txt="Sender Name  : ", align='L')
        pdf.set_font("Arial", size=10)
        pdf.cell(col_width, 5, txt=search_obj.sender_name, align='L')
        pdf.set_font("Arial", style='B', size=10)
        pdf.cell(col_width, 5, txt="Date        : ", align='L')
        pdf.set_font("Arial", size=10)
        pdf.cell(col_width, 5, txt=search_obj.date, align='L')
        pdf.ln(5)

        pdf.set_font("Arial", style='B', size=10)
        pdf.cell(col_width - 2, 5, txt="Address To     : ", align='L')
        pdf.set_font("Arial", size=10)
        pdf.cell(col_width, 5, txt=search_obj.addressed_to, align='L')
        pdf.set_font("Arial", style='B', size=10)
        pdf.cell(col_width, 5, txt="Email ID  : ", align='L')
        pdf.set_font("Arial", size=10)
        pdf.cell(col_width, 5, txt=mail_to, align='L')
        pdf.ln(10)

        pdf.set_font("Arial", style='B', size=10)
        pdf.cell(pdf.w / 4.5, 5, txt="Product Details", border=1)
        pdf.ln(5)
        col_width = pdf.w / 4.5
        table_content = []
        table_row = []
        table_row.append('Product')
        table_row.append('Ordered Qty')
        table_row.append('Unit Price')
        table_content.append(table_row)
        for order in search_obj.sale_order_line:
            table_row = []
            table_row.append(order.product_name)
            table_row.append(str(order.ordered_qty))
            table_row.append(str(order.unit_price))
            table_content.append(table_row)
        pdf.set_font("Arial", style='B', size=10)
        for row in table_content:
            for item in row:
                pdf.cell(col_width, row_height * 1,
                         txt=item, border=1)
            pdf.ln(row_height * 1)
            pdf.set_font("Arial", size=10)
        pdf.output(file_path)
        with open(file_path, "rb") as pdf_file:
            encoded_data = base64.b64encode(pdf_file.read())
        url = 'https://api.parrot.solutions/parrot/nlp'
        files = {'file': open(file_path, 'rb')}
        headers = {'clientId': 'c1a981fa-5728-4394-b5d0-e3ef42c5a7af'}
        response = requests.post(url, headers=headers, files=files)
        bucket_path = ast.literal_eval(response.text)['answer']['url']
        search_attachment_obj = self.env['ir.attachment'].search(
            [('res_model', '=', 'sale.order'), ('name', '=', 'Mail_quotation_document')])
        document_vals = {'name': 'Mail_quotation_document',
                         'datas': encoded_data,
                         'datas_fname': img_name,
                         'res_model': 'sale.order',
                         'res_id': quo_id,
                         'type': 'binary',
                         'url': bucket_path}
        if search_attachment_obj:
            search_attachment_obj.write(document_vals)
            mail_values.update({'attachment_ids': [(4, search_attachment_obj.id)]})
        else:
            ir_id = self.env['ir.attachment'].create(document_vals)
            mail_values.update({'attachment_ids': [(4, ir_id.id)]})
        quo_document_vals = {'name': 'Quotation_document',
                             'datas_fname': img_name,
                             'res_model': 'sale.order',
                             'res_id': quo_id,
                             'type': 'binary',
                             'url': bucket_path}
        quo_ir_id = self.env['ir.attachment'].create(quo_document_vals)
        mail_mail_obj = Mail.browse(Mail.create(mail_values))
        Mail.send(mail_mail_obj.id)
        return True

    @api.model
    def get_quotation_details(self, lead_id):
        result = {}
        result_list = []
        search_obj = self.search(([('lead_id', '=', int(lead_id)), ('state', '=', 'draft')]), order="id DESC", limit=1)
        for detail in search_obj:
            result = {'client_name': detail.client_name, 'sender_name': detail.sender_name, 'date': detail.date,
                      'addressed_to': detail.addressed_to, 'email': detail.email_address,
                      'email_body': detail.email_body,
                      'email_subject': detail.email_subject, 'lead_id': detail.lead_id.id,
                      "quo_id": detail.id, "partner_id": detail.partner_id.id,
                      "partner_name": detail.partner_id.name,
                      "city":detail.city, "street1":detail.street1,
                      "client_state":detail.client_state, "zip":detail.zip, "country":detail.country}
            for det in detail.sale_order_line:
                detail_dict = {}
                detail_dict.update(
                    {'product_name': det.product_name, 'ordered_qty': det.ordered_qty, 'unit_price': det.unit_price,
                     'detail_id': det.id})
                result_list.append(detail_dict)
            result.update({'product_detail': result_list})
        return result

    @api.multi
    def get_quotation_client_details(self):
        result = {}
        search_obj = self.env['res.partner'].search(([('id', '=', self.id)]))
        for detail in search_obj:
            result = {'partner_name': detail.name or "",
                      'email': detail.email or "",
                      "city":detail.city or "",
                      "street1":detail.street or "",
                      "client_state":detail.state or "",
                      "zip":detail.zip or "",
                      "country":detail.country or ""}
        return result


class SaleAttachment(models.Model):
    _name = "sale.attachment"

    email_id = fields.Char(string='Email ID')
    attachment = fields.Char("Sales Attachment")
    document_reference_id = fields.Integer("Document Reference_id", help="lead Id")
    email_body = fields.Text("Email Body", help="lead Id")
    email_subject = fields.Char("Email Subject", help="lead Id")
    email_cc = fields.Char("Email CC", help="lead Id")

    @api.multi
    def send_email_document(self):
        if self.email_id:
            mail_to_temp = self.email_id
            string= mail_to_temp.encode()
            string=string[1:-1]
            mail_to= string.replace("u'", "")
            mail_to=mail_to.replace("'","")

        else:
            mail_to = "parrothelpdesk@googlegroups.com"
        if self.email_cc:
            email_cc_temp=self.email_cc
            string1= email_cc_temp.encode()
            string1=string1[1:-1]
            email_cc= string1.replace("u'", "")
            email_cc=email_cc.replace("'","")
        else:
            email_cc=""

        body_html = self.email_body or ""
        Mail = self.env['mail.mail']
        body_html_content = """<html>
                                <body>
                                <p>Hi,</p>"""
        body_html_content += body_html

        body_html_content += """<p>Thank you.</p>
                                </body>
                                </html>"""
        company_obj = self.pool.get('res.users').browse(self._cr, self._uid, self._uid, context=None).company_id
        temp = company_obj.temp_files_path
        if not temp:
            temp = "/home/parrot/"
        if not os.path.exists(temp + 'EmailDocument'):
            os.makedirs(temp + 'EmailDocument')
        mail_values = {
            'email_from': "parrotsolutionschennai@gmail.com",
            'email_to': mail_to,
            'subject': self.email_subject or "Regarding Email Attachement",
            'body_html': body_html_content,
            'notification': True,
            'email_cc': email_cc or ""
        }
        if self.attachment:
            url1 = self.attachment
            url = url1.replace("https", "http")
            r = requests.get(url)
            a = url.rfind('/')
            img_name = url[a + 1:]
            temp = temp + "EmailDocument"
            file_path = os.path.join(temp, img_name)
            with open(file_path, 'wb') as f:
                f.write(r.content)
            with open(file_path, 'rb') as doc:
                encoded_data = base64.b64encode(doc.read())
            document_vals = {'name': 'email document',
                             'datas': encoded_data,
                             'datas_fname': img_name,
                             'res_model': 'sale.attachment',
                             'res_id': self.id,
                             'type': 'binary'}
            ir_id = self.env['ir.attachment'].create(document_vals)
            mail_values.update({'attachment_ids': [(4, ir_id.id)]})
        else:
            url = " "
        mail_mail_obj = Mail.browse(Mail.create(mail_values))
        Mail.send(mail_mail_obj.id)
        return True

    @api.model
    def view_email_document(self, doc_id):
        result = {}
        res_list = []
        search_attachment_obj = self.env['ir.attachment'].search(
            [('res_model', '=', 'sale.order'), ('name', '=', 'Quotation_document')])
        for dettail in search_attachment_obj:
            search_quo_obj = self.env['sale.order'].search(
                [('id', '=', dettail.res_id)])
            for det_quo in search_quo_obj:
                if det_quo.lead_id.id == doc_id:
                    res = {}
                    res.update(
                        {'date': dettail.create_date, 'url': dettail.url, 'email_to': det_quo.email_address,
                         'mimeType': "pdf"})
                    res_list.append(res)
        search_obj = self.search(([('document_reference_id', '=', doc_id)]))
        for det in search_obj:
            if det.attachment:
                a = det.attachment.rfind('/')
                img_name = det.attachment[a + 1:]
                mime_type = img_name.split('.', )[1]
            else:
                mime_type = ""
            res = {}
            res.update(
                {'date': det.create_date, 'url': det.attachment, 'email_to': det.email_id, 'mimeType': mime_type})
            res_list.append(res)
        result.update({'result': res_list})
        return result


class ProductSaleOrderLine(models.Model):
    _name = "product.sale.order.line"

    product_sale_order_line = fields.Many2one('sale.order', string='Order Reference ', required=True,
                                              ondelete='cascade', index=True, copy=False)
    product_name = fields.Char(string='Product Name')
    ordered_qty = fields.Float(string='Ordered Quantity')
    unit_price = fields.Float(string='Unit Price')
    description = fields.Text(string='Description')
