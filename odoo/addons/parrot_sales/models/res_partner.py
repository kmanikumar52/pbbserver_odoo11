import datetime

from dateutil.relativedelta import *
from odoo import models, fields, api

this_month_id_list = []
last_month_id_list = []
this_year_id_list = []
last_year_id_list = []


class ResPartner(models.Model):
    _inherit = 'res.partner'

    tm_lead_count = fields.Integer('This Month Lead', compute='_compute_tm_lead_count')
    lm_lead_count = fields.Integer('Last Month Lead', compute='_compute_lm_lead_count')
    ytd_lead_count = fields.Integer('This Year Lead', compute='_compute_ytd_lead_count')
    ltd_lead_count = fields.Integer('Last Year Lead', compute='_compute_ltd_lead_count')
    tm_opp_count = fields.Integer('This Month Opportunity', compute='_compute_tm_opp_count')
    lm_opp_count = fields.Integer('Last Month Opportunity', compute='_compute_lm_opp_count')
    ytd_opp_count = fields.Integer('This Year Opportunity', compute='_compute_ytd_opp_count')
    ltd_opp_count = fields.Integer('Last Year Opportunity', compute='_compute_ltd_opp_count')
    tm_park_count = fields.Integer('This Month Park', compute='_compute_tm_park_count')
    lm_park_count = fields.Integer('Last Month Park', compute='_compute_lm_park_count')
    ytd_park_count = fields.Integer('This Year Park', compute='_compute_ytd_park_count')
    ltd_park_count = fields.Integer('Last Year Park', compute='_compute_ltd_park_count')
    tm_wd_count = fields.Integer('This Month Withdrawn', compute='_compute_tm_wd_count')
    lm_wd_count = fields.Integer('Last Month Withdrawn', compute='_compute_lm_wd_count')
    ytd_wd_count = fields.Integer('This Year Withdrawn', compute='_compute_ytd_wd_count')
    ltd_wd_count = fields.Integer('Last Year Withdrawn', compute='_compute_ltd_wd_count')
    tm_ach_count = fields.Integer('This Month Achieved', compute='_compute_tm_ach_count')
    lm_ach_count = fields.Integer('Last Month Achieved', compute='_compute_lm_ach_count')
    ytd_ach_count = fields.Integer('This Year Achieved', compute='_compute_ytd_ach_count')
    ltd_ach_count = fields.Integer('Last Year Achieved', compute='_compute_ltd_ach_count')
    state = fields.Char(string='State')
    country = fields.Char(string='Country')
    res_contact = fields.One2many('customer.contact', 'contact_customer', string='Contact Details')

    @api.multi
    def _compute_tm_lead_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                lead_sr = self.env['crm.lead'].search([('user_id', '=', user_id.id), ('lead_status', '=', 'lead')])
                for lead_id in lead_sr:
                    today = datetime.date.today()
                    lead_date = datetime.datetime.strptime(lead_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    for i in range(0, datetime.date.today().day):
                        month_days = today - datetime.timedelta(days=i)
                        date_strformat = month_days.strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            this_month_id_list.append(lead_id.id)
                            count += 1
                partner.tm_lead_count = count

    @api.multi
    def _compute_lm_lead_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                lead_sr = self.env['crm.lead'].search([('user_id', '=', user_id.id), ('lead_status', '=', 'lead')])
                for lead_id in lead_sr:
                    lead_date = datetime.datetime.strptime(lead_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    one_month_ago = datetime.date.today() - relativedelta(months=1)
                    for i in range(0, datetime.date.today().day):
                        date_strformat = (one_month_ago - datetime.timedelta(days=i)).strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            last_month_id_list.append(lead_id.id)
                            count += 1
                partner.lm_lead_count = count

    @api.multi
    def _compute_ytd_lead_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                lead_sr = self.env['crm.lead'].search([('user_id', '=', user_id.id), ('lead_status', '=', 'lead')])
                for lead_id in lead_sr:
                    today = datetime.date.today()
                    year_start = datetime.date(today.year, 1, 1)
                    lead_date = datetime.datetime.strptime(lead_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    for i in range(0, abs(year_start - today).days):
                        year_days = today - datetime.timedelta(days=i)
                        date_strformat = year_days.strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            this_year_id_list.append(lead_id.id)
                            count += 1
                partner.ytd_lead_count = count

    @api.multi
    def _compute_ltd_lead_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                lead_sr = self.env['crm.lead'].search([('user_id', '=', user_id.id), ('lead_status', '=', 'lead')])
                for lead_id in lead_sr:
                    lead_date = datetime.datetime.strptime(lead_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    one_year_ago = datetime.date.today() - relativedelta(years=1)
                    year_start = datetime.date(one_year_ago.year, 1, 1)
                    for i in range(0, abs(year_start - one_year_ago).days):
                        date_strformat = (one_year_ago - datetime.timedelta(days=i)).strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            last_year_id_list.append(lead_id.id)
                            count += 1
                partner.ltd_lead_count = count

    @api.multi
    def _compute_tm_opp_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                opp_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'opportunity')])
                for opp_id in opp_sr:
                    today = datetime.date.today()
                    lead_date = datetime.datetime.strptime(opp_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    for i in range(0, datetime.date.today().day):
                        month_days = today - datetime.timedelta(days=i)
                        date_strformat = month_days.strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            this_month_id_list.append(opp_id.id)
                            count += 1
                partner.tm_opp_count = count

    @api.multi
    def _compute_lm_opp_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                opp_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'opportunity')])
                for opp_id in opp_sr:
                    lead_date = datetime.datetime.strptime(opp_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    one_month_ago = datetime.date.today() - relativedelta(months=1)
                    for i in range(0, datetime.date.today().day):
                        date_strformat = (one_month_ago - datetime.timedelta(days=i)).strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            last_month_id_list.append(opp_id.id)
                            count += 1
                partner.lm_opp_count = count

    @api.multi
    def _compute_ytd_opp_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                opp_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'opportunity')])
                for opp_id in opp_sr:
                    today = datetime.date.today()
                    year_start = datetime.date(today.year, 1, 1)
                    lead_date = datetime.datetime.strptime(opp_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    for i in range(0, abs(year_start - today).days):
                        year_days = today - datetime.timedelta(days=i)
                        date_strformat = year_days.strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            this_year_id_list.append(opp_id.id)
                            count += 1
                partner.ytd_opp_count = count

    @api.multi
    def _compute_ltd_opp_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                opp_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'opportunity')])
                for opp_id in opp_sr:
                    lead_date = datetime.datetime.strptime(opp_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    one_year_ago = datetime.date.today() - relativedelta(years=1)
                    year_start = datetime.date(one_year_ago.year, 1, 1)
                    for i in range(0, abs(year_start - one_year_ago).days):
                        date_strformat = (one_year_ago - datetime.timedelta(days=i)).strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            last_year_id_list.append(opp_id.id)
                            count += 1
                partner.ltd_opp_count = count

    @api.multi
    def _compute_tm_park_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                park_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'park')])
                for park_id in park_sr:
                    today = datetime.date.today()
                    lead_date = datetime.datetime.strptime(park_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    for i in range(0, datetime.date.today().day):
                        month_days = today - datetime.timedelta(days=i)
                        date_strformat = month_days.strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            this_month_id_list.append(park_id.id)
                            count += 1
                partner.tm_park_count = count

    @api.multi
    def _compute_lm_park_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                park_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'park')])
                for park_id in park_sr:
                    lead_date = datetime.datetime.strptime(park_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    one_month_ago = datetime.date.today() - relativedelta(months=1)
                    for i in range(0, datetime.date.today().day):
                        date_strformat = (one_month_ago - datetime.timedelta(days=i)).strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            last_month_id_list.append(park_id.id)
                            count += 1
                partner.lm_park_count = count

    @api.multi
    def _compute_ytd_park_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                park_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'park')])
                for park_id in park_sr:
                    today = datetime.date.today()
                    year_start = datetime.date(today.year, 1, 1)
                    lead_date = datetime.datetime.strptime(park_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    for i in range(0, abs(year_start - today).days):
                        year_days = today - datetime.timedelta(days=i)
                        date_strformat = year_days.strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            this_year_id_list.append(park_id.id)
                            count += 1
                partner.ytd_park_count = count

    @api.multi
    def _compute_ltd_park_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                park_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'park')])
                for park_id in park_sr:
                    lead_date = datetime.datetime.strptime(park_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    one_year_ago = datetime.date.today() - relativedelta(years=1)
                    year_start = datetime.date(one_year_ago.year, 1, 1)
                    for i in range(0, abs(year_start - one_year_ago).days):
                        date_strformat = (one_year_ago - datetime.timedelta(days=i)).strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            last_year_id_list.append(park_id.id)
                            count += 1
                partner.ltd_park_count = count

    @api.multi
    def _compute_tm_wd_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                wd_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'withdrawn')])
                for wd_id in wd_sr:
                    today = datetime.date.today()
                    lead_date = datetime.datetime.strptime(wd_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    for i in range(0, datetime.date.today().day):
                        month_days = today - datetime.timedelta(days=i)
                        date_strformat = month_days.strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            this_month_id_list.append(wd_id.id)
                            count += 1
                partner.tm_wd_count = count

    @api.multi
    def _compute_lm_wd_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                wd_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'withdrawn')])
                for wd_id in wd_sr:
                    lead_date = datetime.datetime.strptime(wd_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    one_month_ago = datetime.date.today() - relativedelta(months=1)
                    for i in range(0, datetime.date.today().day):
                        date_strformat = (one_month_ago - datetime.timedelta(days=i)).strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            last_month_id_list.append(wd_id.id)
                            count += 1
                partner.lm_wd_count = count

    @api.multi
    def _compute_ytd_wd_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                wd_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'withdrawn')])
                for wd_id in wd_sr:
                    today = datetime.date.today()
                    year_start = datetime.date(today.year, 1, 1)
                    lead_date = datetime.datetime.strptime(wd_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    for i in range(0, abs(year_start - today).days):
                        year_days = today - datetime.timedelta(days=i)
                        date_strformat = year_days.strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            this_year_id_list.append(wd_id.id)
                            count += 1
                partner.ytd_wd_count = count

    @api.multi
    def _compute_ltd_wd_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                wd_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'withdrawn')])
                for wd_id in wd_sr:
                    lead_date = datetime.datetime.strptime(wd_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    one_year_ago = datetime.date.today() - relativedelta(years=1)
                    year_start = datetime.date(one_year_ago.year, 1, 1)
                    for i in range(0, abs(year_start - one_year_ago).days):
                        date_strformat = (one_year_ago - datetime.timedelta(days=i)).strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            last_year_id_list.append(wd_id.id)
                            count += 1
                partner.ltd_wd_count = count

    @api.multi
    def _compute_tm_ach_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                ach_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'achieved')])
                for ach_id in ach_sr:
                    today = datetime.date.today()
                    lead_date = datetime.datetime.strptime(ach_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    for i in range(0, datetime.date.today().day):
                        month_days = today - datetime.timedelta(days=i)
                        date_strformat = month_days.strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            this_month_id_list.append(ach_id.id)
                            count += 1
                partner.tm_ach_count = count

    @api.multi
    def _compute_lm_ach_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                ach_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'achieved')])
                for ach_id in ach_sr:
                    lead_date = datetime.datetime.strptime(ach_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    one_month_ago = datetime.date.today() - relativedelta(months=1)
                    for i in range(0, datetime.date.today().day):
                        date_strformat = (one_month_ago - datetime.timedelta(days=i)).strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            last_month_id_list.append(ach_id.id)
                            count += 1
                partner.lm_ach_count = count

    @api.multi
    def _compute_ytd_ach_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                ach_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'achieved')])
                for ach_id in ach_sr:
                    today = datetime.date.today()
                    year_start = datetime.date(today.year, 1, 1)
                    lead_date = datetime.datetime.strptime(ach_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    for i in range(0, abs(year_start - today).days):
                        year_days = today - datetime.timedelta(days=i)
                        date_strformat = year_days.strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            this_year_id_list.append(ach_id.id)
                            count += 1
                partner.ytd_ach_count = count

    @api.multi
    def _compute_ltd_ach_count(self):
        for partner in self:
            partner_usr = ''
            if partner.is_company is False:
                partner_usr = self.env['res.partner'].search([('id', '=', partner.id)])
                user_id = self.env['res.users'].search([('partner_id', '=', partner_usr.id)])
            if partner_usr:
                count = 0
                ach_sr = self.env['crm.lead'].search(
                    [('user_id', '=', user_id.id), ('lead_status', '=', 'achieved')])
                for ach_id in ach_sr:
                    lead_date = datetime.datetime.strptime(ach_id.write_date, '%Y-%m-%d %H:%M:%S').date()
                    one_year_ago = datetime.date.today() - relativedelta(years=1)
                    year_start = datetime.date(one_year_ago.year, 1, 1)
                    for i in range(0, abs(year_start - one_year_ago).days):
                        date_strformat = (one_year_ago - datetime.timedelta(days=i)).strftime('%Y-%m-%d')
                        if str(lead_date) == date_strformat:
                            last_year_id_list.append(ach_id.id)
                            count += 1
                partner.ltd_ach_count = count

    def get_crm_ids(self, cr, uid, ids, context):
        lead_id_list = []
        if 'value' in context:
            if context['value'] == 'this_month':
                lead_id_list = this_month_id_list
            if context['value'] == 'last_month':
                lead_id_list = last_month_id_list
            if context['value'] == 'this_year':
                lead_id_list = this_year_id_list
            if context['value'] == 'last_year':
                lead_id_list = last_year_id_list
        return {'view_type': 'form',
                'view_mode': 'tree, form',
                'res_model': 'crm.lead',
                'domain': [('id', 'in', lead_id_list)],
                'type': 'ir.actions.act_window'}


class CustomerContact(models.Model):
    _name = 'customer.contact'

    contact_customer = fields.Many2one('res.partner', string='Contacts')
    contact_name = fields.Char(string='Contact Name')
    phone = fields.Char(string='Phone')
    email = fields.Char(string='Email')
    designation = fields.Char(string='Designation')
    department = fields.Char(string='Department')

    @api.model
    def create(self, vals):
        result = super(CustomerContact, self).create(vals)
        return result
