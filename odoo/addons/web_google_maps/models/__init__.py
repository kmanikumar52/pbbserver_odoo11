# -*- coding: utf-8 -*-
from . import ir_ui_view
from . import ir_actions_act_window
from . import ir_action_act_window_view
from . import res_partner
from . import res_config
