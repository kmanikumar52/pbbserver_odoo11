# -*- coding: utf-8 -*-

import json
import logging

import requests
from odoo import models, fields, api
from odoo.osv import osv
from passlib.utils import unicode

_logger = logging.getLogger(__name__)


class RetailGeoTrack(models.Model):
    _name = "retail.geo.track"
    _order = "create_date DESC"

    name = fields.Datetime(string="Date", default=lambda self: fields.datetime.now())
    action = fields.Selection([('start', 'Start'), ('end', 'End'), ('action', 'Action'), ('live', 'Live')], 'Action',
                              required=True)
    latitude = fields.Float('Latitude', digits=(6, 10))
    longitude = fields.Float('Longitude', digits=(6, 10))
    is_lat_long = fields.Boolean(string='GPS status', help='Shows that Lat-Long Data was received from phone')
    flag = fields.Boolean(string='User network Status', help='Used to track the employee is in network', default=True)

    @api.model
    def start_customer_visit(self, values):
        if 'longitude' in values and values['longitude'] and 'latitude' in values and values['latitude']:
            _logger.info("longitude : %s and latitude : %s" % (values['longitude'], values['latitude']))
            visit_id = self.create(
                {'longitude': values['longitude'], 'latitude': values['latitude'], 'action': "start"})
            visit_id.create_uid.write({"start_end": True})
            if (float(unicode(values['longitude'])) and float(unicode(values['latitude']))) == 0:
                _logger.info("With lat-long has 0")
                visit_id.write({"is_lat_long": False})
            else:
                _logger.info("With lat-long values")
                visit_id.write({"is_lat_long": True})
        _logger.info("Start Values : %s", values)

    @api.model
    def stop_customer_visit(self, values):
        if 'longitude' in values and values['longitude'] and 'latitude' in values and values['latitude']:
            _logger.info("longitude : %s and latitude : %s" % (values['longitude'], values['latitude']))
            visit_id = self.create({'longitude': values['longitude'], 'latitude': values['latitude'], 'action': "end"})
            visit_id.create_uid.write({"start_end": False})
            if (float(unicode(values['longitude'])) and float(unicode(values['latitude']))) == 0:
                _logger.info("With lat-long has 0")
                visit_id.write({"is_lat_long": False})
            else:
                _logger.info("With lat-long values")
                visit_id.write({"is_lat_long": True})
        _logger.info("Stop Values : %s", values)

    @api.model
    def track_customer_visit(self, values):
        if 'longitude' in values and values['longitude'] and 'latitude' in values and values['latitude']:
            _logger.info("longitude : %s and latitude : %s" % (values['longitude'], values['latitude']))
            visit_id = self.create(
                {'longitude': values['longitude'], 'latitude': values['latitude'], 'action': "action"})
            if (float(unicode(values['longitude'])) and float(unicode(values['latitude']))) == 0:
                _logger.info("With lat-long has 0")
                visit_id.write({"is_lat_long": False})
            else:
                _logger.info("With lat-long values")
                visit_id.write({"is_lat_long": True})
        _logger.info("Track Values : %s", values)

    @api.model
    def live_customer_visit(self, values):
        send = True
        tokens = []
        author_id = 0
        if 'longitude' in values and values['longitude'] and 'latitude' in values and values['latitude']:
            _logger.info("longitude : %s and latitude : %s" % (values['longitude'], values['latitude']))
            visit_id = self.create({'longitude': values['longitude'], 'latitude': values['latitude'], 'action': "live"})
            if (float(unicode(values['longitude'])) and float(unicode(values['latitude']))) == 0:
                _logger.info("With lat-long has 0")
                visit_id.write({"is_lat_long": False})
            else:
                _logger.info("With lat-long values")
                visit_id.write({"is_lat_long": True})
            visit_id.write({"flag ": True})
            fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
            if 'author_id' in values and values['author_id']:
                author_id = self.env['res.users'].browse(values['author_id'])
            user = self.create_uid.browse(values['user_id'])
            if author_id:
                _logger.info("Sending Location detail to Triggered persons")
                if author_id.user_tokens:
                    for token in author_id.user_tokens:
                        tokens.append(token.name)
            else:
                _logger.info("Sending Location detail to Manager by default(Using old App)")
                if user.manager_id.user_tokens:
                    for token in author_id.user_tokens:
                        tokens.append(token.name)
            data_json = {"channeltype": "LiveLocationResult",
                         "name": user.name,
                         "user_id": user.id,
                         "manager_id": user.manager_id.name,
                         "author_id": author_id.id,
                         "author_name": author_id.name,
                         "message": "Notification for Live Location Result",
                         "latitude": values['latitude'],
                         "longitude": values['longitude']}
            data = {'notification': {'title': "Notification for Live Location Result",
                                     'body': "Notification for Live Location Result",
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true'}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                raise osv.except_osv(_('Notification not sent'))
        _logger.info("Live Values : %s", values)

