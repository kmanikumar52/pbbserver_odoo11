# -*- coding: utf-8 -*-
import datetime
import json
import logging
try:
    import urllib.request as urllib2
except ImportError:
    import urllib2
from datetime import datetime, timedelta
import time

import requests
from odoo import api,fields,models
from odoo.osv import osv

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = 'res.users'

    retail_customer = fields.One2many('retail.customer', 'assigned_to', string='Retail Customer')
    retail_geo_track = fields.One2many('retail.geo.track', 'create_uid', string='Retail Geo Track')
    retail_distance = fields.Float(compute='_calculate_retail_distance', string='Distance', digits=(6, 6))
    retail_duration = fields.Float(compute='_calculate_retail_duration', string='Approximate duration', digits=(2, 2))
    retail_percentage = fields.Integer(compute='_calculate_retail_percentage', string='Total Percentage')
    live_track = fields.Integer(string='Live Track(mins)')
    start_end = fields.Boolean('Start/End Beat', help="Show that employee Started/Ended the Retail POS Beat")
    retail_user_location_id = fields.One2many('retail.user.location', 'retail_user_id', string='Retail User Location')
    user_location_id = fields.One2many('user.location', 'user_id', string='User Location')
    distance = fields.Float("Distance")

    # 'next_retail_distance': fields.function(_calculate_next_retail_distance, string="Next Retail Distance",
    #                                         type='float', digits=(6, 6)),
    # 'next_retail_duration': fields.function(_calculate_next_retail_duration, string="Next Retail Duration",
    #                                         type='char'),

    def _calculate_retail_distance(self, cr, uid, ids, fields, args, context=None):
        _logger.info("Calculate Retail Distance")
        res = {}
        # google_maps_api_key = self.pool['ir.config_parameter'].get_param(cr, uid, 'google_maps_api_key')
        # users = self.browse(cr, uid, ids, context=context)
        # list_lat = []
        # list_lon = []
        # pre_lat = 0
        # pre_lon = 0
        # pre_dis = 0
        # google_distance_final = 0
        # pre_url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='
        # for user in users:
        #     for visit in user.visit_id:
        #         if visit.date == str(datetime.today()):
        #             if list_lat and list_lon:
        #                 pre_lat = list_lat.pop()
        #                 pre_lon = list_lon.pop()
        #             lat = visit.latitude
        #             lon = visit.longitude
        #             list_lat.append(visit.latitude)
        #             list_lon.append(visit.longitude)
        #             if pre_lat and pre_lon and lat and lon != 0:
        #                 url = pre_url + str(pre_lat) + ',' + str(pre_lon) + '&destinations=' + str(lat) + ',' + str(
        #                     lon) + '&key=' + google_maps_api_key
        #                 data = json.loads(urllib2.urlopen(url).read())
        #                 _logger.info(url)
        #                 _logger.info(data)
        #                 if data["status"] == "OK":
        #                     if data["rows"][0]["elements"][0]["status"] == "OK":
        #                         google_distance = data["rows"][0]["elements"][0]["distance"]["value"]
        #                         if google_distance != 0:
        #                             google_distance_final = pre_dis + float(google_distance) / 1000
        #                             pre_dis += float(google_distance) / 1000
        #                         res[user.id] = google_distance_final
        #                     else:
        #                         _logger.info("ZERO_RESULTS/ERROR")
        #                 else:
        #                     _logger.info("OVER_QUERY_LIMIT/ERROR")
        #     break
        return res

    def _calculate_retail_duration(self, cr, uid, ids, fields, args, context=None):
        _logger.info("Calculate Retail Duration")
        res = {}
        # google_maps_api_key = self.pool['ir.config_parameter'].get_param(cr, uid, 'google_maps_api_key')
        # users = self.browse(cr, uid, ids, context=context)
        # list_lat = []
        # list_lon = []
        # pre_lat = 0
        # pre_lon = 0
        # pre_dis = 0
        # google_distance_final = 0
        # pre_url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='
        # for user in users:
        #     for visit in user.visit_id:
        #         if visit.date == str(datetime.today()):
        #             if list_lat and list_lon:
        #                 pre_lat = list_lat.pop()
        #                 pre_lon = list_lon.pop()
        #             lat = visit.latitude
        #             lon = visit.longitude
        #             list_lat.append(visit.latitude)
        #             list_lon.append(visit.longitude)
        #             if pre_lat and pre_lon and lat and lon != 0:
        #                 url = pre_url + str(pre_lat) + ',' + str(pre_lon) + '&destinations=' + str(lat) + ',' + str(
        #                     lon) + '&key=' + google_maps_api_key
        #                 data = json.loads(urllib2.urlopen(url).read())
        #                 _logger.info(url)
        #                 _logger.info(data)
        #                 if data["status"] == "OK":
        #                     if data["rows"][0]["elements"][0]["status"] == "OK":
        #                         _logger.info(data["rows"][0]["elements"][0]["status"])
        #                         google_distance = data["rows"][0]["elements"][0]["distance"]["value"]
        #                         google_duration = data["rows"][0]["elements"][0]["duration"]["value"]
        #                         if google_distance != 0:
        #                             google_distance_final = pre_dis + float(google_distance) / 1000
        #                             pre_dis += float(google_duration)
        #                         res[user.id] = google_distance_final
        #                     else:
        #                         _logger.info("ZERO_RESULTS/ERROR")
        #                 else:
        #                     _logger.info("OVER_QUERY_LIMIT/ERROR")
        #     break
        return res

    def _calculate_retail_percentage(self):
        res = {}
        user = self.env['res.users'].search([('id','in', self.ids)])
        visit_array = []
        retail_count = 0
        for visit in user.visit_id:
            if visit.date == str(datetime.today()):
                visit_array.append(visit.name.id)
        visit_count = len(set(visit_array))
        for retail in user.retail_customer:
            if retail.allotted_date == str(datetime.today()):
                retail_count += 1
        if retail_count and visit_count != 0:
            res[user.id] = visit_count * 100 / retail_count
        return res



    # hde by p3:
    def notify_geo_track(self):

        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        user = self.env['res.users'].search([('id','in', self.ids)])
        if user.live_track:
            send = True
            tokens = []
            if user.user_tokens:
                for token in user.user_tokens:
                    tokens.append(token.name)
            live_track = user.live_track * 60000
            data_json = {"channeltype": "LiveTrack",
                         "name": user.name,
                         "author_id": user.id,
                         "message": "Notification for Live Track",
                         "live_track": live_track}
            data = {'notification': {'title': user.name,
                                     'body': "Notification for Live Track",
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                _logger.info('Notification not sent')
    # hde by p3:
    def live_location(self):
        _logger.info("Live location was triggered")
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        user = self.env['res.users'].search([('id','in', self.ids)])
        if user.retail_geo_track:
            self.env["retail.geo.track"].write(user.retail_geo_track[0].id, {"flag": False})
        author_user = self.env['res.users'].search([('id','in', self.ids)])
        if author_user:
            author_tokens = []
            if author_user.user_tokens:
                for token in author_user.user_tokens:
                    author_tokens.append(token.name)
        _logger.info(author_tokens)
        if user:
            send = True
            tokens = []
            if user.user_tokens:
                for token in user.user_tokens:
                    tokens.append(token.name)
            if len(tokens) > 0:
                data_json = {"channeltype": "LiveLocation",
                             "name": user.name,
                             "author_id": self.id,
                             "message": "Please open the App"}
                data = {'data': {'title': "Please open the App",
                                 'body': "Please open the App",
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                        'data': data_json,
                        'registration_ids': tokens,
                        'priority': 'high',
                        'forceShow': 'true'}
                headers = {'Authorization': fcm_authorization_key,
                           'Content-Type': 'application/json'}
                _logger.info("Input Data ------ %s" % data)
                data = json.dumps(data)
                try:
                    if send:
                        res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                        _logger.info("FCM Response ------ %s" % res.text)
                        time.sleep(30)
                        if not user.retail_geo_track[0].flag:
                            _logger.info("Offline - No response for 30 Secs")
                            data_json = {"channeltype": "LiveLocationResultUpdate",
                                         "name": author_user.name,
                                         "user_id": author_user.id,
                                         "message": "User is in not in network range"}
                            data = {'notification': {'title': "User is in not in network range",
                                                     'body': "User is in not in network range",
                                                     'sound': 'default',
                                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                                     'icon': 'fcm_push_icon',
                                                     'color': '#F8880D'},
                                    'data': data_json,
                                    'registration_ids': author_tokens,
                                    'priority': 'high',
                                    'forceShow': 'true'}
                            headers = {'Authorization': fcm_authorization_key,
                                       'Content-Type': 'application/json'}
                            _logger.info("Input Data ------ %s" % data)
                            data = json.dumps(data)
                            try:
                                if send:
                                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data,
                                                        headers=headers)
                                    _logger.info("FCM Response ------ %s" % res.text)
                            except:
                                _logger.info('Notification not sent')
                except:
                    _logger.info('Notification not sent')
            else:
                _logger.info("Offline - No Token id")
                data_json = {"channeltype": "LiveLocationResultUpdate",
                             "name": author_user.name,
                             "user_id": author_user.id,
                             "message": "User is in offline"}
                data = {'notification': {'title': "The user is offline",
                                         'body': "The user is offline",
                                         'sound': 'default',
                                         'click_action': 'FCM_PLUGIN_ACTIVITY',
                                         'icon': 'fcm_push_icon',
                                         'color': '#F8880D'},
                        'data': data_json,
                        'registration_ids': author_tokens,
                        'priority': 'high',
                        'forceShow': 'true'}
                headers = {'Authorization': fcm_authorization_key,
                           'Content-Type': 'application/json'}
                _logger.info("Input Data ------ %s" % data)
                data = json.dumps(data)
                try:
                    if send:
                        res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                        _logger.info("FCM Response ------ %s" % res.text)
                except:
                    _logger.info('Notification not sent')

    @api.model
    def start_stop_cron(self):
        today = (datetime.today()).strftime('%Y-%m-%d')
        for usr in self.search([]):
            list = []
            for retail in usr.retail_geo_track:
                if retail.action == "start":
                    list.append(retail)
            if list:
                if list[0].name < today:
                    usr.write({"start_end": False})
                    self.env['retail.geo.track'].create({'action': "end"})

    # hde by p3:
    @api.multi
    def get_distance_count(self, type):
        if self.employee_list:
            lists = []
            emp_dict_list = []
            final_dict = {}
            for user_id_1 in self.employee_list:
                list = user_id_1.calc_distance_count(type)
                if list['result']:
                    lists.append(list['result'])
                for user_id_2 in user_id_1.employee_list:
                    list = user_id_2.calc_distance_count(type)
                    if list['result']:
                        lists.append(list['result'])
                    for user_id_3 in user_id_2.employee_list:
                        list = user_id_3.calc_distance_count(type)
                        if list['result']:
                            lists.append(list['result'])
                        for user_id_4 in user_id_3.employee_list:
                            list = user_id_4.calc_distance_count(type)
                            if list['result']:
                                lists.append(list['result'])
            if type == 'week':
                for i in range(7):
                    emp_dict = {}
                    temp_count = 0
                    for j in range(len(lists)):
                        temp_date = lists[j][i]['date']
                        temp_count = temp_count + lists[j][i]['count']
                    emp_dict.update({'date': temp_date, 'count': temp_count})
                    emp_dict_list.append(emp_dict)
                    final_dict.update({'result': emp_dict_list})
            if type == 'month':
                for i in range(5):
                    emp_dict = {}
                    temp_count = 0
                    for j in range(len(lists)):
                        temp_date = lists[j][i]['date']
                        temp_count = temp_count + lists[j][i]['count']
                    emp_dict.update({'date': temp_date, 'count': temp_count})
                    emp_dict_list.append(emp_dict)
                    final_dict.update({'result': emp_dict_list})
            if type == 'year':
                count = 0
                for list in lists:
                    count += list[0]['count']
                final_dict.update({'result': [{'date': 'year', 'count': count}]})
            return final_dict
        else:
            _logger.info("Employee BA")
            return self.calc_distance_count(type)
        _logger.info(final_dict)

    @api.multi
    def calc_distance_count(self, type):
        _logger.info("Calculate Retail Distance for Mobile View in Track")
        today_date = datetime.today()
        google_maps_api_key = self.env['ir.config_parameter'].sudo().get_param('google_maps_api_key')
        pre_url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='
        if type == 'week':
            wk_distance_list = []
            final_dict = {}
            for i in range(7):
                wk_distance_dict = {}
                i += 1
                week_date = datetime.today() + timedelta(days=-i)
                date_strformat = week_date.strftime('%Y-%m-%d')
                visits = self.env['customer.visit'].search([('create_uid', '=', self.id),
                                                            ('date', '=', date_strformat)])
                list_lat = []
                list_lon = []
                pre_lat = 0
                pre_lon = 0
                pre_dis = 0
                distance = 0
                google_distance_final = 0
                for visit in visits:
                    if list_lat and list_lon:
                        pre_lat = list_lat.pop()
                        pre_lon = list_lon.pop()
                    lat = visit.latitude
                    lon = visit.longitude
                    list_lat.append(visit.latitude)
                    list_lon.append(visit.longitude)
                    if pre_lat and pre_lon and lat and lon != 0:
                        url = pre_url + str(pre_lat) + ',' + str(pre_lon) + '&destinations=' + str(lat) + ',' + str(
                            lon) + '&key=' + google_maps_api_key
                        data = json.loads(urllib2.urlopen(url).read())
                        _logger.info(url)
                        _logger.info(data)
                        if data["status"] == "OK":
                            if data["rows"][0]["elements"][0]["status"] == "OK":
                                google_distance = data["rows"][0]["elements"][0]["distance"]["value"]
                                if google_distance != 0:
                                    google_distance_final = pre_dis + float(google_distance) / 1000
                                    pre_dis += float(google_distance) / 1000
                            else:
                                _logger.info("ZERO_RESULTS/ERROR")
                        else:
                            _logger.info("OVER_QUERY_LIMIT/ERROR")
                    distance = distance + google_distance_final
                wk_distance_dict.update({'date': date_strformat, 'count': round(distance, 2)})
                wk_distance_list.append(wk_distance_dict)
                final_dict.update({'result': wk_distance_list})
        if type == 'month':
            _logger.info("Month Data")
            wk_distance_list = []
            final_dict = {}
            mn_count_list = []
            i = 0
            distance = 0
            date_diff = 0
            for j in range(5):
                wk_count_dict = {}
                if j == 0:
                    i += 1
                    start_date = today_date + timedelta(days=-i)
                    start_date_strformat = start_date.strftime('%Y-%m-%d')
                    week_end_date = today_date - timedelta(days=today_date.weekday())
                    end_date_strformat = week_end_date + timedelta(days=-i)
                    date_diff = (start_date - week_end_date).days
                    i += date_diff
                else:
                    i += 1
                    start_date = today_date + timedelta(days=-i)
                    start_date_strformat = start_date.strftime('%Y-%m-%d')
                    if j == 4:
                        end_date = today_date + timedelta(days=-30)
                        end_date_strformat = end_date.strftime('%Y-%m-%d')
                    else:
                        i += 6
                        end_date = today_date + timedelta(days=-i)
                        end_date_strformat = end_date.strftime('%Y-%m-%d')
                visits = self.env['customer.visit'].search([('create_uid', '=', self.id),
                                                            ('date', '>=', end_date_strformat),
                                                            ('date', '<=', start_date_strformat)])
                i += 1
                list_lat = []
                list_lon = []
                pre_lat = 0
                pre_lon = 0
                pre_dis = 0
                google_distance_final = 0
                for visit in visits:
                    if list_lat and list_lon:
                        pre_lat = list_lat.pop()
                        pre_lon = list_lon.pop()
                    lat = visit.latitude
                    lon = visit.longitude
                    list_lat.append(visit.latitude)
                    list_lon.append(visit.longitude)
                    if pre_lat and pre_lon and lat and lon and google_maps_api_key != 0:
                        url = pre_url + str(pre_lat) + ',' + str(pre_lon) + '&destinations=' + str(lat) + ',' + str(
                            lon) + '&key=' + google_maps_api_key
                        data = json.loads(urllib2.urlopen(url).read())
                        _logger.info(url)
                        _logger.info(data)
                        if data["status"] == "OK":
                            if data["rows"][0]["elements"][0]["status"] == "OK":
                                google_distance = data["rows"][0]["elements"][0]["distance"]["value"]
                                if google_distance != 0:
                                    google_distance_final = pre_dis + float(google_distance) / 1000
                                    pre_dis += float(google_distance) / 1000
                                    _logger.info("Google Distance %s %s", google_distance, pre_dis)
                            else:
                                _logger.info(visit)
                                _logger.info("ZERO_RESULTS/ERROR")
                        else:
                            _logger.info("OVER_QUERY_LIMIT/ERROR")
                    distance = distance + google_distance_final
                wk_distance_list.append(distance)
                total_wk_pos = 0
                datetime_object = datetime.strptime(start_date_strformat, '%Y-%m-%d')
                this_wk = datetime_object.isocalendar()[1]
                for pos_count in wk_distance_list:
                    total_wk_pos += pos_count
                wk_count_dict.update({'date': this_wk, 'count': round(total_wk_pos, 2)})
                mn_count_list.append(wk_count_dict)
                final_dict.update({'result': mn_count_list})
            print (final_dict)
        if type == 'year':
            _logger.info("Year Data")
            final_dict = {}
            distance = 0
            financial_year_start_date = datetime.strptime(str(datetime.today().year) + "-03-01", "%Y-%m-%d").date()
            today_strformat = datetime.today().strftime('%Y-%m-%d')
            visits = self.env['customer.visit'].search([('create_uid', '=', self.id),
                                                        ('date', '>=', financial_year_start_date),
                                                        ('date', '<=', today_strformat)])
            list_lat = []
            list_lon = []
            pre_lat = 0
            pre_lon = 0
            pre_dis = 0
            google_distance_final = 0
            for visit in visits:
                visit_distance = 0
                if list_lat and list_lon:
                    pre_lat = list_lat.pop()
                    pre_lon = list_lon.pop()
                lat = visit.latitude
                lon = visit.longitude
                list_lat.append(visit.latitude)
                list_lon.append(visit.longitude)
                if pre_lat and pre_lon and lat and lon != 0:
                    url = pre_url + str(pre_lat) + ',' + str(pre_lon) + '&destinations=' + str(lat) + ',' + str(
                        lon) + '&key=' + google_maps_api_key
                    data = json.loads(urllib2.urlopen(url).read())
                    _logger.info(url)
                    _logger.info(data)
                    if data["status"] == "OK":
                        if data["rows"][0]["elements"][0]["status"] == "OK":
                            google_distance = data["rows"][0]["elements"][0]["distance"]["value"]
                            if google_distance != 0:
                                google_distance_final = pre_dis + float(google_distance) / 1000
                                pre_dis += float(google_distance) / 1000
                        else:
                            _logger.info("ZERO_RESULTS/ERROR")
                    else:
                        _logger.info("OVER_QUERY_LIMIT/ERROR")
                    visit_distance += google_distance_final
                distance = distance + visit_distance
            final_dict.update({'result': [{'date': 'Year', 'count': round(distance, 2)}]})
        _logger.info(final_dict)
        return final_dict

    @api.multi
    def get_distance_time(self, date):
        _logger.info("get_distance_time")
        result = []
        results = {}
        visits = self.env["customer.visit"].search([('create_uid', '=', self.id), ('date', '=', str(date))])
        for visit in visits:
            visit_dict = {}
            visit_dict.update({'id': visit.id,
                               'visit_name': visit.name.name,
                               'Latitude': visit.latitude,
                               'Longitude': visit.longitude,
                               'visit_date': visit.date,
                               'create_date': visit.create_date})
            result.append(visit_dict)
        results.update({'result': result})
        _logger.info("Results %s" % results)
        return results


class RetailUserLocation(models.Model):
    _name = 'retail.user.location'


    retail_user_id = fields.Many2one('res.users', string='User Reference')
    latitude = fields.Float('Latitude', digits=(6, 10))
    longitude = fields.Float('Longitude', digits=(6, 10))
    distance = fields.Float('Distance', digits=(6, 10))
    date = fields.Date('Date')


    @api.model
    def create(self, values):
        result= super(RetailUserLocation, self).create(values)
        return result


class UserLocation(models.Model):
    _name = 'user.location'


    user_id = fields.Many2one('res.users', string='User ID')
    start_latitude = fields.Float('Start Latitude', digits=(6, 10))
    start_longitude = fields.Float('Start Longitude', digits=(6, 10))
    end_latitude = fields.Float('End Latitude', digits=(6, 10))
    end_longitude = fields.Float('End Longitude', digits=(6, 10))
    start_time = fields.Datetime('Start Time')
    end_time = fields.Datetime('End Time')


    @api.model
    def create(self, values):
        result= super(UserLocation, self).create(values)
        return result