# -*- coding: utf-8 -*-
{
    'name': "Retail Geo Track",
    'summary': """Retail Geo Track of POS visit by employee""",
    'description': """Retail Geo Track of POS visit by employee""",
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'version': '0.1',
    'sequence': 4,
    'depends': [
        'retail_track',
        'web_google_maps'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/res_users_view.xml',
        'views/retail_geo_track_view.xml',
        'views/retail_view.xml',
        # 'data/app_menu_data.xml',
        # 'data/retail_geo_cron.xml',
    ],
    'application': True,
}
