# -*- coding: utf-8 -*-
{
    'name': "Airtel Nigeria",
    'summary': """Customization for Airtel nigeria""",
    'description': """
        Customization for Airtel nigeria -  Contains some extra features
    """,
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': [
        'retail_parrot'
    ],
    'data': [
        'views/retail_view.xml',
    ],
}
