# -*- coding: utf-8 -*-
{
    'name': "Retail Track",
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'summary': 'Tracks Retail counts',
    'description': """This module tracks the Retail POS count
    * Cumulative - Total no. of POS visit per employee. Includes all visit in same POS.

    * Unique pos - visit of an employee. Doesn't include multiple visit to same POS.

    * New pos - Count of User created on that day. New POS only. Not visit count.""",
    'version': '0.1',
    'depends': [
        'retail_parrot'
    ],
    'data': [
        'views/retail_track_view.xml',
    ],
}