# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime, timedelta

from odoo import fields, models, api, osv


class ResUsers(models.Model):
    _inherit = 'res.users'

    visit_id = fields.One2many('customer.visit', 'create_uid', 'Retail Track')
    cumulative_count = fields.Integer(compute='_retail_track_cumulative_count', string='Cumulative Visit Count')
    unique_pos_count = fields.Integer(compute='_retail_track_unique_pos_count', string='Unique POS Visit Count')
    new_pos_count_day = fields.Integer(compute='_retail_track_new_pos_count_day', string='Newly Created POS Count')
    new_pos_count_week = fields.Integer(compute='_retail_track_new_pos_count_week', string='Newly Created POS Count')

    @api.multi
    def _retail_track_cumulative_count(self, field_name, arg):
        res = dict(map(lambda x: (x, 0), self.ids))
        try:
            for user in self.env['res.users'].search([('id','in', self.ids)]):
                res[user.id] = len(user.visit_id)
        except:
            pass
        return res

    @api.multi
    def _retail_track_unique_pos_count(self, field_name, arg):
        res = dict(map(lambda x: (x, 0), self.ids))
        try:
            user = self.env['res.users'].search([('id', 'in', self.ids)])
            visit_ids = self.env['customer.visit'].search([('create_uid', '=', user.id)])
            visit_br = self.env['customer.visit'].browse(visit_ids)
            list = []
            for visit in visit_br:
                list.append(visit.name.id)
                new_set = set()
                unique = []
                for id in list:
                    if id not in new_set:
                        unique.append(id)
                        new_set.add(id)
            res[user.id] = len(unique)
        except:
            pass
        return res

    @api.multi
    def _retail_track_new_pos_count_day(self, field_name, arg):
        res = dict(map(lambda x: (x, 0), self.ids))
        try:
            count = 0
            today_date = datetime.today()
            today_date_strformat = today_date.strftime('%Y-%m-%d')
            for user in self.env['res.users'].search([('id','in', self.ids)]):
                pos_users = self.env['retail.customer'].search([])
                pos_users_br = self.env['retail.customer'].browse(pos_users)
                for pos_id in pos_users_br:
                    pos_create_date = pos_id.create_date
                    pos_date = datetime.strptime(pos_create_date, '%Y-%m-%d %H:%M:%S').date()
                    if str(pos_date) == today_date_strformat and user.id == pos_id.create_uid.id:
                        count += 1
            res[user.id] = count
        except:
            pass
        return res

    @api.multi
    def _retail_track_new_pos_count_week(self, field_name, arg):
        res = dict(map(lambda x: (x, 0), self.ids))
        try:
            count = 0
            for user in self.env['res.users'].search('id','in', self.ids):
                pos_users = self.env['retail.customer'].search([])
                pos_users_br = self.env['retail.customer'].browse(pos_users)
                for pos_id in pos_users_br:
                    pos_create_date = pos_id.create_date
                    pos_date = datetime.strptime(pos_create_date, '%Y-%m-%d %H:%M:%S').date()
                    for i in range(6):
                        today_date = datetime.today()
                        week_days = today_date - timedelta(days=i)
                        date_strformat = week_days.strftime('%Y-%m-%d')
                        if str(pos_date) == date_strformat and user.id == pos_id.create_uid.id:
                            count += 1
            res[user.id] = count
        except:
            pass
        return res


