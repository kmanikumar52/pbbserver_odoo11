# -*- coding: utf-8 -*-

import json
import logging
import random
import string
from datetime import datetime, timedelta

import requests
from odoo import models, fields, api, _
from odoo.osv import osv
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class AudioBridge(models.Model):
    _name = 'audio.bridge'

    name = fields.Char('Name', required=True)
    user_id = fields.Many2one('res.users', String='User', default=lambda self: self.env.user, required=True)
    audio_users = fields.One2many('bridge.users', 'audio_bridge_id', String='All Participants')
    audio_user_hand = fields.One2many('bridge.users', 'audio_bridge_id', domain=[('hand', '=', True)],
                                      String='Confirmed Participants')
    audio_user_unmute = fields.One2many('bridge.users', 'audio_bridge_id', domain=[('mute', '=', False)],
                                        String='Un-Muted Participants')
    audio_mute = fields.Boolean('Mute all', help='Audio Mute/Un-mute all', default=True)
    start = fields.Datetime("Start", required=True, default=lambda self: fields.Datetime.now())
    end = fields.Datetime("End", required=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('cancel', 'Cancelled'),
                              ('confirm', 'Confirmed'),
                              ('done', 'Done')], default='draft')
    audio_bridge_state = fields.Char('Audio Bridge State', compute='_set_audio_bridge_state')
    attachment = fields.Many2many('ir.attachment', 'audio_bridge_rel', 'audio_bridge_id', 'attachment_id')
    description = fields.Text()
    random_id = fields.Char('Room ID')
    audio_bridge_channel = fields.Many2one('mail.channel', 'Audio Bridge Channel')
    refresh_count = fields.Integer("Refresh Clicked Count")

    _defaults = {
        'end': datetime.now() + timedelta(hours=1),
    }

    @api.one
    @api.constrains('start', 'end')
    def _constrains_date(self):
        """ Constrains to Check Start and End date"""
        if self.start > self.end:
            raise NameError("Closing Date cannot be set before Beginning Date.")

    @api.multi
    def button_refresh(self):
        _logger.info("Button refresh")
        self.refresh_count = self.refresh_count + 1
        return {'type': 'ir.actions.act_view_reload'}

    @api.one
    def _set_audio_bridge_state(self):
        now = datetime.now()
        start = datetime.strptime(self.start, DEFAULT_SERVER_DATETIME_FORMAT)
        end = datetime.strptime(self.end, DEFAULT_SERVER_DATETIME_FORMAT)
        if end > now > start:
            self.audio_bridge_state = 'InProgress'
        elif start <= now:
            self.audio_bridge_state = 'Completed'
        elif now <= end:
            self.audio_bridge_state = 'Upcoming'

    @api.one
    def mute_all(self):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        self.audio_mute = True
        for user in self.audio_users:
            user.mute = True
            send = True
            tokens = []
            user_id = self.env['res.users'].browse(user.name.id)
            if user_id.user_tokens:
                for token in self.env['user.token'].browse(user_id.user_tokens.ids):
                    tokens.append(token.name)
            data_json = {"channeltype": "AudioBridgeMuteAll",
                         "name": self.name,
                         "message": "You are Muted now",
                         "audio_bridge_id": self.id,
                         "random_id": self.random_id}
            data = {'notification': {'title': self.name,
                                     'body': "You are Muted now",
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            self.env['parrot.notification'].create({'name': data_json["name"],
                                                    'user_id': self._uid,
                                                    'model_name': 'audio.bridge',
                                                    'channel_name': data_json["channeltype"],
                                                    'function_name': 'mute_all',
                                                    'record_id': self.id,
                                                    'message': "You are Muted now",
                                                    'notification_title': data['notification']["title"],
                                                    'notification_body': data['notification']["body"]})
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                raise osv.except_osv(_('Notification not sent'))

    @api.one
    def unmute_all(self):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        self.audio_mute = False
        for user in self.audio_users:
            user.mute = False
            user.hand = False
            send = True
            tokens = []
            user_id = self.env['res.users'].browse(user.name.id)
            if user_id.user_tokens:
                for token in self.env['user.token'].browse(user_id.user_tokens.ids):
                    tokens.append(token.name)
            data_json = {"channeltype": "AudioBridgeUnmuteAll",
                         "name": self.name,
                         "message": "You are Unmuted now",
                         "audio_bridge_id": self.id,
                         "random_id": self.random_id}
            data = {'notification': {'title': self.name,
                                     'body': "You are Unmuted now",
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            self.env['parrot.notification'].create({'name': data_json["name"],
                                                    'user_id': self._uid,
                                                    'model_name': 'audio.bridge',
                                                    'channel_name': data_json["channeltype"],
                                                    'function_name': 'unmute_all',
                                                    'record_id': self.id,
                                                    'message': "You are Unmuted now",
                                                    'notification_title': data['notification']["title"],
                                                    'notification_body': data['notification']["body"]})
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                raise osv.except_osv(_('Notification not sent'))

    @api.model
    def create(self, data, context=None):
        context = dict(context or {})
        if context.get("mail_broadcast"):
            context['mail_create_nolog'] = True
        channel_pool = self.env['mail.channel']
        partner_id = []
        for user in self.env['res.users'].browse(data['user_id']):
            partner_id = user.partner_id.id
        audio_channel = {'name': 'Audio Bridge ' + 'General ' + data['name'],
                         'channel_partner_ids': [(4, [partner_id])],
                         'public': 'private'}
        data['audio_bridge_channel'] = channel_pool.create(audio_channel).id
        data['random_id'] = ''.join(random.choice(string.letters + string.digits) for _ in range(6))
        return super(AudioBridge, self).create(data)

    @api.one
    def confirm_audio_bridge(self):
        _logger.info("\n---Audio Bridge Accepted---")
        self.state = 'confirm'

    @api.one
    def cancel_audio_bridge(self):
        _logger.info("\n---Audio Bridge Cancelled---")
        self.state = 'cancel'

    @api.one
    def draft_audio_bridge(self):
        _logger.info("\n---Audio Bridge Drafted---")
        self.state = 'draft'

    @api.one
    def alert_audio_bridge(self):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        start_time = datetime.strptime(self.start, DEFAULT_SERVER_DATETIME_FORMAT)
        before_5 = start_time + timedelta(minutes=-5)
        if before_5 <= datetime.now() <= start_time:
            time_diff = start_time.minute - datetime.now().minute
            send = True
            tokens = []
            for user in self.audio_users:
                user_id = self.env['res.users'].browse(user.name.id)
                if user_id.user_tokens:
                    for token in self.env['user.token'].browse(user_id.user_tokens.ids):
                        tokens.append(token.name)
            data_json = {"channeltype": "newAudioBridge",
                         "name": self.name,
                         "message": "Audio Bridge '%s' is about to starts in %s minutes" % (self.name, time_diff),
                         "audio_bridge_id": self.id,
                         "random_id": self.random_id}
            data = {'notification': {'title': self.name,
                                     'body': "Audio Bridge '%s' is about to start in %s minutes" % (
                                         self.name, time_diff),
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            self.env['parrot.notification'].create({'name': data_json["name"],
                                                    'user_id': self._uid,
                                                    'model_name': 'audio.bridge',
                                                    'channel_name': data_json["channeltype"],
                                                    'function_name': 'alert_audio_bridge',
                                                    'record_id': self.id,
                                                    'message': data_json["message"],
                                                    'notification_title': data['notification']["title"],
                                                    'notification_body': data['notification']["body"]})
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                raise osv.except_osv(_('Notification not sent'))
        return True

    @api.one
    def notify_audio_bridge(self):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        for user in self.audio_users:
            user_id = self.env['res.users'].browse(user.name.id)
            if user_id.user_tokens:
                for token in self.env['user.token'].browse(user_id.user_tokens.ids):
                    tokens.append(token.name)
        data_json = {"channeltype": "newAudioBridge",
                     "name": self.name,
                     "message": "This is a Reminder about Audio Bridge '%s'" % self.name,
                     "audio_bridge_id": self.id,
                     "random_id": self.random_id}
        data = {'notification': {'title': self.name,
                                 'body': "This is a Reminder about Audio Bridge '%s'" % self.name,
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': self._uid,
                                                'model_name': 'audio.bridge',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'notify_audio_bridge',
                                                'record_id': self.id,
                                                'message': data_json["message"],
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            raise osv.except_osv(_('Notification not sent'))
        return True
