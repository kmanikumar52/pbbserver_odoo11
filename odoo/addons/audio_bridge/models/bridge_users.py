# -*- coding: utf-8 -*-
import json
import logging
from datetime import datetime

import requests
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.osv import osv
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class BridgeUsers(models.Model):
    _name = 'bridge.users'
    _order = 'write_date desc'

    name = fields.Many2one('res.users', String='Name', required=True)
    mute = fields.Boolean(string='Mute', default=True, help='True : Mute and False : Unmuted')
    hand = fields.Boolean(string='Hand raise', default=False, help='True : Hand Raised and False : Hands down')
    audio_bridge_id = fields.Many2one('audio.bridge', String='Audio Bridge')
    state = fields.Selection([('unconfirmed', 'Unconfirmed'),
                              ('rejected', 'Rejected'),
                              ('confirmed', 'Confirmed'),
                              ('attended', 'Attended')], 'Status', default='unconfirmed', readonly=True, copy=False)
    date_closed = fields.Datetime('Attended Date', readonly=True)
    event_begin_date = fields.Datetime(string="Event Start Date", related='audio_bridge_id.start', readonly=True)
    random_id = fields.Char('Random id', related='audio_bridge_id.random_id')

    _sql_constraints = [
        ('name_uniq', 'unique(name,audio_bridge_id)', "User Name already exists!"),
    ]

    @api.one
    def write(self, data):
        self.audio_bridge_id.audio_bridge_channel.write({'channel_partner_ids': [(4, self.name.partner_id.id)]})
        return super(BridgeUsers, self).write(data)

    @api.multi
    def button_unmute(self):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        self.mute = False
        self.hand = False
        send = True
        tokens = []
        if self.name.user_tokens:
            for token in self.env['user.token'].browse(self.name.user_tokens.ids):
                tokens.append(token.name)
        data_json = {"channeltype": "AudioBridgeUnmute",
                     "name": self.audio_bridge_id.name,
                     "message": "You are Unmuted now",
                     "audio_bridge_id": self.audio_bridge_id.id,
                     "random_id": self.random_id}
        data = {'notification': {'title': self.audio_bridge_id.name,
                                 'body': "You are Unmuted now",
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': self._uid,
                                                'model_name': 'bridge.users',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': '_onchange_mute',
                                                'record_id': self.id,
                                                'message': data_json["message"],
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            raise osv.except_osv(_('Notification not sent'))
        return {'type': 'ir.actions.act_view_reload'}

    @api.multi
    def button_mute(self):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        self.mute = True
        send = True
        tokens = []
        if self.name.user_tokens:
            for token in self.env['user.token'].browse(self.name.user_tokens.ids):
                tokens.append(token.name)
        data_json = {"channeltype": "AudioBridgeMute",
                     "name": self.audio_bridge_id.name,
                     "message": "You are Muted now",
                     "audio_bridge_id": self.audio_bridge_id.id,
                     "random_id": self.random_id}
        data = {'notification': {'title': self.audio_bridge_id.name,
                                 'body': "You are Muted now",
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': self._uid,
                                                'model_name': 'bridge.users',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': '_onchange_mute',
                                                'record_id': self.id,
                                                'message': data_json["message"],
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            raise osv.except_osv(_('Notification not sent'))
        return {'type': 'ir.actions.act_view_reload'}

    @api.one
    def button_reg_confirm(self):
        """ Confirm Registration """
        if self.name.id == self._uid or 1 == self._uid:
            self.state = 'confirmed'

    @api.one
    def button_reg_unconfirm(self):
        """ Unconfirm Registration - Only by Admin"""
        if 1 == self._uid:
            self.state = 'unconfirmed'

    @api.one
    def button_reg_close(self):
        """ Close Registration """
        today = fields.datetime.now()
        start_datetime = datetime.strptime(self.audio_bridge_id.start, DEFAULT_SERVER_DATETIME_FORMAT)
        if start_datetime <= today:
            if self.name.id == self._uid or 1 == self._uid:
                self.write({'state': 'attended', 'date_closed': today})
        else:
            raise UserError(_("You must wait for the starting day of the event to do this action."))

    @api.one
    def button_reg_cancel(self):
        if self.name.id == self._uid or 1 == self._uid:
            self.state = 'rejected'

    @api.one
    def hand_raise(self):
        """ Enable Hand Raise"""
        _logger.info("Hand Raise Triggered")
        if self.name.id == self._uid:
            self.hand = True

    @api.one
    def hand_down(self):
        """ Enable Hand Down"""
        _logger.info("Hand Down Triggered")
        if self.name.id == self._uid:
            self.hand = False
