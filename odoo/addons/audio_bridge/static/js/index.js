/*
 * (C) Copyright 2014-2015 Kurento (http://kurento.org/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
//function getParamValue(paramName)
//{
//	var url = window.location.search.substring(1); //get rid of "?" in querystring
//	
//	var qArray = url.split('&'); //get key-value pairs
//	for (var i = 0; i < qArray.length; i++) 
//	{
//	var pArr = qArray[i].split('='); //split key and value
//	if (pArr[0] == paramName) 
//	return pArr[1]; //return value
//	}
//}
//var company = getParamValue('company');
//var id = getParamValue('broadcastid');
//
//console.log(company);
var ws = new WebSocket('wss://lic.affosoft.com:8443/call');
var video;
var webRtcPeer;
var dbName;
window.onload = function() {
    console = new Console();
    video = document.getElementById('video');

    document.getElementById('call').addEventListener('click', function() {
        console.log("=========record==========");
        console.info($("#record").prop('checked'));
        document.getElementById("record").disabled = true;
        presenter();
    });
    document.getElementById('viewer').addEventListener('click', function() {
        viewer();
    });
    document.getElementById('terminate').addEventListener('click', function() {
        document.getElementById("record").disabled = false;
        console.log("=========record==========");
        console.info($("#record").prop('checked'));
        stop();
    });
}

window.onbeforeunload = function() {
    ws.close();
}
$(window).unload(function() {
    stop();
});

window.onbeforeunload = function() {
    stop();
}

ws.onmessage = function(message) {
    var parsedMessage = JSON.parse(message.data);
    console.info('Received message: ' + message.data);

    switch (parsedMessage.id) {
        case 'presenterResponse':
            presenterResponse(parsedMessage);
            break;
        case 'viewerResponse':
            viewerResponse(parsedMessage);
            break;
        case 'recording':
            var filepath = parsedMessage.filePath;
            break;
        case 'stopCommunication':
            dispose();
            break;
        case 'stopped':
            dispose();
            if (parsedMessage.filePath != undefined) {
                saveRecordedFile(parsedMessage.filePath);
            }
            break;
        case 'iceCandidate':
            webRtcPeer.addIceCandidate(parsedMessage.candidate)
            break;
        default:
            console.error('Unrecognized message', parsedMessage);
    }
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function getSessionInfo() {
    var sessionid = readCookie('session_id');
    var serverurl = window.top.location.origin;
    var url = serverurl + "/web/session/get_session_info";
    var dat = {
        "session_id": sessionid,
        "url": url,
        "input": {
            "jsonrpc": "2.0",
            "method": "call",
            "params": {},
            "id": 748267743
        }
    };
    odooService(dat, function(data) {
        dbName = data.result.db;
        console.log("dbname===" + dbName);
    });
}
getSessionInfo();

function saveRecordedFile(filename) {

    var sessionid = readCookie('session_id');
    var serverurl = window.top.location.origin;

    var url = serverurl + "/web/dataset/call_kw/product.broadcast/write";
    var broadcastId = window.top.location.hash.split('#id=')[1].split('&')[0];
    var downloadedPath = "http://lic.affosoft.com/" + dbName + "/" + broadcastId + "/";
    var path = downloadedPath + filename;
    console.log("path" + path);

    var dat = {
        "session_id": sessionid,
        "url": url,
        "input": {
            "jsonrpc": "2.0",
            "method": "call",
            "params": {
                "model": "product.broadcast",
                "method": "write",
                "args": [
                    [parseInt(broadcastId)], {
                        "recorded_broadcasts_ids": [
                            [0, false, {
                                "name": path
                            }]
                        ]
                    }
                ],
                "kwargs": {
                    "context": {
                        "lang": "en_US",
                        "tz": "Asia/Calcutta",
                        "params": {
                            "action": 224
                        }
                    }
                }
            },
            "id": 691755509
        }
    };
    odooService(dat, function(data) {
        if (data.result == true) {
            console.log("recorded");
        }
    });
}

function odooService(params, handleResponse) {
    $.ajax({
        url: "http://gateway.parrot.solutions",
        //    	   url : "http://in.affosoft.com/odoocallback/test/test.php",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(params),
        dataType: "json",
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            handleResponse(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            odooerror = true;
        }
    });
}

function presenterResponse(message) {
    if (message.response != 'accepted') {
        var errorMsg = message.message ? message.message : 'Unknown error';
        console.warn('Call not accepted for the following reason: ' + errorMsg);
        dispose();
    } else {
        webRtcPeer.processAnswer(message.sdpAnswer);
    }
}

function viewerResponse(message) {
    if (message.response != 'accepted') {
        var errorMsg = message.message ? message.message : 'Unknown error';
        console.warn('Call not accepted for the following reason: ' + errorMsg);
        dispose();
    } else {
        webRtcPeer.processAnswer(message.sdpAnswer);
    }
}
var reconnecting = false;
/*var iframe = document.getElementsByTagName('iframe');
console.log(iframe);
console.log(document.getElementById('errorMsg'));

var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
console.log(innerDoc);
alert(innerDoc.getElementById("errorMsg"));
var errorMsg = document.getElementById('errorMsg');
console.log(document.getElementById('errorMsg'));
errorMsg.style.display === 'none';*/

function presenter() {

    if (!webRtcPeer) {
        showSpinner(video);

        var options = {
            localVideo: video,
            onicecandidate: onIceCandidate
        }

        webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
            if (error) return onError(error);

            this.generateOffer(onOfferPresenter);
            console.log("========network=========");
            console.log(navigator.onLine);
            if (navigator.onLine) {
                var reconnecting = false;
            }
            if (!navigator.onLine) {
                console.log(reconnecting);
                if (webRtcPeer) {
                    var message = {
                        id: 'stop'
                    }
                    sendMessage(message);
                    dispose();
                }
                //TODO
                presenter();
            }

            var pc = this.checkStatus();

            pc.oniceconnectionstatechange = function() {
                var status = pc.iceConnectionState;
                console.log("============");
                console.log(status);
                if (status == "disconnected") {
                    reconnecting = true;
                    if (webRtcPeer) {
                        var message = {
                            id: 'stop'
                        }
                        sendMessage(message);
                        dispose();
                    }

                    presenter();
                }

            }
        });
    }
}

function onOfferPresenter(error, offerSdp) {
    //var dbName = window.top.location.search.split('db=')[1];
    console.info("dbname===" + dbName);
    var broadcastId = window.top.location.hash.split('#id=')[1].split('&')[0];
    console.info(broadcastId);
    if (error) return onError(error);
    var message = {
        id: 'presenter',
        companyName: dbName,
        className: broadcastId,
        presenterName: "admin",
        record: $("#record").prop('checked'),
        sdpOffer: offerSdp
    };
    sendMessage(message);
}

function viewer() {
    if (!webRtcPeer) {
        showSpinner(video);

        var options = {
            remoteVideo: video,
            onicecandidate: onIceCandidate
        }
        webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function(error) {
            if (error) return onError(error);
            this.generateOffer(onOfferViewer);
        });
    }
}

function onOfferViewer(error, offerSdp) {
    if (error) return onError(error)
    var message = {
        id: 'viewer',
        sdpOffer: offerSdp
    }
    sendMessage(message);
}

function onIceCandidate(candidate) {
    console.log('Local candidate' + JSON.stringify(candidate));
    var message = {
        id: 'onIceCandidate',
        candidate: candidate
    }
    sendMessage(message);
}

function stop() {
    if (webRtcPeer) {
        var message = {
            id: 'stop'
        }
        sendMessage(message);
        dispose();
    }
}

function dispose() {
    if (webRtcPeer) {
        webRtcPeer.dispose();
        webRtcPeer = null;
    }
    hideSpinner(video);
}

function sendMessage(message) {
    var jsonMessage = JSON.stringify(message);
    console.log('Sending JSON message: ' + jsonMessage);
    ws.send(jsonMessage);
}

function showSpinner() {
    for (var i = 0; i < arguments.length; i++) {
        arguments[i].poster = './img/transparent-1px.png';
        arguments[i].style.background = 'center transparent url("./img/spinner.gif") no-repeat';
    }
}

function hideSpinner() {
    for (var i = 0; i < arguments.length; i++) {
        arguments[i].src = '';
        arguments[i].poster = './img/webrtc.png';
        arguments[i].style.background = '';
    }
}

/**
 * Lightbox utility (to display media pipeline image in a modal dialog)
 */
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});