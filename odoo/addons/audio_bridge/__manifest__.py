# -*- coding: utf-8 -*-
{
    'name': "Audio Bridge",
    'summary': """Audio Bridge for conference audio call.""",
    'description': """This Module is for conference audio call

        Features:
            Audio group call.
            Hand rise and ask doubts.
            Admin can Mute and Unmute all participants.""",
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'category': 'Uncategorized',
    'version': '0.1',
    'sequence': '6',
    'depends': [
        'web',
        'mail',
        'web_ir_actions_act_view_reload',
    ],
    'data': [
        'security/audio_bridge_security.xml',
        'security/ir.model.access.csv',
        'views/audio_bridge_views.xml',
        # 'data/audio_bridge_cron.xml',
        'data/audio_bridge_data.xml',
        'data/app_menu_data.xml',
    ],
    'application': True,
}
