{
    "name": "Web Actions View Reload",
    "summary": "Enables reload of the current view via ActionManager",
    "category": "Web",
    "version": "11.0.1.0.0",
    "license": "LGPL-3",
    "author": "Modoolar, Odoo Community Association (OCA)",
    "website": "https://github.com/OCA/web/",
    "depends": ["web"],
    "data": [
        "views/web_ir_actions_act_view_reload.xml",
    ],
    "installable": True,
}