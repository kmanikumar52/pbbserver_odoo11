odoo.define('web_ir_actions_act_view_reload.ir_actions_act_view_reload', function (require) {
    "use strict";
    var ActionManager = require('web.ActionManager');
    ActionManager.include({
        ir_actions_act_view_reload: function (action, options) {
            if (this.inner_widget && this.inner_widget.active_view && this.inner_widget.active_view.controller)
                console.log("Reload is working");
            this.inner_widget.active_view.controller.reload();
            var active_view_controller = this.inner_widget.active_view.controller
            setInterval(function (action, options) {
                console.log("SetInterval Reload");
                active_view_controller.reload();
            }, 10000);
            return $.when();
        }
    });
});