# -*- coding: utf-8 -*-
{
    'name': "Parrot Business Booster",
    'summary': """Parrot Business Booster""",
    'description': """Parrot Business Booster - List of all modules developed by Parrot Solutions Tech team(Which was added in depends
    """,
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'version': '0.1',
    'depends': [
        'product_information_management',
        'retail_parrot',
        'document',
        # 'web_export_view',
        'parrot_hide_menus',
        'parrot_project',
        # 'parrot_expense',
        'parrot_last_seen',
        # 'parrot_backend_theme',
    ],
    'data': [
    ],
    'application': True,
}
