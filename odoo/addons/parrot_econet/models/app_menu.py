from odoo import models, fields,api,_


class AppMenu(models.Model):
    _inherit = 'app.menu'

    group_ids = fields.Many2many('res.groups', 'ir_model_menu_group_rel', 'menu_id', 'group_id', 'Select Groups')
