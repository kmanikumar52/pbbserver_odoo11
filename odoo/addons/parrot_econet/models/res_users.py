import logging
import re
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from odoo import api, models, fields

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.multi
    def _get_selection_list(self):
        selection_list = self.env['res.users'].get_region_selection()
        return selection_list

    region_id = fields.Selection(_get_selection_list, string='Region')
    compute_field = fields.Boolean(string="check field", compute='get_user')

    @api.multi
    @api.onchange('region_id')
    def _assign_manager(self):
        res = {}
        manager_id = []
        self.manager_id = ""
        for group in self.env['res.groups'].search(['|', ('full_name', '=', 'Regional Manager'),
                                                    ('full_name', '=', 'Account Manager')]):
            for users in group.users:
                if users.region_id:
                    # for values in self.fields_get(['region_id'])['region_id']['selection']:
                    #     if values[0] == self.region_id and users.region_id == values[1]:
                    if self.region_id == users.region_id:
                        manager_id.append(users.id)
                res['domain'] = {'manager_id': [('id', 'in', manager_id)]}
        return res

    # def onchange_state(self, state_id, context=None):
    #     partner_ids = [user.partner_id.id for user in self.browse(self, context=context)]
    #     return self.env['res.partner'].onchange_state(self, partner_ids, state_id, context=context)

    @api.depends('compute_field')
    def get_user(self):
        res_user = self.env['res.users'].search([('id', '=', self._uid)])
        if res_user.has_group('parrot_econet.group_sa'):
            self.compute_field = True
        else:
            self.compute_field = False

    @api.model
    def create(self, vals):
        user_id = super(ResUsers, self).create(vals)
        self.env['hr.employee'].create({'name': user_id.name, 'user_id': user_id.id})
        for app_menu in self.env["app.menu"].search([]):
            for group in app_menu.group_ids:
                for users in group.users:
                    if users.id == user_id.id:
                        app_menu.write({'user_menu': [(4, [[user_id.id]])]})
        return user_id

    @api.multi
    def write(self, vals):
        res = super(ResUsers, self).write(vals)
        for app_menu in self.env["app.menu"].search([]):
            for group in app_menu.group_ids:
                for users in group.users:
                    if users.id == self.id:
                        app_menu.write({'user_menu': [(4, self.id)]})
        return res

    @api.multi
    def pos_visit_done_count(self, type):
        if self.employee_list:
            lists = []
            emp_dict_list = []
            final_dict = {}
            for user_id in self.employee_list:
                list = user_id.calc_pos_visit_done(type)
                if list['result']:
                    lists.append(list['result'])
                for usr_id in user_id.employee_list:
                    list = usr_id.calc_pos_visit_done(type)
                    if list['result']:
                        lists.append(list['result'])
                    for us_id in usr_id.employee_list:
                        list = us_id.calc_pos_visit_done(type)
                        if list['result']:
                            lists.append(list['result'])
                        for u_id in us_id.employee_list:
                            list = u_id.calc_pos_visit_done(type)
                            if list['result']:
                                lists.append(list['result'])
            if type == 'week':
                for i in range(7):
                    emp_dict = {}
                    temp_count = 0
                    for j in range(len(lists)):
                        temp_date = lists[j][i]['date']
                        temp_count = temp_count + lists[j][i]['count']
                    emp_dict.update({'date': temp_date, 'count': temp_count})
                    emp_dict_list.append(emp_dict)
                    final_dict.update({'result': emp_dict_list})

            if type == 'month':
                for i in range(4):
                    emp_dict = {}
                    temp_count = 0
                    for j in range(len(lists)):
                        temp_date = lists[j][i]['date']
                        temp_count = temp_count + lists[j][i]['count']
                    emp_dict.update({'date': temp_date, 'count': temp_count})
                    emp_dict_list.append(emp_dict)
                    final_dict.update({'result': emp_dict_list})
            if type == 'year':
                emp_dict = {}
                temp_count = 0
                for j in range(len(lists)):
                    for i in range(1):
                        temp_count = temp_count + lists[j][i]['count']
                emp_dict.update({'date': 'year', 'count': temp_count})
                final_dict.update({'result': [{'date': 'year', 'count': temp_count}]})
            return final_dict
        else:
            return self.calc_pos_visit_done(type)

    @api.multi
    def calc_pos_visit_done(self, type):
        if type == 'week':
            wk_count_list = []
            final_dict = {}
            for i in range(7):
                wk_count_dict = {}
                week_date = datetime.today() + timedelta(days=-i)
                date_strformat = week_date.strftime('%Y-%m-%d')
                pos_visit = self.env['customer.visit'].search(
                    [('create_uid', '=', self.id), ('date', '=', date_strformat)])
                wk_count_dict.update({'date': date_strformat, 'count': len(pos_visit)})
                wk_count_list.append(wk_count_dict)
                final_dict.update({'result': wk_count_list})
        if type == 'month':
            mn_count_list = []
            final_dict = {}
            today_date = datetime.today()
            i = 0
            date_diff = 0
            for j in range(5):
                wk_pos_list = []
                wk_count_dict = {}
                if j == 0:
                    i += 1
                    start_date = today_date + timedelta(days=-i)
                    start_date_strformat = start_date.strftime('%Y-%m-%d')
                    week_end_date = today_date - timedelta(days=today_date.weekday())
                    end_date_strformat = week_end_date + timedelta(days=-i)
                    date_diff = (start_date - week_end_date).days
                    i += date_diff
                else:
                    i += 1
                    start_date = today_date + timedelta(days=-i)
                    start_date_strformat = start_date.strftime('%Y-%m-%d')
                    if j == 4:
                        end_date = today_date + timedelta(days=-30)
                        end_date_strformat = end_date.strftime('%Y-%m-%d')
                    else:
                        i += 6
                        end_date = today_date + timedelta(days=-i)
                        end_date_strformat = end_date.strftime('%Y-%m-%d')
                pos_visit = self.env['customer.visit'].search(
                    [('create_uid', '=', self.id), ('date', '>=', end_date_strformat),
                     ('date', '<=', start_date_strformat)])
                wk_pos_list.append(len(pos_visit))
                total_wk_pos = 0
                datetime_object = datetime.strptime(start_date_strformat, '%Y-%m-%d')
                this_wk = datetime_object.isocalendar()[1]
                for pos_count in wk_pos_list:
                    total_wk_pos += pos_count
                wk_count_dict.update({'date': this_wk, 'count': total_wk_pos})
                mn_count_list.append(wk_count_dict)
                final_dict.update({'result': mn_count_list})

        if type == 'year':
            wk_count_dict = {}
            final_dict = {}
            yr_of_date = datetime.today().year
            week_date = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
            yesterday_date = datetime.today()
            today_strformat = yesterday_date.strftime('%Y-%m-%d')
            todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
            if todate_format > week_date:
                yr_start_date = week_date
            else:
                last_yr = yr_of_date - 1
                last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                yr_start_date = last_yr_date
            pos_visit = self.env['customer.visit'].search(
                [('create_uid', '=', self.id), ('date', '>=', yr_start_date), ('date', '<=', today_strformat)])
            wk_count_dict.update({'date': 'Year', 'count': len(pos_visit)})
            final_dict.update({'result': [{'date': 'Year', 'count': len(pos_visit)}]})
        return final_dict

    @api.multi
    def get_new_pos_count(self, type):
        if self.employee_list:
            lists = []
            emp_dict_list = []
            final_dict = {}
            for user_id in self.employee_list:
                list = user_id.calc_new_pos_count(type)
                if list['result']:
                    lists.append(list['result'])
                for usr_id in user_id.employee_list:
                    list = usr_id.calc_new_pos_count(type)
                    if list['result']:
                        lists.append(list['result'])
                    for us_id in usr_id.employee_list:
                        list = us_id.calc_new_pos_count(type)
                        if list['result']:
                            lists.append(list['result'])
                        for u_id in us_id.employee_list:
                            list = u_id.calc_new_pos_count(type)
                            if list['result']:
                                lists.append(list['result'])
            if type == 'week':
                for i in range(7):
                    emp_dict = {}
                    temp_count = 0
                    for j in range(len(lists)):
                        temp_date = lists[j][i]['date']
                        temp_count = temp_count + lists[j][i]['count']
                    emp_dict.update({'date': temp_date, 'count': temp_count})
                    emp_dict_list.append(emp_dict)
                    final_dict.update({'result': emp_dict_list})
            if type == 'month':
                for i in range(4):
                    emp_dict = {}
                    temp_count = 0
                    for j in range(len(lists)):
                        temp_date = lists[j][i]['date']
                        datetime_object = datetime.strptime(temp_date, '%Y-%m-%d')
                        this_wk = datetime_object.isocalendar()[1]
                        temp_count = temp_count + lists[j][i]['count']
                    emp_dict.update({'date': this_wk, 'count': temp_count})
                    emp_dict_list.append(emp_dict)
                    final_dict.update({'result': emp_dict_list})
            if type == 'year':
                emp_dict = {}
                temp_count = 0
                for j in range(len(lists)):
                    for i in range(1):
                        temp_count = temp_count + lists[j][i]['count']
                emp_dict.update({'date': 'year', 'count': temp_count})
                final_dict.update({'result': [{'date': 'year', 'count': temp_count}]})
            return final_dict
        else:
            return self.calc_new_pos_count(type)

    @api.multi
    def calc_new_pos_count(self, type):
        cr = self._cr
        if type == 'week':
            start_date = datetime.today()
            start_date_strformat = start_date.strftime('%Y-%m-%d')
            end_date = datetime.today() + timedelta(days=-7)
            end_date_strformat = end_date.strftime('%Y-%m-%d')
            cr.execute('''SELECT   cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int) as ques_count,
                (CASE
                 WHEN cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int) > 0 THEN
                1
                ELSE
                0
                END)as pos_transacted,
                visit.name,visit.date
                FROM customer_visit visit
                join visit_answers answer on visit.id=answer.visit_id
                join visit_questions ques on ques.id=answer.name
                join question_set_rel rel on rel.set_id=ques.id
                join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                join retail_customer pos on pos.id=visit.name
                where visit.create_uid = %s
                AND visit.date >= %s
                AND visit.date <=  %s
                group by visit.name,visit.date order by visit.date desc''',
                       (self.id, end_date_strformat, start_date_strformat))
            result_dict = cr.dictfetchall()
            date_list = []
            for i in range(7):
                date_dict = {}
                week_date = datetime.today() + timedelta(days=-i)
                date_strformat = week_date.strftime('%Y-%m-%d')
                date_dict.update({'date': date_strformat, 'count': 0})
                date_list.append(date_dict)
            fin_dict = {}
            for dat in date_list:
                tot_count = 0
                for res in result_dict:
                    if dat['date'] == res['date']:
                        tot_count = tot_count + res['pos_transacted']
                        dat.update({'count': tot_count})
            fin_dict.update({'result': date_list})
            return fin_dict
        if type == 'month':
            yesterday_date = datetime.today()
            today_strformat = yesterday_date.strftime('%Y-%m-%d')
            week_date = datetime.today() + timedelta(days=-30)
            date_strformat = week_date.strftime('%Y-%m-%d')
            cr.execute('''SELECT   cast(date_part('week',(visit.date::date))as int) as date,cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int) as ques_count,
                (CASE
                 WHEN cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int) > 0 THEN
                1
                ELSE
                0
                END)as pos_transacted,
                visit.name
                FROM customer_visit visit
                join visit_answers answer on visit.id=answer.visit_id
                join visit_questions ques on ques.id=answer.name
                join question_set_rel rel on rel.set_id=ques.id
                join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                join retail_customer pos on pos.id=visit.name
                where visit.create_uid = %s
                AND visit.date >= %s
                AND visit.date <=  %s
                group by visit.name, cast(date_part('week',(visit.date::date))as int) order by cast(date_part('week',(visit.date::date))as int) asc''',
                       (self.id, date_strformat, today_strformat))
            result_dict = cr.dictfetchall()

            datetime_object = datetime.strptime(today_strformat, '%Y-%m-%d')
            this_wk = datetime_object.isocalendar()[1]
            this_wk -= 4
            wk_list = []
            for i in range(5):
                wk_list.append(this_wk)
                this_wk += 1
            mn_pos_trans_list = []
            for wk in wk_list:
                mn_pos_trans_dict = {}
                mn_pos_trans_dict.update({'date': wk, 'count': 0})
                mn_pos_trans_list.append(mn_pos_trans_dict)
            fin_dict = {}
            for dat in mn_pos_trans_list:
                tot_count = 0
                for res in result_dict:
                    if dat['date'] == res['date']:
                        tot_count = tot_count + res['pos_transacted']
                        dat.update({'count': tot_count})
            fin_dict.update({'result': mn_pos_trans_list})
            return fin_dict
        if type == 'year':
            yr_of_date = datetime.today().year
            financial_year_start_date = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
            yesterday_date = datetime.today()
            today_strformat = yesterday_date.strftime('%Y-%m-%d')
            todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
            if todate_format > financial_year_start_date:
                yr_start_date = financial_year_start_date
            else:
                last_yr = yr_of_date - 1
                last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                yr_start_date = last_yr_date
            cr.execute('''SELECT   cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int) as ques_count,
                (CASE
                 WHEN cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int) > 0 THEN
                1
                ELSE
                0
                END)as pos_transacted,
                visit.name,visit.date
                FROM customer_visit visit
                join visit_answers answer on visit.id=answer.visit_id
                join visit_questions ques on ques.id=answer.name
                join question_set_rel rel on rel.set_id=ques.id
                join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                join retail_customer pos on pos.id=visit.name
                where visit.create_uid = %s
                AND visit.date >= %s
                AND visit.date <=  %s
                group by visit.name,visit.date order by visit.date desc''',
                       (self.id, yr_start_date, today_strformat))
            result_dict = cr.dictfetchall()
            final_dict = {'date': 'Year', 'count': 0}
            fin_dict = {}
            tot_count = 0
            for res in result_dict:
                tot_count = tot_count + res['pos_transacted']
            final_dict.update({'count': tot_count})
            fin_dict.update({'result': [final_dict]})
            return fin_dict

    @api.multi
    def get_user_designation(self, user_id):
        group_name = ""
        for group in self.groups_id:
            search_group_obj = self.env["res.groups"].search([('id', '=', group.id)])
            if search_group_obj.name:
                if search_group_obj.name == "BA":
                    if group_name != "AM" and group_name != "RA" and group_name != "RM" and group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                        group_name = "BA"
                elif search_group_obj.name == "Account Manager":
                    if group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                        group_name = "AM"
                elif search_group_obj.name == "Regional Admin":
                    if group_name != "GM" and group_name != "CSO" and group_name != "CCO" and group_name != "AM":
                        group_name = "RA"
                elif search_group_obj.name == "Regional Manager":
                    if group_name != "GM" and group_name != "CSO" and group_name != "CCO" and group_name != "AM" and group_name != "RA":
                        group_name = "RM"
                elif search_group_obj.name == "General Manager":
                    if group_name != "CSO" and group_name != "CCO":
                        group_name = "GM"
                elif search_group_obj.name == "CSO":
                    if group_name != "CCO":
                        group_name = "CSO"
                elif search_group_obj.name == "CCO":
                    group_name = "CCO"
        return group_name

    @api.multi
    def get_employee_list_query(self, group_name):
        if group_name == 'CCO':
            search_manager = self.env['res.users'].search(([('id', '=', self.id)]))
            if search_manager.manager_id:

                query = '''(
                            Select distinct id from res_users where manager_id in
                            (Select distinct id from res_users where manager_id in
                            (Select distinct id from res_users where manager_id in
                            (Select distinct id from res_users where manager_id=%s)))
                            and active='t'

                            )'''
            else:
                query = '''(
                            Select distinct id from res_users where manager_id in
                            (Select distinct id from res_users where manager_id in
                            (Select distinct id from res_users where manager_id in
                            (Select distinct id from res_users where manager_id in
                            (Select distinct id from res_users where manager_id=%s))))
                            and active='t'

                            )'''

        elif group_name == 'CSO':
            query = '''(
                                    (Select distinct id from res_users where manager_id in
                                    (Select distinct id from res_users where manager_id in 
                                    (Select distinct id from res_users where manager_id=%s))
                                    and active='t'
                                    )
                                     )'''
        elif group_name == 'GM':
            query = '''(Select distinct id from res_users where manager_id in 
                                    (Select distinct id from res_users where manager_id=%s)
                                    and active='t')'''
        elif group_name == 'AM' or group_name == 'RM' or group_name == 'RA':
            query = '''(Select distinct id from res_users where manager_id=%s
                                    and active='t')'''

        elif group_name == 'BA':
            query = '''(%s) '''
        else:
            query = '''(%s) '''
        return query

    @api.multi
    def get_questions_count(self, type):
        employee_list_ids = []
        user_group_name = self.get_user_designation(self.id)
        emp_query = self.get_employee_list_query(user_group_name)
        cr = self._cr
        if len(employee_list_ids) <= 0:
            employee_list_ids.append(self.id)
        if employee_list_ids:
            if type == 'week':
                yesterday_date = datetime.today() + timedelta(days=-1)
                today_strformat = yesterday_date.strftime('%Y-%m-%d')
                week_date = datetime.today() + timedelta(days=-7)
                date_strformat = week_date.strftime('%Y-%m-%d')
                query = ''' SELECT
                    COALESCE(cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int),0) as sum,
                    ques.name,visit.date
                    FROM customer_visit visit
                    join visit_answers answer on visit.id=answer.visit_id
                    join visit_questions ques on ques.id=answer.name
                    join question_set_rel rel on rel.set_id=ques.id 
                    join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                    where visit.create_uid in'''
                query += emp_query
                query += '''AND visit.date >= %s
                    AND visit.date <=  %s
                    group by ques.name,visit.date order by visit.date desc'''

                cr.execute(query, (self.id, date_strformat, today_strformat))

                result_dict = cr.dictfetchall()
                question_set = self.env['visit.questions.set'].search([('name', '=', 'Econet Question')])
                ques_list = []
                for quest in question_set.questions:
                    ques_dict = {}
                    date_list = []
                    for i in range(-7, 0, 1):
                        date_dict = {}
                        week_date = datetime.today() + timedelta(days=i)
                        date_strformat = week_date.strftime('%Y-%m-%d')
                        date_dict.update(
                            {'date': date_strformat, 'count': 0, 'q_name': quest.name})
                        date_list.append(date_dict)
                    a = re.search(r'\b(many)\b', quest.name)
                    end = a.end()
                    final_string = quest.name[end:-1]
                    ques_dict.update({'name': final_string.title(), 'date': date_list, 'mtd_count': 0, 'lmtd_count': 0,
                                      'year_count': 0})
                    ques_list.append(ques_dict)
                final_data_list = []
                f_dict = {}
                for que in ques_list:
                    for data in result_dict:
                        for i, que_date in enumerate(que['date'], 0):
                            a = re.search(r'\b(many)\b', data['name'])
                            end = a.end()
                            final_string = data['name'][end:-1]
                            if que_date['date'] == data['date'] and que['name'] == final_string and data['name'] == \
                                    que_date['q_name']:
                                que['date'][i]['count'] = data['sum']
                    final_data_list.append(que)
                f_dict.update({'final': final_data_list})
                month_count_dict = self.get_questions_month_count(self.id, user_group_name, 'month')
                lmtd_count_dict = self.get_questions_lmtd_count(self.id, user_group_name, 'last_month')
                year_count_dict = self.get_questions_year_count(self.id, user_group_name, 'year')
                if 'final' in f_dict:
                    for wk_c in f_dict['final']:
                        for m_c in month_count_dict:
                            if wk_c['name'] == m_c['name']:
                                wk_c.update({'mtd_count': m_c['count']})
                                if wk_c['mtd_count'] is None:
                                    wk_c['mtd_count'] = 0
                        for m_c in lmtd_count_dict:
                            if wk_c['name'] == m_c['name']:
                                wk_c.update({'lmtd_count': m_c['count']})
                                if wk_c['lmtd_count'] is None:
                                    wk_c['lmtd_count'] = 0
                        for y_c in year_count_dict:
                            if wk_c['name'] == y_c['name']:
                                wk_c.update({'year_count': y_c['count']})
                                if wk_c['year_count'] is None:
                                    wk_c['year_count'] = 0
                return f_dict

    @api.multi
    def get_questions_month_count(self, user_id, group_name, type):
        cr = self._cr
        emp_query = self.get_employee_list_query(group_name)
        if user_id:
            if type == 'month':
                yesterday_date = datetime.today()
                today_strformat = yesterday_date.strftime('%Y-%m-%d')
                week_date = yesterday_date.replace(day=1)
                date_strformat = week_date.strftime('%Y-%m-%d')
                query = ''' SELECT
                        cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int) as count,
                        cast(INITCAP(regexp_replace(split_part(lower(ques.name),'how many',2) , '[?]+', '','g'))as character varying)  as name
                        FROM customer_visit visit
                        join visit_answers answer on visit.id=answer.visit_id
                        join visit_questions ques on ques.id=answer.name
                        join question_set_rel rel on rel.set_id=ques.id 
                        join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                        where visit.create_uid in '''

                query += emp_query
                query += '''AND visit.date >= %s
                        AND visit.date <=  %s
                        group by ques.name'''
                cr.execute(query, (user_id, date_strformat, today_strformat))
                # (tuple(employee_list_ids), date_strformat, today_strformat))
                result_dict = cr.dictfetchall()
                if len(result_dict) <= 0:
                    result_dict = []
                    question_set = self.env['visit.questions.set'].search([('name', '=', 'Econet Question')])
                    for ques_obj in question_set.questions:
                        a = re.search(r'\b(many)\b', ques_obj.name)
                        end = a.end()
                        final_string = ques_obj.name[end:-1]
                        ques_name = final_string.title()
                        result_dict_new = {'name': ques_name, 'count': 0}
                        result_dict.append(result_dict_new)
                return result_dict

    @api.multi
    def get_questions_lmtd_count(self, user_id, group_name, type):
        emp_query = self.get_employee_list_query(group_name)
        cr = self._cr
        if user_id:
            if type == 'last_month':
                today_date = datetime.today()
                mon_date = today_date.replace(day=1)
                last_m_first = mon_date - relativedelta(months=1)
                date_strformat = last_m_first.strftime('%Y-%m-%d')
                lastMonth = today_date - relativedelta(months=1)
                lastMonth_strformat = lastMonth.strftime('%Y-%m-%d')
                query = ''' SELECT
                        cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int) as count,
                        cast(INITCAP(regexp_replace(split_part(lower(ques.name),'how many',2) , '[?]+', '','g'))as character varying)  as name
                        FROM customer_visit visit
                        join visit_answers answer on visit.id=answer.visit_id
                        join visit_questions ques on ques.id=answer.name
                        join question_set_rel rel on rel.set_id=ques.id 
                        join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                        where visit.create_uid in'''

                query += emp_query
                query += '''AND visit.date >= %s
                        AND visit.date <=  %s
                        group by ques.name'''
                cr.execute(query, (user_id, date_strformat, lastMonth_strformat))
                result_dict = cr.dictfetchall()
                if len(result_dict) <= 0:
                    result_dict = []
                    question_set = self.env['visit.questions.set'].search([('name', '=', 'Econet Question')])
                    for ques_obj in question_set.questions:
                        a = re.search(r'\b(many)\b', ques_obj.name)
                        end = a.end()
                        final_string = ques_obj.name[end:-1]
                        ques_name = final_string.title()
                        result_dict_new = {'name': ques_name, 'count': 0}
                        result_dict.append(result_dict_new)
                return result_dict

    @api.multi
    def get_questions_year_count(self, user_id, group_name, type):
        cr = self._cr
        emp_query = self.get_employee_list_query(group_name)
        if user_id:
            if type == 'year':
                yr_of_date = datetime.today().year
                financial_year_start_date = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
                yesterday_date = datetime.today()
                today_strformat = yesterday_date.strftime('%Y-%m-%d')
                todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
                if todate_format > financial_year_start_date:
                    yr_start_date = financial_year_start_date
                else:
                    last_yr = yr_of_date - 1
                    last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                    yr_start_date = last_yr_date
                query = ''' SELECT
                    'Year' as date, cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int) as count,
                    cast(INITCAP(regexp_replace(split_part(lower(ques.name),'how many',2) , '[?]+', '','g'))as character varying)  as name
                    FROM customer_visit visit
                    join visit_answers answer on visit.id=answer.visit_id
                    join visit_questions ques on ques.id=answer.name
                    join question_set_rel rel on rel.set_id=ques.id 
                    join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                    where visit.create_uid in'''
                query += emp_query
                query += '''AND visit.date >= %s
                    AND visit.date <=  %s
                    group by ques.name'''
                cr.execute(query, (user_id, yr_start_date, today_strformat))
                result_dict = cr.dictfetchall()
                if len(result_dict) <= 0:
                    result_dict = []
                    question_set = self.env['visit.questions.set'].search([('name', '=', 'Econet Question')])
                    for ques_obj in question_set.questions:
                        a = re.search(r'\b(many)\b', ques_obj.name)
                        end = a.end()
                        final_string = ques_obj.name[end:-1]
                        ques_name = final_string.title()
                        result_dict_new = {'name': ques_name, 'count': 0}
                        result_dict.append(result_dict_new)
                return result_dict

    @api.multi
    def get_am_area_report(self, type):
        group_name = self.get_user_designation(self.id)
        cr = self._cr
        final_dict = {}
        employee_list_ids = []
        count_list = []
        employee_list = []
        if self.employee_list:
            for user_id in self.employee_list:
                emp_dict = {}
                emp_dict.update({'id': user_id.id, 'u_name': user_id.name})
                employee_list_ids.append(user_id.id)
                employee_list.append(emp_dict)
        if employee_list_ids:
            if type == 'year':
                yr_of_date = datetime.today().year
                date_strformat1 = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
                today_date = datetime.today()
                today_strformat = today_date.strftime('%Y-%m-%d')
                todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
                if todate_format > date_strformat1:
                    date_strformat = date_strformat1
                else:
                    last_yr = yr_of_date - 1
                    last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                    date_strformat = last_yr_date
            else:
                today_date = datetime.today()
                today_strformat = today_date.strftime('%Y-%m-%d')
                month_date = today_date.replace(day=1)
                date_strformat = month_date.strftime('%Y-%m-%d')
            cr.execute('''DROP FUNCTION IF EXISTS user_count()''')
            cr.execute('''CREATE OR REPLACE FUNCTION user_count()
                              RETURNS TABLE (
                               count int,
                               name character varying,
                               user_id int,
                               ques_id int
                              ) AS
                            $func$
                            BEGIN
                               RETURN QUERY
                            SELECT
                                            COALESCE(cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int),0) as count,
                                            cast(TRIM(INITCAP(regexp_replace(split_part(lower(ques.name),'how many',2) , '[?]+', '','g')))as character varying)  as name,
                                             visit.create_uid as user_id,
                                             ques.id as ques_id
                                            FROM customer_visit visit
                                            join visit_answers answer on visit.id=answer.visit_id
                                            join visit_questions ques on ques.id=answer.name
                                            join question_set_rel rel on rel.set_id=ques.id 
                                            join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                                            join res_users users on users.id=visit.create_uid
                                            where visit.create_uid in %s
                                            AND visit.date >= %s
                                            AND visit.date <=  %s
                                            group by ques.name,
                                             visit.create_uid,ques.id order by ques.id;

                             END
                            $func$ LANGUAGE plpgsql;''', (tuple(employee_list_ids), date_strformat, today_strformat))
            cr.execute('''Select * from user_count()''')
            result_dict = cr.dictfetchall()
            cr.execute('''WITH user_count AS(select users.id as user_id
                                from res_users users	 
                                where users.manager_id= %s AND users.active = 't'
                                EXCEPT
                                SELECT Distinct user_id
                                 FROM user_count() )
                                select * from user_count order by user_id''', (self.id,))
            query_result_dict = cr.fetchall()
            question_set = self.env['visit.questions.set'].search([('name', '=', 'Econet Question')])
            user_detail_list = []
            for question in question_set.questions:
                a = re.search(r'\b(many)\b', question.name)
                end = a.end()
                final_string = question.name[end:-1]
                question_name = final_string.title()
                for u_id in query_result_dict:
                    user_detail_dict = {'name': '', 'count': 0, 'user_id': ''}
                    user_detail_dict.update({'name': question_name, 'user_id': u_id[0]})
                    user_detail_list.append(user_detail_dict)
            user_list = []
            result_dict.extend(user_detail_list)
            for user in employee_list:
                user_dict = {'user_id': '', 'detail': []}
                detail_list = []
                user_dict.update({'user_id': user['id'], 'user_name': user['u_name'], 'detail': detail_list})
                for details in result_dict:
                    final_list = []
                    if details['user_id'] == user['id']:
                        final_list.append(details)
                        detail_list.append(details)
                user_dict.update({'detail': detail_list})
                user_list.append(user_dict)
            final_dict.update({'result': user_list})
            month_count_dict = self.get_questions_month_count(self.id, group_name, 'month')
            year_count_dict = self.get_questions_year_count(self.id, group_name, 'year')
            for ques_obj in question_set.questions:
                a = re.search(r'\b(many)\b', ques_obj.name)
                end = a.end()
                final_string = ques_obj.name[end:-1]
                ques_name = final_string.title()
                count_dict = {'ques_name': '', 'mtd_count': 0, 'year_count': 0}
                count_dict.update({'ques_name': ques_name, 'mtd_count': 0, 'year_count': 0})

                for m_c in month_count_dict:
                    if count_dict['ques_name'] == m_c['name']:
                        count_dict.update({'mtd_count': m_c['count']})
                        if count_dict['mtd_count'] is None:
                            count_dict['mtd_count'] = 0
                for y_c in year_count_dict:
                    if count_dict['ques_name'] == y_c['name']:
                        count_dict.update({'year_count': y_c['count']})
                        if count_dict['year_count'] is None:
                            count_dict['year_count'] = 0
                count_list.append(count_dict)
        final_dict.update({'total_count': count_list})
        return final_dict

    @api.multi
    def get_gm_area_report(self, type):
        cr = self._cr
        final_dict = {}
        ba_ids_list = []
        employee_list_ids = []
        employee_list = []
        count_list = []
        group_name = self.get_user_designation(self.id)
        if self.employee_list:
            for user_id in self.employee_list:
                emp_dict = {}
                emp_dict.update({'id': user_id.id, 'u_name': user_id.name})
                employee_list_ids.append(user_id.id)
                employee_list.append(emp_dict)
                for usr_id in user_id.employee_list:
                    ba_ids_list.append(usr_id.id)
        if ba_ids_list:
            if type == 'year':
                yr_of_date = datetime.today().year
                date_strformat1 = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
                today_date = datetime.today()
                today_strformat = today_date.strftime('%Y-%m-%d')
                todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
                if todate_format > date_strformat1:
                    date_strformat = date_strformat1
                else:
                    last_yr = yr_of_date - 1
                    last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                    date_strformat = last_yr_date
            else:
                today_date = datetime.today()
                today_strformat = today_date.strftime('%Y-%m-%d')
                month_date = today_date.replace(day=1)
                date_strformat = month_date.strftime('%Y-%m-%d')
            cr.execute('''SELECT
                                    COALESCE(cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int),0) as count,
                                    cast(TRIM(INITCAP(regexp_replace(split_part(lower(ques.name),'how many',2) , '[?]+', '','g')))as character varying)  as name,
                                    ques.id as ques_id,
                                    users.manager_id as user_id
                                FROM customer_visit visit
                                    join visit_answers answer on visit.id=answer.visit_id
                                    join visit_questions ques on ques.id=answer.name
                                    join question_set_rel rel on rel.set_id=ques.id
                                    join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                                    join res_users users on users.id=visit.create_uid
                                where visit.create_uid in %s
                                AND visit.date >= %s
                                AND visit.date <= %s
                                group by ques.name,ques.id,users.manager_id order by users.manager_id''',
                       (tuple(ba_ids_list), date_strformat, today_strformat))
            result_dict = cr.dictfetchall()
            res_list = []
            for res in result_dict:
                if res['user_id'] not in res_list:
                    res_list.append(res['user_id'])
            list_diff = set(employee_list_ids) - set(res_list)
            if list_diff:
                cr.execute('''SELECT
                            0 as count,
                            cast(TRIM(INITCAP(regexp_replace(split_part(lower(ques.name),'how many',2) , '[?]+', '','g')))as character varying)  as name,
                            ques.id as ques_id,
                            users.id as user_id
                    FROM
                    visit_questions ques
                    join question_set_rel rel on rel.set_id=ques.id
                    join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                    join res_users users on users.id in %s
                    group by ques.name,ques.id,users.id order by users.id''', (tuple(list_diff),))
                manager_res_dict = cr.dictfetchall()
                result_dict.extend(manager_res_dict)
            user_list = []
            for user in employee_list:
                user_dict = {'user_id': '', 'detail': []}
                detail_list = []
                user_dict.update({'user_id': user['id'], 'user_name': user['u_name'], 'detail': detail_list})
                for details in result_dict:
                    final_list = []
                    if details['user_id'] == user['id']:
                        final_list.append(details)
                        detail_list.append(details)
                user_dict.update({'detail': detail_list})
                user_list.append(user_dict)
            final_dict.update({'result': user_list})
            month_count_dict = self.get_questions_month_count(self.id, group_name, 'month')
            year_count_dict = self.get_questions_year_count(self.id, group_name, 'year')
            question_set = self.env['visit.questions.set'].search([('name', '=', 'Econet Question')])
            for ques_obj in question_set.questions:
                a = re.search(r'\b(many)\b', ques_obj.name)
                end = a.end()
                final_string = ques_obj.name[end:-1]
                ques_name = final_string.title()
                count_dict = {'ques_name': '', 'mtd_count': 0, 'year_count': 0}
                count_dict.update({'ques_name': ques_name})
                for m_c in month_count_dict:
                    if count_dict['ques_name'] == m_c['name']:
                        count_dict.update({'mtd_count': m_c['count']})
                        if count_dict['mtd_count'] is None:
                            count_dict['mtd_count'] = 0
                for y_c in year_count_dict:
                    if count_dict['ques_name'] == y_c['name']:
                        count_dict.update({'year_count': y_c['count']})
                        if count_dict['year_count'] is None:
                            count_dict['year_count'] = 0
                count_list.append(count_dict)
        final_dict.update({'total_count': count_list})
        return final_dict

    @api.multi
    def get_regional_report(self, type):
        cr = self._cr
        count_list = []
        if type == 'year':
            yr_of_date = datetime.today().year
            date_strformat1 = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
            today_date = datetime.today()
            today_strformat = today_date.strftime('%Y-%m-%d')
            todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
            if todate_format > date_strformat1:
                date_strformat = date_strformat1
            else:
                last_yr = yr_of_date - 1
                last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                date_strformat = last_yr_date
        else:
            today_date = datetime.today()
            today_strformat = today_date.strftime('%Y-%m-%d')
            month_date = today_date.replace(day=1)
            date_strformat = month_date.strftime('%Y-%m-%d')
        cr.execute('''SELECT
                                COALESCE(cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int),0) as count,
                                cast(TRIM(INITCAP(regexp_replace(split_part(lower(ques.name),'how many',2) , '[?]+', '','g')))as character varying)  as name,
                                ques.id as ques_id,
                                INITCAP(regexp_replace(users.region_id , '[_]+', ' ','g')) as region
                            FROM customer_visit visit
                                join visit_answers answer on visit.id=answer.visit_id
                                join visit_questions ques on ques.id=answer.name
                                join question_set_rel rel on rel.set_id=ques.id
                                join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                                join res_users users on users.id=visit.create_uid
                            where visit.create_uid in (select id from res_users where region_id not in('marketing','parrot','matabeleland','hq'))
                            AND visit.date >= %s
                            AND visit.date <= %s
                            group by ques.name,ques.id,users.region_id order by ques.id''',
                   (date_strformat, today_strformat))
        result_dict = cr.dictfetchall()
        region_list = []
        res_region_list = []
        final_dict = {}
        cr.execute(
            '''select distinct INITCAP(regexp_replace(region_id , '[_]+', ' ','g')) from res_users where region_id not in('marketing','parrot','matabeleland','hq') ''')
        total_regions = cr.fetchall()
        for details1 in result_dict:
            if details1['region'] not in res_region_list:
                res_region_list.append(details1['region'])
        no_reg_res = []
        for reg in total_regions:
            if reg[0] not in res_region_list:
                reg_name = str(reg[0]).lower()
                no_reg_res.append(reg_name.replace(" ", "_"))
        if len(no_reg_res) > 0:
            cr.execute(''' SELECT
                                0 as count,
                                cast(TRIM(INITCAP(regexp_replace(split_part(lower(ques.name),'how many',2) , '[?]+', '','g')))as character varying)  as name,
                                ques.id as ques_id,
                                INITCAP(regexp_replace(users.region_id , '[_]+', ' ','g')) as region
                            FROM 
                                visit_questions ques 
                                join question_set_rel rel on rel.set_id=ques.id
                                join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                                ,res_users users
                            where users.id in (select id from res_users where region_id in %s)
                            group by ques.name,ques.id,users.region_id order by ques.id''', (tuple(no_reg_res),))
            res_dict = cr.dictfetchall()
            result_dict.extend(res_dict)
        for region in total_regions:
            region_dict = {'region': '', 'detail': []}
            detail_list = []
            if region[0] == 'Mwc':
                region_name = 'MWC'
            else:
                region_name = region[0]
            region_dict.update({'region': region_name, 'detail': detail_list})
            for details in result_dict:
                if details['region'] == region[0]:
                    detail_list.append(details)
            region_dict.update({'detail': detail_list})
            region_list.append(region_dict)
        final_dict.update({'result': region_list})
        month_count_dict = self.get_regional_counts('month')
        year_count_dict = self.get_regional_counts('year')
        question_set = self.env['visit.questions.set'].search([('name', '=', 'Econet Question')])
        for ques_obj in question_set.questions:
            a = re.search(r'\b(many)\b', ques_obj.name)
            end = a.end()
            final_string = ques_obj.name[end:-1]
            ques_name = final_string.title()
            count_dict = {'ques_name': '', 'mtd_count': 0, 'year_count': 0}
            count_dict.update({'ques_name': ques_name.strip()})
            for m_c in month_count_dict:
                if count_dict['ques_name'] == m_c['name']:
                    count_dict.update({'mtd_count': m_c['count']})
            for y_c in year_count_dict:
                if count_dict['ques_name'] == y_c['name']:
                    if count_dict is not None:
                        count_dict.update({'year_count': y_c['count']})
            count_list.append(count_dict)
        final_dict.update({'total_count': count_list})
        return final_dict

    @api.multi
    def get_regional_counts(self, type):
        cr = self._cr
        if type == 'year':
            yr_of_date = datetime.today().year
            date_strformat1 = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
            today_date = datetime.today()
            today_strformat = today_date.strftime('%Y-%m-%d')
            todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
            if todate_format > date_strformat1:
                date_strformat = date_strformat1
            else:
                last_yr = yr_of_date - 1
                last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                date_strformat = last_yr_date
        else:
            today_date = datetime.today()
            today_strformat = today_date.strftime('%Y-%m-%d')
            month_date = today_date.replace(day=1)
            date_strformat = month_date.strftime('%Y-%m-%d')
        cr.execute('''SELECT
                                COALESCE(cast(sum(cast(regexp_replace(answer.choosen_answer,'[^0-9]+','0')as int))as int),0) as count,
                                TRIM(INITCAP(regexp_replace(split_part(lower(ques.name),'how many',2) , '[?]+', '','g'))) as name
                            FROM customer_visit visit
                                join visit_answers answer on visit.id=answer.visit_id
                                join visit_questions ques on ques.id=answer.name
                                join question_set_rel rel on rel.set_id=ques.id
                                join visit_questions_set ques_set on rel.question_id=ques_set.id and ques_set.name='Econet Question'
                                join res_users users on users.id=visit.create_uid
                            where visit.create_uid in (select id from res_users  where region_id not in('marketing','parrot','matabeleland','hq'))
                            AND visit.date >= %s
                            AND visit.date <=  %s
                            group by ques.name
                            ''', (date_strformat, today_strformat,))
        month_result_dict = cr.dictfetchall()
        if len(month_result_dict) <= 0:
            month_result_dict = []
            question_set = self.env['visit.questions.set'].search([('name', '=', 'Econet Question')])
            for ques_obj in question_set.questions:
                a = re.search(r'\b(many)\b', ques_obj.name)
                end = a.end()
                final_string = ques_obj.name[end:-1]
                ques_name = final_string.title()
                result_dict_new = {'name': ques_name, 'count': 0}
                month_result_dict.append(result_dict_new)
        return month_result_dict
