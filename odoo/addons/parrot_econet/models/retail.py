import ast
import base64
import logging
import os
import pytz
import requests

import datetime
import xlsxwriter
from datetime import datetime, timedelta
from odoo import models, fields, api
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.translate import _

_logger = logging.getLogger(__name__)


class CustomerVisit(models.Model):
    _inherit = 'customer.visit'

    @api.one
    def _get_pos_user_region(self):
        self.write({'user_regions': self.create_uid.region_id})

    user_region = fields.Char(string='Compute Region', compute='_get_pos_user_region')

    @api.multi
    def _get_selection_list(self):
        selection_list = self.env['res.users'].get_region_selection()
        return selection_list

    user_regions = fields.Selection(_get_selection_list, string='Region')


class PosVisitReport(models.Model):
    _inherit = "pos.visit.report"

    @api.model
    def _default_start_date(self):
        uid_sr = self.env['res.users'].search([('id', '=', self._uid)])
        if not uid_sr.has_group('parrot_econet.group_sa'):
            today = datetime.today()
            three_days_before = today + timedelta(days=-3)
            date_strformat = three_days_before.strftime('%Y-%m-%d')
            date_strp = datetime.strptime(date_strformat, '%Y-%m-%d')
            return date_strp

    @api.model
    def _default_end_date(self):
        uid_sr = self.env['res.users'].search([('id', '=', self._uid)])
        if not uid_sr.has_group('parrot_econet.group_sa'):
            today = datetime.today()
            yesterday = today + timedelta(days=-1)
            date_strformat = yesterday.strftime('%Y-%m-%d')
            date_strp = datetime.strptime(date_strformat, '%Y-%m-%d')
            return date_strp

    @api.one
    @api.constrains('start_date', 'end_date')
    def _check_start_date(self):
        uid_sr = self.env['res.users'].search([('id', '=', self._uid)])
        if not uid_sr.has_group('parrot_econet.group_sa'):
            if (fields.Date.from_string(self.end_date) - fields.Date.from_string(
                    self.start_date)).days > 2 or self.start_date > self.end_date:
                raise Warning(_('Please select date range within three days'))

    start_date = fields.Date(string='Start Date', default=_default_start_date)
    end_date = fields.Date(string="End Date", default=_default_end_date)
    is_sent_mail = fields.Boolean(string='Is Sent Mail', default=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('submitted', 'Submitted'),
                              ('inprogress', 'In Progress'),
                              ('completed', 'Completed')], 'State', default='draft')
    url = fields.Char(string='URL')

    @api.one
    def button_inprogress(self):
        return self.write({'state': 'inprogress'})

    @api.one
    def button_completed(self):
        return self.write({'state': 'completed'})

    @api.one
    def button_submit(self):
        return self.write({'state': 'submitted'})

    @api.one
    def button_draft(self):
        return self.write({'state': 'draft'})

    @api.multi
    def _get_selection_list(self):
        selection_list = self.env['res.users'].get_region_selection()
        return selection_list

    region = fields.Selection(_get_selection_list, string='Region')
    user_category = fields.Many2one('user.category', string='User Category')

    @api.onchange('region')
    def onchange_region(self):
        res = {}
        vis_list = []
        if self.region and self.start_date and self.end_date:
            visit_sr = self.env['customer.visit'].search(
                [('user_regions', '=', self.region), ('date', '>=', self.start_date), ('date', '<=', self.end_date)])
            for vis in visit_sr:
                vis_list.append(vis.id)
            res['domain'] = {'visit_ids': [('id', 'in', vis_list)]}
        else:
            visit_sr = self.env['customer.visit'].search([])
            for vis in visit_sr:
                vis_list.append(vis.id)
            res['domain'] = {'visit_ids': [('id', 'in', vis_list)]}
        return res

    @api.multi
    def send_pos_visit_report(self):
        activity_sr = self.search([('is_sent_mail', '=', True)])
        for activity in activity_sr:
            activity.write({'state': 'inprogress'})
            dict = {'is_sent_mail': False, 'state': 'completed'}
            res = self.get_pos_visit_report(activity.id, activity.start_date, activity.end_date,
                                            activity.user_category.id)
            _logger.info("POS Visit Report - Send Mail Triggered")
            # if 'ir_id' in res:
            #     # res_user = self.env['res.users'].search([('login', '=', login)])
            #     mail = self.env['mail.mail'].create({'body_html': '<div><p>Hi Sir/Madam,</p>'
            #                                                       '<br/><p>Please download the User Activity Report.</p></div>',
            #                                          'subject': 'User Activity',
            #                                          'email_to': 'r.hema90@gmail.com',
            #                                          'attachment_ids': [(4, res['ir_id'])],
            #                                          'auto_delete': False})
            #     mail_br = self.env['mail.mail'].browse(mail.id)
            #     mail_br.send(auto_commit=False)
            if 'url' in res:
                dict.update({'url': res['url']})
            activity.write(dict)
        return True

    @api.multi
    def get_pos_visit_request_status(self, user_id, offset):
        result_list = []
        res = {}
        limit = 10
        cr = self._cr
        user = self.env['res.users'].search([('id', '=', user_id)])
        if user.login == 'superadmin' or user.login == 'admin':
            activity_sr = self.search([], offset=offset, limit=limit, order="id desc")
            cr.execute('''select count(id) from pos_visit_report''')
            total_count = cr.fetchone()[0]
        else:
            activity_sr = self.search([('create_uid', '=', user_id)], offset=offset, limit=limit, order="id desc")
            cr.execute('''select count(id) from pos_visit_report where create_uid=%s''', (user_id,))
            total_count = cr.fetchone()[0]
        for activity in activity_sr:
            if activity.state:
                status = activity.state.title()
            else:
                status = ''
            result = {'status': status, 'url': activity.url or '', 'requested_by': activity.create_uid.name or '',
                      'start_date': activity.start_date or '', 'end_date': activity.end_date or ''}
            result_list.append(result)
        res.update({'res': result_list, 'total_count': total_count})
        return res

    @api.multi
    def download_pos_visit_report(self, ):
        start_date = self.start_date
        end_date = self.end_date
        user_category = self.user_category.id
        res = self.get_pos_visit_report(self.id, start_date, end_date, user_category)
        if 'ir_id' in res:
            self.write({'visit_report_download_xl': [(6, 0, [res['ir_id']])]})

    @api.multi
    def get_user_category(self, ):
        usr_category_ids = self.env['user.category'].search([])
        result = []
        for category in usr_category_ids:
            vals = {'id': '', 'name': ''}
            vals.update({'id': category.id, 'name': category.name})
            result.append(vals)
        res = {'result': result}
        return res

    @api.multi
    def mail_pos_visit_report(self, values):
        user_category = ''
        if 'user_category' in values:
            user_category = values['user_category']
        if 'start_date' in values:
            start_date = values['start_date']
        if 'end_date' in values:
            end_date = values['end_date']
        vals = {}
        result = {'result': 'success'}
        vals.update({'start_date': start_date, 'end_date': end_date, 'is_sent_mail': True, 'state': 'submitted',
                     'user_category': user_category})
        self.create(vals)
        return result

    def get_pos_visit_report(self, pos_visit_id, start_date, end_date, user_category):
        cr = self._cr
        _logger.info("Download Pos Visit Triggered")
        attachment_obj = self.env['ir.attachment']
        company_obj = self.env['res.users'].search([('id', '=', self._uid)]).company_id
        temp = company_obj.temp_files_path
        if not temp:
            company_write = self.env['res_company'].write(self._uid, [company_obj.id],
                                                          {"temp_files_path": "/home/parrot/"})
            temp = "/home/parrot/"
        pos_label = "Ad Location"

        if not os.path.exists(temp + 'Samples'):
            os.makedirs(temp + 'Samples')
        temp = temp + 'Samples/'
        cr.execute(
            '''SELECT Distinct region_id from res_users where region_id not in ('parrot','marketing','undefined','matabeleland','hq')''')
        region_res = cr.fetchall()
        default_masters = ['BA Name', 'Address', 'Ward', 'POS Created Date', 'Mobile', 'Account Manager', 'Region',
                           'POS Latitude',
                           'POS Longitude', 'Visit Date', 'Image name', 'Image URL', 'Uploaded Date',
                           'Start Time(First POS Entry)', 'End Time(Last POS Exit)', 'Distance travelled']
        sample_xls = xlsxwriter.Workbook(temp + 'pos_visit_report' + str(pos_visit_id) + '.xls')
        border_format = sample_xls.add_format({'border': 2,
                                               'align': 'left',
                                               'bold': True,
                                               'bg_color': '#d9ead3'})
        row_format = sample_xls.add_format({'align': 'left'})
        row_border = sample_xls.add_format({'border': 1})
        for region in region_res:
            region_name = region[0].replace("_", " ")
            sheet = sample_xls.add_worksheet(region_name.title())
            row = 0
            col = 0
            default_masters1 = []
            ques_br = self.env['visit.questions.set'].search([])
            q_list = []
            for q in ques_br:
                q_list.append(q.name)
            for q_set in ques_br:
                if q_set.name in q_list:
                    for ques in q_set.questions:
                        default_masters1.append(ques.name)
                        if ques.child_question:
                            default_masters1.append(ques.child_question.name)
            sheet_head = default_masters + default_masters1
            ques_list = []
            for master in sheet_head:
                ques_dict = {}
                sheet.set_row(0, 50)
                sheet.write(row, col, master, border_format)
                col += 1
                ques_dict.update({'q_name': master, 'excel_col': col})
                ques_list.append(ques_dict)
            row += 1
            cr.execute('''select visit.id, usr.user_category, category.name from customer_visit visit 
                            join res_users usr on visit.create_uid = usr.id
                            join user_category category on usr.user_category=category.id
                            where visit.date::date>=%s and visit.date::date<=%s and usr.user_category = %s''',
                       (start_date, end_date, user_category))
            result_dict = cr.dictfetchall()
            res_id_list = []
            for result in result_dict:
                res_id_list.append(result['id'])
            visit_id_br = self.env['customer.visit'].search([('id', 'in', res_id_list)])
            for visit in visit_id_br:
                col = 0
                if region[0] == visit.user_regions:
                    if visit.name.questions_set.name in q_list:
                        sheet.write(row, col, visit.create_uid.name or ' ', row_format)
                        col += 1
                        sheet.write(row, col, visit.name.address or ' ', row_format)
                        col += 1
                        sheet.write(row, col, visit.name.pos_id or ' ', row_format)
                        col += 1
                        sheet.write(row, col, visit.name.create_date or ' ', row_format)
                        col += 1
                        sheet.write(row, col, visit.name.mobile or ' ', row_format)
                        col += 1
                        sheet.write(row, col, visit.create_uid.manager_id.name or ' ', row_format)
                        col += 1
                        str_replace = visit.user_regions.replace('_', ' ')
                        sheet.write(row, col, str_replace.title() or ' ', row_format)
                        col += 1
                        sheet.write(row, col, visit.name.latitude or 0, row_format)
                        col += 1
                        sheet.write(row, col, visit.name.longitude or 0, row_format)
                        col += 1
                        sheet.write(row, col, visit.date or ' ', row_format)
                        col += 1
                    att_ids = attachment_obj.search(
                        [('res_id', '=', visit.id), ('res_model', '=', 'customer.visit')])
                    row_temp = row
                    col_temp = col
                    count = 0
                    for att in att_ids:
                        if att:
                            count += 1
                            if visit.name.questions_set.name in q_list:
                                sheet.write(row_temp, col_temp, att.name or ' ', row_format)
                                col_temp += 1
                            img_name = str(visit.create_uid.id) + '.jpg'
                            file_path = os.path.join(temp, img_name)
                            fh = open(file_path, "w+")
                            if att.datas:
                                fh.write(att.datas.decode('base64'))
                                url = 'https://api.parrot.solutions/parrot/nlp'
                                files = {'file': open(file_path, 'rb')}
                                headers = {'clientId': 'c1a981fa-5728-4394-b5d0-e3ef42c5a7af'}
                                response = requests.post(url, headers=headers, files=files)
                                bucket_path = ast.literal_eval(response.text)['answer']['url']
                                if visit.name.questions_set.name == q_list:
                                    sheet.write(row_temp, col_temp, bucket_path or ' ', row_format)
                                    col_temp += 1
                            timezone = pytz.timezone(self._context.get('tz') or 'UTC')
                            create_date = pytz.UTC.localize(
                                datetime.strptime(att.create_date, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(
                                timezone).strftime("%Y-%m-%d %H:%M:%S")
                            if visit.name.questions_set.name in q_list:
                                sheet.write(row_temp, col_temp, create_date or ' ', row_format)
                                col_temp += 1
                        row_temp += 1
                        col_temp -= 3
                    row_temp -= 1
                    col_temp += 5
                    col = col_temp
                    if visit.name.questions_set.name in q_list:
                        sheet.write(row, col, visit.create_uid.retail_distance or 0, row_format)
                    for vals in ques_list:
                        for answer in visit.answers:
                            if vals['q_name'] == answer.name.name:
                                col = vals['excel_col']
                                if visit.name.questions_set.name in q_list:
                                    if answer.choosen_answer and answer.choosen_answer.find("{u") >= 0:
                                        num_dict = ast.literal_eval(answer.choosen_answer)
                                        num_list = []
                                        for nums in num_dict:
                                            num_list.append(num_dict[nums])
                                        sheet.write(row, col - 1, str(num_list) or 0, row_format)
                                    else:
                                        sheet.write(row, col - 1, answer.choosen_answer or 0, row_format)
                    if len(att_ids) > 1:
                        temp_row = len(att_ids)
                        row += temp_row
                    else:
                        row += 1
                    sheet.conditional_format(0, 0, row + 1, col, {'type': 'cell',
                                                                  'criteria': '=',
                                                                  'value': 'None',
                                                                  'format': row_border})
                    sheet.conditional_format(0, 0, row + 1, col, {'type': 'cell',
                                                                  'criteria': '!=',
                                                                  'value': 'None',
                                                                  'format': row_border})
        sample_xls.close()
        file_name = 'pos_visit_report' + str(pos_visit_id) + '.xls'
        with open(temp + file_name, 'rb') as doc:
            encoded_data = base64.b64encode(doc.read())
            document_vals = {'name': 'pos_visit_report_file' + str(pos_visit_id) + '.xls',
                             'datas': encoded_data,
                             'datas_fname': 'pos_visit_report_file' + str(pos_visit_id) + '.xls',
                             'res_model': 'pos.visit.report',
                             'res_id': self.id,
                             'type': 'binary'}
        ir_id = self.env['ir.attachment'].create(document_vals)
        url = 'https://api.parrot.solutions/parrot/nlp'
        files = {'file': open(temp + file_name, 'rb')}
        headers = {'clientId': 'c1a981fa-5728-4394-b5d0-e3ef42c5a7af'}
        response = requests.post(url, headers=headers, files=files)
        bucket_path = ast.literal_eval(response.text)['answer']['url']
        result = {'result': 'success', 'url': bucket_path}
        if ir_id:
            result.update({'ir_id': ir_id.id})
        return result


class RetailCustomer(models.Model):
    _inherit = 'retail.customer'

    region = fields.Selection(related='assigned_to.region_id', string='User Region', store=True, readonly=True)
