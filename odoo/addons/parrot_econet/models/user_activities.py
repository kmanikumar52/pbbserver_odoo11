import base64
import datetime
import logging
from datetime import timedelta, date

import xlsxwriter
from odoo import api, fields, models, tools
from datetime import datetime, date, time, timedelta
import os


_logger = logging.getLogger(__name__)


class UserActivities(models.Model):
    _inherit = 'user.activities'

    @api.one
    def _get_default_user_region(self):
        self.write({'user_regions': self.name.region_id})

    user_region = fields.Char(string='Compute Region', compute='_get_default_user_region')
    user_regions = fields.Selection([('manicaland', 'Manicaland'),
                                     ('bulawayo', 'Bulawayo'),
                                     ('harare_north', 'HARARE NORTH'),
                                     ('harare_south', 'HARARE SOUTH'),
                                     ('hq', 'HQ'),
                                     ('mashonaland_east', 'Mashonaland East & Chitungwiza'),
                                     ('masvingo', 'Masvingo'),
                                     ('matabeleland', 'Matabeleland'),
                                     ('midlands', 'Midlands'),
                                     ('mwc', 'MWC'),
                                     ('marketing', 'Marketing'),
                                     ('parrot', 'Parrot'),
                                     ('demo_region1', 'Demo Region1'),
                                     ('demo_region2', 'Demo Region2'),
                                     ('east', 'East'),
                                     ('north1', 'North 1'),
                                     ('north2', 'North 2'),
                                     ('south', 'South'),
                                     ('west', 'West'),
                                     ('e_comm', 'E-comm'),
                                     ('modern_trade', 'Modern Trade'),
                                     ('b2b', 'B2B'),], string='Region')

    @api.multi
    def download_user_activity_cron(self, login):
        user_activities_list=[]
        _logger.info("User Activity")
        yesterday_start = (datetime.now()-timedelta(days=1)).replace(hour=00, minute=00, second=0, microsecond=0).strftime(
            tools.DEFAULT_SERVER_DATETIME_FORMAT)
        yesterday_end = (datetime.now()- timedelta(days=1)).replace(hour=23, minute=59, second=59, microsecond=59).strftime(
        tools.DEFAULT_SERVER_DATETIME_FORMAT)
        # prev_monday = (datetime.datetime.now() - datetime.timedelta(days=datetime.datetime.now().weekday(),
        #                                                             weeks=1)).strftime('%Y-%m-%d 00:00:00')
        # this_monday = (datetime.datetime.now() - datetime.timedelta(days=datetime.datetime.now().weekday(),
        #                                                             weeks=0)).strftime('%Y-%m-%d 00:00:00')
        today_start = datetime.now().replace(hour=1, minute=00, second=0, microsecond=0).strftime(
            tools.DEFAULT_SERVER_DATETIME_FORMAT)
        today_end = datetime.now().replace(hour=2, minute=00, second=0, microsecond=0).strftime(
            tools.DEFAULT_SERVER_DATETIME_FORMAT)
        nowstrf = datetime.now().strftime(tools.DEFAULT_SERVER_DATETIME_FORMAT)
        nowstrf_time = datetime.now().strftime('%Y-%m-%d')
        # this_monday_date = (datetime.now() - datetime.timedelta(days=datetime.datetime.now().weekday(),
        #                                                                  weeks=0)).strftime('%Y-%m-%d')
        # For generating report. Checking This week monday and timing from start to end.
        # if today_start < nowstrf < today_end and nowstrf_time == this_monday_date:
        if today_start < nowstrf < today_end:
            # _logger.info("User Activity - User Activity of last week data from '%s' to '%s'", prev_monday, this_monday)
            user_activities = self.env['user.activities'].search([('date', '>=', yesterday_start),
                                                                  ('date', '<=', yesterday_end)])
            count = 0
            user_activities_list = []
            for user_activity in user_activities:
                group_name = ""
                search_obj = self.env["res.users"].search([('id', '=', user_activity.name.id)])
                for group in search_obj.groups_id:
                    search_group_obj = self.env["res.groups"].search([('id', '=', group.id)])
                    if search_group_obj.name:
                        if search_group_obj.name == "BA":
                            if group_name != "AM" and group_name != "RA" and group_name != "RM" and group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                                group_name = "BA"
                        elif search_group_obj.name == "Account Manager":
                            if group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                                group_name = "AM"
                        elif search_group_obj.name == "Regional Admin":
                            group_name = "RA"
                        elif search_group_obj.name == "Regional Manager":
                            group_name = "RM"
                        elif search_group_obj.name == "General Manager":
                            if group_name != "CSO" and group_name != "CCO":
                                group_name = "GM"
                        elif search_group_obj.name == "CSO":
                            if group_name != "CCO":
                                group_name = "CSO"
                        elif search_group_obj.name == "CCO":
                            group_name = "CCO"
                if group_name == "BA":
                    user_activities_list.append(user_activity)
                    count = count + 1
            ir_id = self.report_user_activity(user_activities_list, count)
            self.write({'econet_user_activity.xls': [(6, 0, [ir_id])]})
            self.mail_user_activity(ir_id, login)

    def report_user_activity(self, user_activity_obj, count):
        _logger.info("User Activity - Report Generated, Count = %s" % count)
        user_dates = []
        user_activity_list = []
        for user_activity in user_activity_obj:
            user_activity_dict = {}
            user_dates.append(user_activity.date)
            user_activity_dict.update({'name': user_activity.name.name,
                                       'activity_date': user_activity.date,
                                       'category_activity': user_activity.category.name,
                                       'region': user_activity.user_region,
                                       'regions': user_activity.user_regions,
                                       'current_activity': user_activity.current_activity,
                                       'time_spent': user_activity.time_spent,
                                       'from_url': user_activity.from_url,
                                       'to_url': user_activity.to_url})
            user_activity_list.append(user_activity_dict)
        company_obj = self.env['res.users'].search([('id', '=', self._uid)]).company_id
        temp = company_obj.temp_files_path
        if not temp:
            company_write = self.env['res.company'].write(self._uid, [company_obj.id],
                                                          {"temp_files_path": "/home/parrot/"})
            temp = "/home/parrot/"

        if not os.path.exists(temp + 'Samples'):
            os.makedirs(temp + 'Samples')
        temp = temp + 'Samples/'
    # temp = temp+"Samples/"
        sample_xls = xlsxwriter.Workbook(temp + 'econet_user_activity.xls')
        border_format = sample_xls.add_format({'border': 2, 'bold': True, 'bg_color': '#90af48'})
        cr = self._cr
        # cr.execute(
        #     '''SELECT Distinct region_id from res_users where region_id not in ('parrot','marketing','undefined','matabeleland','hq')''')
        region_res = cr.fetchall()
        for region in region_res:
            region_name = region[0].replace("_", " ")
            sheet = sample_xls.add_worksheet(region_name.title())
            row = 0
            col = 0
            sheet.write(row, col, 'name', border_format)
            sheet.write(row, col + 1, 'category', border_format)
            sheet.write(row, col + 2, 'Current Activity', border_format)
            sheet.write(row, col + 3, 'Time Spent', border_format)
            sheet.write(row, col + 4, 'Date', border_format)
            sheet.write(row, col + 5, 'Region', border_format)
            row += 1
            for detail in user_activity_list:
                if region[0] == detail['regions']:
                    sheet.write(row, col, detail['name'])
                    sheet.write(row, col + 1, detail['category_activity'])
                    sheet.write(row, col + 2, detail['current_activity'])
                    sheet.write(row, col + 3, detail['time_spent'])
                    sheet.write(row, col + 4, detail['activity_date'])
                    sheet.write(row, col + 5, region_name.title())
                    row += 1
        sample_xls.close()
        # files = {'file': open(file_path, 'rb')}
        with open(temp + 'econet_user_activity.xls', 'rb') as doc:
            encoded_data = base64.b64encode(doc.read())
        document_vals = {'name': 'econet_user_activity_details.xls',
                         'datas': encoded_data,
                         'datas_fname': 'econet_user_activity_details.xls',
                         'res_model': 'user.activity',
                         'type': 'binary'}
        ir_id = self.env['ir.attachment'].create(document_vals)
        return ir_id

    def mail_user_activity(self, attachment_id, login):
        _logger.info("User Activity - Mail Triggered")
        res_user = self.env['res.users'].search([('login', '=', login)])
        mail = self.env['mail.mail'].create({'body_html': '<div><p>Hi Sir/Madam,</p>'
                                                          '<br/><p>Please download the User Activity.</p></div>',
                                             'subject': 'User Activity',
                                             'email_to': res_user.email,
                                             'attachment_ids': [(4, attachment_id.id)],
                                             'auto_delete': False})
        mail_br = self.env['mail.mail'].browse(mail.id)
        _logger.info("User Activity - Mail created successfully to login '%s'" % res_user.name)
        return mail_br.send(auto_commit=False)
