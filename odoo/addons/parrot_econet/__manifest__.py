# -*- coding: utf-8 -*-
{
    'name': "Parrot Econet",
    'summary': """Econet - Customized Module.""",
    'description': """
        Econet:

        * Added some extra customized functions.

        * Groups and Access Rights.
    """,
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': [
        'product_information_management',
        'retail_group',
        'parrot_project',
        'parrot_survey',
        'parrot_last_seen',
        'retail_geo_track',
    ],
    'data': [
        'security/parrot_econet_security.xml',
        'security/ir.model.access.csv',
        # 'data/parrot_econet_cron.xml',
        'views/app_menu_view.xml',
        'views/user_activities_view.xml',
        'views/parrot_econet_view.xml',
        'views/retail_view.xml',
    ],
}
