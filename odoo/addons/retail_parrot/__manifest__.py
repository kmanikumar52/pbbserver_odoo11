# -*- coding: utf-8 -*-
{
    'name': 'Parrot Retail',
        'category': 'retail',
        'version': '1.0',
        'sequence': 3,
        'author': "Parrot Solutions",
        'website': "parrot.solutions",
        'summary': """Retail Module for Parrot""",
        'description': """Retail Module for Parrot Solutions""",
        'depends': [
            'parrot_notification',
            'parrot_base',
        ],
        'data': [
            'security/retail_security.xml',
            'security/ir.model.access.csv',
            'views/retail_customer_view.xml',
            'views/retail_team_view.xml',
            'views/retail_config_view.xml',
            'views/order_form_view.xml',
#             'data/retail_parrot_cron.xml',
#             'data/app_menu_data.xml',
#             'order_form_sequence.xml'
        ],
        'application': True,
    }