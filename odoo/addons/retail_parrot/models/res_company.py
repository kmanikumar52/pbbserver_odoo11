from odoo import fields, models,api,_

class ResCompany(models.Model):
    _inherit = 'res.company'

    retail_questions_set = fields.Many2one('visit.questions.set', 'Default Questions Set')
    # 'pos_label':fields.char('Label/Title for POS Name in Excel report')


    # _defaults = {
    #   'pos_label':'POS Name'
    # }
