import datetime
import logging
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api
from odoo.tools.translate import _
import json
from odoo.osv import osv
import requests

_logger = logging.getLogger(__name__)


class RetailOrderForm(models.Model):
    _name = "retail.order.form"
    _order = "create_date desc"

    order_number = fields.Char(string="Order Number")
    sales_executive = fields.Many2one('res.users', string="Sales Executive")
    pos_id = fields.Many2one('retail.customer', string="Pos Name")
    order_date = fields.Date(string="Order Date")
    dispached_by = fields.Date(string="Dispached By")
    order_details = fields.One2many('retail.order.details', 'retail_order_detail_id', string='Order Details', copy=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('submitted', 'Submitted'),
                              ('approved', 'Approved'),
                              ('rejected', 'Rejected')], 'State', default='draft')
    description = fields.Char(string="Description")

    @api.multi
    def update_status(self, order_id, detail):
        if 'status' in detail and detail['status']:
            status = detail['status'].lower()
        else:
            status = 'submitted'
        if 'status' in detail and detail['status']:
            description = detail['description']
        else:
            description = ''
        order_obj = self.search([('id', '=', order_id)])
        order_obj.write({'state': status, 'description': description})
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        user = self.env['res.users'].search([('id', '=', order_obj.sales_executive.id)])
        if user.user_tokens:
            for token in self.env['user.token'].browse(user.user_tokens.ids):
                tokens.append(token.name)
        data_json = {"channeltype": "UpdateOrderFormStatus",
                     "name": order_obj.order_number,
                     "message": "%s has been %s by %s" % (
                         order_obj.order_number, order_obj.state, user.manager_id.name),
                     "order_id": order_obj.id,
                     "order_date": order_obj.order_date}
        data = {'notification': {'title': order_obj.order_number,
                                 'body': "%s has been %s by %s" % (
                                     order_obj.order_number, order_obj.state, user.manager_id.name),
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': self._uid,
                                                'model_name': 'employee.exception',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'write',
                                                'record_id': order_obj.id,
                                                'message': data_json["message"],
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            raise osv.except_osv(_('Notification not sent'))
        return True

    @api.one
    def button_approve(self):
        return self.write({'state': 'approved'})

    @api.one
    def button_reject(self):
        return self.write({'state': 'rejected'})

    @api.one
    def button_submit(self):
        return self.write({'state': 'submitted'})

    @api.one
    def button_draft(self):
        return self.write({'state': 'draft'})

    @api.multi
    def get_order_form_dropdown(self):
        cr = self._cr
        dropdown_dict = {}
        product_sr = self.env['retail.product'].search([])
        prod_list = []
        for prod in product_sr:
            product_dict = {'id': prod.id, 'name': str(prod.name)}
            prod_list.append(product_dict)
        dropdown_dict['prod_ids'] = prod_list
        user_sr = self.env['res.users'].search([('id', '=', self.id)])
        user_category_sr = self.env['user.category'].search([('id', '=', user_sr.user_category.id)])
        if user_sr.user_category.name:
            prod_classification = self.env['retail.product.classification'].search(
                [('name', '=', user_category_sr.name)])
            if prod_classification:
                # prod_category = self.env['retail.product.category'].search(
                #     [('product_classification', '=', prod_classification.id)])
                cr.execute('''select prod.id as prod_id, prod.name as prod_name from retail_product prod
                    join retail_product_classification clas on clas.id=prod.product_classification and clas.name=%s
                    group by prod.id order by prod.name''', (user_category_sr.name,))
            else:
                # prod_category = self.env['retail.product.category'].search([])
                cr.execute('''select prod.id as prod_id, prod.name as prod_name from retail_product prod
                    join retail_product_classification clas on clas.id=prod.product_classification
                    group by prod.id order by prod.name''')
            prod_category = cr.dictfetchall()
        else:
            # prod_category = self.env['retail.product.category'].search([])
            cr.execute('''select prod.id as prod_id, prod.name as prod_name from retail_product prod
                    join retail_product_classification clas on clas.id=prod.product_classification
                    group by prod.id order by prod.name''')
            prod_category = cr.dictfetchall()
        prod_category_list = []
        for prod_cat in prod_category:
            prod_category_dict = {'id': prod_cat['prod_id'], 'name': prod_cat['prod_name']}
            prod_category_list.append(prod_category_dict)
        dropdown_dict['prod_cate_ids'] = prod_category_list
        payment_terms = self.env['retail.payment.terms'].search([])
        payment_list = []
        for payment in payment_terms:
            payment_dict = {'id': payment.id, 'name': str(payment.name)}
            payment_list.append(payment_dict)
        dropdown_dict['payment_ids'] = payment_list
        return dropdown_dict

    @api.multi
    def get_item_dropdown(self, prod_id):
        dropdown_dict = {}
        category_sr = self.env['retail.product.category'].search([('product_id', '=', prod_id)])
        cat_list = []
        for cat in category_sr:
            cate_dict = {'id': cat.id, 'name': str(cat.name), 'category_id': cat.id, 'category_name': cat.name,
                         }
            cat_list.append(cate_dict)
        dropdown_dict['category_ids'] = cat_list
        return dropdown_dict

    @api.multi
    def get_product_model_dropdown(self, cate_id):
        dropdown_dict = {}
        cr = self._cr
        cr.execute('''select distinct name as sub_category,
                    product_category_id
            from retail_product_model where product_category_id=%s
            order by name''', (cate_id,))
        sub_category_sr = cr.dictfetchall()
        sub_category_list = []
        for sub in sub_category_sr:
            if sub['sub_category']:
                sub_category__dict = {'name': sub['sub_category'],
                                      'category_id': sub['product_category_id']}
                sub_category_list.append(sub_category__dict)
            dropdown_dict['sub_category_ids'] = sub_category_list
        product_sr = self.env['retail.product.model'].search([('product_category_id', '=', cate_id)])
        prod_list = []
        for prod in product_sr:
            if prod.model_number:
                model_number = prod.model_number
            else:
                model_number = prod.description
            product_dict = {'id': prod.id, 'name': model_number, 'price': prod.mrp,
                            'category_id': prod.product_category_id.id,
                            'category_name': prod.product_category_id.name,
                            'model_number': model_number}
            prod_list.append(product_dict)
        dropdown_dict['prod_ids'] = prod_list

        return dropdown_dict

    @api.multi
    def get_product_detail_dropdown(self, model_id):
        dropdown_dict = {}
        # product_sr = self.env['retail.product.model'].search([('id', '=', model_id)])
        cr = self._cr
        cr.execute('''select
        distinct name,id,description,mrp,model_number,
        product_category_id,product_id,model_name from retail_product_model

        where name ilike %s ''', (model_id,))
        product_sr = cr.dictfetchall()
        prod_list = []
        for prod in product_sr:
            product_dict = {'id': prod['id'], 'name': prod['description'], 'price': prod['mrp'],
                            'category_id': prod['product_category_id'],
                            'model_number': prod['model_number']}
            prod_list.append(product_dict)
        dropdown_dict['prod_ids'] = prod_list
        return dropdown_dict

    @api.multi
    def get_product_model_detail(self, model_id):
        dropdown_dict = {}
        product_sr = self.env['retail.product.model'].search([('id', '=', model_id)])
        prod_list = []
        for prod in product_sr:
            if prod.model_number:
                model_number = prod.model_number
            else:
                model_number = prod.description
            product_dict = {'id': prod.id, 'name': model_number, 'price': prod.mrp,
                            'category_id': prod.product_category_id.id,
                            'category_name': prod.product_category_id.name,
                            'model_number': prod.model_number or ''}
            prod_list.append(product_dict)
        dropdown_dict['prod_ids'] = prod_list
        return dropdown_dict

    @api.multi
    def view_past_orders(self, user_id):
        cr = self._cr
        result = {}
        order_list = []
        employee_list_ids = []
        search_user = self.env['res.users'].search([('id', '=', user_id)])
        for user_id in search_user.employee_list:
            employee_list_ids.append(user_id.id)
            for usr_id in user_id.employee_list:
                employee_list_ids.append(usr_id.id)
                for us_id in usr_id.employee_list:
                    employee_list_ids.append(us_id.id)
                    for u_id in us_id.employee_list:
                        employee_list_ids.append(u_id.id)

        employee_list_ids.append(search_user.id)
        query = '''SELECT orders.id,orders.pos_id,orders.order_number,pos.name as pos_name,partner.name as user_name,
                    orders.sales_executive,users.login,orders.order_date,orders.dispached_by,orders.description,orders.state
                    from retail_order_form orders
                    left outer join res_users users on users.id=orders.sales_executive
                    left outer join retail_customer pos on pos.id=orders.pos_id
                    left outer join res_partner partner on partner.id=users.partner_id
                    where sales_executive in %s'''
        cr.execute(query, (tuple(employee_list_ids),))
        view_orders = cr.dictfetchall()
        for order in view_orders:
            if order['state']:
                state = order['state'].title()
            else:
                state = ""
            order_dict = {}
            order_dict.update(
                {'id': order['id'], 'order_number': order['order_number'], 'name': order['pos_name'],
                 'user_id': order['sales_executive'], 'user_name': order['user_name'],
                 'order_date': order['order_date'], 'dispatched_by': order['dispached_by'],
                 'status': state, 'description': order['description'] or ''
                 })
            order_list.append(order_dict)
            result.update({'orders_list': order_list})
        return result

    @api.multi
    def view_past_orders_details(self, order_id):
        result = {}
        detail_list = []
        view_orders = self.env['retail.order.form'].search([('id', '=', order_id)])
        for order in view_orders:
            if order.state:
                state = order.state.title()
            else:
                state = ""
            for detail in order.order_details:
                detail_dict = {}
                detail_dict.update(
                    {'id': order.id, 'order_number': order.order_number, 'name': str(order.pos_id.name),
                     'order_date': order.order_date, 'dispatched_by': order.dispached_by,
                     'status': state,
                     'description': order.description or '',
                     'product_category_id': detail.product_category_id.id,
                     'product_category_name': detail.product_category_id.name,
                     'product_id': detail.product_id.id,
                     'product_name': detail.product_id.name,
                     'product_quantity': detail.product_quantity,
                     'payment_term_id': detail.payment_term_id.id,
                     'total': detail.price or 0,
                     'price': detail.product_model.mrp or 0,
                     'payment_term_name': detail.payment_term_id.name})
                detail_list.append(detail_dict)
        result.update({'orders_list': detail_list})
        return result

    @api.model
    def create(self, vals):
        vals['order_number'] = self.env['ir.sequence'].next_by_code('retail.order.form') or 'New'
        order_form_id = super(RetailOrderForm, self).create(vals)
        send = True
        tokens = []
        if 'sales_executive' in vals:
            user = self.env['res.users'].search([('id', '=', vals['sales_executive'])])
            fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
            if user.manager_id.user_tokens:
                for token in self.env['user.token'].browse(user.manager_id.user_tokens.ids):
                    tokens.append(token.name)
            data_json = {"channeltype": "CreateOrderForm",
                         "name": vals['order_number'],
                         "message": "%s has been created by %s" % (
                             vals['order_number'], user.name),
                         "order_form_id": order_form_id.id,
                         "order_date": vals['order_date'],
                         "dispached_by": vals['dispached_by']}
            data = {'notification': {'title': vals['order_number'],
                                     'body': "%s has been created by %s" % (
                                         vals['order_number'], user.name),
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            self.env['parrot.notification'].create({'name': data_json["name"],
                                                    'user_id': self._uid,
                                                    'model_name': 'employee.exception',
                                                    'channel_name': data_json["channeltype"],
                                                    'function_name': 'create',
                                                    'record_id': order_form_id.id,
                                                    'message': data_json["message"],
                                                    'notification_title': data['notification']["title"],
                                                    'notification_body': data['notification']["body"]})
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                raise osv.except_osv(_('Notification not sent'))

        return order_form_id

    @api.multi
    def mobile_create(self, values):
        values.update({'state': 'submitted'})
        retail_order_form_id = self.create(values)
        if 'product_details' in values:
            for detail in values['product_details']:
                if 'category_id' in detail:
                    product_category_id = detail['category_id']
                if 'product_category_id' in detail:
                    product_id = detail['product_category_id']
                if 'product_quantity' in detail:
                    product_quantity = detail['product_quantity']
                if 'product_total' in detail:
                    price = detail['product_total']
                if 'product_id' in detail:
                    sub_category_id = detail['product_id']
                else:
                    price = 0
                if retail_order_form_id and product_category_id and product_id and product_quantity:
                    detail_val = {
                        'retail_order_detail_id': retail_order_form_id.id, 'product_category_id'
                        : product_category_id, 'product_id': product_id,
                        'product_quantity': product_quantity,
                        'product_model': sub_category_id,
                        'price': price}
                    order_detail_id = self.env['retail.order.details'].create(detail_val)
        return True

    @api.multi
    def get_emp_list_query(self, group_name):
        if group_name == 'CCO':
            search_manager = self.env['res.users'].search(([('id', '=', self.id)]))
            if search_manager.manager_id:

                query = '''(
                        Select distinct id from res_users where manager_id in
                        (Select distinct id from res_users where manager_id in
                        (Select distinct id from res_users where manager_id in
                        (Select distinct id from res_users where manager_id=%s)))
                        and active='t'

                        )'''
            else:
                query = '''(
                        Select distinct id from res_users where manager_id in
                        (Select distinct id from res_users where manager_id in
                        (Select distinct id from res_users where manager_id in
                        (Select distinct id from res_users where manager_id in
                        (Select distinct id from res_users where manager_id=%s))))
                        and active='t'

                        )'''

        elif group_name == 'CSO':
            query = '''(
                                (Select distinct id from res_users where manager_id in
                                (Select distinct id from res_users where manager_id in
                                (Select distinct id from res_users where manager_id=%s))
                                and active='t'
                                )
                                 )'''
        elif group_name == 'GM':
            query = '''(Select distinct id from res_users where manager_id in
                                (Select distinct id from res_users where manager_id=%s)
                                and active='t')'''
        elif group_name == 'AM' or group_name == 'RM' or group_name == 'RA':
            query = '''(Select distinct id from res_users where manager_id=%s
                                and active='t')'''

        elif group_name == 'BA':
            query = '''(%s) '''
        else:
            query = '''(%s) '''
        return query

    @api.multi
    def get_usr_designation(self, user_id):
        group_name = ""
        user_obj = self.env['res.users'].search([('id', '=', self.id)])
        for group in user_obj.groups_id:
            search_group_obj = self.env["res.groups"].search([('id', '=', group.id)])
            if search_group_obj.name:
                if search_group_obj.name == "BA":
                    if group_name != "AM" and group_name != "RA" and group_name != "RM" and group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                        group_name = "BA"
                elif search_group_obj.name == "Account Manager":
                    if group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                        group_name = "AM"
                elif search_group_obj.name == "Regional Admin":
                    if group_name != "GM" and group_name != "CSO" and group_name != "CCO" and group_name != "AM":
                        group_name = "RA"
                elif search_group_obj.name == "Regional Manager":
                    if group_name != "GM" and group_name != "CSO" and group_name != "CCO" and group_name != "AM" and group_name != "RA":
                        group_name = "RM"
                elif search_group_obj.name == "General Manager":
                    if group_name != "CSO" and group_name != "CCO":
                        group_name = "GM"
                elif search_group_obj.name == "CSO":
                    if group_name != "CCO":
                        group_name = "CSO"
                elif search_group_obj.name == "CCO":
                    group_name = "CCO"
        return group_name

    @api.multi
    def get_hsil_questions_month_value(self, user_id, group_name, type):
        cr = self._cr
        emp_query = self.get_emp_list_query(group_name)
        if user_id:
            yesterday_date = datetime.today()
            today_strformat = yesterday_date.strftime('%Y-%m-%d')
            week_date = yesterday_date.replace(day=1)
            date_strformat = week_date.strftime('%Y-%m-%d')
            cr.execute('''DROP FUNCTION IF EXISTS get_hsil_questions_month_value()''')
            query = '''CREATE OR REPLACE FUNCTION get_hsil_questions_month_value()
                          RETURNS TABLE (

                           product_name character varying,
                           product_id int,
                           count float,
                           name character varying,
                           prod_model_name character varying,
                           prod_model_description text

                          ) AS
                        $func$
                        BEGIN
                           RETURN QUERY
                            select
                               Distinct prod.name as product_name,
                               prod.id as product_id,
                               sum(det.product_quantity) as count,
                                cate.name as name,
                                prod_model.name as prod_model_name,
                                prod_model.description as prod_model_description
                                from retail_order_form form
                                left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                left outer join retail_product prod on prod.id=det.product_id
                                 left outer join retail_product_category cate on cate.product_id=prod.id
                                 join retail_product_model prod_model on prod_model.product_id=prod.id
                                     and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                 where form.sales_executive in'''
            query += emp_query
            query += '''AND form.state='approved' AND form.order_date >= %s
                          AND form.order_date <=  %s
               group by prod.name,prod.id,cate.name,prod_model.name,prod_model.description order by prod.name;
               END
                        $func$ LANGUAGE plpgsql;
            '''
            cr.execute(query, (user_id, date_strformat, today_strformat))
            cr.execute(
                '''select product_name,sum(count) as count,name from get_hsil_questions_month_value() group by product_name,name''')
            result_dict = cr.dictfetchall()
            return result_dict

    @api.multi
    def get_hsil_questions_month_count(self, user_id, group_name, type):
        cr = self._cr
        emp_query = self.get_emp_list_query(group_name)
        if user_id:
            yesterday_date = datetime.today()
            today_strformat = yesterday_date.strftime('%Y-%m-%d')
            week_date = yesterday_date.replace(day=1)
            date_strformat = week_date.strftime('%Y-%m-%d')
            cr.execute('''DROP FUNCTION IF EXISTS get_hsil_questions_month_count()''')
            query = '''CREATE OR REPLACE FUNCTION get_hsil_questions_month_count()
                          RETURNS TABLE (

                           product_name character varying,
                           product_id int,
                           count float,
                           name character varying,
                           prod_model_name character varying,
                           prod_model_description text

                          ) AS
                        $func$
                        BEGIN
                           RETURN QUERY
                            select
                               Distinct prod.name as product_name,
                               prod.id as product_id,
                               sum(det.product_quantity*prod_model.mrp) as count,
                                cate.name as name,
                                prod_model.name as prod_model_name,
                                prod_model.description as prod_model_description
                                from retail_order_form form
                                left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                left outer join retail_product prod on prod.id=det.product_id
                                 left outer join retail_product_category cate on cate.product_id=prod.id
                                 join retail_product_model prod_model on prod_model.product_id=prod.id
                                     and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                 where form.sales_executive in'''
            query += emp_query
            query += '''AND form.state='approved' AND form.order_date >= %s
                          AND form.order_date <=  %s
               group by prod.name,prod.id,cate.name,prod_model.name,prod_model.description order by prod.name;
               END
                        $func$ LANGUAGE plpgsql;
            '''
            cr.execute(query, (user_id, date_strformat, today_strformat))
            cr.execute(
                '''select product_name,sum(count) as count,name from get_hsil_questions_month_count() group by product_name,name''')
            result_dict = cr.dictfetchall()
            return result_dict

    @api.multi
    def get_hsil_questions_year_value(self, user_id, group_name, type):
        cr = self._cr
        emp_query = self.get_emp_list_query(group_name)
        if user_id:
            if type == 'year':
                yr_of_date = datetime.today().year
                financial_year_start_date = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
                yesterday_date = datetime.today()
                today_strformat = yesterday_date.strftime('%Y-%m-%d')
                todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
                if todate_format > financial_year_start_date:
                    yr_start_date = financial_year_start_date
                else:
                    last_yr = yr_of_date - 1
                    last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                    yr_start_date = last_yr_date
                cr.execute('''DROP FUNCTION IF EXISTS get_hsil_questions_year_value()''')
                query = '''CREATE OR REPLACE FUNCTION get_hsil_questions_year_value()
                              RETURNS TABLE (

                               product_name character varying,
                               product_id int,
                               date text,
                               count float,
                               name character varying,
                               prod_model_name character varying,
                                prod_model_description text

                              ) AS
                            $func$
                            BEGIN
                               RETURN QUERY
                                select Distinct prod.name as product_name,
                                prod.id as product_id,
                                    'Year' as date,
                                   sum(det.product_quantity) as count,
                                    cate.name as name,
                                    prod_model.name as prod_model_name,
                                    prod_model.description as prod_model_description
                                    from retail_order_form form
                                    left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                    left outer join retail_product prod on prod.id=det.product_id
                                     left outer join retail_product_category cate on cate.product_id=prod.id
                                     join retail_product_model prod_model on prod_model.product_id=prod.id
                                     and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                     where form.sales_executive in'''
                query += emp_query
                query += '''AND form.state='approved' AND form.order_date >= %s
                              AND form.order_date <=  %s
                    group by prod.name,cate.name,prod.id,prod_model.name,prod_model.description order by prod.name;
                   END
                            $func$ LANGUAGE plpgsql;
                '''
                cr.execute(query, (user_id, yr_start_date, today_strformat))
                cr.execute(
                    '''select product_name,sum(count) as count,name from get_hsil_questions_year_value() group by product_name,name''')
                result_dict = cr.dictfetchall()
                return result_dict

    @api.multi
    def get_daily_report_common(self, type, user_id, from_function):
        cr = self._cr
        employee_list_ids = []
        user_obj = self.env['res.users'].search([('id', '=', user_id)])
        if user_obj:
            user_category_name = user_obj.user_category.name
        else:
            user_category_name = ''
        user_group_name = self.get_usr_designation(user_id)
        emp_query = self.get_emp_list_query(user_group_name)
        if len(employee_list_ids) <= 0:
            employee_list_ids.append(self.id)
        if employee_list_ids:
            if type == 'week':
                # yesterday_date = datetime.today() + timedelta(days=-1)
                yesterday_date = datetime.today()
                today_strformat = yesterday_date.strftime('%Y-%m-%d')
                week_date = datetime.today() + timedelta(days=-7)
                date_strformat = week_date.strftime('%Y-%m-%d')
        cr.execute('''DROP FUNCTION IF EXISTS get_daily_value()''')
        query = ''' CREATE OR REPLACE FUNCTION get_daily_value()
                                  RETURNS TABLE (
                                   sum float,
                                   prod_id int,
                                   product_name character varying,
                                   cat_id int,
                                   cate_name character varying,
                                   order_date date,
                                   prod_model_name character varying,
                                   prod_model_description text
                                  ) AS
                                $func$
                                BEGIN
                                   RETURN QUERY
                                   '''
        if from_function == 'volume':
            query += '''select sum(det.product_quantity) as sum,'''
        else:
            query += '''select sum(det.product_quantity*prod_model.mrp) as sum,'''
        query += '''prod.id as prod_id,
                                    prod.name as product_name,
                                    cate.id as cat_id,
                                    cate.name as cate_name,
                                    form.order_date as order_date,
                                    prod_model.name as prod_model_name,
                                    prod_model.description as prod_model_description
                                    from retail_order_form form
                                    left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                    left outer join retail_product prod on prod.id=det.product_id
                                     left outer join retail_product_category cate on cate.product_id=prod.id
                                     join retail_product_model prod_model on prod_model.product_id=prod.id
                                     and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                    join retail_product_classification clas on clas.id=prod.product_classification and clas.name=%s
                                     where form.sales_executive in '''
        query += emp_query
        query += '''AND form.state='approved' AND form.order_date >= %s
                              AND form.order_date <=  %s
                    group by prod.id,prod.name,form.order_date,cate.id,cate.name,prod_model.name,prod_model.description order by prod.name;
                    END
                                $func$ LANGUAGE plpgsql; '''

        cr.execute(query, (user_category_name, user_id, date_strformat, today_strformat))
        cr.execute('''select * from get_daily_value()''')
        result_dict = cr.dictfetchall()

        cr.execute('''select sum(sum),product_name,order_date,cat_id,cate_name
                from get_daily_value() group by product_name,order_date,cat_id,cate_name
                order by product_name''')
        result_dict1 = cr.dictfetchall()
        # cr.execute('''select DISTINCT prod.name as prod_name from retail_product prod
        #         join retail_product_category cate on cate.id=prod.product_category_id
        #         join retail_product_classification clas on clas.id=cate.product_classification and clas.name=%s
        #         group by prod.id order by prod.name''', (user_category_name,))
        cr.execute('''select prod.id as prod_id, prod.name as prod_name from retail_product prod
        join retail_product_classification clas on clas.id=prod.product_classification and clas.name=%s
        group by prod.id order by prod.name''', (user_category_name,))
        all_product_dict = cr.dictfetchall()
        all_product_temp_list = []
        i = 0
        cr.execute('''select sum(sum),
                            product_name,order_date
                            from get_daily_value() group by product_name,order_date''')
        result_dict_prod = cr.dictfetchall()
        for prod_res in all_product_dict:
            i += 1
            result_temp_dict1 = {}
            prod_date_list = []
            for i in range(-6, 1, 1):
                prod_date_dict = {}
                week_date = datetime.today() + timedelta(days=i)
                date_strformat = week_date.strftime('%Y-%m-%d')
                prod_date_dict.update(
                    {'date': date_strformat, 'count': 0, 'product_name': prod_res['prod_name']})
                prod_date_list.append(prod_date_dict)
            result_temp_dict1.update(
                {'product': prod_res['prod_name'],
                 'name': prod_res['prod_name'],
                 'date': prod_date_list,
                 'mtd_count': 0,
                 'lmtd_count': 0,
                 'year_count': 0})
            all_product_temp_list.append(result_temp_dict1)
        for prod_res_temp in all_product_temp_list:
            for prod_date_obj in prod_res_temp['date']:
                for res_obj in result_dict_prod:
                    if prod_date_obj['product_name'] == res_obj['product_name'] and prod_date_obj['date'] == \
                            res_obj['order_date']:
                        prod_date_obj.update({'count': res_obj['sum']})
        f_dict = {'final_product': all_product_temp_list}

        # cr.execute(''' select DISTINCT prod.name as prod_name,cate.name as category from retail_product prod
        #                 join retail_product_category cate on cate.id=prod.product_category_id
        #                 join retail_product_classification clas on clas.id=cate.product_classification and clas.name=%s
        #                 group by cate.name,prod.id order by prod.name''', (user_category_name,))
        cr.execute('''select DISTINCT prod.name as prod_name,cate.name as category from retail_product prod
                        join retail_product_category cate on cate.product_id=prod.id
                        join retail_product_classification clas on clas.id=prod.product_classification and clas.name=%s
                        group by cate.name,prod.id order by prod.name''', (user_category_name,))
        category_dict = cr.dictfetchall()
        result_temp_list1 = []
        i = 0
        for res in category_dict:
            i += 1
            result_temp_dict1 = {}
            date_list = []
            for i in range(-6, 1, 1):
                date_dict = {}
                week_date = datetime.today() + timedelta(days=i)
                date_strformat = week_date.strftime('%Y-%m-%d')
                date_dict.update(
                    {'date': date_strformat, 'count': 0, 'product_name': res['prod_name'],
                     'category_name': res['category']})
                date_list.append(date_dict)
            result_temp_dict1.update(
                {'product': res['prod_name'],
                 'category': res['category'],
                 'name': res['category'],
                 'date': date_list,
                 'mtd_count': 0,
                 'lmtd_count': 0,
                 'year_count': 0})
            result_temp_list1.append(result_temp_dict1)
        for res_temp in result_temp_list1:
            for date_obj in res_temp['date']:
                for res_obj in result_dict1:
                    if date_obj['product_name'] == res_obj['product_name'] and date_obj['category_name'] == \
                            res_obj[
                                'cate_name'] and date_obj['date'] == res_obj['order_date']:
                        date_obj.update({'count': res_obj['sum']})
        f_dict.update({'final': result_temp_list1})
        # cr.execute('''select  prod.id as prod_id, prod.name as prod_name,prod.model_number as prod_model,cate.id,cate.name as category
        #                         from retail_product prod
        #                         join retail_product_category cate on cate.id=prod.product_category_id
        #                         join retail_product_classification clas on clas.id=cate.product_classification and clas.name=%s
        #                     group by cate.id,cate.name,prod.id order by prod.id''', (user_category_name,))
        # cr.execute('''select prod_model.id as model_id,
        #             prod.id as prod_id,
        #             prod.name as prod_name,
        #             prod_model.name as prod_model
        #         from retail_product_model prod_model
        #             join retail_product prod on prod_model.product_id=prod.id
        #             join retail_product_classification clas on clas.id=prod.product_classification and clas.name='CAG'
        #             group by
        #             prod_model.id,prod_model.name
        #             ,prod.id
        #             order by prod_model.id''')

        cr.execute('''select  DISTINCT prod_model.id as model_id,
                    prod.id as prod_id,
                    prod.name as prod_name,
                    prod_model.name as prod_model
                    ,cate.id,cate.name as category,
                    prod_model.description as description
                from retail_product_model prod_model
                    join retail_product prod on prod_model.product_id=prod.id
                    join retail_product_category cate on cate.product_id=prod.id and cate.id=prod_model.product_category_id
                    join retail_product_classification clas on clas.id=prod.product_classification and clas.name=%s
                    group by
                    cate.id,cate.name,
                    prod_model.id,prod_model.name
                    ,prod.id
                    order by prod_model.id''', (user_category_name,))
        category_dict_all = cr.dictfetchall()
        result_temp_list2 = []
        i = 0
        for res1 in category_dict_all:
            i += 1
            result_temp_dict2 = {}
            date_list1 = []
            for i in range(-6, 1, 1):
                date_dict1 = {}
                week_date = datetime.today() + timedelta(days=i)
                date_strformat = week_date.strftime('%Y-%m-%d')
                date_dict1.update(
                    {'date': date_strformat, 'count': 0, 'product_id': res1['prod_id'],
                     'product_name': res1['prod_name'],
                     'category_name': res1['category'], 'prod_model_name': res1['prod_model'],
                     'prod_model_description': res1['description']})
                date_list1.append(date_dict1)
            result_temp_dict2.update(
                {'product': res1['prod_name'],
                 'product_id': res1['prod_id'],
                 'category': res1['category'],
                 'name': res1['category'],
                 'date': date_list1,
                 'mtd_count': 0,
                 'lmtd_count': 0,
                 'year_count': 0,
                 'prod_model_name': res1['prod_model'],
                 'prod_model_description': res1['description']})
            if res1['prod_model']:
                result_temp_dict2.update(
                    {'name': res1['prod_model'] + '-' + res1[
                        'description']})
            else:
                result_temp_dict2.update(
                    {'name': res1['description']})
            result_temp_list2.append(result_temp_dict2)
        for res_temp2 in result_temp_list2:
            for date_obj2 in res_temp2['date']:
                for res_obj2 in result_dict:
                    if date_obj2['product_id'] == res_obj2['prod_id'] and date_obj2['category_name'] == \
                            res_obj2[
                                'cate_name'] and date_obj2['date'] == res_obj2['order_date'] and res_obj2[
                        'prod_model_description'] == date_obj2['prod_model_description']:
                        date_obj2.update({'count': res_obj2['sum']})
        f_dict.update({'final_all': result_temp_list2})
        return f_dict

    @api.multi
    def get_hsil_total_count(self, user_id, group_name):
        cr = self._cr
        emp_query = self.get_emp_list_query(group_name)
        if user_id:
            yesterday_date = datetime.today()
            today_strformat = yesterday_date.strftime('%Y-%m-%d')
            week_date = datetime.today() + timedelta(days=-7)
            date_strformat = week_date.strftime('%Y-%m-%d')
            cr.execute('''DROP FUNCTION IF EXISTS get_hsil_total_count()''')
            query = '''CREATE OR REPLACE FUNCTION get_hsil_total_count()
                          RETURNS TABLE (
                           count float,
                           order_date date
                          ) AS
                        $func$
                        BEGIN
                           RETURN QUERY
                            select
                               sum(det.product_quantity*prod_model.mrp) as count,
                                form.order_date as order_date
                                from retail_order_form form
                                left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                left outer join retail_product prod on prod.id=det.product_id
                                 left outer join retail_product_category cate on cate.product_id=prod.id
                                 join retail_product_model prod_model on prod_model.product_id=prod.id
                                     and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                 where form.sales_executive in'''
            query += emp_query
            query += '''AND form.state='approved' AND form.order_date >= %s
                          AND form.order_date <=  %s
               group by form.order_date order by form.order_date;
               END
                        $func$ LANGUAGE plpgsql;
            '''
            cr.execute(query, (user_id, date_strformat, today_strformat))
            cr.execute(
                '''select * from get_hsil_total_count()''')
            result_dict = cr.dictfetchall()
            return result_dict

    @api.multi
    def get_hsil_total_value(self, user_id, group_name):
        cr = self._cr
        emp_query = self.get_emp_list_query(group_name)
        if user_id:
            yesterday_date = datetime.today()
            today_strformat = yesterday_date.strftime('%Y-%m-%d')
            week_date = datetime.today() + timedelta(days=-7)
            date_strformat = week_date.strftime('%Y-%m-%d')
            cr.execute('''DROP FUNCTION IF EXISTS get_hsil_total_value()''')
            query = '''CREATE OR REPLACE FUNCTION get_hsil_total_value()
                          RETURNS TABLE (
                           count float,
                           order_date date
                          ) AS
                        $func$
                        BEGIN
                           RETURN QUERY
                            select
                               sum(det.product_quantity) as count,
                                form.order_date as order_date
                                from retail_order_form form
                                left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                left outer join retail_product prod on prod.id=det.product_id
                                 left outer join retail_product_category cate on cate.product_id=prod.id
                                 join retail_product_model prod_model on prod_model.product_id=prod.id
                                     and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                 where form.sales_executive in'''
            query += emp_query
            query += '''AND form.state='approved' AND form.order_date >= %s
                          AND form.order_date <=  %s
               group by form.order_date order by form.order_date;
               END
                        $func$ LANGUAGE plpgsql;
            '''
            cr.execute(query, (user_id, date_strformat, today_strformat))
            cr.execute(
                '''select * from get_hsil_total_value()''')
            result_dict = cr.dictfetchall()
            return result_dict

    @api.multi
    def get_daily_report_month_count_common(self, type, user_id, from_function, f_dict):
        cr = self._cr
        user_group_name = self.get_usr_designation(user_id)
        if from_function == 'volume':
            month_count_dict = self.get_hsil_questions_month_value(self.id, user_group_name, 'month')
            lmtd_count_dict = self.get_hsil_questions_lmtd_value(self.id, user_group_name, 'last_month')
            year_count_dict = self.get_hsil_questions_year_value(self.id, user_group_name, 'year')
            total_count_dict = self.get_hsil_total_value(self.id, user_group_name)
        else:
            month_count_dict = self.get_hsil_questions_month_count(self.id, user_group_name, 'month')
            lmtd_count_dict = self.get_hsil_questions_lmtd_count(self.id, user_group_name, 'last_month')
            year_count_dict = self.get_hsil_questions_year_count(self.id, user_group_name, 'year')
            total_count_dict = self.get_hsil_total_count(self.id, user_group_name)

        i = 1
        result_temp_list3 = []
        result_temp_total = {}
        date_list2 = []
        for i in range(-6, 1, 1):
            date_dict2 = {}
            week_date = datetime.today() + timedelta(days=i)
            date_strformat = week_date.strftime('%Y-%m-%d')
            date_dict2.update(
                {'date': date_strformat, 'count': 0})
            date_list2.append(date_dict2)
        result_temp_total.update(
            {
                'date': date_list2,
                'mtd_count': '-',
                'lmtd_count': '-',
                'year_count': '-'})
        result_temp_list3.append(result_temp_total)
        for res_temp3 in result_temp_list3:
            for date_obj3 in res_temp3['date']:
                for res_obj3 in total_count_dict:
                    if date_obj3['date'] == res_obj3['order_date']:
                        date_obj3.update({'count': res_obj3['count']})
        f_dict.update({'total': result_temp_list3})
        if 'final' in f_dict:
            for wk_c in f_dict['final']:
                for m_c in month_count_dict:
                    if wk_c['category'] == m_c['name'] and wk_c['product'] == m_c['product_name']:
                        wk_c.update({'mtd_count': m_c['count']})
                        if wk_c['mtd_count'] is None:
                            wk_c['mtd_count'] = 0
                for l_m_c in lmtd_count_dict:
                    if wk_c['category'] == l_m_c['name'] and wk_c['product'] == l_m_c['product_name']:
                        wk_c.update({'lmtd_count': l_m_c['count']})
                        if wk_c['lmtd_count'] is None:
                            wk_c['lmtd_count'] = 0
                for y_c in year_count_dict:
                    if wk_c['category'] == y_c['name'] and wk_c['product'] == y_c['product_name']:
                        wk_c.update({'year_count': y_c['count']})
                        if wk_c['year_count'] is None:
                            wk_c['year_count'] = 0
        if from_function == 'volume':
            cr.execute('''select product_name,sum(count) as count,
                          product_id,name,prod_model_name,prod_model_description
                          from get_hsil_questions_month_value()
                          group by product_name,name,product_id,prod_model_name,prod_model_description''')
            month_result_dict = cr.dictfetchall()
            cr.execute('''select sum(count) as count
                          from get_hsil_questions_month_value()
                          ''')
            month_total_count_dict = cr.dictfetchone()
            cr.execute('''select product_name,sum(count) as count,
                          product_id,name,prod_model_name,prod_model_description
                          from get_hsil_questions_lmtd_value()
                          group by product_name,name,product_id,prod_model_name,prod_model_description''')
            last_month_result_dict = cr.dictfetchall()
            cr.execute('''select sum(count) as count
                          from get_hsil_questions_lmtd_value()
                          ''')
            last_month_total_result_dict = cr.dictfetchone()

            cr.execute('''select product_name,sum(count) as count,
                          product_id,name,prod_model_name,prod_model_description
                          from get_hsil_questions_year_value()
                          group by product_name,name,product_id,prod_model_name,prod_model_description''')
            year_result_dict = cr.dictfetchall()

            cr.execute('''select sum(count) as count
                          from get_hsil_questions_year_value()
                          ''')
            year_total_result_dict = cr.dictfetchone()
            cr.execute('''select product_name,sum(count) as count
                          from get_hsil_questions_month_value()
                          group by product_name''')
            prod_month_result_dict = cr.dictfetchall()
            cr.execute('''select product_name,sum(count) as count
                          from get_hsil_questions_lmtd_value()
                          group by product_name''')
            prod_last_month_result_dict = cr.dictfetchall()
            cr.execute('''select product_name,sum(count) as count
                          from get_hsil_questions_year_value()
                          group by product_name''')
            prod_year_result_dict = cr.dictfetchall()
        else:
            cr.execute('''select product_name,sum(count) as count,
                          product_id,name,prod_model_name,prod_model_description
                          from get_hsil_questions_month_count()
                          group by product_name,name,product_id,prod_model_name,prod_model_description''')
            month_result_dict = cr.dictfetchall()
            cr.execute('''select sum(count) as count
                          from get_hsil_questions_month_count()
                          ''')
            month_total_count_dict = cr.dictfetchone()
            cr.execute('''select product_name,sum(count) as count,
                          product_id,name,prod_model_name,prod_model_description
                          from get_hsil_questions_lmtd_count()
                          group by product_name,name,product_id,prod_model_name,prod_model_description''')
            last_month_result_dict = cr.dictfetchall()
            cr.execute('''select sum(count) as count
                          from get_hsil_questions_lmtd_count()
                          ''')
            last_month_total_result_dict = cr.dictfetchone()
            cr.execute('''select product_name,sum(count) as count,
                          product_id,name,prod_model_name,prod_model_description
                          from get_hsil_questions_year_count()
                          group by product_name,name,product_id,prod_model_name,prod_model_description''')
            year_result_dict = cr.dictfetchall()
            cr.execute('''select sum(count) as count
                          from get_hsil_questions_year_count()
                          ''')
            year_total_result_dict = cr.dictfetchone()
            cr.execute('''select product_name,sum(count) as count
                          from get_hsil_questions_month_count()
                          group by product_name''')
            prod_month_result_dict = cr.dictfetchall()
            cr.execute('''select product_name,sum(count) as count
                          from get_hsil_questions_lmtd_count()
                          group by product_name''')
            prod_last_month_result_dict = cr.dictfetchall()
            cr.execute('''select product_name,sum(count) as count
                          from get_hsil_questions_year_count()
                          group by product_name''')
            prod_year_result_dict = cr.dictfetchall()
        if 'final_all' in f_dict:
            for wk_c in f_dict['final_all']:
                for m_c in month_result_dict:
                    if wk_c['category'] == m_c['name'] and wk_c['product'] == m_c['product_name'] and wk_c[
                        'product_id'] == m_c['product_id'] and wk_c['prod_model_description'] == m_c[
                        'prod_model_description'] and wk_c['prod_model_name'] == m_c['prod_model_name']:
                        wk_c.update({'mtd_count': m_c['count']})
                        if wk_c['mtd_count'] is None:
                            wk_c['mtd_count'] = 0
                for l_m_c in last_month_result_dict:
                    if wk_c['category'] == l_m_c['name'] and wk_c['product'] == l_m_c['product_name'] and wk_c[
                        'product_id'] == l_m_c['product_id'] and wk_c['prod_model_description'] == l_m_c[
                        'prod_model_description'] and wk_c['prod_model_name'] == l_m_c['prod_model_name']:
                        wk_c.update({'lmtd_count': l_m_c['count']})
                        if wk_c['lmtd_count'] is None:
                            wk_c['lmtd_count'] = 0
                for y_c in year_result_dict:
                    if wk_c['category'] == y_c['name'] and wk_c['product'] == y_c['product_name'] and wk_c[
                        'product_id'] == y_c['product_id'] and y_c['prod_model_description'] == wk_c[
                        'prod_model_description']:
                        wk_c.update({'year_count': y_c['count']})
                        if wk_c['year_count'] is None:
                            wk_c['year_count'] = 0
        if 'final_product' in f_dict:
            for wk_c in f_dict['final_product']:
                for m_c in prod_month_result_dict:
                    if wk_c['product'] == m_c['product_name']:
                        wk_c.update({'mtd_count': m_c['count']})
                        if wk_c['mtd_count'] is None:
                            wk_c['mtd_count'] = 0
                for l_m_c in prod_last_month_result_dict:
                    if wk_c['product'] == l_m_c['product_name']:
                        wk_c.update({'lmtd_count': l_m_c['count']})
                        if wk_c['lmtd_count'] is None:
                            wk_c['lmtd_count'] = 0
                for y_c in prod_year_result_dict:
                    if wk_c['product'] == y_c['product_name']:
                        wk_c.update({'year_count': y_c['count']})
                        if wk_c['year_count'] is None:
                            wk_c['year_count'] = 0
        if 'total' in f_dict:
            for month_count in f_dict['total']:
                if month_total_count_dict['count'] != None:
                    month_count['mtd_count'] = month_total_count_dict['count']
                if last_month_total_result_dict['count'] != None:
                    month_count['lmtd_count'] = last_month_total_result_dict['count']
                if year_total_result_dict['count'] != None:
                    month_count['year_count'] = year_total_result_dict['count']
        return f_dict

    @api.multi
    def get_daily_sales_value_report(self, type):
        user_id = self.id
        f_dict = self.get_daily_report_common(type, user_id, "volume")
        res = self.get_daily_report_month_count_common(type, user_id, "volume", f_dict)
        return res

    @api.multi
    def get_daily_kpi_report(self, type):
        user_id = self.id
        f_dict = self.get_daily_report_common(type, user_id, "value")
        res = self.get_daily_report_month_count_common(type, user_id, "value", f_dict)
        return res

    @api.multi
    def get_hsil_am_area_report(self, type):
        group_name = self.get_usr_designation(self.id)
        final_dict = self.get_hsil_area_report_common(type, self.id, group_name)
        return final_dict

    @api.multi
    def get_hsil_area_report_common(self, type, user, group_name):
        final_dict = {}
        if group_name != 'BA':
            cr = self._cr
            ba_ids_list = []
            employee_list_ids = []
            employee_list = []
            search_user = self.env['res.users'].search(([('id', '=', user)]))
            if search_user:
                user_category_name = search_user.user_category.name
            else:
                user_category_name = ''
            if search_user.employee_list:
                for user_id in search_user.employee_list:
                    emp_dict = {}
                    emp_dict.update({'id': user_id.id, 'u_name': user_id.name})
                    employee_list_ids.append(user_id.id)
                    employee_list.append(emp_dict)
                    if group_name == 'GM':
                        for usr_id in user_id.employee_list:
                            ba_ids_list.append(usr_id.id)
                if type == 'year':
                    yr_of_date = datetime.today().year
                    date_strformat1 = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
                    today_date = datetime.today()
                    today_strformat = today_date.strftime('%Y-%m-%d')
                    todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
                    if todate_format > date_strformat1:
                        date_strformat = date_strformat1
                    else:
                        last_yr = yr_of_date - 1
                        last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                        date_strformat = last_yr_date
                else:
                    today_date = datetime.today()
                    today_strformat = today_date.strftime('%Y-%m-%d')
                    month_date = today_date.replace(day=1)
                    date_strformat = month_date.strftime('%Y-%m-%d')
                cr.execute('''DROP FUNCTION IF EXISTS get_hsil_gm_report()''')
                query = '''CREATE OR REPLACE FUNCTION get_hsil_gm_report()
                                  RETURNS TABLE (
                                   count float,
                                   prod_id int,
                                   product_name character varying,
                                   cat_id int,
                                   cat_name character varying,
                                   name text,
                                   user_id int,
                                   prod_model_name character varying,
                                    prod_model_description text

                                  ) AS
                                $func$
                                BEGIN
                                   RETURN QUERY
                                    select sum(det.product_quantity) as count,
                                    prod.id as prod_id,
                                    prod.name as product_name,
                                    cate.id as cat_id,
                                    cate.name as cat_name,
                                    CONCAT  (cate.name, ' - ',prod.name) as name,'''
                if group_name == "GM":
                    query += '''    users.manager_id as user_id,'''
                else:
                    query += '''    users.id as user_id,'''
                query += '''   prod_model.name as prod_model_name,
                                    prod_model.description as prod_model_description
                                         from retail_order_form form
                                    left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                    left outer join retail_product prod on prod.id=det.product_id
                                     left outer join retail_product_category cate on cate.product_id=prod.id
                                     join retail_product_classification clas on clas.id=prod.product_classification
                                     and clas.name=%s
                                     join retail_product_model prod_model on prod_model.product_id=prod.id
                                     and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                     join res_users users on users.id=form.sales_executive
                                     where form.sales_executive in %s
                                     AND form.order_date >= %s
                              AND form.order_date <=  %s
                              AND form.state='approved' '''
                if group_name == "GM":
                    query += '''group by prod.id,prod.name,cate.id,cate.name,users.manager_id,prod_model.name,prod_model.description order by prod.name;'''
                else:
                    query += '''group by prod.id,prod.name,cate.id,cate.name,users.id,prod_model.name,prod_model.description order by prod.name;'''
                query += '''    END
                                $func$ LANGUAGE plpgsql; '''

                if group_name == "GM":
                    cr.execute(query, (user_category_name, tuple(ba_ids_list), date_strformat, today_strformat))
                else:
                    cr.execute(query, (user_category_name, tuple(employee_list_ids), date_strformat, today_strformat))
                cr.execute('''Select * from get_hsil_gm_report() order by product_name''')
                res_sub_category_dict = cr.dictfetchall()
                # cr.execute('''select users.id as user_id
                #                 from res_users users
                #                 where users.manager_id= %s AND users.active = 't' ''', (self.id,))
                # query_result_dict = cr.fetchall()
                query_result_dict = self.env['res.users'].search([('manager_id', '=', self.id)])

                cr.execute('''select prod.id as prod_id, prod.name as prod_name from retail_product prod
                    join retail_product_classification clas on clas.id=prod.product_classification and clas.name=%s
                    group by prod.id order by prod.name''', (user_category_name,))
                all_product_dict = cr.dictfetchall()
                prod_count_list = []
                prod_cate_list = []
                month_count_dict = self.get_hsil_questions_month_value(self.id, group_name, 'month')
                year_count_dict = self.get_hsil_questions_year_value(self.id, group_name, 'year')
                cr.execute('''select product_name,sum(count) as count,
                name from get_hsil_questions_month_value()
                group by product_name,name''')
                month_prod_dict = cr.dictfetchall()
                cr.execute('''select product_name,sum(count) as count,
                name from get_hsil_questions_year_value()
                group by product_name,name''')
                year_prod_dict = cr.dictfetchall()
                cr.execute('''Select sum(count) as count,product_name,
                                user_id from get_hsil_gm_report() group by product_name,user_id order by product_name''')
                res_product_dict = cr.dictfetchall()
                cr.execute('''Select sum(count) as count,prod_id,product_name,cat_id,cat_name,
                                user_id,name from get_hsil_gm_report()
                                group by prod_id,product_name,cat_id,cat_name,user_id,name
                                    order by product_name ''')
                res_category_dict = cr.dictfetchall()
                for prod in all_product_dict:
                    user_detail_list = []
                    for u_id in query_result_dict:
                        user_detail_dict = {'product_name': prod['prod_name'], 'count': 0, 'user_id': u_id.id,
                                            'user_name': u_id.name}
                        user_detail_list.append(user_detail_dict)
                    prod_count_dict = {'prod_name': '', 'mtd_count': 0, 'year_count': 0,
                                       'user': user_detail_list}
                    prod_count_dict.update({'prod_name': prod['prod_name']})
                    for m_c in month_prod_dict:
                        if prod_count_dict['prod_name'] == m_c['product_name']:
                            prod_count_dict.update({'mtd_count': m_c['count']})
                    for y_c in year_prod_dict:
                        if prod_count_dict['prod_name'] == y_c['product_name']:
                            if prod_count_dict is not None:
                                prod_count_dict.update({'year_count': y_c['count']})
                    prod_count_list.append(prod_count_dict)
                final_dict.update({'product': prod_count_list})
                if 'product' in final_dict:
                    for res in final_dict['product']:
                        for user in res['user']:
                            for result in res_product_dict:
                                if user['user_id'] == result['user_id'] and user['product_name'] == result[
                                    'product_name']:
                                    user.update({'count': result['count']})
                cr.execute('''select DISTINCT prod.name as prod_name,cate.name as category from retail_product prod
                        join retail_product_category cate on cate.product_id=prod.id
                        join retail_product_classification clas on clas.id=prod.product_classification and clas.name=%s
                        group by cate.name,prod.id order by prod.name''', (user_category_name,))
                category_dict = cr.dictfetchall()
                for prod_cat in category_dict:
                    user_detail_list1 = []
                    for u_id in query_result_dict:
                        user_detail_dict1 = {'product_name': prod_cat['prod_name'], 'category': prod_cat['category'],
                                             'count': 0, 'user_id': u_id.id, 'user_name': u_id.name}
                        user_detail_list1.append(user_detail_dict1)
                    prod_cate_dict = {'cat_name': '', 'prod_name': '', 'mtd_count': 0, 'year_count': 0,
                                      'user': user_detail_list1}
                    prod_cate_dict.update({'prod_name': prod_cat['prod_name'], 'cat_name': prod_cat['category']})
                    for m_c in month_count_dict:
                        if prod_cate_dict['prod_name'] == m_c['product_name'] and m_c['name'] == prod_cate_dict[
                            'cat_name']:
                            prod_cate_dict.update(
                                {'mtd_count': m_c['count']})
                    for y_c in year_count_dict:
                        if prod_cate_dict['prod_name'] == y_c['product_name'] and y_c['name'] == prod_cate_dict[
                            'cat_name']:
                            if prod_cate_dict is not None:
                                prod_cate_dict.update(
                                    {'year_count': y_c['count']})
                    prod_cate_list.append(prod_cate_dict)
                final_dict.update({'category': prod_cate_list})
                if 'category' in final_dict:
                    for res1 in final_dict['category']:
                        for user1 in res1['user']:
                            for result1 in res_category_dict:
                                if user1['user_id'] == result1['user_id'] and user1['product_name'] == result1[
                                    'product_name'] and user1['category'] == result1['cat_name']:
                                    user1.update({'count': result1['count']})
                cr.execute('''select  DISTINCT prod_model.id as model_id,
                            prod.id as prod_id,
                            prod.name as prod_name,
                            prod_model.name as prod_model
                            ,cate.id,cate.name as name,
                            prod_model.description as description
                        from retail_product_model prod_model
                            join retail_product prod on prod_model.product_id=prod.id
                            join retail_product_category cate on cate.product_id=prod.id and cate.id=prod_model.product_category_id
                            join retail_product_classification clas on clas.id=prod.product_classification and clas.name=%s
                            group by
                            cate.id,cate.name,
                            prod_model.id,prod_model.name
                            ,prod.id
                            order by prod_model.id''', (user_category_name,))
                all_sub_cat = cr.dictfetchall()
                cr.execute('''Select * from get_hsil_gm_report() order by product_name''')
                month_sub_cat_dict = cr.dictfetchall()
                cr.execute('''Select sum(count) as count from get_hsil_gm_report()''')
                month_total_count_dict = cr.dictfetchone()
                cr.execute('''Select * from get_hsil_gm_report() order by product_name''')
                year_sub_cat_dict = cr.dictfetchall()
                cr.execute('''Select sum(count) as count from get_hsil_gm_report()''')
                year_total_count_dict = cr.dictfetchone()
                prod_sub_cate_list = []
                for sub_cat in all_sub_cat:
                    user_detail_list2 = []
                    for u_id in query_result_dict:
                        user_detail_dict2 = {'product_name': sub_cat['prod_name'],
                                             'sub_category': sub_cat['description'],
                                             'category': sub_cat['name'],
                                             'count': 0, 'user_id': u_id.id, 'user_name': u_id.name,
                                             'prod_description': sub_cat['description']}
                        user_detail_list2.append(user_detail_dict2)
                    prod_sub_cate_dict = {'cat_name': '', 'prod_name': '', 'mtd_count': 0, 'year_count': 0,
                                          'user': user_detail_list2}
                    prod_sub_cate_dict.update({'prod_name': sub_cat['prod_name'], 'cat_name': sub_cat['name'],
                                               'sub_category': sub_cat['description']})
                    for m_c in month_sub_cat_dict:
                        if prod_sub_cate_dict['prod_name'] == m_c['product_name'] and m_c['cat_name'] == \
                                prod_sub_cate_dict[
                                    'cat_name'] and prod_sub_cate_dict['sub_category'] == m_c['prod_model_description']:
                            prod_sub_cate_dict.update(
                                {'mtd_count': m_c['count']})
                    for y_c in year_sub_cat_dict:
                        if prod_sub_cate_dict['prod_name'] == y_c['product_name'] and y_c['cat_name'] == \
                                prod_sub_cate_dict[
                                    'cat_name'] and prod_sub_cate_dict['sub_category'] == y_c['prod_model_description']:
                            if prod_sub_cate_dict is not None:
                                prod_sub_cate_dict.update(
                                    {'year_count': y_c['count']})
                    prod_sub_cate_list.append(prod_sub_cate_dict)
                final_dict.update({'sub_category': prod_sub_cate_list})
                if 'sub_category' in final_dict:
                    for res2 in final_dict['sub_category']:
                        for user2 in res2['user']:
                            for result2 in res_sub_category_dict:
                                if user2['user_id'] == result2['user_id'] and user2['product_name'] == result2[
                                    'product_name'] and user2['category'] == result2['cat_name'] and user2[
                                    'prod_description'] == result2['prod_model_description']:
                                    user2.update({'count': result2['count']})

                user_detail_list3 = []
                prod_total_list = []
                for u_id in query_result_dict:
                    user_detail_dict3 = {'count': 0, 'user_id': u_id.id, 'user_name': u_id.name}
                    user_detail_list3.append(user_detail_dict3)
                prod_total_dict = {'mtd_count': '-', 'year_count': '-', 'user': user_detail_list3}
                prod_total_list.append(prod_total_dict)
                final_dict.update({'total': prod_total_list})
                res_total_dict = self.get_am_total_count(self.id, group_name, type)
                if 'total' in final_dict:
                    for res3 in final_dict['total']:
                        for user3 in res3['user']:
                            for result3 in res_total_dict:
                                if user3['user_id'] == result3['user_id']:
                                    user3.update({'count': result3['count']})
                        if month_total_count_dict['count'] != None:
                            res3['mtd_count'] = month_total_count_dict['count']
                        if year_total_count_dict['count'] != None:
                            res3['year_count'] = year_total_count_dict['count']
        return final_dict

    @api.multi
    def get_am_total_count(self, user_id, group_name, type):
        cr = self._cr
        emp_query = self.get_emp_list_query(group_name)
        if type == 'year':
            yr_of_date = datetime.today().year
            date_strformat1 = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
            today_date = datetime.today()
            today_strformat = today_date.strftime('%Y-%m-%d')
            todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
            if todate_format > date_strformat1:
                date_strformat = date_strformat1
            else:
                last_yr = yr_of_date - 1
                last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                date_strformat = last_yr_date
        else:
            today_date = datetime.today()
            today_strformat = today_date.strftime('%Y-%m-%d')
            month_date = today_date.replace(day=1)
            date_strformat = month_date.strftime('%Y-%m-%d')
        cr.execute('''DROP FUNCTION IF EXISTS get_am_total_count()''')
        query = '''CREATE OR REPLACE FUNCTION get_am_total_count()
                          RETURNS TABLE (
                           count float,
                           user_id int
                          ) AS
                        $func$
                        BEGIN
                           RETURN QUERY
                            select
                               sum(det.product_quantity) as count,
                                form.sales_executive as user_id
                                from retail_order_form form
                                left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                left outer join retail_product prod on prod.id=det.product_id
                                 left outer join retail_product_category cate on cate.product_id=prod.id
                                 join retail_product_model prod_model on prod_model.product_id=prod.id
                                     and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                 where form.sales_executive in '''
        query += emp_query
        query += '''AND form.state='approved' AND form.order_date >= %s
                          AND form.order_date <=  %s
                group by form.sales_executive order by form.sales_executive;
               END
                        $func$ LANGUAGE plpgsql;
            '''
        cr.execute(query, (user_id, date_strformat, today_strformat))
        cr.execute(
            '''select * from get_am_total_count()''')
        result_dict = cr.dictfetchall()
        return result_dict

    @api.multi
    def get_hsil_gm_area_report(self, type):
        group_name = self.get_usr_designation(self.id)
        final_dict = self.get_hsil_area_report_common(type, self.id, group_name)
        return final_dict

    @api.multi
    def get_hsil_questions_lmtd_value(self, user_id, group_name, type):
        cr = self._cr
        emp_query = self.get_emp_list_query(group_name)
        if user_id:
            today_date = datetime.today()
            mon_date = today_date.replace(day=1)
            last_m_first = mon_date - relativedelta(months=1)
            date_strformat = last_m_first.strftime('%Y-%m-%d')
            lastMonth = today_date - relativedelta(months=1)
            today_strformat = lastMonth.strftime('%Y-%m-%d')

            cr.execute('''DROP FUNCTION IF EXISTS get_hsil_questions_lmtd_value()''')
            query = '''CREATE OR REPLACE FUNCTION get_hsil_questions_lmtd_value()
                          RETURNS TABLE (

                           product_name character varying,
                           product_id int,
                           count float,
                           name character varying,
                           prod_model_name character varying,
                            prod_model_description text

                          ) AS
                        $func$
                        BEGIN
                           RETURN QUERY
                            select
                               Distinct prod.name as product_name,
                               prod.id as product_id,
                               sum(det.product_quantity) as count,
                                cate.name as name,
                                prod_model.name as prod_model_name,
                                prod_model.description as prod_model_description
                                from retail_order_form form
                                left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                left outer join retail_product prod on prod.id=det.product_id
                                 left outer join retail_product_category cate on cate.product_id=prod.id
                                  join retail_product_model prod_model on prod_model.product_id=prod.id
                                and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                 where form.sales_executive in'''
            query += emp_query
            query += '''AND form.state='approved' AND form.order_date >= %s
                          AND form.order_date <=  %s
               group by prod.name,prod.id,cate.name,prod_model.name,prod_model.description order by prod.name;
               END
                        $func$ LANGUAGE plpgsql;
            '''
            cr.execute(query, (user_id, date_strformat, today_strformat))
            cr.execute(
                '''select product_name,sum(count) as count,name from get_hsil_questions_lmtd_value() group by product_name,name''')
            result_dict = cr.dictfetchall()
            return result_dict

    @api.multi
    def get_hsil_questions_lmtd_count(self, user_id, group_name, type):
        cr = self._cr
        emp_query = self.get_emp_list_query(group_name)
        if user_id:
            today_date = datetime.today()
            mon_date = today_date.replace(day=1)
            last_m_first = mon_date - relativedelta(months=1)
            date_strformat = last_m_first.strftime('%Y-%m-%d')
            lastMonth = today_date - relativedelta(months=1)
            today_strformat = lastMonth.strftime('%Y-%m-%d')
            cr.execute('''DROP FUNCTION IF EXISTS get_hsil_questions_lmtd_count()''')
            query = '''CREATE OR REPLACE FUNCTION get_hsil_questions_lmtd_count()
                          RETURNS TABLE (
                           product_name character varying,
                           product_id int,
                           count float,
                           name character varying,
                           prod_model_name character varying,
                            prod_model_description text
                          ) AS
                        $func$
                        BEGIN
                           RETURN QUERY
                            select
                               Distinct prod.name as product_name,
                               prod.id as product_id,
                               sum(det.product_quantity*prod_model.mrp) as count,
                                cate.name as name,
                                prod_model.name as prod_model_name,
                                prod_model.description as prod_model_description
                                from retail_order_form form
                                left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                left outer join retail_product prod on prod.id=det.product_id
                                 left outer join retail_product_category cate on cate.product_id=prod.id
                                  join retail_product_model prod_model on prod_model.product_id=prod.id
                                and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                 where form.sales_executive in'''
            query += emp_query
            query += '''AND form.state='approved' AND form.order_date >= %s
                          AND form.order_date <=  %s
               group by prod.name,prod.id,cate.name,prod_model.name,prod_model.description order by prod.name;
               END
                        $func$ LANGUAGE plpgsql;
            '''
            cr.execute(query, (user_id, date_strformat, today_strformat))
            cr.execute(
                '''select product_name,sum(count) as count,name from get_hsil_questions_lmtd_count() group by product_name,name''')
            result_dict = cr.dictfetchall()
            return result_dict

    @api.multi
    def get_hsil_questions_year_count(self, user_id, group_name, type):
        cr = self._cr
        emp_query = self.get_emp_list_query(group_name)
        if user_id:
            if type == 'year':
                yr_of_date = datetime.today().year
                financial_year_start_date = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
                yesterday_date = datetime.today()
                today_strformat = yesterday_date.strftime('%Y-%m-%d')
                todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
                if todate_format > financial_year_start_date:
                    yr_start_date = financial_year_start_date
                else:
                    last_yr = yr_of_date - 1
                    last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                    yr_start_date = last_yr_date

                cr.execute('''DROP FUNCTION IF EXISTS get_hsil_questions_year_count()''')
                query = '''CREATE OR REPLACE FUNCTION get_hsil_questions_year_count()
                              RETURNS TABLE (

                               product_name character varying,
                               product_id int,
                               date text,
                               count float,
                               name character varying,
                               prod_model_name character varying,
                            prod_model_description text

                              ) AS
                            $func$
                            BEGIN
                               RETURN QUERY
                                select Distinct prod.name as product_name,
                                prod.id as product_id,
                                    'Year' as date,
                                   sum(det.product_quantity*prod_model.mrp) as count,
                                    cate.name as name,
                                    prod_model.name as prod_model_name,
                                    prod_model.description as prod_model_description
                                    from retail_order_form form
                                    left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                    left outer join retail_product prod on prod.id=det.product_id
                                     left outer join retail_product_category cate on cate.product_id=prod.id
                                     join retail_product_model prod_model on prod_model.product_id=prod.id
                                     and prod_model.product_category_id=cate.id and prod_model.id=det.product_model

                                     where form.sales_executive in'''
                query += emp_query
                query += '''AND form.state='approved' AND form.order_date >= %s
                              AND form.order_date <=  %s
                    group by prod.name,cate.name,prod.id,prod_model.name,prod_model.description order by prod.name;
                   END
                            $func$ LANGUAGE plpgsql;
                '''
                cr.execute(query, (user_id, yr_start_date, today_strformat))
                cr.execute(
                    '''select product_name,sum(count) as count,name from get_hsil_questions_year_count() group by product_name,name''')
                result_dict = cr.dictfetchall()

                return result_dict

    @api.multi
    def get_hsil_regional_report(self, type):
        final_dict = {}
        user_obj = self.env['res.users'].search([('id', '=', self.id)])
        if user_obj:
            user_category_name = user_obj.user_category.name
        else:
            user_category_name = ''
        group_name = self.get_usr_designation(self.id)
        if group_name != 'BA':
            cr = self._cr
            count_list = []
            if type == 'year':
                yr_of_date = datetime.today().year
                date_strformat1 = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
                today_date = datetime.today()
                today_strformat = today_date.strftime('%Y-%m-%d')
                todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
                if todate_format > date_strformat1:
                    date_strformat = date_strformat1
                else:
                    last_yr = yr_of_date - 1
                    last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                    date_strformat = last_yr_date
            else:
                today_date = datetime.today()
                today_strformat = today_date.strftime('%Y-%m-%d')
                month_date = today_date.replace(day=1)
                date_strformat = month_date.strftime('%Y-%m-%d')
            cr.execute('''DROP FUNCTION IF EXISTS get_hsil_regional_report()''')
            cr.execute('''CREATE OR REPLACE FUNCTION get_hsil_regional_report()
                            RETURNS TABLE (
                            count float,
                            region character varying,
                            prod_id int,
                            product_name character varying,
                            cat_id int,
                            name character varying,
                            prod_model_name character varying,
                            prod_model_description text
                            ) AS
                            $func$
                            BEGIN
                            RETURN QUERY
                                select sum(det.product_quantity) as count,
                                users.region_id as region,
                                prod.id as prod_id,
                                prod.name as product_name,
                                cate.id as cat_id,
                                cate.name as name,
                                prod_model.name as prod_model_name,
                                prod_model.description as prod_model_description
                                from retail_order_form form
                                left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                left outer join retail_product prod on prod.id=det.product_id
                                left outer join retail_product_category cate on cate.product_id=prod.id
                                join retail_product_classification clas on clas.id=prod.product_classification and clas.name=%s
                                            join retail_product_model prod_model on prod_model.product_id=prod.id
                                            and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                            join res_users users on users.id=form.sales_executive
                                            where form.sales_executive in (select id from res_users where
                                            region_id in('east','north1','north2','south','west'))
                                             AND form.order_date >= %s
                             AND form.order_date <=  %s AND form.state='approved'
                    group by prod.id,prod.name,users.region_id,cate.id,cate.name,prod_model.name,prod_model.description order by prod.name;
                    END
	                $func$ LANGUAGE plpgsql;''',
                       (user_category_name, date_strformat, today_strformat))
            cr.execute('''select sum(count) as count,region,prod_id,product_name
                         from get_hsil_regional_report()
                         group by region,prod_id,product_name order by product_name''')
            res_product_dict = cr.dictfetchall()
            cr.execute('''select * from get_hsil_regional_report() order by product_name''')
            res_sub_category_dict = cr.dictfetchall()
            final_dict = {}
            cr.execute(
                '''select distinct region_id from res_users where region_id in('east','north1','north2','south','west') ''')
            total_regions = cr.fetchall()
            cr.execute('''select prod.id as prod_id, prod.name as prod_name from retail_product prod
                    join retail_product_classification clas on clas.id=prod.product_classification and clas.name=%s
                    group by prod.id order by prod.name''', (user_category_name,))
            all_product_dict = cr.dictfetchall()
            month_count_dict = self.get_hsil_regional_month_count('month')
            year_count_dict = self.get_hsil_regional_year_count('year')
            cr.execute('''select product_name,sum(count) as count,
                name from get_hsil_regional_month_count()
                group by product_name,name''')
            month_prod_dict = cr.dictfetchall()
            cr.execute('''select product_name,sum(count) as count,
                name from get_hsil_regional_year_count()
                group by product_name,name''')
            year_prod_dict = cr.dictfetchall()
            prod_count_list = []
            for prod in all_product_dict:
                region_detail_list = []
                for region in total_regions:
                    region_detail_dict = {'product_name': prod['prod_name'], 'count': 0, 'region': region[0]}
                    region_detail_list.append(region_detail_dict)
                prod_count_dict = {'prod_name': '', 'mtd_count': 0, 'year_count': 0,
                                   'region': region_detail_list}
                prod_count_dict.update({'prod_name': prod['prod_name']})
                for m_c in month_prod_dict:
                    if prod_count_dict['prod_name'] == m_c['product_name']:
                        prod_count_dict.update({'mtd_count': m_c['count']})
                for y_c in year_prod_dict:
                    if prod_count_dict['prod_name'] == y_c['product_name']:
                        if prod_count_dict is not None:
                            prod_count_dict.update({'year_count': y_c['count']})
                prod_count_list.append(prod_count_dict)
            final_dict.update({'product': prod_count_list})
            if 'product' in final_dict:
                for res in final_dict['product']:
                    for user in res['region']:
                        for result in res_product_dict:
                            if user['region'] == result['region'] and user['product_name'] == result[
                                'product_name']:
                                user.update({'count': result['count']})
                        user.update({'region': user['region'].title()})
            cr.execute('''select DISTINCT prod.name as prod_name,cate.name as category from retail_product prod
                        join retail_product_category cate on cate.product_id=prod.id
                        join retail_product_classification clas on clas.id=prod.product_classification and clas.name=%s
                        group by cate.name,prod.id order by prod.name''', (user_category_name,))
            category_dict = cr.dictfetchall()
            cr.execute('''Select sum(count) as count,prod_id,product_name,cat_id,name,
                        region from get_hsil_regional_report()
                        group by product_name,prod_id,cat_id,name,region
                        order by product_name ''')
            res_category_dict = cr.dictfetchall()
            prod_cate_list = []
            for prod_cat in category_dict:
                region_detail_list1 = []
                for region1 in total_regions:
                    region_detail_dict1 = {'product_name': prod_cat['prod_name'], 'category': prod_cat['category'],
                                           'count': 0, 'region': region1[0]}
                    region_detail_list1.append(region_detail_dict1)
                prod_cate_dict = {'cat_name': '', 'prod_name': '', 'mtd_count': 0, 'year_count': 0,
                                  'region': region_detail_list1}
                prod_cate_dict.update({'prod_name': prod_cat['prod_name'], 'cat_name': prod_cat['category']})
                for m_c in month_count_dict:
                    if prod_cate_dict['prod_name'] == m_c['product_name'] and m_c['name'] == prod_cate_dict[
                        'cat_name']:
                        prod_cate_dict.update(
                            {'mtd_count': m_c['count']})
                for y_c in year_count_dict:
                    if prod_cate_dict['prod_name'] == y_c['product_name'] and y_c['name'] == prod_cate_dict[
                        'cat_name']:
                        if prod_cate_dict is not None:
                            prod_cate_dict.update(
                                {'year_count': y_c['count']})
                prod_cate_list.append(prod_cate_dict)
            final_dict.update({'category': prod_cate_list})
            if 'category' in final_dict:
                for res1 in final_dict['category']:
                    for user1 in res1['region']:
                        for result1 in res_category_dict:
                            if user1['region'] == result1['region'] and user1['product_name'] == result1[
                                'product_name'] and user1['category'] == result1['name']:
                                user1.update({'count': result1['count']})
                        user1.update({'region': user1['region'].title()})
            cr.execute('''select  DISTINCT prod_model.id as model_id,
                            prod.id as prod_id,
                            prod.name as prod_name,
                            prod_model.name as prod_model
                            ,cate.id,cate.name as name,
                            prod_model.description as description
                        from retail_product_model prod_model
                            join retail_product prod on prod_model.product_id=prod.id
                            join retail_product_category cate on cate.product_id=prod.id and cate.id=prod_model.product_category_id
                            join retail_product_classification clas on clas.id=prod.product_classification and clas.name=%s
                            group by
                            cate.id,cate.name,
                            prod_model.id,prod_model.name
                            ,prod.id
                            order by prod_model.id''', (user_category_name,))
            all_sub_cat = cr.dictfetchall()
            cr.execute('''Select * from get_hsil_regional_month_count() order by product_name''')
            month_sub_cat_dict = cr.dictfetchall()
            cr.execute('''Select sum(count) as count from get_hsil_regional_month_count()''')
            month_total_count_dict = cr.dictfetchone()
            cr.execute('''Select * from get_hsil_regional_year_count() order by product_name''')
            year_sub_cat_dict = cr.dictfetchall()
            cr.execute('''Select sum(count) as count from get_hsil_regional_year_count()''')
            year_total_count_dict = cr.dictfetchone()
            prod_sub_cate_list = []
            for sub_cat in all_sub_cat:
                region_detail_list2 = []
                for region2 in total_regions:
                    region_detail_dict2 = {'product_name': sub_cat['prod_name'],
                                           'sub_category': sub_cat['description'], 'category': sub_cat['name'],
                                           'count': 0, 'region': region2[0], 'prod_description': sub_cat['description']}
                    region_detail_list2.append(region_detail_dict2)
                prod_sub_cate_dict = {'cat_name': '', 'prod_name': '', 'mtd_count': 0, 'year_count': 0,
                                      'region': region_detail_list2}
                prod_sub_cate_dict.update({'prod_name': sub_cat['prod_name'], 'cat_name': sub_cat['name'],
                                           'sub_category': sub_cat['description']})
                for m_c in month_sub_cat_dict:
                    if prod_sub_cate_dict['prod_name'] == m_c['product_name'] and prod_sub_cate_dict['cat_name'] == m_c[
                        'name'] and prod_sub_cate_dict['sub_category'] == m_c['prod_model_description']:
                        prod_sub_cate_dict.update(
                            {'mtd_count': m_c['count']})
                for y_c in year_sub_cat_dict:
                    if prod_sub_cate_dict['prod_name'] == y_c['product_name'] and prod_sub_cate_dict['cat_name'] == y_c[
                        'name'] and prod_sub_cate_dict['sub_category'] == y_c['prod_model_description']:
                        if prod_sub_cate_dict is not None:
                            prod_sub_cate_dict.update(
                                {'year_count': y_c['count']})
                prod_sub_cate_list.append(prod_sub_cate_dict)
            final_dict.update({'sub_category': prod_sub_cate_list})
            if 'sub_category' in final_dict:
                for res2 in final_dict['sub_category']:
                    for region2 in res2['region']:
                        for result2 in res_sub_category_dict:
                            if region2['region'] == result2['region'] and region2['product_name'] == result2[
                                'product_name'] and region2['category'] == result2['name'] and region2[
                                'prod_description'] == result2['prod_model_description']:
                                region2.update({'count': result2['count']})
                        region2.update({'region': region2['region'].title()})
            region_detail_list3 = []
            prod_total_list = []
            for region3 in total_regions:
                region_detail_dict3 = {
                    'count': 0, 'region': region3[0]}
                region_detail_list3.append(region_detail_dict3)
            prod_total_dict = {'mtd_count': '-', 'year_count': '-', 'region': region_detail_list3}
            prod_total_list.append(prod_total_dict)
            final_dict.update({'total': prod_total_list})
            res_region_total = self.get_hsil_regional_total_count(type)
            if 'total' in final_dict:
                for res3 in final_dict['total']:
                    for region3 in res3['region']:
                        for result3 in res_region_total:
                            if region3['region'] == result3['region']:
                                region3.update({'count': result3['count']})
                        region3.update({'region': region3['region'].title()})
                    if month_total_count_dict['count'] != None:
                        res3['mtd_count'] = month_total_count_dict['count']
                    if year_total_count_dict['count'] != None:
                        res3['year_count'] = year_total_count_dict['count']
        return final_dict

    @api.multi
    def get_hsil_regional_total_count(self, type):
        cr = self._cr
        if type == 'year':
            yr_of_date = datetime.today().year
            date_strformat1 = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
            today_date = datetime.today()
            today_strformat = today_date.strftime('%Y-%m-%d')
            todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
            if todate_format > date_strformat1:
                date_strformat = date_strformat1
            else:
                last_yr = yr_of_date - 1
                last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                date_strformat = last_yr_date
        else:
            today_date = datetime.today()
            today_strformat = today_date.strftime('%Y-%m-%d')
            month_date = today_date.replace(day=1)
            date_strformat = month_date.strftime('%Y-%m-%d')
        cr.execute('''DROP FUNCTION IF EXISTS get_hsil_regional_total_count()''')
        cr.execute('''CREATE OR REPLACE FUNCTION get_hsil_regional_total_count()
                                  RETURNS TABLE (
                                   count float,
                                   region character varying
                                  ) AS
                                $func$
                                BEGIN
                                   RETURN QUERY

                                    select sum(det.product_quantity) as count,
                                                    users.region_id as region


                                                            from retail_order_form form
                                                            left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                                            left outer join retail_product prod on prod.id=det.product_id
                                                             left outer join retail_product_category cate on cate.product_id=prod.id
                                                             join res_users users on users.id=form.sales_executive
                                                             join retail_product_model prod_model on prod_model.product_id=prod.id
                                                        and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                                             where form.sales_executive in (select id from res_users where
                                                             region_id in('east','north1','north2','south','west'))
                                                             AND form.state='approved'
                                                             AND form.order_date >= %s
                                                      AND form.order_date <=  %s
                                                     group by users.region_id order by users.region_id;
                                                    END
                                $func$ LANGUAGE plpgsql;
                        ''', (date_strformat, today_strformat))
        cr.execute(
            '''select * from get_hsil_regional_total_count()''')
        month_result_dict = cr.dictfetchall()
        return month_result_dict

    @api.multi
    def get_hsil_regional_month_count(self, type):
        cr = self._cr
        if type == 'year':
            yr_of_date = datetime.today().year
            date_strformat1 = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
            today_date = datetime.today()
            today_strformat = today_date.strftime('%Y-%m-%d')
            todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
            if todate_format > date_strformat1:
                date_strformat = date_strformat1
            else:
                last_yr = yr_of_date - 1
                last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                date_strformat = last_yr_date
        else:
            today_date = datetime.today()
            today_strformat = today_date.strftime('%Y-%m-%d')
            month_date = today_date.replace(day=1)
            date_strformat = month_date.strftime('%Y-%m-%d')
        cr.execute('''DROP FUNCTION IF EXISTS get_hsil_regional_month_count()''')
        cr.execute('''CREATE OR REPLACE FUNCTION get_hsil_regional_month_count()
                                  RETURNS TABLE (
                                   count float,
                                   region character varying,
                                   product_name character varying,
                                   name character varying,
                                   prod_model_name character varying,
                                prod_model_description text

                                  ) AS
                                $func$
                                BEGIN
                                   RETURN QUERY

                                    select sum(det.product_quantity) as count,
                                                    users.region_id as region,
                                                            prod.name as product_name,
                                                            cate.name as name,
                                                            prod_model.name as prod_model_name,
                                                    prod_model.description as prod_model_description
                                                            from retail_order_form form
                                                            left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                                            left outer join retail_product prod on prod.id=det.product_id
                                                             left outer join retail_product_category cate on cate.product_id=prod.id
                                                             join res_users users on users.id=form.sales_executive
                                                             join retail_product_model prod_model on prod_model.product_id=prod.id
                                                        and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                                             where form.sales_executive in (select id from res_users where
                                                             region_id in('east','north1','north2','south','west'))
                                                             AND form.order_date >= %s
                                                      AND form.order_date <=  %s
                                                      AND form.state='approved'
                                                    group by prod.name,users.region_id,cate.name,prod_model.name,prod_model.description order by prod.name;
                                                    END
                                $func$ LANGUAGE plpgsql;
                        ''', (date_strformat, today_strformat))
        cr.execute(
            '''select product_name,sum(count) as count,name from get_hsil_regional_month_count() group by product_name,name''')
        month_result_dict = cr.dictfetchall()
        return month_result_dict

    @api.multi
    def get_hsil_regional_year_count(self, type):
        cr = self._cr
        if type == 'year':
            yr_of_date = datetime.today().year
            date_strformat1 = datetime.strptime(str(yr_of_date) + "-03-01", "%Y-%m-%d").date()
            today_date = datetime.today()
            today_strformat = today_date.strftime('%Y-%m-%d')
            todate_format = datetime.strptime(today_strformat, "%Y-%m-%d").date()
            if todate_format > date_strformat1:
                date_strformat = date_strformat1
            else:
                last_yr = yr_of_date - 1
                last_yr_date = datetime.strptime(str(last_yr) + "-03-01", "%Y-%m-%d").date()
                date_strformat = last_yr_date
        else:
            today_date = datetime.today()
            today_strformat = today_date.strftime('%Y-%m-%d')
            month_date = today_date.replace(day=1)
            date_strformat = month_date.strftime('%Y-%m-%d')
        cr.execute('''DROP FUNCTION IF EXISTS get_hsil_regional_year_count()''')
        cr.execute('''CREATE OR REPLACE FUNCTION get_hsil_regional_year_count()
                                  RETURNS TABLE (
                                   count float,
                                   region character varying,
                                   product_name character varying,
                                   name character varying,
                                   prod_model_name character varying,
                                prod_model_description text

                                  ) AS
                                $func$
                                BEGIN
                                   RETURN QUERY

                                    select sum(det.product_quantity) as count,
                                                    users.region_id as region,
                                                            prod.name as product_name,
                                                            cate.name as name,
                                                            prod_model.name as prod_model_name,
                                                    prod_model.description as prod_model_description
                                                            from retail_order_form form
                                                            left outer join retail_order_details det on form.id=det.retail_order_detail_id
                                                            left outer join retail_product prod on prod.id=det.product_id
                                                             left outer join retail_product_category cate on cate.product_id=prod.id
                                                             join res_users users on users.id=form.sales_executive
                                                             join retail_product_model prod_model on prod_model.product_id=prod.id
                                                        and prod_model.product_category_id=cate.id and prod_model.id=det.product_model
                                                             where form.sales_executive in (select id from res_users where
                                                             region_id in('east','north1','north2','south','west'))
                                                             AND form.order_date >= %s
                                                      AND form.order_date <=  %s
                                                      AND form.state='approved'
                                                    group by prod.name,users.region_id,cate.name,prod_model.name,prod_model.description order by prod.name;
                                                    END
                                $func$ LANGUAGE plpgsql;
                        ''', (date_strformat, today_strformat))
        cr.execute(
            '''select product_name,sum(count) as count,name from get_hsil_regional_year_count() group by product_name,name''')
        month_result_dict = cr.dictfetchall()
        return month_result_dict


class RetailOrderDetails(models.Model):
    _name = 'retail.order.details'

    retail_order_detail_id = fields.Many2one('retail.order.form', string="Order Details")
    product_category_id = fields.Many2one('retail.product.category', string="Product Category")
    product_model = fields.Many2one('retail.product.model', string="Product Model")
    product_id = fields.Many2one('retail.product', string="Product")
    product_quantity = fields.Float(string="Quantity")
    payment_term_id = fields.Many2one('retail.payment.terms', string="Payment Term")
    price = fields.Float(string="Price")


class RetailProductCategory(models.Model):
    _name = 'retail.product.category'
    _order = 'name asc'

    name = fields.Char(string='Name')
    product_id = fields.Many2one('retail.product', string="Product")
    active = fields.Boolean(string="active")


class RetailProduct(models.Model):
    _name = 'retail.product'
    _order = 'name asc'

    name = fields.Char(string='Name')
    active = fields.Boolean(string="active")
    product_classification = fields.Many2one('retail.product.classification', string='Classification')
    description = fields.Text(string="Description")


class RetailPaymentTerms(models.Model):
    _name = 'retail.payment.terms'

    name = fields.Char(string='Name')
    active = fields.Boolean(string="active")


class RetailProductClassification(models.Model):
    _name = 'retail.product.classification'

    name = fields.Char(string='Name')
    active = fields.Boolean(string='Active')


class RetailProductModel(models.Model):
    _name = 'retail.product.model'
    _rec_name = 'description'
    _order = 'name,description asc'

    name = fields.Char(string='Sub Category')
    model_name = fields.Char(string='Model Name')
    active = fields.Boolean(string="active")
    product_category_id = fields.Many2one('retail.product.category', string="Product Category")
    product_id = fields.Many2one('retail.product', string="Product")
    model_number = fields.Char(string="Model Number")
    capacity = fields.Char(string="Capacity")
    description = fields.Text(string="Description")
    mrp = fields.Float(string="MRP")
    hsn_code = fields.Char(string="HSN Code")
    sku = fields.Char(string="SKU")
