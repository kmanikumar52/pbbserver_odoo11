import logging
from odoo import models, fields, api,tools,_
from odoo.osv import osv
from odoo.tools.translate import _
import datetime
import urllib.request  as urllib2
from datetime import datetime, date, time, timedelta
import json
import requests
import time

_logger = logging.getLogger(__name__)


class RetailViewConfiguration(models.Model):
    _name = 'retail.view.configuration'

    view_map = fields.Boolean('View Map')
    view_info = fields.Boolean('View Info')
    group_ids = fields.Many2many('res.groups', 'retail_view_group_rel', 'retail_view_id', 'group_id',
                                 'Retail View Group')


class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.multi
    def search_employee_list_users(self, user_id):
        result_dict = {}
        result_emp_list = []
        search_obj = self.search([('id', '=', user_id)])
        for user in search_obj.employee_list:
            result_emp_dict = {}
            result_emp_dict.update({'user_id': user.id, 'name': user.name, 'partner_id': user.partner_id.id})
            list_emp = []
            group_name = ""
            for group in user.groups_id:
                search_group_obj = self.env["res.groups"].search([('id', '=', group.id)])
                if search_group_obj.name:
                    if search_group_obj.name == "BA":
                        if group_name != "AM" and group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                            group_name = "BA"
                    elif search_group_obj.name == "Account Manager":
                        if group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                            group_name = "AM"
                    elif search_group_obj.name == "General Manager":
                        if group_name != "CSO" and group_name != "CCO":
                            group_name = "GM"
                    elif search_group_obj.name == "CSO":
                        if group_name != "CCO":
                            group_name = "CSO"
                    elif search_group_obj.name == "CCO":
                        group_name = "CCO"
                    else:
                        if search_group_obj.name == "Manager":
                            if group_name != "BA" and group_name != "AM" and group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                                group_name = "MA"
                        if search_group_obj.name == "Employee":
                            if group_name != "BA" and group_name != "AM" and group_name != "GM" and group_name != "CSO" and group_name != "CCO" and group_name != "Manager":
                                group_name = "BA"
            result_emp_dict.update({"group": group_name})
            for emp in user.employee_list:
                list_emp.append(emp.id)
            result_emp_dict.update({'emp_list': list_emp})
            result_emp_list.append(result_emp_dict)
            result_dict.update({'result': result_emp_list})
        return result_dict

    @api.multi
    def search_employee_access(self):
        result_dict = {}
        view_map = False
        view_info = False
        for access in self.env["retail.view.configuration"].search([]):
            for group in access.group_ids:
                for users in group.users:
                    if users.id == self.id:
                        if access.view_map:
                            view_map = access.view_map
                        if access.view_info:
                            view_info = access.view_info
                    result_dict.update({'view_map': view_map, 'view_info': view_info})
        return result_dict

    @api.multi
    def search_users_hierarchy(self):
        _logger.info("search user hierarchy started")
        user_id = self.id
        search_obj = self.search([('id', '=', user_id)])
        group_name = ""
        result_dict = {}
        for group in search_obj.groups_id:
            search_group_obj = self.env["res.groups"].search([('id', '=', group.id)])
            if search_group_obj.name:
                if search_group_obj.name == "BA":
                    if group_name != "AM" and group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                        group_name = "BA"
                elif search_group_obj.name == "Account Manager":
                    if group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                        group_name = "AM"
                elif search_group_obj.name == "General Manager":
                    if group_name != "CSO" and group_name != "CCO":
                        group_name = "GM"
                elif search_group_obj.name == "CSO":
                    if group_name != "CCO":
                        group_name = "CSO"
                elif search_group_obj.name == "CCO":
                    group_name = "CCO"
                else:
                    if search_group_obj.name == "Manager":
                        if group_name != "BA" and group_name != "AM" and group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                            group_name = "MA"
                    if search_group_obj.name == "Employee":
                        if group_name != "BA" and group_name != "AM" and group_name != "GM" and group_name != "CSO" and group_name != "CCO" and group_name != "Manager":
                            group_name = "BA"
        result_dict.update({'user_name': search_obj.name, 'group_name': group_name})
        _logger.info(result_dict)
        return result_dict


class RetailDistanceCalulation(models.Model):
    _name = 'retail.distance.calculation'

    name = fields.Char('Name')
    start_time = fields.Datetime(String='Start Time')
    end_time = fields.Datetime(String='End Time')
    frequency = fields.Integer(String='Frequency in Minutes')

    @api.multi
    def start_beat(self, vals):
        result = {}

        today_start = datetime.now().replace(hour=9, minute=00, second=0, microsecond=0).strftime(
            tools.DEFAULT_SERVER_DATETIME_FORMAT)
        today_end = datetime.now().replace(hour=10, minute=5, second=0, microsecond=0).strftime(
            tools.DEFAULT_SERVER_DATETIME_FORMAT)
        nowstrf = datetime.now().strftime(tools.DEFAULT_SERVER_DATETIME_FORMAT)
        print( datetime.now() + timedelta(hours=5,minutes=30))
        nowstrf_time = datetime.now().strftime('%Y-%m-%d')
        notification_end_start = datetime.now().replace(hour=17, minute=00, second=0, microsecond=0).strftime(
            tools.DEFAULT_SERVER_DATETIME_FORMAT)
        print (notification_end_start)
        notification_end = datetime.now().replace(hour=18, minute=5, second=0, microsecond=0).strftime(
            tools.DEFAULT_SERVER_DATETIME_FORMAT)
        print (notification_end)
        if today_start < nowstrf < today_end:
            print ("hhhhhhhhhhhhhhhhhhhhhh")
            message='start'
        elif notification_end_start < nowstrf < notification_end:
            print ("hhhhhhhhhhhhhhhhhhhhhh")
            message='stop'
        else:
            message=''
        message='stop'
        if message=='start' or message=='stop':
            print ("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj")
            fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
            send = True
            tokens = []
            users = self.env['res.users'].search([('id', '=', 48)])
            for user in users:
                send = True
                tokens = []
                if user.user_tokens:
                    for token in user.user_tokens:
                        tokens.append(token.name)
                data_json = {"channeltype": "currentLocation",
                             "name": user.name,
                             "author_id": user.id,
                             "message": message}
                data = {'data': {'title': "Get Current Locaton",
                                 'body': "Get Current Locaton",
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                        'data': data_json,
                        'registration_ids': tokens,
                        'priority': 'high',
                        'forceShow': 'true'}
                headers = {'Authorization': fcm_authorization_key,
                           'Content-Type': 'application/json'}
                _logger.info("Input Data ------ %s" % data)
                data = json.dumps(data)
                try:
                    if send:
                        res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                        _logger.info("FCM Response ------ %s" % res.text)
                        print ("jjjjjjjjjjjjjjjjjjj")
                except:
                    raise osv.except_osv(_('Notification not sent'))
        return result

    @api.multi
    def get_retail_user_location(self, values):
        cr = self._cr
        vals ={}
        result = {}
        print (self, values, "ggggggggggggggggggggggggggggggggg")
        today_date = datetime.today()
        today_date_strformat = today_date.strftime('%Y-%m-%d')
        vals.update({'retail_user_id': self.id, 'latitude': values['latitude'],
                     'longitude': values['longitude']})
        detail_id = self.env['retail.user.location'].create(self._cr, self._uid, vals)

        return result

    @api.multi
    def calculate_user_retail_distance(self, values):
        print("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh")
        result={}
        cr=self._cr
        uid=self._uid
        _logger.info("Calculate Retail Distance")
        today_date = datetime.today()
        today_date_strformat = today_date.strftime('%Y-%m-%d')
        res = {}
        google_maps_api_key = self.env['ir.config_parameter'].get_param('google_maps_api_key')
        list_lat = []
        list_lon = []
        pre_lat = 0
        pre_lon = 0
        pre_dis = 0
        google_distance_final = 0
        pre_url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='
        users = self.env['res.users'].search([])
        for user in users:
            for loc in user.retail_user_location_id:
                if loc.date == today_date_strformat:
                    print(loc.date)
                    if list_lat and list_lon:
                        pre_lat = list_lat.pop()
                        pre_lon = list_lon.pop()
                    lat = loc.latitude
                    lon = loc.longitude
                    list_lat.append(loc.latitude)
                    list_lon.append(loc.longitude)
                    if pre_lat and pre_lon and lat and lon != 0:
                        url = pre_url + str(pre_lat) + ',' + str(pre_lon) + '&destinations=' + str(lat) + ',' + str(
                            lon) + '&key=' + google_maps_api_key
                        print(url)
                        # data = json.loads(urllib2.urlopen(url).read())
                        # _logger.info(url)
                        # _logger.info(data)
                        # if data["status"] == "OK":
                        #     if data["rows"][0]["elements"][0]["status"] == "OK":
                        #         google_distance = data["rows"][0]["elements"][0]["distance"]["value"]
                        #         if google_distance != 0:
                        #             google_distance_final = pre_dis + float(google_distance) / 1000
                        #             pre_dis += float(google_distance) / 1000
                        #         # res[user.id] = google_distance_final
                        #         print google_distance_final
                        #         user.write({'distance':google_distance_final})
                        #     else:
                        #         _logger.info("ZERO_RESULTS/ERROR")
                        # else:
                        #     _logger.info("OVER_QUERY_LIMIT/ERROR")
        # cr = self._crs
        # vals ={}
        # result = {}
        # res_dict=[]
        # # pre_dis = 0
        # google_distance_final = 0
        # print self, values, "ggggggggggggggggggggggggggggggggg"
        # today_date = datetime.today()
        # today_date_strformat = today_date.strftime('%Y-%m-%d')
        # cr.execute('''select id,latitude,longitude
        #                 from
        #                 retail_user_location
        #                 where create_date::date=%s order by id desc limit 1''',(today_date_strformat,))
        # res_dict=cr.dictfetchone()
        # print res_dict,"llllllllllllllllllllllll"
        # vals.update({'retail_user_id': self.id, 'latitude': values['latitude'],
        #              'longitude': values['longitude']})
        # if res_dict:
        #     if len(res_dict)>0:
        #         res = {}
        #         google_maps_api_key = self.pool['ir.config_parameter'].get_param(cr, self._uid, 'google_maps_api_key')
        #         pre_lat = res_dict['latitude']
        #         pre_lon =res_dict['longitude']
        #         pre_url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='
        #         lat = values['latitude']
        #         lon = values['longitude']
        #         if pre_lat and pre_lon and lat and lon != 0:
        #             url = pre_url + str(pre_lat) + ',' + str(pre_lon) + '&destinations=' + str(lat) + ',' + str(
        #                 lon) + '&key=' + google_maps_api_key
        #             data = json.loads(urllib2.urlopen(url).read())
        #             _logger.info(url)
        #             _logger.info(data)
        #             print data
        #             if data["status"] == "OK":
        #                 if data["rows"][0]["elements"][0]["status"] == "OK":
        #                     _logger.info(data["rows"][0]["elements"][0]["status"])
        #                     google_distance = data["rows"][0]["elements"][0]["distance"]["value"]
        #                     google_duration = data["rows"][0]["elements"][0]["duration"]["value"]
        #                     if google_distance != 0:
        #                     #     google_distance_final = pre_dis + float(google_distance) / 1000
        #                     #     pre_dis += float(google_duration)
        #                     # print google_distance_final,"google_distance_finalgoogle_distance_finalgoogle_distance_final"
        #                         print google_distance,"google_distancegoogle_distance"
        #                         vals.update({'distance':google_distance})
        #                 else:
        #                     _logger.info("ZERO_RESULTS/ERROR")
        #             else:
        #                 _logger.info("OVER_QUERY_LIMIT/ERROR")
        # detail_id = self.env['retail.user.location'].create(vals)

        return result

    @api.multi
    def update_status(self, values):
        current_time = datetime.now()
        if 'user_id' in values:
            user_obj = self.env['res.users'].search([('id', '=', values['user_id'])])
            if 'state' in values:
                if values['state'] == 'Start':
                    vals = {'user_id': values['user_id'], 'start_latitude': values['latitude'],
                            'start_longitude': values['longitude'], 'start_time': current_time, 'end_latitude': 0,
                            'end_longitude': 0, 'end_time': False}
                else:
                    vals = {'user_id': values['user_id'], 'end_latitude': values['latitude'],
                            'end_longitude': values['longitude'], 'end_time': current_time, 'start_time': False,
                            'start_longitude': 0, 'start_latitude': 0}
                detail_id = self.env['user.location'].create(vals)
        return True

    @api.multi
    def get_past_trends(self, user_id):
        cr = self._cr
        result = {}
        res_list = []
        start_date = datetime.today() + timedelta(days=-1)
        start_date_strformat = start_date.strftime('%Y-%m-%d')
        end_date = datetime.today() + timedelta(days=-14)
        end_date_strformat = end_date.strftime('%Y-%m-%d')
        cr.execute('''DROP FUNCTION IF EXISTS retail_user_location()''')
        query = '''CREATE OR REPLACE FUNCTION retail_user_location()
                                  RETURNS TABLE (
                                  loc_id int,
                                   s_lat numeric,
                        e_lat numeric,
                        s_long numeric,
                        e_long numeric,
                        s_time timestamp,
                        e_time timestamp,
                        usr_id int
                          ) AS
                        $func$
                        BEGIN
                           RETURN QUERY
                        SELECT
                        id as loc_id,
                        start_latitude as s_lat,
                        end_latitude as e_lat,
                        start_longitude as s_long,
                        end_longitude as e_long,
                        start_time as s_time,
                        end_time as e_time,
                        user_id as usr_id
                    FROM
                        user_location
                    where user_id=%s ;
                     --AND start_time::date >= %s
                       -- AND start_time::date <= %s;

                                 END
                                $func$ LANGUAGE plpgsql; '''
        cr.execute(query, (user_id, end_date_strformat, start_date_strformat))
        for i in range(15):
            i += 1
            week_date = datetime.today() + timedelta(days=-i)
            date_strformat = week_date.strftime('%Y-%m-%d')
            res_dict = {'date': date_strformat, 'start_time': '', 'end_time': '', 'pos_visit': 0,
                        'distance_travelled': 0}
            cr.execute(
                '''select loc_id,
                    s_lat,
                    s_long,
                    to_char(s_time::time + interval '5:30 hours','HH:MM:SS') as s_time,
                    s_time::date as date
                from retail_user_location()
                 where s_time::date= %s
                order by loc_id limit 1 ;''', (date_strformat,))
            start_time_dict = cr.dictfetchone()
            if start_time_dict:
                res_dict.update({'start_time': start_time_dict['s_time']})
            cr.execute(
                '''select loc_id,
                    e_lat,
                    e_long,
                    to_char(e_time::time + interval '5:30 hours','HH:MM:SS') as e_time,
                    e_time::date as date
                 from retail_user_location() where e_time::date=%s
                 order by loc_id DESC limit 1''', (date_strformat,))
            end_time_dict = cr.dictfetchone()
            if end_time_dict:
                res_dict.update({'end_time': end_time_dict['e_time']})
            else:
                res_dict.update({'end_time': ''})
            cr.execute('''SELECT count(visit.id) as count,
                    visit.create_uid as user_id,
                    visit.date as visit_date
                            FROM customer_visit visit
                            where visit.create_uid = %s
                            AND visit.date = %s
                            group by visit.date,visit.create_uid
                            order by visit.date desc''', (user_id, date_strformat))
            visit_count_dict = cr.dictfetchone()
            if visit_count_dict:
                res_dict.update({'pos_visit': visit_count_dict['count']})
            res_list.append(res_dict)
            cr.execute('''select
                sum(distance) as distance from retail_user_location
            where retail_user_id=%s and create_date::date=%s''', (user_id, date_strformat))
            distance_travelled = cr.dictfetchone()
            if distance_travelled:
                if distance_travelled['distance']:
                    res_dict.update({'distance_travelled': distance_travelled['distance']})
        result.update({'result': res_list})
        print(result)
        return result
