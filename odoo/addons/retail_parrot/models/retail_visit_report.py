import base64
import datetime
import logging
import os
from datetime import datetime
from odoo.tools.translate import _
from odoo import api, tools

import xlsxwriter
from odoo import api, models, fields
from datetime import datetime, date, time, timedelta
import pytz
import xlsxwriter
# from odoo.osv import fields, osv
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class PosVisitReport(models.Model):
    _name = "pos.visit.report"
    _description = "Customer Visit report"


    name = fields.Char('Report Name')
    visit_ids = fields.Many2many('customer.visit', 'customer_visit_ids', 'visit_id', 'customer_id', 'Visits')
    visit_report_download_xl = fields.Many2many('ir.attachment', 'visit_report_xl_down',
                                                 'visit_report_xl_down_id',
                                                     'attachment_id', 'Download Report', readonly=True)

    @api.multi
    def download_pos_visit_report(self,login):
        context = self._context or {}
        _logger.info("Download Pos Visit Triggered")
        attachment_obj = selfenv['ir.attachment']
        company_obj = self.env['res.users'].browse(self.id).company_id
        temp = company_obj.temp_files_path
        if not temp:
            company_write = self.env['res.company'].write([company_obj.id],
                                                               {"temp_files_path": "/home/parrot/"})
            temp = "/home/parrot/"
        pos_label = "Ad Location"
        if not os.path.exists(temp + 'Samples'):
            os.makedirs(temp + 'Samples')
        temp = temp + 'Samples/'
        if cr.dbname == "centum":
            default_masters = (['User'],
                               [pos_label],
                               ['Address'],
                               ['Email'],
                               ['Mobile'],
                               ['POS Latitude'],
                               ['POS Longitude'],
                               ['Visit Date'],
                               ['Question'],
                               ['Answer'],
                               ['Description'],
                               ['Image name'],
                               ['Image URL'],
                               ['Uploaded Date'])
        else:
            default_masters = (['User'],
                               [pos_label],
                               ['Address'],
                               ['Pos Id'],
                               ['Mobile'],
                               ['POS Latitude'],
                               ['POS Longitude'],
                               ['Visit Date'],
                               ['Question'],
                               ['Answer'],
                               ['Description'],
                               ['Image name'],
                               ['Image URL'],
                               ['Uploaded Date'],)
        sample_xls = xlsxwriter.Workbook(temp + 'sample_document.xls')
        sheet = sample_xls.add_worksheet()
        border_format = sample_xls.add_format({'border': 2,
                                               'bold': True,
                                               'bg_color': '#90af48'})
        row = 0
        col = 0
        for master, in (default_masters):
            sheet.set_row(0, 50)
            sheet.write(row, col, master, border_format)
            col += 1
        row += 1
        res_ids = self.env['pos.visit.report'].search('id','in', self.ids)
        for res in res_ids:
            for visit in res.visit_ids:
                att_ids = attachment_obj.search([('res_id', '=', visit.id), ('res_model', '=', 'customer.visit')])
                col = 0
                sheet.write(row, col, visit.create_uid.name)
                col += 1
                sheet.write(row, col, visit.name.name)
                col += 1
                sheet.write(row, col, visit.name.address)
                col += 1
                if cr.dbname == "centum":
                    sheet.write(row, col, visit.name.email_id)
                    col += 1
                else:
                    sheet.write(row, col, visit.name.pos_id)
                    col += 1
                sheet.write(row, col, visit.name.mobile)
                col += 1
                sheet.write(row, col, visit.name.latitude)
                col += 1
                sheet.write(row, col, visit.name.longitude)
                col += 1
                sheet.write(row, col, visit.date)
                col += 1
                count = 0
                for answer in visit.answers:
                    sheet.write(row, col, answer.name.name)
                    col += 1
                    sheet.write(row, col, answer.choosen_answer)
                    col += 1
                    description = answer.description
                    if not description:
                        description = ""
                    sheet.write(row, col, description)
                    col -= 2
                    row += 1
                    count += 1
                col += 3
                for att in attachment_obj.browse(att_idsontext):
                    col_temp = col
                    if att:
                        sheet.write(row - count, col, att.name)
                        col += 1
                        sheet.write(row - count, col, att.url)
                        col += 1
                        timezone = pytz.timezone(context.get('tz') or 'UTC')
                        create_date = pytz.UTC.localize(
                            datetime.strptime(att.create_date, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(
                            timezone).strftime("%Y-%m-%d %H:%M:%S")
                        sheet.write(row - count, col, create_date)
                        col = col_temp
                    row += 1
        sample_xls.close()
        with open(temp + 'sample_document.xls', 'rb') as doc:
            encoded_data = base64.b64encode(doc.read())
            document_vals = {'name': 'visit_report_file.xls',
                             'datas': encoded_data,
                             'datas_fname': 'visit_report_file.xls',
                             'res_model': 'pos.visit.report',
                             'res_id': ids[0],
                             'type': 'binary'}
        ir_id = self.env['ir.attachment'].create(document_vals)
        self.write(ids[0], {'visit_report_download_xl': [(6, 0, [ir_id])]})



