import datetime
import logging
from datetime import datetime

import pytz
from odoo import fields, models, api,_
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class CustomerVisit(models.Model):
    _name = "customer.visit"
    _description = "Customer Visit Details"
    _order = "create_date desc"

    @api.multi
    def _get_attachment_names(self, name, args):
        context = self._context or {}
        if not self.ids: return {False}
        res = {}
        attachment_obj = self.env['ir.attachment']
        for record in self.env['customer.visit'].search([('id','in', self.ids)]):
            att_ids = attachment_obj.search([('res_id', '=', record.id), ('res_model', '=', 'customer.visit')])
            display_text = '''<html>
                                <body>
                                    <table class="table table-condensed">
                                        <tr>
                                            <th>Name</th>
                                            <th>URL</th>
                                            <th>Taken On</th>
                                            <th>Address</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th>GPS Status</th>
                                        </tr>'''
            for att in attachment_obj.browse(att_ids):
                timezone = pytz.timezone(context.get('tz') or 'UTC')
                create_date = pytz.UTC.localize(
                    datetime.strptime(att.create_date, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(
                    timezone).strftime("%Y-%m-%d %H:%M:%S")
                display_text += '<tr><td>' + str(att.name) + '</td>'
                if att.url:
                    display_text += '<td><a href="' + str(att.url) + '" target="_blank">' + str(
                        att.url) + '</a></td>'
                else:
                    display_text += '<td>''</td>'
                display_text += '<td>' + str(create_date) + '</td>'
                display_text += '<td>' + str(att.address) + '</td>'
                display_text += '<td>' + str(att.latitude) + '</td>'
                display_text += '<td>' + str(att.longitude) + '</td>'
                display_text += '<td>' + str(att.is_lat_long) + '</td></tr>'
            display_text += '''
                                    </table>
                                </body>
                            </html>'''
            res[record.id] = display_text
        return res

    @api.multi
    def _get_count(self, name, args):
        if not self.ids: return {False}
        res = {}
        attachment_obj = self.env['ir.attachment']
        for record in self.env['customer.visit'].search([('id','in', self.ids)]):
            att_ids = attachment_obj.search([('res_id', '=', record.id), ('res_model', '=', 'customer.visit')])
            res[record.id] = len(att_ids)
        return res

    name = fields.Many2one('retail.customer', 'POS')
    date = fields.Date('Visit Date',default=fields.date.today())
    answers = fields.One2many('visit.answers', 'visit_id', 'Visit Questions',default='_get_default_answers')
    attachments_info = fields.Binary( string='Attachments Info')
    attachments_count = fields.Integer(string='No of Attachments')
    manager_id = fields.Many2one('res.users', related='create_uid.manager_id', string="Manager", readonly=True,
                                  store=True)
    latitude = fields.Float('Latitude', digits=(6, 10))
    longitude = fields.Float('Longitude', digits=(6, 10))
    is_lat_long = fields.Boolean(string='GPS status', help='Shows that Lat-Long Data was received from phone')
    tracker = fields.Boolean(string='Tracker', help='Current POS Visit is at Nearby Location or Not')

    @api.multi
    def _get_default_answers(self):
        company_id = self.env['res.users'].search([('id','in', self.id)]).company_id
        if company_id:
            if company_id.retail_questions_set:
                vals = []
                for question in company_id.retail_questions_set.questions:
                    vals += [{'name': question.id}]
                return vals
            else:
                return []
        return []

    # _defaults = {
    #     # 'answers':_get_default_answers,
    #     'date': lambda obj, cr, uid, context: fields.date.today(),
    # }

    @api.multi
    def new_create_visit(self, vals):
        '''
        vals = {"customer_id":3 ,
                   "visit_date":'2016-02-11'}
        '''
        visit_id = False
        if 'customer_id' in vals and vals['customer_id'] and "visit_date" in vals and vals['visit_date']:
            visit_id = self.create({'name': vals['customer_id'], 'date': vals['visit_date']})
        return {"visit_id": visit_id}

    @api.multi
    def create_visit(self, vals):
        '''
        vals = {"customer_id":1 ,
                "visit_date":'2016-02-11',
                'answers':[{"id":8,"answer":'no','description':'Phenominal Bounce/man'},
                           {"id":9,"answer":'option_b'},{"id":12,"answer":'Heyyy'}]}
         - or -
        vals = {"customer_id":1 ,
                "visit_date":'2016-06-06',
                'latitude':'12.0000;,
                'longitude':'80.0000;,
                'answers':[{"id":8,"answer":'no','description':'Phenominal Bounce/man'},
                           {"id":9,"answer":'option_b'},{"id":12,"answer":'Heyyy'}]}
        '''

        visit_answers = self.env['visit.answers']
        visit_id = False
        _logger.info("From Mobile Client - Create Visit triggered")
        _logger.info(vals)
        if 'customer_id' in vals and vals['customer_id'] and 'visit_date' in vals and vals['visit_date'] and \
                'longitude' in vals and vals['longitude'] and 'latitude' in vals and vals['latitude']:
            _logger.info("longitude : %s and latitude : %s" % (vals['longitude'], vals['latitude']))
            visit_id = self.create({'name': vals['customer_id'], 'date': vals['visit_date'],
                                             'longitude': vals['longitude'], 'latitude': vals['latitude']})
            if (float(unicode(vals['longitude'])) and float(unicode(vals['latitude']))) == 0:
                _logger.info("With lat-long has 0")
                self.write([visit_id], {"is_lat_long": False})
            else:
                _logger.info("With lat-long values")
                self.write([visit_id], {"is_lat_long": True})
        # Need to removed after every app updated with latitude and longitude fields
        # Last updated on 05 April, 2018
        elif 'customer_id' in vals and vals['customer_id'] and "visit_date" in vals and vals['visit_date']:
            _logger.info("Without lat-long")
            visit_id = self.create({'name': vals['customer_id'], 'date': vals['visit_date']})
            self.write([visit_id], {"is_lat_long": False})
        # Tracker information:Return 'True' if Latitude and Longitude of Customer Visit is as same as Retail Customer
        if visit_id:
            retail_customer_id = self.env['customer.visit'].browse(visit_id).name
            if 'longitude' in vals and vals['longitude'] and 'latitude' in vals and vals['latitude']:
                round_retail_lat = round(retail_customer_id.latitude, 5)
                round_retail_lon = round(retail_customer_id.longitude, 5)
                round_visit_lat = round(vals['latitude'], 5)
                round_visit_lon = round(vals['longitude'], 5)
                if round_retail_lat - 0.00005 <= round_visit_lat <= round_retail_lat + 0.00005 and round_retail_lon - 0.00005 <= round_visit_lon <= round_retail_lon + 0.00005:
                    _logger.info("Tracking True")
                    _logger.info("Retail : %s %s Visit : %s %s", round_retail_lat, round_visit_lat, round_retail_lon,
                                 round_visit_lon)
                    self.write([visit_id], {"tracker": True})
                else:
                    _logger.info("Tracking False")
                    self.write([visit_id], {"tracker": False})
        if 'answers' in vals and vals['answers'] and visit_id:
            for answer in vals['answers']:
                question = self.env['visit.questions'].browse(answer['id'])
                if question:
                    if question.type == 'multiple':
                        visit_answers.create({'name': answer['id'], 'answer': answer['answer'],
                                                       'visit_id': visit_id, 'type': 'multiple', })
                    elif question.type == 'yes_no':
                        visit_answers.create({'name': answer['id'], 'yes_no': answer['answer'],
                                                       'visit_id': visit_id,
                                                       'type': 'yes_no'})
                    elif question.type == 'numeric':
                        visit_answers.create({'name': answer['id'], 'description': answer['answer'],
                                                       'visit_id': visit_id, 'type': 'numeric'})
                    elif question.type == 'descriptive':
                        visit_answers.create({'name': answer['id'], 'description': answer['answer'],
                                                       'visit_id': visit_id, 'type': 'descriptive'})
                    elif question.type == 'multiple_loop':
                        visit_answers.create({'name': answer['id'], 'answer': answer['answer'],
                                                       'visit_id': visit_id, 'type': 'multiple_loop', })
                    elif question.type == 'yes_no_loop':
                        visit_answers.create({'name': answer['id'], 'yes_no': answer['answer'],
                                                       'visit_id': visit_id, 'description': answer['description'],
                                                       'type': 'yes_no_loop'})
                if question:
                    if answer['child_question']:
                        if 'answerChild' in answer:
                            visit_answers.create({'name': answer['child_question_id'],
                                                           'description': answer['answerChild'],
                                                           'visit_id': visit_id,
                                                           'type': answer['child_question_type']})
        _logger.info(visit_id)
        return {"visit_id": visit_id}

    @api.multi
    def hsil_create_visit(self, vals):
        '''
        vals = {"customer_id":1 ,
                "visit_date":'2016-02-11',
                'answers':[{"id":8,"answer":'no','description':'Phenominal Bounce/man'},
                           {"id":9,"answer":'option_b'},{"id":12,"answer":'Heyyy'}]}
         - or -
        vals = {"customer_id":1 ,
                "visit_date":'2016-06-06',
                'latitude':'12.0000;,
                'longitude':'80.0000;,
                'answers':[{"id":8,"answer":'no','description':'Phenominal Bounce/man'},
                           {"id":9,"answer":'option_b'},{"id":12,"answer":'Heyyy'}]}
        '''
        visit_answers = self.env['visit.answers']
        visit_id = False
        _logger.info("From Mobile Client - Create Visit triggered")
        _logger.info(vals)
        if 'customer_id' in vals and vals['customer_id'] and 'visit_date' in vals and vals['visit_date'] and \
                'longitude' in vals and vals['longitude'] and 'latitude' in vals and vals['latitude']:
            _logger.info("longitude : %s and latitude : %s" % (vals['longitude'], vals['latitude']))
            visit_id = self.create({'name': vals['customer_id'], 'date': vals['visit_date'],
                                             'longitude': vals['longitude'], 'latitude': vals['latitude']})
            if (float(unicode(vals['longitude'])) and float(unicode(vals['latitude']))) == 0:
                _logger.info("With lat-long has 0")
                self.write([visit_id], {"is_lat_long": False})
            else:
                _logger.info("With lat-long values")
                self.write([visit_id], {"is_lat_long": True})
        # Need to removed after every app updated with latitude and longitude fields
        # Last updated on 05 April, 2018
        elif 'customer_id' in vals and vals['customer_id'] and "visit_date" in vals and vals['visit_date']:
            _logger.info("Without lat-long")
            visit_id = self.create({'name': vals['customer_id'], 'date': vals['visit_date']})
            self.write([visit_id], {"is_lat_long": False})
        # Tracker information:Return 'True' if Latitude and Longitude of Customer Visit is as same as Retail Customer
        if visit_id:
            retail_customer_id = self.env['customer.visit'].browse(visit_id).name
            if 'longitude' in vals and vals['longitude'] and 'latitude' in vals and vals['latitude']:
                round_retail_lat = round(retail_customer_id.latitude, 5)
                round_retail_lon = round(retail_customer_id.longitude, 5)
                round_visit_lat = round(vals['latitude'], 5)
                round_visit_lon = round(vals['longitude'], 5)
                if round_retail_lat - 0.00005 <= round_visit_lat <= round_retail_lat + 0.00005 and round_retail_lon - 0.00005 <= round_visit_lon <= round_retail_lon + 0.00005:
                    _logger.info("Tracking True")
                    _logger.info("Retail : %s %s Visit : %s %s", round_retail_lat, round_visit_lat, round_retail_lon,
                                 round_visit_lon)
                    self.write([visit_id], {"tracker": True})
                else:
                    _logger.info("Tracking False")
                    self.write([visit_id], {"tracker": False})

        if 'answers' in vals and vals['answers'] and visit_id:
            for answer in vals['answers']:
                seq = answer['sequence']
                question_sr = self.env['visit.questions'].search([('sequence', '=', seq)])
                question_obj = self.env['visit.questions'].browse(question_sr)
                for question in question_obj:
                    if question.sequence == 1:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type,
                                                           'hsil_product': [[6, 0, [answer['answer']]]]})
                        else:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type,
                                                           })

                    if question.sequence == 2:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create({'name': question.id, 'description': answer['description'],
                                                           'visit_id': visit_id, 'type': question.type,
                                                           'hsil_brand': [[6, 0, [answer['answer']]]]})
                        else:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type})

                    if question.sequence == 3:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type,
                                                           'hsil_brand': [[6, 0, [answer['answer']]]]})
                        else:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 4:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  'description': answer['answer']})
                        else:
                            visit_answers.create({'name': question.id, 'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 5:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type,
                                                           'description': answer['answer']})
                        else:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 6:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  'description': answer['answer']})
                        else:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 7:
                        if 'answer1' in answer and answer['answer1'] and 'answer' in answer and answer[
                            'answer'] and 'answer2' in answer and answer['answer2']:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  'yes_no': answer['answer'],
                                                  'hsil_product': [[6, 0, [answer['answer1']]]],
                                                  'description': answer['answer2']})
                        elif 'answer1' in answer and answer['answer1'] == '':
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  'yes_no': answer['answer']})
                        else:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 8:
                        if 'answer1' in answer and answer['answer1'] and 'answer' in answer and answer[
                            'answer'] and 'answer2' in answer and answer['answer2']:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  'yes_no': answer['answer'],
                                                  'hsil_product': [[6, 0, [answer['answer1']]]],
                                                  'description': answer['answer2']})
                        elif 'answer1' in answer and answer['answer1'] == '':
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  'yes_no': answer['answer'],
                                                  })
                        else:
                            visit_answers.create({'name': question.id, 'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 9:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create({'name': question.id, 'numeric': answer['description'],
                                                           'visit_id': visit_id, 'type': question.type,
                                                           'description': answer['answer']})
                        else:
                            visit_answers.create({'name': question.id, 'numeric': 0,
                                                           'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 10:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  'description': answer['answer']})
                        else:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 11:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type,
                                                           'description': answer['answer']})
                        else:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 12:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type,
                                                           'branding_activity': [[6, 0, [answer['answer']]]]})
                        else:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 13:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  'description': answer['answer']})
                        else:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 14:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type,
                                                           'branding_activity': [[6, 0, [answer['answer']]]]})
                        else:
                            visit_answers.create({'name': question.id,
                                                           'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 15:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create({'name': question.id, 'numeric': answer['description'],
                                                           'visit_id': visit_id, 'type': question.type,
                                                           'description': answer['answer']})
                        else:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 16:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  'yes_no': answer['answer'],
                                                  'description': answer['description']})
                        elif 'answer1' not in answer:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  })

                    if question.sequence == 17:
                        if 'answer' in answer and answer['answer'] and 'answer1' in answer and answer['answer1']:
                            visit_answers.create({'name': question.id, 'yes_no': answer['answer'],
                                                           'visit_id': visit_id, 'type': question.type,
                                                           'hsil_product': [[6, 0, [answer['answer1']]]],
                                                           'description': answer['answer2']})
                        elif 'answer1' in answer and answer['answer1'] == '':
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  'yes_no': answer['answer'],
                                                  })
                        else:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 18:
                        if 'answer' in answer and answer['answer']:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  'service_rate': answer['answer']})
                        else:
                            visit_answers.create({'name': question.id, 'visit_id': visit_id, 'type': question.type})
                    if question.sequence == 19:
                        if 'answer' in answer and answer['answer'] and not answer['answer1']:
                            visit_obj = visit_answers.create({'name': question.id, 'visit_id': visit_id,
                                                              'type': question.type,
                                                              'description': answer['answer']})

                        elif 'answer1' in answer and answer['answer1'] and 'answer' in answer and answer['answer']:
                            visit_obj = visit_answers.create(
                                                             {'name': question.id, 'visit_id': visit_id,
                                                              'type': question.type})
                            visit_obj_sr = self.env['visit.answers'].search([('id', '=', visit_obj)])
                            visit_obj_br = self.env['visit.answers'].browse(visit_obj_sr)
                            for num in answer['number']:
                                visit_obj_br.write({'name': question.id, 'visit_id': visit_id,
                                                    'type': question.type,
                                                    'description': answer['answer'],
                                                    'complaint_count' + num: answer['number'][num],
                                                    'service_complaint': [[6, 0, [answer['answer1']]]]})
                        elif 'answer' not in answer:
                            visit_obj = visit_answers.create(
                                                             {'name': question.id, 'visit_id': visit_id,
                                                              'type': question.type})
                            visit_obj_sr = self.env['visit.answers'].search([('id', '=', visit_obj)])
                            visit_obj_br = self.env['visit.answers'].browse(visit_obj_sr)
                            for num in answer['number']:
                                visit_obj_br.write({'name': question.id, 'visit_id': visit_id,
                                                    'type': question.type,
                                                    'complaint_count' + num: answer['number'][num],
                                                    'service_complaint': [[6, 0, [answer['answer1']]]]})
                        else:
                            visit_answers.create(
                                                 {'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                  })

        _logger.info(visit_id)
        return {"visit_id": visit_id}


    # Creating empty visit for centum by Parrot Tech Team
    @api.multi
    def create_empty_visit(self, vals):
        visit_id = False
        if 'customer_id' in vals and vals['customer_id'] and "visit_date" in vals and vals['visit_date']:
            visit_id = self.create({'name': vals['customer_id'], 'date': vals['visit_date']})
        return {"visit_id": visit_id}

    @api.multi
    def update_visit(self, vals):
        '''vals = {"visit_id":23 ,
           'answers':[{"id":8,"answer":'no','description':'Fluerrr'},
                      {"id":9,"answer":'option_c'},
                      {"id":12,"answer":'Shineyo'}]}'''
        _logger.info("Update Visit Triggered")
        _logger.info(vals)
        visit_answers = self.env['visit.answers']
        visit_id = False
        if 'visit_id' in vals and vals['visit_id']:
            visit_id = vals['visit_id']
        if 'answers' in vals and vals['answers'] and visit_id:
            for answer in vals['answers']:
                question = self.env['visit.questions'].browse(answer['id'])
                if question:
                    if question.type == 'multiple':
                        visit_answers.create({'name': answer['id'], 'answer': answer['answer'],
                                                       'visit_id': visit_id, 'type': 'multiple'})
                    elif question.type == 'yes_no':
                        visit_answers.create({'name': answer['id'], 'yes_no': answer['answer'],
                                                       'visit_id': visit_id, 'description': answer['description'],
                                                       'type': 'yes_no'})
                    elif question.type == 'numeric':
                        visit_answers.create({'name': answer['id'], 'description': answer['answer'],
                                                       'visit_id': visit_id, 'type': 'numeric'})
                    elif question.type == 'descriptive':
                        visit_answers.create({'name': answer['id'], 'description': answer['answer'],
                                                       'visit_id': visit_id, 'type': 'descriptive'})
                    elif question.type == 'multiple_loop':
                        visit_answers.create({'name': answer['id'], 'answer': answer['answer'],
                                                       'visit_id': visit_id, 'type': 'multiple_loop'})
                    elif question.type == 'yes_no_loop':
                        visit_answers.create({'name': answer['id'], 'yes_no': answer['answer'],
                                                       'visit_id': visit_id, 'description': answer['description'],
                                                       'type': 'yes_no_loop'})
                if question:
                    if answer['answerChild']:
                        if 'answerChild' in answer:
                            visit_answers.create({'name': answer['child_question_id'],
                                                           'description': answer['answerChild'],
                                                           'visit_id': visit_id,
                                                           'type': answer['child_question_type']})
        _logger.info(visit_id)
        return {"visit_id": visit_id}

    # Creating visit editing function for centum by Parrot Tech Team
    @api.multi
    def write_visit(self, vals):
        visit_id = False
        if 'visit_id' in vals and vals['visit_id']:
            visit_id = vals['visit_id']
        if 'answers' in vals and vals['answers'] and visit_id:
            customer_visits = self.env['customer.visit'].browse(visit_id)
            for visit_answer in customer_visits.answers:
                for answer in vals['answers']:
                    question = self.env['visit.questions'].browse( answer['id'])
                    if question == visit_answer.name:
                        _logger.info("question,type,id %s %s %s" % (question, question.type, question.id))
                        _logger.info("answer,type,id %s %s %s %s" % (
                            visit_answer, visit_answer.type, visit_answer.id, visit_answer.name))
                        if question.type == 'multiple':
                            if answer['answer']:
                                visit_answer.write({'name': answer['id'], 'answer': answer['answer'],
                                                    'visit_id': visit_id, 'type': 'multiple'})
                        elif question.type == 'yes_no':
                            if answer['answer']:
                                visit_answer.write({'name': answer['id'], 'yes_no': answer['answer'],
                                                    'visit_id': visit_id, 'description': answer['description'],
                                                    'type': 'yes_no'})
                        elif question.type == 'numeric':
                            if answer['answer']:
                                visit_answer.write({'name': answer['id'], 'description': answer['answer'],
                                                    'visit_id': visit_id, 'type': 'numeric'})
                        elif question.type == 'descriptive':
                            if answer['answer']:
                                visit_answer.write({'name': answer['id'], 'description': answer['answer'],
                                                    'visit_id': visit_id, 'type': 'descriptive'})
                        elif question.type == 'yes_no_loop':
                            if answer['answer']:
                                visit_answer.write({'name': answer['id'], 'yes_no': answer['answer'],
                                                    'visit_id': visit_id, 'description': answer['description'],
                                                    'type': 'yes_no_loop'})
                        elif question.type == 'multiple_loop':
                            if answer['answer']:
                                visit_answer.write({'name': answer['id'], 'answer': answer['answer'],
                                                    'visit_id': visit_id, 'type': 'multiple_loop'})
        return {"visit_id": visit_id}

    @api.multi
    def hsil_write_visit(self, vals):
        visit_id = False
        if 'visit_id' in vals and vals['visit_id']:
            visit_id = vals['visit_id']
        if 'answers' in vals and vals['answers'] and visit_id:
            customer_visits = self.env['customer.visit'].browse(visit_id)
            for visit_answer in customer_visits.answers:
                for answer in vals['answers']:
                    question_sr = self.env['visit.questions'].search(
                                                                          [('sequence', '=', answer['sequence'])])
                    question = self.env['visit.questions'].browse(question_sr)
                    if question == visit_answer.name:
                        _logger.info("question,type,id %s %s %s" % (question, question.type, question.id))
                        _logger.info("answer,type,id %s %s %s %s" % (
                            visit_answer, visit_answer.type, visit_answer.id, visit_answer.name))
                        if question.sequence == 1:
                            if 'answer' in answer and answer['answer']:
                                visit_answer.write({'name': question.id, 'hsil_product': [[6, 0, [answer['answer']]]],
                                                    'visit_id': visit_id, 'type': question.type})
                            else:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type})
                        if question.sequence == 2:
                            if 'answer' in answer and answer['answer'] and 'description' in answer and answer['description']:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    'description': answer['description'],
                                                    'hsil_brand': [[6, 0, answer['answer']]]})
                            elif 'answer' in answer and answer['answer'] and 'description' not in answer:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    'hsil_brand': [[6, 0, answer['answer']]]})
                            else:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    })
                        if question.sequence == 3:
                            if 'answer' in answer and answer['answer']:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    'hsil_brand': [[6, 0, answer['answer']]]})
                            else:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    })
                        if question.sequence == 4:
                            if 'answer' in answer and answer['answer']:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    'description': answer['answer']})
                            else:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    })
                        if question.sequence == 5:
                            if 'answer' in answer and answer['answer']:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    'description': answer['answer']})
                            else:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    })
                        if question.sequence == 6:
                            visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                'description': answer['answer']})
                        if question.sequence == 7:
                            if 'answer1' in answer and answer['answer1'] and 'answer' in answer and answer[
                                'answer'] and 'answer2' in answer and answer['answer2']:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    'yes_no': answer['answer'],
                                                    'hsil_product': [[6, 0, [answer['answer1']]]],
                                                    'description': answer['answer2']})
                            elif 'answer1' in answer and answer['answer1'] == '':
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    'yes_no': answer['answer'],
                                                    })
                        if question.sequence == 8:
                            if 'answer1' in answer and answer['answer1'] and 'answer' in answer and answer[
                                'answer'] and 'answer2' in answer and answer['answer2']:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    'yes_no': answer['answer'],
                                                    'hsil_product': [[6, 0, [answer['answer1']]]],
                                                    'description': answer['answer2']})
                            elif 'answer1' in answer and answer['answer1'] == '':
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    'yes_no': answer['answer'],
                                                    })
                        if question.sequence == 9:
                            if 'answer' in answer and answer['answer']:
                                visit_answer.write({'name': question.id, 'numeric': answer['description'],
                                                    'visit_id': visit_id, 'type': question.type,
                                                    'description': answer['answer']})
                            else:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    })
                        if question.sequence == 10:
                            if 'answer' in answer and answer['answer']:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    'description': answer['answer']})
                            else:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    })
                        if question.sequence == 11:
                            if 'answer' in answer and answer['answer']:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    'description': answer['answer']})
                            else:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    })
                        if question.sequence == 12:
                            if 'answer' in answer and answer['answer']:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    'branding_activity': [[6, 0, [answer['answer']]]]})
                            else:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    })
                        if question.sequence == 13:
                            if 'answer' in answer and answer['answer']:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    'description': answer['answer']})
                            else:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    })
                        if question.sequence == 14:
                            if 'answer' in answer and answer['answer']:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    'branding_activity': [[6, 0, [answer['answer']]]]})
                            else:
                                visit_answer.write({'name': question.id,
                                                    'visit_id': visit_id, 'type': question.type,
                                                    })
                        if question.sequence == 15:
                            if 'answer' in answer and answer['answer']:
                                visit_answer.write({'name': question.id, 'numeric': answer['description'],
                                                    'visit_id': visit_id, 'type': question.type,
                                                    'description': answer['answer']})
                            else:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    })
                        if question.sequence == 16:
                            if 'answer' in answer and answer['answer'] and 'description' in answer and answer[
                                'description']:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    'yes_no': answer['answer'],
                                                    'description': answer['description']})
                            elif 'answer1' not in answer:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    'yes_no': answer['answer'],
                                                    })
                        if question.sequence == 17:
                            if 'answer' in answer and answer['answer'] and 'answer1' in answer and answer['answer1']:
                                visit_answer.write({'name': question.id, 'yes_no': answer['answer'],
                                                    'visit_id': visit_id, 'type': question.type,
                                                    'hasil_product': [[6, 0, answer['answer1']]],
                                                    'description': answer['answer2']})
                            elif 'answer1' in answer and answer['answer1'] == '':
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    'yes_no': answer['answer'],
                                                    })
                        if question.sequence == 18:
                            if 'answer' in answer and answer['answer']:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    'service_rate': answer['answer']})
                            else:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    })
                        if question.sequence == 19:
                            if 'answer' in answer and answer['answer'] and not answer['answer1']:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id,
                                                    'type': question.type,
                                                    'description': answer['answer']})
                            else:
                                visit_answer.write({'name': question.id, 'visit_id': visit_id, 'type': question.type,
                                                    })

        return {"visit_id": visit_id}

    # Deleting Empty POS Visits - Created by Parrot Tech Team
    @api.multi
    def _unlink_cron(self):
        _logger.info("Cron job for deleting POS Visits has no answers and attachments")
        for records in self.search([]):
            if not records.answers and records.attachments_count == 0:
                _logger.info("Deleted %s Because it has no answers and attachments", records)
                records.unlink()
        return True

class VisitQuestions(models.Model):
    _name = 'visit.questions'
    _description = "Customer Visit Questions"
    _order = 'sequence'

    type = fields.Selection([('descriptive', 'Descriptive'),
                              ('yes_no', 'Yes Or No'),
                              ('numeric', 'Numeric'),
                              ('selection', 'Selection'),
                              ('n_description', 'N-Description'),
                              ('multiple', 'Multiple Options'),
                              ('select_multiple', 'Select Multiples'),
                              ('multiple_loop', 'Multiple with Loop'),
                              ('yes_no_loop', 'Yes/No with Loop')], 'Type of Question', required=True)
    option_a = fields.Char('Option A')
    option_b = fields.Char('Option B')
    option_c = fields.Char('Option C')
    option_d = fields.Char('Option D')
    option_multi_a = fields.Many2one('visit.answer', 'Option A')
    option_multi_b = fields.Many2one('visit.answer', 'Option B')
    option_multi_c = fields.Many2one('visit.answer', 'Option C')
    option_multi_d = fields.Many2one('visit.answer', 'Option D')
    description_type = fields.Selection([('always', 'Always'),
                                          ('yes', 'Yes'),
                                          ('no', 'No'),
                                          ('never', 'Never')], 'Descriptive Type', default='never')
    name = fields.Char('Question', required=True)
    sequence = fields.Integer('Sequence', required=True,
                               help="Gives the sequence order.",default=lambda *a: 1)

    option_multi_yes = fields.Many2one('visit.answer', 'Yes')
    option_multi_no = fields.Many2one('visit.answer', 'No')
    yes_heading = fields.Char('Yes Heading')
    no_heading = fields.Char('No Heading')
    question_set = fields.Many2many('visit.questions.set', 'question_set_rel', 'set_id', 'question_id',
                                     'Question Set')
    category = fields.Many2one('visit.questions.category', 'Question Category')
    input_fields = fields.Selection([('alpha_num', 'Alpha Numerical'),
                                      ('num_only', 'Numerical only')], 'Input Fields',default='alpha_num')
    child_question = fields.Many2one('visit.questions', 'Child Questions')
    numeric = fields.Integer('Numeric')
    description = fields.Text('Description')


#     _defaults = {
#         'description_type': 'never',
#         'sequence': lambda *a: 1,
#         'input_fields': 'alpha_num',
#     }

    @api.multi
    def get_questions(self, customer_id):
        _logger.info("Get Question Triggered")
        res = {}
        records = []
        pos = self.env['retail.customer']
        if pos.search([('id', '=', customer_id)]):
            company_id = self.env['res.users'].browse(uid).company_id
            questions_set = False
            if company_id:
                if company_id.retail_questions_set:
                    questions_set = company_id.retail_questions_set
            pos_obj = pos.browse(customer_id)
            if pos_obj.questions_set:
                questions_set = pos_obj.questions_set
            questions = [x.id for x in questions_set.questions]
            # for question in self.browse(cr,uid,questions):
            records = self.read(questions, ['name', 'type', 'option_a', 'option_b', 'option_c', 'option_d',
                                                     'option_multi_a', 'option_multi_b', 'option_multi_c',
                                                     'option_multi_d', 'option_multi_yes', 'option_multi_no',
                                                     'yes_heading', 'no_heading', 'category',
                                                     'description_type', 'input_fields', 'child_question'],
                                )
            visits = self.env['customer.visit'].search([('name', '=', customer_id)],
                                                            order='create_date DESC')
            for visit in self.env['customer.visit'].browse(visits):
                for answer in visit.answers:
                    if answer.name.id in questions:
                        if answer.name.id in res:
                            res[answer.name.id].append({'visit_date': visit.date, 'answer': answer.choosen_answer})
                        else:
                            res[answer.name.id] = [{'visit_date': visit.date, 'answer': answer.choosen_answer}]
            for record in records:
                if record['id'] in res:
                    record['previous_answers'] = [res[record['id']][0]]
                else:
                    record['previous_answers'] = []
            for record in records:
                if record['child_question']:
                    child_question = self.env['visit.questions'].browse(record['id']).child_question
                    record['child_question_id'] = child_question.id
                    record['child_question_name'] = child_question.name
                    record['child_question_type'] = child_question.type
        _logger.info(records)
        return {'records': records}

    @api.multi
    def get_question_set_common(self, type, group, pos_visit_id):
        visit_last = False
        if pos_visit_id != 0:
            _logger.info(pos_visit_id)
            visit_last = self.env['customer.visit'].search([('id', '=', pos_visit_id)])
            last_ans = [x for x in visit_last.answers]
        quest_set = self.env['visit.questions.set'].search([('name', '=', type)])
        question_list = []
        activity_obj = self.env['branding.activity'].search([])
        activity_list = []
        for act in activity_obj:
            activity_dict = {}
            activity_dict.update({'id': act.id, 'name': act.name})
            activity_list.append(activity_dict)
        product = self.env['question.product.config'].search([('group', '=', group)])
        prod_list = []
        for prod in product:
            prod_dict = {}
            prod_dict.update({'id': prod.id, 'name': prod.name})
            prod_list.append(prod_dict)
        count = 0
        for ques in quest_set.questions:

            question_dict = {'name': '', 'sequence': '', 'type': '', 'hsil_product': '', 'answer_field': '',
                             'previous_answer': ''}
            if ques.sequence == 1:
                if visit_last:
                    prev_ans_list = []
                    for x in last_ans[count].hsil_product:
                        prev_ans_dict = {'id': x.id, 'name': x.name}
                        prev_ans_list.append(prev_ans_dict)
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': prod_list, 'answer_field': 'hsil_product',
                         'previous_answer': prev_ans_list})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': prod_list, 'answer_field': 'hsil_product'})
            if ques.sequence == 2:
                product = self.env['question.product.config'].search([('group', '=', group)])
                prod_temp_lst = []
                for id in product:
                    brand_obj = self.env['hsil.product.brand'].search([('product', '=', id.id)])
                    brand_list = []
                    prod_temp_dict = {}
                    for brand in brand_obj:
                        brand_dict = {}
                        brand_dict.update({'id': brand.id, 'name': brand.name})
                        brand_list.append(brand_dict)
                        prod_temp_dict = {'id': id.id, 'name': id.name, 'brand': brand_list}
                    prod_temp_lst.append(prod_temp_dict)
                if visit_last:
                    prev_ans_list = []
                    for x in last_ans[count].hsil_brand:
                        prev_ans_dict = {'id': x.id, 'name': x.name}
                        prev_ans_list.append(prev_ans_dict)
                    prev_ans_dict = {'hsil_product': prev_ans_list, 'description': ''}
                    if last_ans[count].description:
                        prev_ans_dict.update({'description': last_ans[count].description})
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': prod_temp_lst, 'answer_field': 'hsil_brand',
                         'previous_answer': prev_ans_dict})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': prod_temp_lst, 'answer_field': 'hsil_brand'})
            if ques.sequence == 3:
                pro_list = []
                for id in product:
                    pro_list.append(id.id)
                brand = self.env['hsil.product.brand'].search([('product', 'in', pro_list)])
                brand_list = []
                for br in brand:
                    brand_dict = {'id': br.id, 'name': br.name}
                    brand_list.append(brand_dict)
                if visit_last:
                    prev_ans_list = []
                    for x in last_ans[count].hsil_brand:
                        prev_ans_dict = {'id': x.id, 'name': x.name}
                        prev_ans_list.append(prev_ans_dict)
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': prod_temp_lst, 'answer_field': 'brands', 'previous_answer': prev_ans_list})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': brand_list, 'answer_field': 'brands'})
            if ques.sequence == 4:
                if visit_last:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'answer_field': 'descriptive_text', 'input_fields': 'alpha_num',
                         'previous_answer': [{'name': ''}]
                         })
                    if last_ans[count].description:
                        question_dict.update({'previous_answer': [{'name': last_ans[count].description}]})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'answer_field': 'descriptive_text', 'input_fields': 'alpha_num'})
            if ques.sequence == 5:
                if visit_last:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'answer_field': 'descriptive_text', 'input_fields': 'alpha_num',
                         'previous_answer': [{'name': ''}]})
                    if last_ans[count].description:
                        question_dict.update({'previous_answer': [{'name': last_ans[count].description}]})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'answer_field': 'descriptive_text', 'input_fields': 'alpha_num'})
            if ques.sequence == 6:
                if visit_last:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'answer_field': 'descriptive_text', 'input_fields': 'alpha_num',
                         'previous_answer': [{'name': ''}]})
                    if last_ans[count].description:
                        question_dict.update({'previous_answer': [{'name': last_ans[count].description}]})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'answer_field': 'descriptive_text', 'input_fields': 'alpha_num'})
            if ques.sequence == 7:
                if visit_last:
                    prev_ans_list = []
                    for x in last_ans[count].hsil_product:
                        prev_ans_dict = {'id': x.id, 'name': x.name}
                        prev_ans_list.append(prev_ans_dict)
                    prev_ans_dict = {'yes_no': last_ans[count].yes_no, 'hsil_product': prev_ans_list,
                                     'description': ''}
                    if last_ans[count].description:
                        prev_ans_dict.update({'yes_no': last_ans[count].yes_no, 'hsil_product': prev_ans_list,
                                              'description': last_ans[count].description})
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': prod_list, 'answer_field': 'dd_descriptive',
                         'previous_answer': prev_ans_dict})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': prod_list, 'answer_field': 'dd_descriptive'})
            if ques.sequence == 8:
                if visit_last:
                    prev_ans_list = []
                    for x in last_ans[count].hsil_product:
                        prev_ans_dict = {'id': x.id, 'name': x.name}
                        prev_ans_list.append(prev_ans_dict)
                    prev_ans_dict = {'yes_no': last_ans[count].yes_no, 'hsil_product': prev_ans_list,
                                     'description': ''}
                    if last_ans[count].description:
                        prev_ans_dict.update({'yes_no': last_ans[count].yes_no, 'hsil_product': prev_ans_list,
                                              'description': last_ans[count].description})
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': prod_list, 'answer_field': 'dd_descriptive',
                         'previous_answer': prev_ans_dict})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': prod_list, 'answer_field': 'dd_descriptive'})
            if ques.sequence == 9:
                alpha_num = [{'name': 'mop', 'type': 'alpha_numeric'}, {'name': 'dp', 'type': 'numeric'}]
                if visit_last:
                    prev_ans_dict = {'numeric': last_ans[count].numeric, 'description': ''}
                    if last_ans[count].description:
                        prev_ans_dict.update(
                            {'numeric': last_ans[count].numeric, 'description': last_ans[count].description})
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'input_fields': 'alpha_num', 'option': alpha_num,
                         'previous_answer': prev_ans_dict})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'input_fields': 'alpha_num', 'option': alpha_num})
            if ques.sequence == 10:
                if visit_last:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'input_fields': 'alpha_num',
                         'previous_answer': [{'name': ''}]})
                    if last_ans[count].description:
                        question_dict.update({'previous_answer': [{'name': last_ans[count].description}]})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'input_fields': 'alpha_num'})
            if ques.sequence == 11:
                if visit_last:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'input_fields': 'alpha_num',
                         'previous_answer': [{'name': ''}]})
                    if last_ans[count].description:
                        question_dict.update({'previous_answer': [{'name': last_ans[count].description}]})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'input_fields': 'alpha_num'})
            if ques.sequence == 12:
                if visit_last:
                    prev_ans_list = []
                    for x in last_ans[count].branding_activity:
                        prev_ans_dict = {'id': x.id, 'name': x.name}
                        prev_ans_list.append(prev_ans_dict)
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': activity_list, 'answer_field': 'branding_activity',
                         'previous_answer': prev_ans_list})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': activity_list, 'answer_field': 'branding_activity'})
            if ques.sequence == 13:
                if visit_last:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'answer_field': 'descriptive_text', 'input_fields': 'alpha_num',
                         'previous_answer': [{'name': ''}]})
                    if last_ans[count].description:
                        question_dict.update({'previous_answer': [{'name': last_ans[count].description}]})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'answer_field': 'descriptive_text', 'input_fields': 'alpha_num'})
            if ques.sequence == 14:
                if visit_last:
                    prev_ans_list = []
                    for x in last_ans[count].branding_activity:
                        prev_ans_dict = {'id': x.id, 'name': x.name}
                        prev_ans_list.append(prev_ans_dict)
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': activity_list, 'answer_field': 'branding_activity',
                         'previous_answer': prev_ans_list})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': activity_list, 'answer_field': 'branding_activity'})
            if ques.sequence == 15:
                alpha_num = [{'name': 'mop', 'type': 'alpha_numeric'}, {'name': 'dp', 'type': 'numeric'}]
                if visit_last:
                    prev_ans_dict = {'numeric': last_ans[count].numeric, 'description': ''}
                    if last_ans[count].description:
                        prev_ans_dict.update(
                            {'numeric': last_ans[count].numeric, 'description': last_ans[count].description})
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'answer_field': 'descriptive_text', 'input_fields': 'alpha_num',
                         'option': alpha_num, 'previous_answer': prev_ans_dict})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'answer_field': 'descriptive_text', 'input_fields': 'alpha_num',
                         'option': alpha_num})
            if ques.sequence == 16:
                if visit_last:
                    prev_ans_dict = {'yes_no': last_ans[count].yes_no, 'description': ''}
                    if last_ans[count].description:
                        prev_ans_dict = {'yes_no': last_ans[count].yes_no, 'description': last_ans[count].description}
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'answer_field': 'descriptive_text', 'input_fields': 'alpha_num',
                         'previous_answer': prev_ans_dict})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': '', 'answer_field': 'descriptive_text', 'input_fields': 'alpha_num'})
            if ques.sequence == 17:
                pro_list = []
                for id in product:
                    prod_dict = {'id': id.id, 'name': id.name}
                    pro_list.append(prod_dict)
                if visit_last:
                    prev_ans_list = []
                    for x in last_ans[count].hsil_product:
                        prev_ans_dict = {'id': x.id, 'name': x.name}
                        prev_ans_list.append(prev_ans_dict)
                    prev_ans_dict = {'yes_no': last_ans[count].yes_no, 'hsil_product': prev_ans_list,
                                     'description': ''}
                    if last_ans[count].description:
                        prev_ans_dict.update({'yes_no': last_ans[count].yes_no, 'hsil_product': prev_ans_list,
                                              'description': last_ans[count].description})
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': pro_list, 'answer_field': 'dd_descriptive',
                         'previous_answer': prev_ans_dict})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': pro_list, 'answer_field': 'dd_descriptive'})
            if ques.sequence == 18:
                if visit_last:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': [{'value': 'highly_satisfactory', 'name': 'Highly Satisfactory'},
                                          {'value': 'satisfactory', 'name': 'Satisfactory'},
                                          {'value': 'very_good', 'name': 'Very Good'},
                                          {'value': 'good', 'name': 'Good'}, {'value': 'average', 'name': 'Average'},
                                          {'value': 'not_satisfactory', 'name': 'Not Satisfactory'}],
                         'answer_field': 'selection',
                         'previous_answer': [{'name': last_ans[count].service_rate}]})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': [{'value': 'highly_satisfactory', 'name': 'Highly Satisfactory'},
                                          {'value': 'satisfactory', 'name': 'Satisfactory'},
                                          {'value': 'very_good', 'name': 'Very Good'},
                                          {'value': 'good', 'name': 'Good'}, {'value': 'average', 'name': 'Average'},
                                          {'value': 'not_satisfactory', 'name': 'Not Satisfactory'}],
                         'answer_field': 'selection'})
            if ques.sequence == 19:
                complaint_obj = self.env['service.complaint'].search([])
                comp_list = []
                for comp in complaint_obj:
                    comp_dict = {}
                    comp_dict.update({'id': comp.id, 'name': comp.name})
                    comp_list.append(comp_dict)
                if visit_last:
                    prev_ans_list = []
                    for x in last_ans[count].service_complaint:
                        prev_ans_dict = {'id': x.id, 'name': x.name}
                        prev_ans_list.append(prev_ans_dict)
                    prev_ans_dict = {'service': prev_ans_list, 'number': [{'0': last_ans[count].complaint_count0},
                                                                          {'1': last_ans[count].complaint_count1},
                                                                          {'2': last_ans[count].complaint_count2},
                                                                          {'3': last_ans[count].complaint_count3}]}
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': comp_list, 'answer_field': 'multiple', 'input_fields': 'alpha_num',
                         'answer1': prev_ans_list,
                         'previous_answer': [{'name': ''},
                                             {'service_complaint': [prev_ans_dict]}]})
                    if last_ans[count].description:
                        question_dict.update(
                            {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                             'hsil_product': comp_list, 'answer_field': 'multiple', 'input_fields': 'alpha_num',
                             'answer1': prev_ans_list,
                             'previous_answer': [{'name': last_ans[count].description},
                                                 {'service_complaint': [prev_ans_dict]}]})
                    count += 1
                else:
                    question_dict.update(
                        {'name': ques.name, 'sequence': ques.sequence, 'type': ques.type,
                         'hsil_product': comp_list, 'answer_field': 'multiple', 'input_fields': 'alpha_num'})

            question_list.append(question_dict)
        final_dict = {'data': question_list}
        return final_dict

    @api.multi
    def get_question_set(self, user_id, pos_visit):
        final_dict = {}
        user_obj = self.env['res.users'].search([('id', '=', user_id)])
        if user_obj.user_category:
            if user_obj.user_category.name == "CAG":
                final_dict = self.get_question_set_common('CAG', 'cag', pos_visit)
            elif user_obj.user_category.name == "KAG":
                final_dict = self.get_question_set_common('KAG', 'kag', pos_visit)
            else:
                final_dict = self.get_questions(self.id)
        else:
            final_dict = self.get_questions(self.id)
        return final_dict


class VisitQuestionsSet(models.Model):
    _name = 'visit.questions.set'
    _description = "Questions Set"

    name = fields.Char('Question Set Name')
    questions = fields.Many2many('visit.questions', 'Questions')
    retail_team_id = fields.Many2one('retail.team', 'Retail Team')
    set_default = fields.Boolean('Set Default', help='Sets This question set has default for all users',
                                      default=False)


    @api.model
    def create(self, values):
        q_set_id = super(VisitQuestionsSet, self).create(values)
        if 'set_default' in values and values['set_default']:
            for q_set in self.search([('id', '!=', q_set_id.id), ('set_default', '=', True)]):
                q_set.write({'set_default': False})
            if q_set_id.retail_team_id:
                q_set_id.retail_team_id.write({'set_default': True})
        return q_set_id

    @api.multi
    def write(self, values):
        if 'set_default' in values and values['set_default']:
            for q_set in self.search([('id', '!=', self.id), ('set_default', '=', True)]):
                q_set.write({'set_default': False})
            self.retail_team_id.write({'set_default': True})
        return super(VisitQuestionsSet, self).write(values)


class VisitQuestionsCategory(models.Model):
    _name = 'visit.questions.category'
    _description = "Questions Category"

    name = fields.Char('Question Category Name')



class VisitAnswers(models.Model):
    _name = 'visit.answers'
    _description = "Customer Visit Answers"
    
    @api.multi
    def _get_answer(self, ids, name, args):
        if not ids: return {False}
        res = {}
        for record in self.browse(ids):
            if record.name.type == 'multiple':
                if record.answer == 'option_a':
                    res[record.id] = record.name.option_a
                elif record.answer == 'option_b':
                    res[record.id] = record.name.option_b
                elif record.answer == 'option_c':
                    res[record.id] = record.name.option_c
                elif record.answer == 'option_d':
                    res[record.id] = record.name.option_d
            elif record.name.type == 'multiple_loop':
                if record.answer == 'option_multi_a':
                    res[record.id] = record.name.option_multi_a.name
                elif record.answer == 'option_multi_b':
                    res[record.id] = record.name.option_multi_b.name
                elif record.answer == 'option_multi_c':
                    res[record.id] = record.name.option_multi_c.name
                elif record.answer == 'option_multi_d':
                    res[record.id] = record.name.option_multi_d.name
                else:
                    res[record.id] = False
            elif record.name.type == 'yes_no':
                res[record.id] = record.yes_no
            elif record.name.type == 'yes_no_loop':
                res[record.id] = record.yes_no
            elif record.name.type == 'descriptive':
                res[record.id] = record.description
            elif record.name.type == 'numeric':
                res[record.id] = record.description
            elif record.name.type == 'selection':
                res[record.id] = record.service_rate
            elif record.name.type == 'n_description':
                if record.numeric and record.description:
                    string = str(record.numeric) + ', ' + record.description
                else:
                    string = ''
                res[record.id] = string
            elif record.name.type == 'select_multiple':
                if record.name.sequence == 1:
                    string = ''
                    for id in record.hsil_product:
                        string += id.name
                    res[record.id] = string
                if record.name.sequence == 2:
                    string = ''
                    for id in record.hsil_brand:
                        string += id.name
                    res[record.id] = string
                if record.name.sequence == 3:
                    string_brand = ''
                    for id in record.hsil_brand:
                        string_brand += id.name
                    res[record.id] = string_brand
                if record.name.sequence == 12:
                    string_brand = ''
                    for id in record.branding_activity:
                        string_brand += id.name
                    res[record.id] = string_brand
                if record.name.sequence == 14:
                    string_brand = ''
                    for id in record.branding_activity:
                        string_brand += id.name
                    res[record.id] = string_brand
                if record.name.sequence == 20:
                    string_brand = ''
                    for id in record.service_complaint:
                        string_brand += id.name
                    res[record.id] = string_brand
            else:
                res[record.id] = False
        return res


    name = fields.Many2one('visit.questions', 'Question')
    type = fields.Selection([('descriptive', 'Descriptive'),
                              ('yes_no', 'Yes Or No'),
                              ('numeric', 'Numeric'),
                              ('selection', 'Selection'),
                              ('n_description', 'N-Description'),
                              ('multiple', 'Multiple Options'),
                              ('select_multiple', 'Select Multiples'),
                              ('multiple_loop', 'Multiple with Loop'),
                              ('yes_no_loop', 'Yes/No with Loop'), ], 'Type of Question'),
    yes_no = fields.Selection([('yes', 'Yes'), ('no', 'No')], 'Yes/No Type')

    visit_id = fields.Many2one('customer.visit', 'POS Visits')
    choosen_answer = fields.Char(string='Answer', store=True)
    description = fields.Text('Description')

    hsil_product = fields.Many2many('question.product.config', 'answer_product_rel', 'answer_id',
                                     'product_id', 'Product')

    answer = fields.Selection([('option_a', 'Option A'),
                                ('option_b', 'Option B'),
                                ('option_c', 'Option C'),
                                ('option_d', 'Option D'),
                                ('option_multi_a', 'Option A'),
                                ('option_multi_b', 'Option B'),
                                ('option_multi_c', 'Option C'),
                                ('option_multi_d', 'Option D'),
                                ('option_multi_yes', 'Yes'),
                                ('option_multi_no', 'No')], 'Answer'),

    numeric = fields.Integer('Numeric')
    descriptive_text = fields.Text('Description')
    hsil_brand = fields.Many2many('hsil.product.brand', 'product_brand_ans_rel', 'answer_id',
                                   'brand_ans_id', 'Brand')
    branding_activity = fields.Many2many('branding.activity', 'branding_activity_ans_rel', 'answer_id',
                                          'activity_ans_id', 'Branding Activity')
    service_rate = fields.Selection([('highly_satisfactory', 'Highly Satisfactory'), ('satisfactory', 'Satisfactory'),
         ('very_good', 'Very Good'), ('good', 'Good'), ('average', 'Average'),
         ('not_satisfactory', 'Not Satisfactory')], 'Service Rate')
    service_complaint = fields.Many2many('service.complaint', 'service_complaint_ans_rel', 'answer_id',
                                          'complaint_ans_id', 'Service Complaint')
    complaint_count0 = fields.Char('Complaint 1')
    complaint_count1 = fields.Char('Complaint 2')
    complaint_count2 = fields.Char('Complaint 3')
    complaint_count3 = fields.Char('Complaint 4')



class QuestionProductConfig(models.Model):
    _name = 'question.product.config'

    name = fields.Char('Name')
    active = fields.Boolean('Active')
    group = fields.Selection([('cag', 'CAG'), ('kag', 'KAG')], 'Group')



class QuestionProductProduct(models.Model):
    _name = 'question.product.product'


    question_id = fields.Many2one('visit.questions', 'Question')
    group = fields.Selection([('cag', 'CAG'), ('kag', 'KAG')], 'Group')
    product_id = fields.Many2one('question.product.config', 'Product')



class HsilProductBrand(models.Model):
    _name = 'hsil.product.brand'


    name = fields.Char('Name')
    product = fields.Many2one('question.product.config', 'Product')
    active = fields.Boolean('Active')



class BrandingActivity(models.Model):
    _name = 'branding.activity'


    name = fields.Char('Name')
    active = fields.Boolean('Active')



class ServiceComplaint(models.Model):
    _name = 'service.complaint'


    name = fields.Char('Name')
    active = fields.Boolean('Active')

