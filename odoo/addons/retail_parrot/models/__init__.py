from . import customer_visit
from . import order_form
from . import res_company
from . import res_users
from . import retail_customer
from . import retail_team
from . import retail_view_config
from . import retail_visit_report