import datetime
import json
import logging
import xlsxwriter
import base64
from googlemaps import *
import requests
from odoo import fields, models, api,tools,_
# from odoo.osv import fields, osv
from odoo.tools.translate import _
from datetime import datetime, date, time, timedelta
import os

_logger = logging.getLogger(__name__)


class RetailCustomer(models.Model):
    _name = "retail.customer"
    _description = "Customer"
    _order = "create_date desc"

    @api.multi
    def _find_last_tracker(self, fields, args):
        result = {}
        for record in self.env['retail.customer'].search([('id','in', self.ids)]):
            if record.visits:
                for visit in reversed(record.visits):
                    if visit.tracker:
                        result[record.id] = "Green"
                    else:
                        result[record.id] = "Grey"
            else:
                result[record.id] = "Red"
        return result


    name = fields.Char('POS Name')
    pos_id = fields.Char('POS Id')
    kit_ids = fields.Char('KIT Ids')
    mobile = fields.Char('Mobile No.')
    start_date = fields.Date('Start Date', default= fields.datetime.now())
    end_date = fields.Date('End Date', default= datetime.strftime(datetime.now() + timedelta(days=365), '%Y-%m-%d'))
    allotted_date = fields.Date('Allotted Date', default= fields.datetime.now())
    frequency = fields.Integer('Frequency', default=1)
    next_date = fields.Date('Next Allotted Date')
    address = fields.Text('Address')
    visits = fields.One2many('customer.visit', 'name', 'Visits')
    location = fields.Char('Location')
    city = fields.Char('City')
    zip = fields.Char('Zip', size=8, change_default=True)
    latitude = fields.Float('Latitude', digits=(6, 10))
    longitude = fields.Float('Longitude', digits=(6, 10))
    is_lat_long = fields.Boolean(string='GPS status', help='Shows that Lat-Long Data was received from phone')
    tracker = fields.Char(string='Last Tracker', help='Current POS Visit is at Nearby Location or Not')
    questions_set = fields.Many2one('visit.questions.set', 'Questions Set')
    state = fields.Selection([('draft', 'Draft'),
                               ('approved', 'Approved'),
                               ('rejected', 'Rejected')], 'State', default='draft')
    manager_id = fields.Many2one('res.users', related='create_uid.manager_id', string="Manager",
                                  readonly=True, store=True)
    assigned_to = fields.Many2one('res.users', string="Assigned To")
    email_id = fields.Char(string='Email ID')

#     _defaults = {
#         'start_date': lambda obj, cr, uid, context: fields.datetime.now(),
#         'end_date': lambda *a: datetime.strftime(datetime.now() + timedelta(days=365), '%Y-%m-%d'),
#         'allotted_date': lambda obj, cr, uid, context: fields.datetime.now(),
#         'assigned_to': lambda obj, cr, uid, context: uid,
#     }

class CustomerLocation(models.Model):
    _name = "customer.location"
    _description = "Customer's Location"

    name = fields.Char('Name')
    


class RetailDiscussion(models.Model):
    _name = "retail.discussion"
    _description = "Retail Discussion"

    pos_id = fields.Many2one('retail.customer', 'POS')
    date = fields.Datetime('Date')
    description = fields.Text('Description')
    

    @api.model
    def create(self, vals):
        vals.update({'date': datetime.today(), 'pos_id': vals['pos_id']})
        result = super(RetailDiscussion, self).create(vals)
        return result

    @api.multi
    def get_retail_discussion(self, pos_id):
        res = {}
        pos_sr = self.env['retail.customer'].search([('id', '=', pos_id)])
        discussion_sr = self.env['retail.discussion'].search([('pos_id', '=', pos_sr.pos_id)])[-1]
        res.update(
            {'pos_id': discussion_sr.pos_id, 'date': discussion_sr.date, 'description': discussion_sr.description})
        return res
