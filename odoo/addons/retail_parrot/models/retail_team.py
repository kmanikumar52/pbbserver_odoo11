import logging

from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class RetailTeam(models.Model):
    _name = 'retail.team'

    name = fields.Char('Retail Team')
    retail_id = fields.One2many('visit.questions.set', 'retail_team_id', 'Question Set')
    user_id = fields.Many2many('res.users', 'retail_user_rel', 'team_id', 'user_id', 'Users')
    set_default = fields.Boolean('Set Default', help='This Team was assigned default for all users', default=False)

    @api.model
    def create(self, values):
        if 'set_default' in values and values['set_default']:
            for retail_team in self.search([('id', '!=', self.id), ('set_default', '=', True)]):
                retail_team.write({'set_default': False})
        return super(RetailTeam, self).create(values)

    @api.multi
    def write(self, values):
        if 'set_default' in values and values['set_default']:
            for retail_team in self.search([('id', '!=', self.id), ('set_default', '=', True)]):
                retail_team.write({'set_default': False})
        return super(RetailTeam, self).write(values)





