import datetime
import logging
from datetime import datetime, timedelta
import json
import requests
import time
from odoo import models, fields, api
from odoo.tools.translate import _

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = 'res.users'

    is_start = fields.Boolean('Start')

    @api.model
    def create(self, values):
        res_users_id = super(ResUsers, self).create(values)
        self.env['retail.team'].search([('set_default', '=', True)]).write({'user_id': [(4, res_users_id.id)]})
        return res_users_id

    @api.multi
    def get_pos_count(self):
        _logger.info("Get POS Count")
        pos_result = {}
        result_dict={}
        today_date = datetime.today()
        today_date_strformat = today_date.strftime('%Y-%m-%d')
        total_count = 0
        result_list = []
        start_date = today_date + timedelta(days=-13)
        today_date_strp = datetime.strptime(today_date_strformat, '%Y-%m-%d')
        search_obj = self.env['retail.customer'].search(
            [('assigned_to', '=', self.id), ('start_date', '<=', today_date_strp), ('state', '=', 'approved')])
        for days_ago in range(0, -14, -1):
            date_N_days_ago = today_date + timedelta(days=days_ago)
            date_strformat = date_N_days_ago.strftime('%Y-%m-%d')
            date_strp = date_N_days_ago.strptime(date_strformat, '%Y-%m-%d')
            count = 0
            for detail in search_obj:
                result_dict = {'count': 0, 'start_date': date_strformat}
                start_date_strp = datetime.strptime(detail.start_date, '%Y-%m-%d')
                end_date_strp = datetime.strptime(detail.end_date, '%Y-%m-%d')
                if start_date_strp <= date_strp <= end_date_strp:
                    count += 1
                    if result_dict['start_date'] == date_strformat:
                        result_dict['count'] = count
            result_list.append(result_dict)
            total_count += count
        pos_result.update({"total_count": total_count, "result_list": result_list})
        return pos_result

    @api.multi
    def get_pos_visit_details(self, visit_date, name):
        _logger.info("Get POS Visit Details")
        search_list = []
        visit_list = []
        result = {}
        if name:
            pos_ids = self.env["retail.customer"].search([('assigned_to', '=', self.id), ('pos_id', '=', str(name))])
        else:
            pos_ids = self.env["retail.customer"].search([('assigned_to', '=', self.id)])
        for pos_id in pos_ids:
            if visit_date and pos_id.id:
                search_objs = self.env["customer.visit"].search([('create_uid', '=', self.id),
                                                                 ('date', '=', visit_date),
                                                                 ('name', '=', pos_id.id)])
            elif visit_date:
                search_objs = self.env["customer.visit"].search([('create_uid', '=', self.id),
                                                                 ('date', '=', visit_date)])
            else:
                search_objs = self.env["customer.visit"].search([('create_uid', '=', self.id),
                                                                 ('name', '=', pos_id.id)])

            for search_obj in search_objs:
                count = 0
                visit_search_obj = self.env["customer.visit"].search([('id', '=', search_obj.id)])
                for visit in visit_search_obj:
                    visit_dict = {}
                    count += 1
                    visit_dict.update({'id': visit.id,
                                       'visit_name': visit.name.name,
                                       'Latitude': visit.latitude,
                                       'Longitude': visit.longitude,
                                       'visit_date': visit.date,
                                       'created_user': visit.create_uid.name,
                                       'create_uid': visit.create_uid.id,
                                       'create_date': visit.create_date,
                                       'sequence': count, 'location': pos_id.location or ''})
                    visit_list.append(visit_dict)
                result.update({"result": visit_list, "count": count})
                _logger.info(result)
        return result

    @api.multi
    def get_user_pos_details(self, detail):
        pos_list = []
        result = {}
        if 'date' in detail:
            pos_ids = self.env["retail.customer"].search([('allotted_date', '=', detail['date'])])
        elif 'pos_id' in detail:
            pos_ids = self.env["retail.customer"].search([('id', '=', detail['pos_id'])])
        else:
            pos_ids = self.env["retail.customer"].search([])
        for pos in pos_ids:
            pos_dict = {}
            pos_dict.update({'id': pos.id,
                             'pos_name': pos.name,
                             'pos_id': pos.pos_id})
            pos_list.append(pos_dict)
        result.update({"result": pos_list})
        return result

    @api.multi
    def get_previous_visit_details(self, pos_id, date, stage):
        res_dict = {}
        date_obj = str(date)
        pos_name = self.env['retail.customer'].search([('id', '=', pos_id)])
        date_formate = datetime.strptime(date_obj, '%Y-%m-%d %H:%M:%S')
        if stage == 'Previous':
            day_ago = date_formate + timedelta(days=-1)
            date_str_formate = day_ago.strftime('%Y-%m-%d')
            visit_id = self.env['customer.visit'].search(
                [('name', '=', pos_name.name), ('date', '<=', date_str_formate)], limit=1)
            if visit_id:
                res_dict.update({'visit_id': visit_id.id})
                return res_dict
            else:
                return False
        if stage == 'Next':
            day_next = date_formate + timedelta(days=+1)
            date_str_formate = day_next.strftime('%Y-%m-%d')
            visit_id = self.env['customer.visit'].search(
                [('name', '=', pos_name.name), ('date', '>=', date_str_formate)], order='id asc', limit=1)
            if visit_id:
                res_dict.update({'visit_id': visit_id.id})
                return res_dict
            else:
                return False


