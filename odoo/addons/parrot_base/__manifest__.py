# -*- coding: utf-8 -*-
{
    'name': "Parrot Base",
    'summary': """Parrot Base Code""",
    'description': """Parrot Business Booster - Base Code changes will be added here""",
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'version': '0.1',
    'depends': [
        'base',
    ],
    'data': [
        'views/res_partner_view.xml',
        'views/res_users_view.xml',
        'views/res_user_kpi_view.xml',
        'views/app_menu_view.xml',
        'views/res_team_view.xml',
        'views/user_activities_view.xml',

    ],
    'auto_install': True,
}
