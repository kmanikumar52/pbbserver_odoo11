from odoo import models, fields

class AppMenu(models.Model):
    _name = 'app.menu'
    _description = 'Menus in mobile app to show according to user'
    _order = 'name'

    name = fields.Char('Menu')
    display_name = fields.Char(string='Display Name')
    parent_menu = fields.Many2one('app.menu', 'Parent Menu')
    user_menu = fields.Many2many('res.users', 'res_user_menu_rel', 'menu_id', 'user_id', 'Users')
    show = fields.Boolean('Show Menu', default=True)
#     group_ids = fields.Many2many('res.groups', 'ir_model_menu_group_rel', 'menu_id', 'group_id', 'Select Groups')
