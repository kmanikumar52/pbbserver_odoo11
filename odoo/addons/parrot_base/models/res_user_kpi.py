# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime, date
from lxml import etree
import time

from odoo import api
from odoo import SUPERUSER_ID
from odoo import tools
from odoo import fields, models,api,_
from odoo.tools.translate import _
from odoo.exceptions import UserError


class ResUserKpi(models.Model):
    _name = 'res.user.kpi'
    _description = 'User KPI'

    name_temp = fields.Char('Key')
    user_id = fields.Many2one('res.users', 'Users')
