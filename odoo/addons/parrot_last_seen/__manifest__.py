{
    'name': "Parrot Last Seen",
    'summary': """Parrot User's Last Seen""",
    'description': """
        To show the last seen for users - Parrot Business Booster app
    """,
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': [],
    'data': [
        'views/res_users_views.xml',
    ],
    'auto_install': True,
}