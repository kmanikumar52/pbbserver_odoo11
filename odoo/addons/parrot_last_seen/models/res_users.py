# -*- coding: utf-8 -*-

import datetime

import pytz
from odoo import models, fields, api,_
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class ResUsers(models.Model):
    _inherit = "res.users"

    last_seen = fields.Datetime('Last Seen', compute="_find_last_seen")

    @api.one
    def _find_last_seen(self):
        context = dict(self._context or {})
        timezone = pytz.timezone(context.get('tz') or 'UTC')
        for user in self:
            user_activity_sr_limit = self.env['user.activities'].search([('name.id', '=', user.id)], limit=1)
            if user_activity_sr_limit.date:
                self.last_seen = pytz.UTC.localize(
                    datetime.datetime.strptime(user_activity_sr_limit.date, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(
                    timezone).strftime("%Y-%m-%d %H:%M:%S")
