# -*- coding: utf-8 -*-

import datetime
import logging

import pytz
from odoo import models, fields, api
from odoo.addons.bus.models.bus_presence import AWAY_TIMER
from odoo.addons.bus.models.bus_presence import DISCONNECTION_TIMER
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    last_seen = fields.Datetime('Last Seen', compute="_find_last_seen")
    last_seen_store = fields.Datetime('Last Seen', store=True)

    @api.one
    def _find_last_seen(self):
        context = dict(self._context or {})
        for user in self:
            user_activity_sr_limit = self.env['user.activities'].search([('name.partner_id.id', '=', user.id)], limit=1)
            timezone = pytz.timezone(context.get('tz') or 'UTC')
            if user_activity_sr_limit.date:
                last_seen = pytz.UTC.localize(
                    datetime.datetime.strptime(user_activity_sr_limit.date, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(
                    timezone).strftime("%Y-%m-%d %H:%M:%S")
                self.write({'last_seen_store': last_seen})
                self.last_seen = last_seen

    @api.model
    def im_search_mobile(self, name, limit=20):
        """ IM Search for Mobile """
        name = '%' + name + '%'
        excluded_partner_ids = [self.env.user.partner_id.id]
        last_seen = [self.env.user.last_seen]
        _logger.info("IM Search Mobile")
        _logger.info(last_seen, excluded_partner_ids)
        bus_presence = self.env['bus.presence'].search([('user_id', 'in', excluded_partner_ids)])
        _logger.info(bus_presence)
        _logger.info(last_seen, bus_presence.last_poll, bus_presence.last_presence)
        self.env.cr.execute("""SELECT 
                        U.id as user_id,
                        P.id as id,
                        P.name as name,
                        CASE WHEN B.last_poll IS NULL THEN 'offline'
                             WHEN age(now() AT TIME ZONE 'UTC', B.last_poll) > interval %s THEN 'offline'
                             WHEN age(now() AT TIME ZONE 'UTC', B.last_presence) > interval %s THEN 'away'
                             ELSE 'online'
                        END as im_status
                    FROM res_users U
                        JOIN res_partner P ON P.id = U.partner_id
                        LEFT JOIN bus_presence B ON B.user_id = U.id
                    WHERE P.name ILIKE %s
                        AND P.id NOT IN %s
                        AND U.active = 't'
                    LIMIT %s
        """, ("%s seconds" % DISCONNECTION_TIMER, "%s seconds" % AWAY_TIMER, name, tuple(excluded_partner_ids), limit))
        return self.env.cr.dictfetchall()
