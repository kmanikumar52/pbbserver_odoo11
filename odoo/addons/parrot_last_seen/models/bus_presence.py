# -*- coding: utf-8 -*-
import datetime
import logging
import time

import pytz
from odoo import api, fields, models
from odoo.addons.bus.models.bus import TIMEOUT
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)

DISCONNECTION_TIMER = TIMEOUT + 5
AWAY_TIMER = 1800  # 30 minutes


class BusPresence(models.Model):
    _inherit = 'bus.presence'

    last_seen = fields.Char('Last Seen', compute="_find_last_seen")

    @api.one
    def _find_last_seen(self):
        context = dict(self._context or {})
        for user in self:
            user_activity_sr_limit = self.env['user.activities'].search([('name.id', '=', user.id)], limit=1)
            timezone = pytz.timezone(context.get('tz') or 'UTC')
            if user_activity_sr_limit.date:
                self.last_seen = pytz.UTC.localize(
                    datetime.datetime.strptime(user_activity_sr_limit.date, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(
                    timezone).strftime("%Y-%m-%d %H:%M:%S")

    @api.model
    def update(self, inactivity_period):
        """ Updates the last_poll and last_presence of the current user
            :param inactivity_period: duration in milliseconds """
        _logger.info("compute last_presence timestamp")
        presence = self.search([('user_id', '=', self._uid)], limit=1)
        ps = self.env['res.partner'].search([('id', '=', self._uid)])
        _logger.info("ps.last_seen")
        _logger.info(ps, self.last_presence, self.last_poll, ps.last_seen, ps.id, self._uid)
        self.last_presence = ps.last_seen
        values = {'last_poll': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)}
        super(BusPresence, self).update(inactivity_period)
