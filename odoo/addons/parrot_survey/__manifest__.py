# -*- coding: utf-8 -*-
{
    'name': "Parrot Survey",
    'summary': """Parrot Survey/Certification a product from Parrot Solutions""",
    "description": """Parrot Survey/Certification a product from Parrot Solutions""",
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'category': 'Uncategorized',
    'version': '0.1',
    'sequence': 2,
    'depends': [
        'base',
    ],
    'data': [
        'data/app_menu_data.xml',
        # 'data/parrot_survey_data.xml',
        'security/ir.model.access.csv',
        'security/parrot_survey_security.xml',
        'views/parrot_survey_view.xml',
    ],
    'application': True,
}
