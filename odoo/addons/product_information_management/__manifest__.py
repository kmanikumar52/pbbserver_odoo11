# -*- coding: utf-8 -*-
{
    'name': "Product Information Management",
    'summary': """Product Information Management""",
    "description": """Product Information Management""",
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'category': 'Uncategorized',
    'version': '0.1',
    'sequence': 1,
    'depends': [
        'mail',
        'web',
        'parrot_notification',
        'parrot_survey',
        'parrot_base',
    ],
    'data': [
        'security/product_information_management_security.xml',
        'security/ir.model.access.csv',
        'views/product_information_management_view.xml',
        'views/document_report_view.xml',
        # 'wizard/broadcast_query_view.xml',
#         'data/product_information_management_data.xml'
    ],
    'application': True,
}
