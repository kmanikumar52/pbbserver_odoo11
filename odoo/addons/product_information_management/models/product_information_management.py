import base64
import contextlib
import datetime
import json
import logging
import os
import shutil
import time
import zipfile

# from datetime import datetime
from legacy import xrange
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning
from datetime import timedelta
from operator import itemgetter

import odoo
from odoo.osv import osv
import requests
from odoo import fields, models, api,_
# from odoo.osv import fields, osv
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DATETIME_FORMAT
from odoo.tools.translate import _
from xlrd import open_workbook

_logger = logging.getLogger(__name__)


class ProductInformation(models.Model):
    _name = 'product.information'
    _description = 'Product Information'

    name = fields.Char('Name', required=True)
    product_documents = fields.Many2many('ir.attachment', 'product_attachment_rel', 'attachment_id', 'product_id',
                                          'Product Documents')
    description = fields.Text('Description')
    product_videos = fields.One2many('product.videos', 'product_information', 'Product Videos')
    upcoming_product_broadcasts = fields.One2many('product.broadcast', 'product_information',
                                                   'Upcoming Product Broadcasts')
    product_quiz = fields.Many2many('product.question.bank', 'information_question_rel', 'qustion_id',
                                     'information_id', 'Product Quiz')



class ProductVideos(models.Model):
    _name = 'product.videos'
    _description = 'Product Videos'


    name = fields.Char('Name', required=True)
    duration = fields.Char('Duration')
    url = fields.Char('URL')
    product_information = fields.Many2one('product.information', 'Product Information')



class BroadcastReceivers(models.Model):
    _name = 'broadcast.receivers'
    _description = 'Broadcast Receivers'


    name = fields.Many2one('product.broadcast', 'Product Broadcast', required=True)
    date_joined = fields.Datetime(string='Date ',default= fields.datetime.now(),required=True, readonly=True)
    user_id = fields.Many2one('res.users', 'User')
    state = fields.Char('State')


class BroadcastRecords(models.Model):
    _name = 'broadcast.records'
    _description = 'Recorded Broadcasts'


    broadcast_id = fields.Many2one('product.broadcast', 'Product Broadcast', required=True)
    name = fields.Char('Url')
    recorded_date = fields.Datetime(string='Date', required=True, readonly=True,default = fields.datetime.now())


class ProductBroadcast(models.Model):
    _name = 'product.broadcast'
    _description = 'Product Broadcast'

    @api.multi
    def _check_user(self):
        if not self.ids: return {False}
        res = {}
        for users in self.env['product.broadcast'].search([('id', 'in', self.ids)]):
            if users.user_id.id is not False:
                if (users.user_id.id == self.id):
                    value = True
                    res[users.id] = value
        return res

    @api.depends('date_begin','date_end')
    def _set_broadcast_state(self):
        from datetime import datetime
        if not self.ids: return {False}
        res = {}
        for event in self.env['product.broadcast'].search([('id', 'in', self.ids)]):
            now = fields.datetime.now()
            #             print "Now -- %s"%now
            #             print "Start -- %s"%event.date_begin
            #             print "End -- %s"%event.date_end
            print(now, type(now),'zzzzzzzzzzz')
            date_begin = datetime.strptime(event.date_begin, '%Y-%m-%d %H:%M:%S')
            date_end = datetime.strptime(event.date_begin, '%Y-%m-%d %H:%M:%S')
            # print(start_date,end_date,type(start_date),type(end_date),'fgggggggg')
            if now >= date_begin and now <= date_end:
                print('aaaa')
                res[event.id] = 'InProgress'
            elif date_begin <= now:
                print('bbbb')
                res[event.id] = 'Completed'
            elif now <= date_end:
                print('cccc')
                res[event.id] = 'Upcoming'
            print(event.id,'ddddd')
            print(res,'eeee')
        return res

    @api.multi
    def upcoming_broadcast(self):
        today = fields.datetime.now()
        data = []
        upcoming_ids = self.env['product.broadcast'].search([('date_end', '>', today)])
        all_records = self.read(upcoming_ids,
                                ['name', 'date_end', 'instructor_name', 'seats_min', 'seats_max', 'seats_unconfirmed',
                                 'seats_used', 'answer_ids', 'state', 'date_begin', 'date_begin', 'seats_available',
                                 'seats_reserved', 'ques_set_id', 'broadcast_state', 'description', 'registration_ids',
                                 'recorded_broadcasts_ids', 'broadcast_receiver_ids', 'broadcast_ques_ids', 'user_id'])
        for record in all_records:
            registered_users = []
            record['registered'] = False
            user_id = [record['user_id'][0], record['user_id'][1]]
            for training in self.env['product.training'].browse(record['registration_ids']):
                registered_users.append(training.user_id.id)
            if user_id[0] == self._uid or self.uid in registered_users:
                if record['ques_set_id']:
                    question_set_id = [record['ques_set_id'][0], record['ques_set_id'][1]]
                    record['ques_set_id'] = question_set_id
                record['registration_ids_data'] = []
                record['broadcast_ques_ids_data'] = []
                record['answer_ids_data'] = []
                record['broadcast_receiver_ids_data'] = []
                record['recorded_broadcasts_ids_data'] = []
                record['user_id'] = user_id
                for broadcast in self.env['broadcast.questions'].browse(record['broadcast_ques_ids']):
                    obj = {}
                    obj['id'] = broadcast.id
                    obj['name'] = [broadcast.name.id, broadcast.name.name]
                    obj['posted_time'] = broadcast.posted_time
                    record['broadcast_ques_ids_data'].append(obj)
                for ans in self.env['product.answers'].browse(record['answer_ids']):
                    obj = {}
                    obj['id'] = ans.id
                    obj['user_id'] = [ans.user_id.id, ans.user_id.name]
                    obj['question_id'] = [ans.question_id.id, ans.question_id.name]
                    obj['name'] = ans.name
                    obj['result'] = ans.result
                    record['answer_ids_data'].append(obj)
                for br in self.env['broadcast.receivers'].browse(record['broadcast_receiver_ids']):
                    obj = {}
                    obj['id'] = br.id
                    obj['name'] = [br.name.id, br.name.name]
                    obj['date_joined'] = br.date_joined
                    obj['user_id'] = [br.user_id.id, br.user_id.name]
                    obj['state'] = br.state
                    record['broadcast_receiver_ids_data'].append(obj)
                for rb in self.env['broadcast.records'].browse(record['recorded_broadcasts_ids']):
                    obj = {}
                    obj['id'] = rb.id
                    obj['name'] = rb.name
                    obj['recorded_date'] = rb.recorded_date
                    record['recorded_broadcasts_ids_data'].append(obj)
                for training in self.env['product.training'].browse(record['registration_ids']):
                    obj = {}
                    obj['id'] = training.id
                    obj['user_id'] = [training.user_id.id, training.user_id.name]
                    obj['email'] = training.email
                    obj['phone'] = training.phone
                    obj['state'] = training.state
                    record['registration_ids_data'].append(obj)
                    if training.user_id.id == self.uid:
                        if training.state == 'open' or training.state == 'cancel' or training.state == 'done':
                            record['registered'] = True
                        elif training.state == 'draft':
                            record['registered'] = False
                    _logger.info("Training State--%s,%s" % (training.state, record['registered']))
                if record['registered']:
                    if record['date_begin'] > today:
                        data.append(record)
                else:
                    if user_id[0] == self.uid and record['date_begin'] > today:
                        _logger.info("IN ELSE IF------")
                        data.append(record)
                    elif user_id[0] != self.uid:
                        data.append(record)
        return {'records': data, 'length': len(data)}


    name = fields.Char('Event Name', translate=True, required=True, readonly=False, states={'done': [('readonly', True)]})
    date_end = fields.Datetime(string='End Time', required=True,default = datetime.datetime.now()  + timedelta(hours=5))
    date_begin = fields.Datetime(string='Start Time', required=True, default = datetime.datetime.today())
    instructor_name = fields.Char('Instructor Name')
    user_id = fields.Many2one('res.users', 'Trainer', readonly=False, states={'done': [('readonly', True)]},default=lambda self: self.env.user)
    seats_min = fields.Integer('Minimum Reserved Seats', readonly=True, states={'draft': [('readonly', False)]})
    seats_max = fields.Integer(string='Maximum Available Seats', readonly=True,
                                states={'draft': [('readonly', False)]}, )
    seats_unconfirmed = fields.Integer('Current Registrations', store=True, readonly=True,
                                        compute='_compute_seats')
    seats_used = fields.Integer('Number of Participations', store=True, readonly=True, compute='_compute_seats')
    description = fields.Binary('Description', translate=True, readonly=False,
                               states={'done': [('readonly', True)]})
    # 'course_id':fields.many2one('hr.training.course','Training Courses',required=True),
    # 'email_confirmation_id': fields.many2one('email.template','Event Confirmation Email',
    #  domain=[('model', '=', 'hr.training.registration')],
    #  help="If you set an email template, each participant will receive this email announcing the
    #  confirmation of the event."),
    # 'email_registration_id': fields.many2one('email.template','Registration Confirmation Email'),
    state = fields.Selection([('draft', 'Unconfirmed'),
                               ('cancel', 'Cancelled'),
                               ('confirm', 'Confirmed'),
                               ('done', 'Done')], string='Status', readonly=True, required=True, copy=False, default = 'draft',
                              help="If event is created, the status is 'Draft'. "
                                   "If event is confirmed for a particular dates the status is set to 'Confirmed'. "
                                   "If the event is over, the status is set to 'Done'. "
                                   "If event is cancelled the status is set to 'Cancelled'.")
    registration_ids = fields.One2many('product.training', 'event_id', 'Training', readonly=False,
                                        states={'done': [('readonly', True)]}, ondelete='delete')
    seats_available = fields.Integer('Available Seats', store=True, readonly=True, compute='_compute_seats')
    seats_reserved = fields.Integer(string='Reserved Seats', store=True, readonly=True, compute='_compute_seats')
    count_registrations = fields.Integer(string='Registrations', compute='_count_registrations')
    ques_set_id = fields.Many2one('question.set', 'Select Question Set')
    broadcast_ques_ids = fields.One2many('broadcast.questions', 'event_id', 'Training Questions',
                                          states={'done': [('readonly', True)]})
    attachment = fields.Many2many('ir.attachment', 'training_attachment_rel', 'event_id', 'attachment_id',
                                   'Study Material')
    check_user = fields.Boolean(compute='_check_user', type='boolean', string='Check User')
    general_channel = fields.Many2one('mail.channel', 'General Channel')
    question_channel = fields.Many2one('mail.channel', 'Question Channel')
    user_question_channel = fields.Many2one('mail.channel', 'User Question Channel')
    answer_ids = fields.One2many('product.answers', 'event_id', 'Results')
    high_bandwidth_video_url = fields.Char('High Bandwidth Video URL', default='wss://lic.affosoft.com:8443/call')
    low_bandwidth_video_url = fields.Char('Low Bandwidth Video URL',default='wss://lic.affosoft.com:8443/call')
    attendance = fields.Many2many('product.attendances', 'attendances_schedule_ques_rel', 'event_id',
                                   'attendances_id', 'Training Attendance')
    product_information = fields.Many2one('product.information', 'Product Information')
    broadcast_type = fields.Selection([('broadcast', 'Broadcast'),
                                        ('product_training', 'New Product Training'),
                                        ('refresher_courses', 'Refresher Courses'),
                                        ('code_of_conduct', 'Code Of Conduct')], string='Broadcast Type')
    broadcast_state = fields.Char(compute='_set_broadcast_state', type='char', string='Broadcast State', store=True,
                                       readonly=True)
    refresh_count = fields.Integer("Refresh Clicked Count")
    broadcast_receiver_ids = fields.One2many('broadcast.receivers', 'name', 'Broadcast Receivers')
    recorded_broadcasts_ids = fields.One2many('broadcast.records', 'broadcast_id', 'Recorded Broadcasts')
    cron_id = fields.Many2one('ir.cron', 'Cron Job')
    group_id = fields.Many2one('res.groups', 'Select Group')
    survey_id = fields.Many2one('test', 'Survey')
    team_id = fields.Many2one('res.team', 'Select Team')


#     _defaults = {
#         'user_id': lambda obj, cr, uid, context: uid,
#         'state': 'draft',
#         'high_bandwidth_video_url': 'wss://lic.affosoft.com:8443/call',
#         'low_bandwidth_video_url': 'wss://lic.affosoft.com:8443/call',
#         'date_begin': datetime.datetime.today(),
#         'date_end': datetime.datetime.now() + timedelta(hours=5),
#     }
    @api.model
    def create(self, data):
        context = dict(self._context or {})
        if context.get('mail_broadcast'):
            context['mail_create_nolog'] = True
        channel_pool = self.env['mail.channel']
        for user in self.env['res.users'].browse(data['user_id']):
            partner_id = user.partner_id.id
        gen_channel = {'name': 'Group ' + 'General ' + data['name'],
                       'channel_partner_ids': [(4, partner_id)],
                      'public': 'private'}
        ques_channel = {'name': 'Group ' + 'Question ' + data['name'],
                          'channel_partner_ids': [(4, partner_id)],
                        'public': 'private'}
        user_ques_channel = {'name': 'Group ' + 'User Question ' + data['name'],
                             'channel_partner_ids': [(4, partner_id)],
                             'public': 'private'}

        data['general_channel'] = channel_pool.create(gen_channel).id
        data['question_channel'] = channel_pool.create(ques_channel).id
        data['user_question_channel'] = channel_pool.create(user_ques_channel).id
        return super(ProductBroadcast, self).create(data)

    @api.model
    def unlink(self):
        for broadcast in self.env['product.broadcast'].search([('id', 'in', self.ids)]):
            general_channel = broadcast.general_channel.id
            question_channel = broadcast.question_channel.id
            user_channel = broadcast.user_question_channel.id
            mail_channel_ids = self.env['mail.channel'].search([('id', 'in', [general_channel, question_channel, user_channel])])
            _logger.info(mail_channel_ids)
            for mail_channel_id in mail_channel_ids:
                self.env['mail.channel'].unlink(mail_channel_id)
        return super(ProductBroadcast, self).unlink(self.ids)

    @api.multi
    def write(self, vals):
        channel_pool = self.env['mail.channel'].search([('id','in',self.ids)])
        for broadcast in channel_pool:
            if 'name' in vals:
                gen_channel = {'name': 'Group ' + 'General ' + vals['name'], }
                ques_channel = {'name': 'Group ' + 'Question ' + vals['name'], }
                user_ques_channel = {'name': 'Group ' + 'User Question ' + vals['name'], }
                channel_pool.write([broadcast.general_channel.id], gen_channel)
                channel_pool.write([broadcast.question_channel.id], ques_channel)
                channel_pool.write([broadcast.user_question_channel.id], user_ques_channel)
        return super(ProductBroadcast, self).write(vals)

    # def onchange_group_id(self, cr, uid, ids, group_id, context=None):
    #     if group_id:
    #         group = self.pool.get('res.groups').browse(cr, uid, group_id, context=context)
    #         reg_ids = []
    #         for user in group.users:
    #             reg_ids += [{'user_id':user.id,'state':'draft'}]
    #         return {'value': {'registration_ids': reg_ids}}
    #     return {'value': {}}

    @api.multi
    def onchange_team_id(self, team_id):
        if team_id:
            team = self.env['res.team'].browse(team_id)
            reg_ids = []
            for user in team.members_ids:
                reg_ids += [{'user_id': user.id, 'state': 'draft'}]
            return {'value': {'registration_ids': reg_ids}}
        return {'value': {}}

    @api.multi
    def remind_broadcast(self):
        meeting_obj = self.env['mail.channel'].search([('id', 'in', self.ids)])
        for event in meeting_obj:
            msg_body = _(
                '{"channeltype":"reminderClass","message":"%s class will start in 10min","channelName":"%s"}') % (
                           event.name, event.question_channel.name)
            meeting_obj.message_post(event.question_channel.id,
                                     body=msg_body,
                                     message_type="comment",
                                     content_subtype="html",
                                     subtype="mail.mt_comment",
                                     )
            self.write([event.id], {"broadcast_state": "InProgress"})

    @api.multi
    def notify_broadcast(self):
        meeting_obj = self.env['mail.channel'].search([('id', 'in', self.ids)])
        today = fields.datetime.now()
        for event in meeting_obj:
            if event.date_end <= today:
                msg_body = _('{"channeltype":"classStarted","message":"%s class has started","channelName":"%s"}') % (
                    event.name, event.question_channel.name)
                meeting_obj.message_post(event.question_channel.id,
                                         body=msg_body,
                                         message_type="comment",
                                         content_subtype="html",
                                         subtype="mail.mt_comment")

    @api.multi
    def onchange_QuestionSet(self, ques_set_id):
        question_set_pool = self.env['question.set']
        questionbank_pool = self.env['product.question.bank']
        result = []
        res = {'value': {'broadcast_ques_ids': [], }}
        if ques_set_id:
            ques_ids = questionbank_pool.search([('ques_set_id', '=', ques_set_id)])
            for question in questionbank_pool.browse(ques_ids):
                questions = {'name': question.id, }
                result += [questions]
            res['value'].update({'broadcast_ques_ids': result, })
        return res

    @api.multi
    @api.depends('seats_max', 'registration_ids.state', 'registration_ids.no_of_participants')
    def _compute_seats(self):
        """ Determine reserved, available, reserved but unconfirmed and used seats. """
        # initialize fields to 0
        for event in self:
            event.seats_unconfirmed = event.seats_reserved = event.seats_used = 0
        # aggregate registrations by event and by state
        if self.ids:
            state_field = {'draft': 'seats_unconfirmed',
                           'open': 'seats_reserved',
                           'done': 'seats_used', }
            query = """ SELECT event_id, state, sum(no_of_participants)
                        FROM product_training
                        WHERE event_id IN %s AND state IN ('draft', 'open', 'done')
                        GROUP BY event_id, state
                    """
            self._cr.execute(query, (tuple(self.ids),))
            for event_id, state, num in self._cr.fetchall():
                event = self.env['product.broadcast'].browse(event_id)
                event[state_field[state]] += num
        # compute seats_available
        for event in self:
            event.seats_available = event.seats_max - (event.seats_reserved + event.seats_used) \
                if event.seats_max > 0 else 0

    # Commented because It adds date in name and increase the lenght of broadcasting name.
    # @api.multi
    # @api.depends('name', 'date_begin', 'date_end')
    # def name_get(self):
    #     result = []
    #     for event in self:
    #         dates = [dt.split(' ')[0] for dt in [event.date_begin, event.date_end] if dt]
    #         dates = sorted(set(dates))
    #         result.append((event.id, '%s (%s)' % (event.name, ' - '.join(dates))))
    #     return result

    @api.one
    @api.constrains('seats_max', 'seats_available')
    def _check_seats_limit(self):
        if self.seats_max and self.seats_available < 0:
            raise Warning(_('No more available seats.'))

    @api.one
    @api.constrains('date_begin', 'date_end')
    def _check_closing_date(self):
        if self.date_end < self.date_begin:
            raise Warning(_('Closing Date cannot be set before Beginning Date.'))

    @api.one
    @api.depends('registration_ids')
    def _count_registrations(self):
        self.count_registrations = len(self.registration_ids)

    @api.one
    def button_draft(self):
        self.state = 'draft'

    @api.one
    def button_cancel(self):
        for event_reg in self.registration_ids:
            if event_reg.state == 'done':
                raise Warning(_("You have already set a registration for this event as 'Attended'."
                                "Please reset it to draft if you want to cancel this event."))
        self.registration_ids.write({'state': 'cancel'})
        self.state = 'cancel'

    @api.one
    def button_done(self):
        self.state = 'done'

    def confirm_event(self):
        # send reminder that will confirm the event for all the people that were already confirmed
        # regs = self.registration_ids.filtered(lambda reg: reg.state not in ('draft', 'cancel'))
        # regs.mail_user_confirm()
        # send notification to registered users to confirm the event
        meeting_obj = self.env['mail.channel'].search([('id','in', self.ids)])
        for event in meeting_obj:
            event.write([event.id], {'state': 'confirm'})
            reg_ids = []
            for reg_id in event.registration_ids:
                reg_ids.append(reg_id.user_id.id)
            # msg_body = _('{"channeltype":"classRegistration","message":"Register for %s","className":"%s","reg_user_ids":"%s"}')%(
            #                                 event.name,event.name,reg_ids)
            msg_body = _("You're registered for '%s'") % event.name
            event.message_post(event.general_channel.id,
                                     body=msg_body,
                                     message_type="comment",
                                     content_subtype="html",
                                     subtype="mail.mt_comment",
                                     )
    @api.multi
    def start_broadcast(self):
        for event in self.env['product.broadcast'].search([('id', 'in', self.ids)]):
            reg_ids = []
            for reg_id in event.registration_ids:
                reg_ids.append(reg_id.user_id.id)
            msg_body = _("'%s' has been started") % (event.name)
            self.env['mail.channel'].message_post(event.general_channel.id,
                                                   body=msg_body,
                                                   message_type="comment",
                                                   content_subtype="html",
                                                   subtype="mail.mt_comment",
                                                   )

    @api.multi
    def stop_broadcast(self):
        for event in self.env['product.broadcast'].search([('id', 'in', self.ids)]):
            reg_ids = []
            for reg_id in event.registration_ids:
                reg_ids.append(reg_id.user_id.id)
            msg_body = _("'%s' has been completed") % (event.name)
            self.env['mail.channel'].message_post(event.general_channel.id,
                                                   body=msg_body,
                                                   message_type="comment",
                                                   content_subtype="html",
                                                   subtype="mail.mt_comment",
                                                   )

    @api.one
    def button_confirm(self):
        """ Confirm Event and send confirmation email to all register peoples """
        self.confirm_event()

    @api.one
    def button_refresh_results(self):
        self.write({'refresh_count': self.refresh_count + 1})


class BroadcastQuestions(models.Model):
    _name = 'broadcast.questions'
    _description = 'Broadcast Questions'


    name = fields.Many2one('product.question.bank', 'Question', required=True)
    posted_time = fields.Datetime('Posted on')
    event_id = fields.Many2one('product.broadcast', 'Product Broadcast')
    state = fields.Selection([('to_post', 'To Post'), ('posted', 'Posted')], 'State', default= 'draft')


    @api.multi
    def post_question(self):
        today = fields.datetime.now()
        meeting_obj = self.env['mail.channel']
        for bQues in self.env['mail.channel'].search([('id','in',self.ids)]):
            if bQues.event_id.date_begin <= today:
                msg_body = _(
                    '{"channeltype":"postQuestions","questionId":"%s","classId":"%s","question":"%s","option1":"%s","option2":"%s","option3":"%s","option4":"%s"}') % (
                               bQues.name.id, bQues.event_id.id, bQues.name.name, bQues.name.option_a,
                               bQues.name.option_b, bQues.name.option_c, bQues.name.option_d)
                bQues.message_post(bQues.event_id.question_channel.id,
                                         body=msg_body,
                                         message_type="comment",
                                         content_subtype="html",
                                         subtype="mail.mt_comment",
                                         )
                self.write(bQues.id, {'state': 'posted', 'posted_time': today})
            else:
                raise Warning(_("You must wait for the starting day of the event to do this action."))


class ProductAnswers(models.Model):
    _name = 'product.answers'
    _description = 'Product Answers'

    @api.multi
    def _get_result(self, field_name, arg):
        if not self.ids: return {}
        res = {}
        for answer in self.env['product.answers'].search([('id', 'in', self.ids)]):
            if answer.name == answer.question_id.correct_answer:
                res[answer.id] = "correct"
            else:
                res[answer.id] = "wrong"
        return res


    name = fields.Selection([('option_a', 'Option 1'),
                              ('option_b', 'Option 2'),
                              ('option_c', 'Option 3'),
                              ('option_d', 'Option 4'),
                              ('option_e', 'Option 5'),
                              ('option_f', 'Option 6'),
                              ('option_g', 'Option 7'),
                              ('option_h', 'Option 8')], 'Answer')
    event_id = fields.Many2one('product.broadcast', 'Event Name')
    question_id = fields.Many2one('product.question.bank', 'Question')
    user_id = fields.Many2one('res.users', 'User')
    result = fields.Selection([('correct', 'Correct'), ('wrong', 'Wrong')], string='Result')
    # result = fields.Selection(compute='_get_result',[('correct', 'Correct'), ('wrong', 'Wrong')], string='Result')



class ProductAttendances(models.Model):
    _name = 'product.attendances'
    _description = 'Attendance'


    name = fields.Many2one('res.users', 'Name')
    product_broadcast_id = fields.Many2one('product.broadcast', 'Product Broadcast')
    registeration_id = fields.Many2one('product.training', 'Training')
    start_time = fields.Datetime('Start Time')
    end_time = fields.Datetime('End Time')
    max_login = fields.Datetime('Maximum Login Time')
    min_login = fields.Datetime('Minimum Login Time')
    total_time = fields.Float('Total Time')


class ProductTraining(models.Model):
    _name = 'product.training'
    _description = 'Training'
    _inherit = ['mail.thread',]
    _order = 'name, create_date desc'

    @api.multi
    def _update_name(self):
        if not self.ids: return {False}
        res = {}
        for training in self.env['product.training'].search([('id','in', self.ids)]):
            if training.user_id.id is not False:
                res[training.id] = "%s's registration to %s" % (training.user_id.name, training.event_id.name)
        return res

    @api.multi
    def send_mail_to_trainer(self, query):
        Mail = self.env['mail.mail']
        u_pool = self.env['res.users']
        user_pool = self.env['res.users'].search([('user_id.id', '=', self.id)])
        u_name = user_pool.browse(self.uid)
        mail_from = u_name.email
        question = query
        mail_to = self.env['product.training'].search('id', 'in', self.ids).event_id.user_id.email
        ins_name = self.env['product.training'].search('id', 'in', self.ids).event_id.user_id.name
        event_name = self.env['product.training'].search('id', 'in', self.ids).event_id.name
        mail_values = {
            'email_from': mail_from,
            'email_to': mail_to,
            'subject': "Query regarding %s" % event_name,
            'body_html': """<p>Hello %s</p>
                        <p>i\'ve a query regarding  %s </p>
                        <p>%s</p>
                         <p>Thank you for your consideration.</p>""" % (ins_name, event_name, question),
            'notification': True,
        }
        mail_mail_obj = Mail.browse(Mail.create(mail_values))
        Mail.send(mail_mail_obj.id)
        return True


    name = fields.Char(compute='_update_name', type='char', string='Training Name')
    user_id = fields.Many2one('res.users', 'Employee', required=True)
    email = fields.Char('Email')
    phone = fields.Char('Phone')
    date_open = fields.Datetime('Registration Date')
    date_closed = fields.Datetime('Attended Date', readonly=True)
    no_of_participants = fields.Integer('No of Participants', required=True, default=1,
                                         readonly=True, states={'draft': [('readonly', False)]})
    event_id = fields.Many2one('product.broadcast', string='Event', readonly=True, states={'draft': [('readonly', False)]})
    feedback = fields.Text('Feedback about the course')
    state = fields.Selection([('draft', 'Unconfirmed'),
                               ('cancel', 'Rejected'),
                               ('open', 'Confirmed'),
                               ('done', 'Attended'), ], 'Status', default='draft', readonly=True, copy=False)
    event_begin_date = fields.Datetime(string="Event Start Date", related='event_id.date_begin', readonly=True)
    event_end_date = fields.Datetime(string="Event End Date", related='event_id.date_end', readonly=True)
    high_bandwidth_video_url_related = fields.Char(related='event_id.high_bandwidth_video_url',
                                                       string='High Bandwidth Video URL')
    low_bandwidth_video_url_related = fields.Char(related='event_id.low_bandwidth_video_url',
                                                      string='Low Bandwidth Video URL')
    general_channel_related = fields.Many2one(related='event_id.general_channel',
                                              relation='mail.channel', string='General Channel')
    user_question_channel_related = fields.Many2one(related='event_id.user_question_channel',
                                                    relation='mail.channel', string='User Question Channel')
    training_questions = fields.Many2many('product.question.bank', 'training_registeration_ques_rel',
                                           'registeration', 'ques_id', 'Training Questions')
    attendance = fields.Many2many('product.attendances', 'attendances_reg_ques_rel', 'reg_id',
                                   'attendances_id', 'Training Attendance')
    broadcast_type = fields.Selection(related='event_id.broadcast_type',
                                     selection=[('broadcast', 'Broadcast'),
                                                ('product_training', 'New Product Training'),
                                                ('refresher_courses', 'Refresher Courses'),
                                                ('code_of_conduct', 'Code Of Conduct')], string='Broadcast Type')


    _sql_constraints = [
        ('user_event_unique', 'unique(user_id, event_id)', 'User and Event must be unique'),
    ]

    @api.model
    def unlink(self):
        for training in self.env['product.training'].search([('id','in', self.ids)]):
            question_channel = training.event_id.question_channel.id
            general_channel = training.event_id.general_channel.id
            user_channel = training.event_id.user_question_channel.id
            channel_partner_ids = self.env['mail.channel.partner'].search([('channel_id', 'in', [question_channel, general_channel, user_channel]),
                ('partner_id', '=', training.user_id.partner_id.id)])
            for ch_par_id in channel_partner_ids:
                self.env['mail.channel.partner'].unlink(ch_par_id)
        return super(ProductTraining, self).unlink(self.uid, self.ids)

    @api.model
    def write(self, vals):
        for training in self.env['product.training'].search([('id','in', self.ids)]):
            question_channel = training.event_id.question_channel.id
            general_channel = training.event_id.general_channel.id
            user_channel = training.event_id.user_question_channel.id
            # print "question_channel -- %s" % question_channel
            # print "general_channel -- %s" % general_channel
            # print "user_channel -- %s" % user_channel
            if 'user_id' in vals:
                channel_partner_ids = self.env['mail.channel.partner'].search([('channel_id', 'in', [question_channel, general_channel, user_channel]),
                    ('partner_id', '=', training.user_id.partner_id.id)])
                for ch_par_id in channel_partner_ids:
                    self.env['mail.channel.partner'].unlink(ch_par_id)
                for user in self.env['res.users'].browse(vals['user_id']):
                    partner_id = user.partner_id.id
                    self.env['mail.channel'].write(question_channel,
                                                        {'channel_partner_ids': [(4, partner_id)]})
                    self.env['mail.channel'].write(general_channel,
                                                        {'channel_partner_ids': [(4, partner_id)]})
                    self.env['mail.channel'].write(user_channel,
                                                        {'channel_partner_ids': [(4, partner_id)]})
            if 'state' in vals:
                if vals['state'] == 'cancel' or vals['state'] == 'done':
                    channel_partner_ids = self.env['mail.channel.partner'].search([
                        ('channel_id', 'in', [question_channel, general_channel, user_channel]),
                        ('partner_id', '=', training.user_id.partner_id.id)])
                    for ch_par_id in channel_partner_ids:
                        self.env['mail.channel.partner'].unlink(ch_par_id)
                elif vals['state'] == 'open':
                    self.env['mail.channel'].write(question_channel,
                                                        {'channel_partner_ids': [(4, training.user_id.partner_id.id)]})
                    self.env['mail.channel'].write(general_channel,
                                                        {'channel_partner_ids': [(4, training.user_id.partner_id.id)]})
                    self.env['mail.channel'].write(user_channel,
                                                        {'channel_partner_ids': [(4, training.user_id.partner_id.id)]})
        return super(ProductTraining, self).write(vals)

    @api.one
    def do_draft(self):
        self.state = 'draft'

    @api.one
    def confirm_registration(self):
        # self.event_id.message_post(body=_('New registration confirmed: %s.') % (self.name or ''),
        #   subtype="event.mt_product_training")
        # self.message_post(body=_('Event Registration confirmed.'))
        self.state = 'open'

    @api.one
    def registration_open(self):
        """ Open Registration """
        # self.mail_user()
        self.confirm_registration()

    # @api.one
    # def button_reg_close(self):
    #     """ Close Registration """
    #     today = fields.datetime.now()
    #     if self.event_id.date_begin <= today:
    #         self.write({'state': 'done', 'date_closed': today})
    #     else:
    #         raise Warning(_("You must wait for the starting day of the event to do this action."))

    @api.one
    def button_reg_cancel(self):
        self.state = 'cancel'

    # @api.one
    # def mail_user(self):
    #     """Send email to user with email_template when registration is done """
    #     if self.event_id.state == 'confirm' and self.event_id.email_confirmation_id:
    #         self.mail_user_confirm()

    # @api.one
    # def mail_user_confirm(self):
    #     """Send email to user when the event is confirmed """
    #     template = self.event_id.email_confirmation_id
    #     if template:
    #         mail_message = template.send_mail(self.id)


# Knowledge Menu Class
class ParrotKnowledge(models.Model):
    _name = 'parrot.knowledge'
    _description = 'Parrot Knowledge'
    _order = 'id desc'

    @api.model
    def search_read(self, domain=None, fields=None, offset=0,limit=None, order=None):
        user_obj=self.env['res.users'].search([('id','=',self.id)])
        user_br=self.env['res.users'].browse(user_obj)
        if user_br.user_category.id:
            if user_br.user_category.name=='KAG' or user_br.user_category.name=='CAG':
                domain=[('user_category','=',user_br.user_category.id)]
            else:
                domain=None
        else:
            domain=None
        res = super(ParrotKnowledge, self).search_read(domain, fields, offset, limit, order)
        return res


    def _has_group_access(self):
        groups_pool = self.env['res.groups']
        user_pool = self.env['res.users']
        res = {}
        user_group_ids = []
        has_access = False
        # get Current User groups
        user_groups = user_pool.browse(self._uid).groups_id
        for user_grp in user_groups:
            user_group_ids.append(user_grp.id)
        # check in knowledge groups
        for know in self.env['parrot.knowledge'].search([('id','in',self.ids)]):
            if not user_pool.has_group('base.group_erp_manager'):
                for group in know.group_ids:
                    if group.id in user_group_ids:
                        has_access = True
                res[know.id] = has_access
            else:
                res[know.id] = True
        return res

    def _search_has_group_access(self, obj):
        user_pool = self.env['res.users']
        user_group_ids = []
        user_groups = user_pool.browse(self.uid).groups_id
        for user_grp in user_groups:
            user_group_ids.append(user_grp.id)
        knowledge_ids = self.env['parrot.knowledge'].search_read([
            ('group_ids', 'in', user_group_ids)])
        return [('id', 'in', [know['id'] for know in knowledge_ids])]
    #
    # def mobile_create(self, cr, uid, vals, context=None):
    #     encoded_data = vals['fileData']
    #     header_vals={}
    #     user_id = vals['user_id']
    #     file_name=vals['fileName']
    #     img_file=file_name.rfind('\\')
    #     img_file_name=file_name[img_file + 1:]
    #     group_id=self.pool.get('res.groups').search(cr, uid,[('name','=','Employee')],context=context)
    #     document_vals = {'name': img_file_name,
    #                      'datas': encoded_data,
    #                      'datas_fname': '111sssimage.jpeg',
    #                      'res_model': 'parrot.knowledge',
    #                      'type': 'binary'}
    #     ir_id = self.pool.get('ir.attachment').create(cr, uid,document_vals,context=context)
    #     header_vals.update({'menutype':'document_upload','document_ids':[[6, False, [ir_id]]],'group_ids':[[6, False, [group_id]]],'name':img_file_name,'type':'attachment','user_id':user_id})
    #     record_id = self.create(cr, uid, header_vals, context=context)
    #     return True

    @api.model
    def create(self, vals):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        tokens = []
        send = True
        today_date = datetime.date.today()
        if 'due_date' in vals:
            if str(vals['due_date']) <= str(today_date):
                raise UserError(_("Due Date should not be the same as or anterior to Today's Date"))
            else:
                result = super(ParrotKnowledge, self).create(vals)
                if 'group_ids' in vals:
                    name = vals['name']
                    training = result
                    for record in self.env['parrot.knowledge'].browse(training.id):
                        menutype = record.menutype
                    users_pool = self.env['res.users']
                    groups_pool = self.env['res.groups']
                    obj_id = users_pool.search([('id', 'in', [vals['user_id']])])
                    for user in users_pool.browse(obj_id):
                        group_ids = groups_pool.browse(vals['group_ids'][0][2])
                        user_arr = []
                        tokens = []
                        for group_id in group_ids:
                            for user in group_id.users:
                                if user not in user_arr:
                                    user_arr.append(user)
                        for user in user_arr:
                            if user.user_tokens:
                                token_pool = self.env['user.token']
                                for tok in token_pool.browse(user.user_tokens.ids):
                                    tokens.append(tok.name)
                    data_json = {"channeltype": "newTraining",
                                 "name": name,
                                 "message": "You have a new training: %s" % name,
                                 "training_id": training.id,
                                 "menutype": menutype}
                    data = {'notification': {'title': name,
                                             'body': "You have a new training: %s" % name,
                                             'sound': 'default',
                                             'click_action': 'FCM_PLUGIN_ACTIVITY',
                                             'icon': 'fcm_push_icon',
                                             'color': '#F8880D'},
                            'data': data_json,
                            'registration_ids': tokens,
                            'priority': 'high',
                            'forceShow': 'true',
                            'restricted_package_name': ''}
                    headers = {'Authorization': fcm_authorization_key,
                               'Content-Type': 'application/json'}
                    _logger.info("Input Data ------ %s" % data)
                    data = json.dumps(data)
                    try:
                        if send:
                            res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                            _logger.info("FCM Response ------ %s" % res.text)
                    except:
                        _logger.info('Notification not sent')
                return result
        else:
            result = super(ParrotKnowledge, self).create(vals)
            return result
                # return super(ParrotKnowledge, self).create(vals)

    @api.model
    def write(self, vals):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        training = self.ids
        for record in self.env['parrot.knowledge'].browse(training):
            menutype = record.menutype
        today_date = datetime.date.today()
        search_id = self.env['parrot.knowledge'].search([('id', 'in', self.ids)])
        for page_id in self.env['parrot.knowledge'].browse(search_id):
            name = page_id.name
            menu_type = page_id.menutype
            due_date = page_id.due_date
            group = page_id.group_ids
            if 'due_date' in vals:
                if str(vals['due_date']) <= str(today_date):
                    raise UserError(_("Due Date should not be the same as or anterior to Today's Date"))
                if 'group_ids' in vals:
                    users_pool = self.env['res.users']
                    groups_pool = self.env['res.groups']
                    obj_id = users_pool.search([('id', 'in', [page_id.user_id.id])])
                    for user in users_pool.browse(obj_id):
                        group_ids = groups_pool.browse(vals['group_ids'][0][2])
                        user_arr = []
                        tokens = []
                        for group_id in group_ids:
                            for user in group_id.users:
                                if user not in user_arr:
                                    user_arr.append(user)
                        for user in user_arr:
                            if user.user_tokens:
                                token_pool = self.env['user.token']
                                for tok in token_pool.browse(user.user_tokens.ids):
                                    tokens.append(tok.name)
                    data_json = {"channeltype": "newTraining",
                                 "name": name,
                                 "message": "You Have a new Training: %s" % name,
                                 'training_id': training,
                                 'menutype': menutype}
                    data = {'notification': {'title': name,
                                             'body': "You Have a new Training: %s" % name,
                                             'sound': 'default',
                                             'click_action': 'FCM_PLUGIN_ACTIVITY',
                                             'icon': 'fcm_push_icon'},
                            'data': data_json,
                            'registration_ids': tokens,
                            'priority': 'high',
                            'forceShow': 'true',
                            'restricted_package_name': ''}
                    headers = {'Authorization': fcm_authorization_key,
                               'Content-Type': 'application/json'}
                    _logger.info("Input Data ------ %s" % data)
                    data = json.dumps(data)
                    if send:
                        res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                        _logger.info("FCM Response ------ %s" % res.text)
                else:
                    users_pool = self.env['res.users']
                    groups_pool = self.env['res.groups']
                    obj_id = users_pool.search([('id', 'in', [page_id.user_id.id])])
                    for user in users_pool.browse(obj_id):
                        group_ids = groups_pool.browse(group.ids)
                        user_arr = []
                        tokens = []
                        for group_id in group_ids:
                            for user in group_id.users:
                                if user not in user_arr:
                                    user_arr.append(user)
                        for user in user_arr:
                            if user.user_tokens:
                                token_pool = self.env['user.token']
                                for tok in token_pool.browse(user.user_tokens.ids):
                                    tokens.append(tok.name)
                    data_json = {"channeltype": "newTraining",
                                 "name": name,
                                 "message": "You Have a new Training: %s" % name,
                                 'training_id': training,
                                 'menutype': menutype}
                    data = {'notification': {'title': name,
                                             'body': "You Have a new Training: %s" % name,
                                             'sound': 'default',
                                             'click_action': 'FCM_PLUGIN_ACTIVITY',
                                             'icon': 'fcm_push_icon'},
                            'data': data_json,
                            'registration_ids': tokens,
                            'priority': 'high',
                            'forceShow': 'true',
                            'restricted_package_name': ''}
                    headers = {'Authorization': fcm_authorization_key,
                               'Content-Type': 'application/json'}
                    _logger.info("Input Data ------ %s" % data)
                    data = json.dumps(data)
                    if send:
                        res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                        _logger.info("FCM Response ------ %s" % res.text)
        res = super(ParrotKnowledge, self).write(vals)
        write_id = self.env['parrot.knowledge'].search('id', 'in', self.ids)
        user_obj = self.env['res.users'].browse(self.uid)
        group_name = ""
        for group in user_obj.groups_id:
            search_group_id = self.env["res.groups"].search([('id', '=', group.id)])
            search_group_obj = self.env["res.groups"].browse(search_group_id)
            if search_group_obj.name:
                if search_group_obj.name == "BA":
                    if group_name != "AM" and group_name != "RA" and group_name != "RM" and group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                        group_name = "BA"
                elif search_group_obj.name == "Account Manager":
                    if group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                        group_name = "AM"
                elif search_group_obj.name == "Regional Admin":
                    group_name = "RA"
                elif search_group_obj.name == "Regional Manager" and group_name != "AM" and group_name != "GM":
                    group_name = "RM"
                elif search_group_obj.name == "General Manager":
                    if group_name != "CSO" and group_name != "CCO":
                        group_name = "GM"
                elif search_group_obj.name == "CSO":
                    if group_name != "CCO":
                        group_name = "CSO"
                elif search_group_obj.name == "CCO":
                    group_name = "CCO"
        vals = {'name': write_id.name, 'type': write_id.type, 'delete_by': self.uid, 'user_id': write_id.user_id.id,
                'delete_date': datetime.date.today(), 'region': user_obj.region_id, 'designation': group_name,
                'module': 'Parrot Knowledge', 'status': 'Updated', 'due_date': write_id.due_date}
        if write_id.type == 'link':
            vals.update({'document_name': write_id.link})
        elif write_id.type == 'video':
            vals.update({'document_name': write_id.video_ids.name})
        elif write_id.type == 'attachment':
            vals.update({'document_name': write_id.document_ids.name})
        elif write_id.type == 'audio':
            vals.update({'document_name': write_id.audio_ids.name})
        self.env['document.delete'].create(vals)
        return res

    @api.model
    def unlink(self):
        deleted_id = self.env['parrot.knowledge'].search('id','in', self.ids)
        user_obj = self.env['res.users'].browse(self.uid)
        group_name = ""
        for group in user_obj.groups_id:
            search_group_id = self.env["res.groups"].search([('id', '=', group.id)])
            search_group_obj = self.env["res.groups"].browse(search_group_id)
            if search_group_obj.name:
                if search_group_obj.name == "BA":
                    if group_name != "AM" and group_name != "RA" and group_name != "RM" and group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                        group_name = "BA"
                elif search_group_obj.name == "Account Manager":
                    if group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                        group_name = "AM"
                elif search_group_obj.name == "Regional Admin":
                    group_name = "RA"
                elif search_group_obj.name == "Regional Manager" and group_name != "AM" and group_name != "GM":
                    group_name = "RM"
                elif search_group_obj.name == "General Manager":
                    if group_name != "CSO" and group_name != "CCO":
                        group_name = "GM"
                elif search_group_obj.name == "CSO":
                    if group_name != "CCO":
                        group_name = "CSO"
                elif search_group_obj.name == "CCO":
                    group_name = "CCO"
        db_name = self.cr.dbname
        if db_name.find("eco") >= 0:
            vals = {'name': deleted_id.name, 'type': deleted_id.type, 'delete_by': self.uid,
                    'user_id': deleted_id.user_id.id,
                    'delete_date': datetime.date.today(), 'region': user_obj.region_id, 'designation': group_name,
                    'module': 'Parrot Knowledge', 'status': 'Deleted', 'due_date': deleted_id.due_date}
        else:
            vals = {'name': deleted_id.name, 'type': deleted_id.type, 'delete_by': self.uid,
                    'user_id': deleted_id.user_id.id,
                    'delete_date': datetime.date.today(), 'designation': group_name,
                    'module': 'Parrot Knowledge', 'status': 'Deleted', 'due_date': deleted_id.due_date}
        if deleted_id.type == 'link':
            vals.update({'document_name': deleted_id.link})
        elif deleted_id.type == 'video':
            vals.update({'document_name': deleted_id.video_ids.name})
        elif deleted_id.type == 'attachment':
            vals.update({'document_name': deleted_id.document_ids.name})
        elif deleted_id.type == 'audio':
            vals.update({'document_name': deleted_id.audio_ids.name})
        self.env['document.delete'].create(vals)
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        today_date = datetime.date.today()
        # search_id = self.search(cr, uid, ids)
        for page_id in self.env['parrot.knowledge'].search('id', 'in', self.ids):
            training = page_id.id
            for record in self.env['parrot.knowledge'].browse(training):
                menutype = record.menutype
            name = page_id.name
            menu_type = page_id.menutype
            due_date = page_id.due_date
            group = page_id.group_ids
            users_pool = self.env['res.users']
            groups_pool = self.env['res.groups']
            obj_id = users_pool.search([('id', 'in', [page_id.user_id.id])])
            for user in users_pool.browse(obj_id):
                group_ids = groups_pool.browse(group.ids)
                user_arr = []
                for group_id in group_ids:
                    for user in group_id.users:
                        if user not in user_arr:
                            user_arr.append(user)
                for user in user_arr:
                    if user.user_tokens:
                        token_pool = self.env['user.token']
                        for tok in token_pool.browse(user.user_tokens.ids):
                            tokens.append(tok.name)
            data_json = {"channeltype": "newTraining",
                         "name": name,
                         "message": "The Training %s has been removed" % name,
                         'training_id': training,
                         'menutype': menutype}
            data = {'notification': {'title': name,
                                     'body': "The Training %s has been removed" % name,
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                raise UserError(_('Notification not sent'))
        return super(ParrotKnowledge, self).unlink(self.uid, self.ids)


    def _due_date_reminder(self):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param(self.uid, 'fcm.authorization.key')
        search_ids = self.env['parrot_knowledge'].search([])
        send = True
        today = datetime.date.today()
        _logger.info("Due Date Reminder Cron  ------ ")
        for item in self.env['parrot.knowledge'].browse(search_ids):
            training = item.id
            for record in self.env['parrot.knowledge'].browse(training):
                menutype = record.menutype
            if item.due_date:
                if datetime.datetime.strptime(item.due_date, '%Y-%m-%d').date() - datetime.timedelta(days=1) == today:
                    name = item.name
                    group = item.group_ids
                    users_pool = self.env['res.users']
                    groups_pool = self.env['res.groups']
                    obj_id = users_pool.search([('id', 'in', [item.user_id.id])])
                    for user in users_pool.browse(obj_id):
                        group_ids = groups_pool.browse(group.ids)
                        user_arr = []
                        tokens = []
                        for group_id in group_ids:
                            for user in group_id.users:
                                if user not in user_arr:
                                    user_arr.append(user)
                        for user in user_arr:
                            if user.user_tokens:
                                token_pool = self.env['user.token']
                                for tok in token_pool.browse(user.user_tokens.ids):
                                    tokens.append(tok.name)
                    data_json = {"channeltype": "newTraining",
                                 "name": "Document Due for Tomorrow",
                                 "message": "The Document in Training %s is due tomorrow" % name,
                                 'training_id': training,
                                 'menutype': menutype}
                    data = {'notification': {'title': "Document Due for Tomorrow",
                                             'body': "The Document in Training %s is due tomorrow" % name,
                                             'sound': 'default',
                                             'click_action': 'FCM_PLUGIN_ACTIVITY',
                                             'icon': 'fcm_push_icon'},
                            'data': data_json,
                            'registration_ids': tokens,
                            'priority': 'high',
                            'forceShow': 'true',
                            'restricted_package_name': ''}
                    headers = {'Authorization': fcm_authorization_key,
                               'Content-Type': 'application/json'}
                    _logger.info("Input Data ------ %s" % data)
                    data = json.dumps(data)
                    try:
                        if send:
                            res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                            _logger.info("FCM Response ------ %s" % res.text)
                    except:
                        raise UserError(_('Notification not sent'))
        return True

    def post_question_to_trainer(self, training_id, query):
        Mail = self.env['mail.mail']
        u_pool = self.env['res.users']
        u_name = u_pool.browse(self.uid)
        mail_from = u_name.email
        question = query
        mail_to = self.env['parrot.knowledge'].browse(training_id).user_id.email
        trainer_name = self.env['parrot.knowledge'].browse(training_id).user_id.name
        training_name = self.env['parrot.knowledge'].browse(training_id).name
        mail_values = {
            'email_from': mail_from,
            'email_to': mail_to,
            'subject': "Query regarding %s" % training_name,
            'body_html': """<p>Hello %s</p>
                        <p>i\'ve a query regarding  %s </p>
                        <p>%s</p>
                         <p>Thank you for your consideration.</p>""" % (trainer_name, training_name, question),
            'notification': True,
        }
        mail_mail_obj = Mail.browse(Mail.create(mail_values))
        Mail.send(mail_mail_obj.id)
        return True

    '''def write(self,cr, uid, ids,vals,context):
        tokens = []
        if 'group_ids' in vals:
            obj = self.browse(cr,uid,ids)
            users_pool = self.env['res.users']
            groups_pool = self.env['res.groups']
            for training in self.browse(cr,uid,ids):
                group_ids = groups_pool.browse(cr,uid,vals['group_ids'][0][2],context)
                notify_users = []
                for group_id in group_ids:
                    for user in group_id.users:
                        if user.id not in notify_users:
                            for token in user.user_tokens:
                                tokens.append(token.name)
                            notify_users.append(user.id)
            data_json ={
                'id': 'training',
                'title':obj.name}
            data = {
                'notification':{
                    'title': obj.name,
                    'body':'New Training:'+ obj.name,
                    'sound':'default',
                    'click_action':'FCM_PLUGIN_ACTIVITY',
                    'icon':'fcm_push_icon'},
                'data':data_json,
                'registration_ids':tokens,
                'priority':'high',
                'forceShow': 'true',
                'restricted_package_name':''}
            headers = {'Authorization':fcm_authorization_key,
                       'Content-Type': 'application/json'}
        """_logger.info("Input Data ------ %s"%data)"""
        data=json.dumps(data)
        res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
        _logger.info("FCM Response ------ %s"%res.text)'''

    def _time_exceeded(self):
        res = {}
        if not self.ids:
            return {False}
        for rec in self.env['parrot.knowledge'].search([('id','in', self.ids)]):
            res[rec.id] = False
            d_date = rec.due_date
            t_date = datetime.date.today()
            if d_date:
                if (str(d_date) < str(t_date)):
                    res[rec.id] = True
            else:
                res[rec.id] = False
        return res


    name = fields.Char('Name', required=True)
    menutype = fields.Selection([('processes', 'Processes'),
                                  ('policies', 'Policies'),
                                  ('manuals', 'Manuals'),
                                  ('products', 'Products'),
                                  ('marketing', 'Marketing'),
                                  ('prices', 'Prices'),
                                  ('product_training', 'New Product Training'),
                                  ('refresher_courses', 'Refresher Courses'),
                                  ('code_of_conduct', 'Code Of Conduct'),
                                  ('document_upload', 'Document Upload')], 'Menu Type')
    type = fields.Selection([('link', 'Youtube Link'), ('video', 'Video Attachment'),
                              ('attachment', 'Attachment'), ('audio', 'Audio Attachment')], 'Types of Content',default='attachment')
    link = fields.Char('Youtube Link')
    registration_ids = fields.Many2many('product.broadcast', 'registration_knowledge_rel', 'registration_id',
                                         'knowledge_id', 'Upcoming Broadcasts')
    group_ids = fields.Many2many('res.groups', 'groups_knowlwdge_rel', 'group_id', 'knowledge_id', 'Group Access',
                                  required=True)
    document_ids = fields.Many2many('ir.attachment', 'knowledge_attachment_rel', 'attachment_id', 'knowledge_id',
                                     'Document')
    video_ids = fields.Many2many('ir.attachment', 'knowledge_video_rel', 'video_id', 'knowledge_id', 'Video')
    audio_ids = fields.Many2many('ir.attachment', 'knowledge_audio_rel', 'audio_id', 'knowledge_id', 'Audio')
    has_group_access = fields.Boolean(compute='_has_group_access', fnct_search=_search_has_group_access,
                                        string='Has Group Access')
    user_id = fields.Many2one('res.users', 'Owner', required=True)
    user_category = fields.Many2one('user.category', 'User Category')
    due_date = fields.Date('Content Expiry Date')
    time_exceeded = fields.Boolean(compute='_time_exceeded', string='Time Exceeded',default=False)
    question_ids = fields.Many2many('product.question.bank', 'question_knowledge_rel', 'question_id',
                                     'knowledge_id', 'Questions')


#     _defaults = {
#         'type': 'attachment',
#         'time_exceeded': False,
#     }


class EmpDocDetails(models.Model):
    _name = 'emp.doc.details'
    _description = 'Employee Document Details'

    def _total_docs(self):
        user_dict = {}
        res = {}
        user_ids = self.env['res.users'].search([])
        for user in user_ids:
            user_dict[user] = 0
        record_ids = self.env['parrot.knowledge'].search([])
        for record in self.env['parrot.knowledge'].browse(record_ids.ids):
            dummy = []
            for group in record.group_ids:
                users = self.env['res.users'].search([('groups_id', 'in', group.id)])
                if users:
                    for user in users:
                        if record.type == 'audio':
                            if user not in dummy:
                                user_dict[user] += len(record.audio_ids)
                                dummy.append(user)
                        elif record.type == 'video':
                            if user not in dummy:
                                user_dict[user] += len(record.video_ids)
                                dummy.append(user)
                        elif record.type == 'attachment':
                            if user not in dummy:
                                user_dict[user] += len(record.document_ids)
                                dummy.append(user)
                        elif record.type == 'link':
                            if user not in dummy:
                                if record.link:
                                    if record.link.strip() and record.link.strip() != "":
                                        user_dict[user] += 1
                                        dummy.append(user)
        for record in self.env['emp.doc.details'].search([('id','in', self.ids)]):
            res[record.id] = user_dict[record.name]
        return res

    def _read_docs(self):
        user_dict = {}
        res = {}
        user_ids = self.env['res.users'].search([])
        for user in user_ids:
            user_dict[user] = 0
            record_ids = self.env['document.eye'].search([('name', '=', user.id)])
            dummy = []
            for record in self.env['document.eye'].browse(record_ids):
                if record.document_id not in dummy:
                    user_dict[user] += 1
                    dummy.append(record.document_id)
        for record in self.env['emp.doc.details'].search([('id','in', self.ids)]):
            res[record.id] = user_dict[record.name]
        return res

    def _unread_docs(self):
        user_dict = {}
        res = {}
        user_ids = self.env['res.users'].search([('id','in',self.ids)])
        for user in user_ids:
            user_dict[user] = 0
        for record in self.env['emp.doc.details'].search([('id','in', self.ids)]):
            res[record.id] = record.total_docs - record.read_docs
        return res


    name = fields.Many2one('res.users', "User")
    total_docs = fields.Integer(compute='_total_docs', string='Total Documents')
    read_docs = fields.Integer(compute='_read_docs', string='Read Documents')
    unread_docs = fields.Integer(compute='_unread_docs', string='Unread Documents')


#     _sql_constraints = [
#         ('name_uniq', 'unique(name)', 'The User must be unique !'),
#     ]


class DocumentEye(models.Model):
    _name = 'document.eye'
    _description = 'Documents View Status'
    _order = 'id desc'


    name = fields.Many2one('res.users', 'User', required=True)
    document_id = fields.Many2one('parrot.knowledge', 'Document', required=True, ondelete='cascade')
    last_seen = fields.Datetime('Last Seen', default=fields.datetime.now())


#     _defaults = {
#         'last_seen': lambda obj, cr, uid, context: fields.datetime.now(),
#     }

#     _sql_constraints = [
#         ('document_eye_unique', 'unique (name, document_id)', 'User already viewed this document'),
#     ]


class MyPerformance(models.Model):
    _name = 'my.performance'
    _description = 'Employee Performance'

    def _create_name(self):
        res = {}
        for rec in self.env['my.performance'].search([('id','in', self.ids)]):
            if rec.user_id and rec.kpi and rec.date:
                res[rec.id] = "Performance of %s in %s on %s" % (rec.user_id.name, rec.kpi.name, rec.date)
            else:
                res[rec.id] = "New Performance"
        return res


    name = fields.Char(compute='_create_name', string='Name', store=True)
    user_id = fields.Many2one('res.users', 'User', required=True,default=lambda self: self.env.user)
    date = fields.Date('Date', required=True,default=fields.datetime.now())
    type = fields.Selection([('image', 'Image'), ('text', 'Text'),
                              ('integer', 'Integer'), ('boolean', 'Boolean')], 'Type', required=True,default='text')
    kpi = fields.Many2one('track.kpi', 'KPI', required=True)
    field = fields.Char('Field')
    value_text = fields.Char('Value')
    value_int = fields.Float('Value')
    value_boolean = fields.Boolean('Value')
    parameter = fields.Char('Parameter')

    image = odoo.fields.Binary("Image", attachment=True,
                                  help="This field holds the image used as avatar for this contact, limited to 1024x1024px")

#     _defaults = {
#         'type': 'text',
#         'user_id': lambda obj, cr, uid, context: uid,
#         'date': lambda obj, cr, uid, context: fields.datetime.now(),
#     }

    def get_params(self, kpi, limit):
        params = []
        per_data = self.env['my.performance'].search([('kpi', '=', kpi), ('user_id', '=', self.id),
                                                                         ('type', 'in', ['integer', 'boolean'])],
                                                               order="date DESC")
        for dat in per_data:
            params.append(dat['parameter'])
        return params

    def get_dates(self, kpi, limit):
        dates = []
        per_data = self.env['my.performance'].search([('kpi', '=', kpi), ('user_id', '=', self.id),
                                                                         ('type', 'in', ['integer', 'boolean'])],
                                                               order="date DESC", limit=limit)
        for dat in per_data:
            dates.append(dat['date'])
        return dates

    def get_param_values(self, kpi, date):
        params = {}
        per_data = self.env['my.performance'].search([('kpi', '=', kpi), ('user_id', '=', self.id),
                                                                         ('date', '=', date),
                                                                         ('type', 'in', ['integer', 'boolean'])],
                                                               order="date DESC")

        for dat in per_data:
            params_dat = self.env['my.performance'].search([('kpi', '=', kpi), ('user_id', '=', self.id),
                                                                               ('date', '=', date),
                                                                               ('type', 'in', ['integer', 'boolean']),
                                                                               ('parameter', '=', dat['parameter'])],
                                                                     order="date DESC")
            vals = {}
            for param_dat in params_dat:
                if param_dat['type'] == 'integer':
                    val = param_dat['value_int']
                elif param_dat['type'] == 'boolean':
                    if param_dat['value_boolean']:
                        param_dat['value_boolean'] = 1
                    elif not param_dat['value_boolean']:
                        param_dat['value_boolean'] = 0
                    val = param_dat['value_boolean']
                vals[date] = val
            params[dat['parameter']] = vals
        return params

    def get_date_values(self, kpi, parameter):
        dates = {}
        per_data = self.env['my.performance'].search([('kpi', '=', kpi), ('user_id', '=', self.id),
                                                                         ('parameter', '=', parameter),
                                                                         ('type', 'in', ['integer', 'boolean'])],
                                                               order="date DESC")
        for dat in per_data:
            dates_dat = self.env['my.performance'].search([('kpi', '=', kpi), ('user_id', '=', self.id),
                                                                              ('parameter', '=', parameter),
                                                                              ('type', 'in', ['integer', 'boolean']),
                                                                              ('date', '=', dat['date'])],
                                                                    order="date DESC")
            vals = {}
            for date_dat in dates_dat:
                if date_dat['type'] == 'integer':
                    val = date_dat['value_int']
                elif date_dat['type'] == 'boolean':
                    if date_dat['value_boolean']:
                        date_dat['value_boolean'] = 1
                    elif not date_dat['value_boolean']:
                        date_dat['value_boolean'] = 0
                    val = date_dat['value_boolean']
                vals[parameter] = val
            dates[dat['date']] = vals
        return dates

    # Need to removed after every app implemented with get_performance_list
    # Last updated on Tuesday, 20 February 2018
    def get_permormance_list(self):
        _logger.info("In get List")
        user_name = self.env['res.users'].browse(self.id).name
        all_per = self.env['my.performance'].search_read([('user_id', '=', self.id)], order="date ASC")
        # print all_per
        # print "------"
        sorted(all_per, key=itemgetter('date'))
        # print all_per
        all_performances = {}
        team_user_names = [user_name]
        for tper in all_per:
            _logger.info("In all per loop---")
            _logger.info(tper)
            if tper['kpi'] != False:
                kpi = tper['kpi'][1]
                usr = tper['user_id'][1]
                if 'parameter' in tper:
                    param = tper['parameter']
                else:
                    param = False
                if 'value' in tper:
                    val = tper['value']
                else:
                    if tper['type'] == 'text':
                        val = tper['value_text']
                    elif tper['type'] == 'integer':
                        val = tper['value_int']
                    elif tper['type'] == 'boolean':
                        val = tper['value_boolean']
                    elif tper['type'] == 'image':
                        val = tper['image']
                if kpi in all_performances:
                    if param:
                        if param in all_performances[kpi]:
                            all_performances[kpi][param][usr] = val
                        else:
                            all_performances[kpi][param] = {usr: val}
                    else:
                        all_performances[kpi]['team_performance'] = {'user_name': usr,
                                                                     'type': tper['type'],
                                                                     'id': tper['id'],
                                                                     'value': val}
                else:
                    if param:
                        all_performances[kpi] = {param: {usr: val}}
                    else:
                        all_performances[kpi] = {'team_performance': {'user_name': usr,
                                                                      'type': tper['type'],
                                                                      'id': tper['id'],
                                                                      'value': val}}
        _logger.info(all_performances)
        result = {}
        for kpi, params in all_performances.iteritems():
            result[kpi] = {}
            header = []
            for p, v in params.iteritems():
                header.append(p)
            result[kpi]['header'] = header
            for usr_name in team_user_names:
                user_list = []
                found = False
                i = 0
                for heading in header:
                    if heading == 'team_performance':
                        if heading in params.keys():
                            if 'id' in params[heading].keys():
                                result[kpi]['id'] = params[heading]['id']
                            if 'user_name' in params[heading].keys():
                                result[kpi]['user_name'] = params[heading]['user_name']
                            if 'type' in params[heading].keys():
                                result[kpi]['type'] = params[heading]['type']
                            if 'value' in params[heading].keys():
                                result[kpi]['value'] = params[heading]['value']
                    else:
                        i = i + 1
                        if i < 5:
                            if heading in params.keys():
                                if usr_name in params[heading].keys():
                                    user_list.append(params[heading][usr_name])
                                    found = True
                                else:
                                    user_list.append('-')
                            else:
                                user_list.append('-')
                        if found:
                            result[kpi][usr_name] = user_list
        graphs_ids = self.env['performance.graph'].search([('type', '=', 'employee_performance')])
        data = []
        all_cols = {}
        rows = []
        performance_pool = self.env['my.performance']
        for each_graph in self.env['performance.graph'].browse(graphs_ids):
            kpi = each_graph.kpi.id
            kpi_name = each_graph.kpi.name
            # print "graph KIP: %s" % kpi
            if each_graph.graph_row == 'date' or each_graph.graph_col == 'parameter':
                if each_graph.row_limit:
                    row_limit = each_graph.row_limit
                else:
                    row_limit = 6
                if each_graph.col_limit:
                    col_limit = each_graph.col_limit
                else:
                    col_limit = 6
                rows = self.get_dates(kpi, row_limit)
                for each_date in rows:
                    column = self.get_param_values(kpi, each_date)
                    data.append(column)
            if each_graph.graph_row == 'parameter' or each_graph.graph_col == 'date':
                if each_graph.row_limit:
                    row_limit = each_graph.row_limit
                else:
                    row_limit = 6
                if each_graph.col_limit:
                    col_limit = each_graph.col_limit
                else:
                    col_limit = 6
                rows = self.get_params(kpi, row_limit)
                for each_param in rows:
                    column = self.get_date_values(kpi, each_param)
                    data.append(column)
            ret_data = {}
            for dat in data:
                for col, obj in dat.iteritems():
                    ret_obj = {}
                    if col in ret_data:
                        ret_obj = ret_data[col]
                    for row, val in obj.iteritems():
                        ret_obj[row] = val
                    ret_data[col] = ret_obj

                for key in dat.keys():
                    all_cols[key] = 1
            # print "------dat------"
            # print ret_data
            graph_data = {'header': ['graph'],
                          'graph_type': each_graph.graph_type,
                          'data': ret_data,
                          'rows': list(set(rows)),
                          'columns': all_cols.keys()}
            result[kpi_name] = graph_data
            # print"------graph kpis------"
            # print result[kpi_name]
            # print"------graph kpis------"
        return {'records': result}

    def get_performance_list(self):
        _logger.info("In get List")
        user_name = self.env['res.users'].search('id','=', self.id).name
        all_per = self.env['my.performance'].search_read([('user_id', '=', self.id)], order="date ASC")
        # print all_per
        # print "------"
        sorted(all_per, key=itemgetter('date'))
        # print all_per
        all_performances = {}
        team_user_names = [user_name]
        for tper in all_per:
            _logger.info("In all per loop---")
            _logger.info(tper)
            if tper['kpi']:
                kpi = tper['kpi'][1]
                usr = tper['user_id'][1]
                if 'parameter' in tper:
                    param = tper['parameter']
                else:
                    param = False
                if 'value' in tper:
                    val = tper['value']
                else:
                    if tper['type'] == 'text':
                        val = tper['value_text']
                    elif tper['type'] == 'integer':
                        val = tper['value_int']
                    elif tper['type'] == 'boolean':
                        val = tper['value_boolean']
                    elif tper['type'] == 'image':
                        val = tper['image']
                if kpi in all_performances:
                    if param:
                        if param in all_performances[kpi]:
                            all_performances[kpi][param][usr] = val
                        else:
                            all_performances[kpi][param] = {usr: val}
                    else:
                        all_performances[kpi]['team_performance'] = {'user_name': usr,
                                                                     'type': tper['type'],
                                                                     'id': tper['id'],
                                                                     'value': val}
                else:
                    if param:
                        all_performances[kpi] = {param: {usr: val}}
                    else:
                        all_performances[kpi] = {'team_performance': {'user_name': usr,
                                                                      'type': tper['type'],
                                                                      'id': tper['id'],
                                                                      'value': val}}
        _logger.info(all_performances)
        result = {}
        for kpi, params in all_performances.iteritems():
            result[kpi] = {}
            header = []
            for p, v in params.iteritems():
                header.append(p)
            result[kpi]['header'] = header
            for usr_name in team_user_names:
                user_list = []
                found = False
                i = 0
                for heading in header:
                    if heading == 'team_performance':
                        if heading in params.keys():
                            if 'id' in params[heading].keys():
                                result[kpi]['id'] = params[heading]['id']
                            if 'user_name' in params[heading].keys():
                                result[kpi]['user_name'] = params[heading]['user_name']
                            if 'type' in params[heading].keys():
                                result[kpi]['type'] = params[heading]['type']
                            if 'value' in params[heading].keys():
                                result[kpi]['value'] = params[heading]['value']
                    else:
                        i = i + 1
                        if i < 5:
                            if heading in params.keys():
                                if usr_name in params[heading].keys():
                                    user_list.append(params[heading][usr_name])
                                    found = True
                                else:
                                    user_list.append('-')
                            else:
                                user_list.append('-')
                        if found:
                            result[kpi][usr_name] = user_list
        graphs_ids = self.env['performance.graph'].search([('type', '=', 'employee_performance')])
        data = []
        all_cols = {}
        rows = []
        performance_pool = self.env['my.performance']
        for each_graph in self.env['performance.graph'].browse(graphs_ids):
            kpi = each_graph.kpi.id
            kpi_name = each_graph.kpi.name
            # print "graph KIP: %s" % kpi
            if each_graph.graph_row == 'date' or each_graph.graph_col == 'parameter':
                if each_graph.row_limit:
                    row_limit = each_graph.row_limit
                else:
                    row_limit = 6
                if each_graph.col_limit:
                    col_limit = each_graph.col_limit
                else:
                    col_limit = 6
                rows = self.get_dates(kpi, row_limit)
                for each_date in rows:
                    column = self.get_param_values(kpi, each_date)
                    data.append(column)
            if each_graph.graph_row == 'parameter' or each_graph.graph_col == 'date':
                if each_graph.row_limit:
                    row_limit = each_graph.row_limit
                else:
                    row_limit = 6
                if each_graph.col_limit:
                    col_limit = each_graph.col_limit
                else:
                    col_limit = 6
                rows = self.get_params(kpi, row_limit)
                for each_param in rows:
                    column = self.get_date_values(kpi, each_param)
                    data.append(column)
            ret_data = {}
            for dat in data:
                for col, obj in dat.iteritems():
                    ret_obj = {}
                    if col in ret_data:
                        ret_obj = ret_data[col]
                    for row, val in obj.iteritems():
                        ret_obj[row] = val
                    ret_data[col] = ret_obj
                for key in dat.keys():
                    all_cols[key] = 1
            # print "------dat------"
            # print ret_data
            graph_data = {'header': ['graph'],
                          'graph_type': each_graph.graph_type,
                          'data': ret_data,
                          'rows': list(set(rows)),
                          'columns': all_cols.keys()}
            result[kpi_name] = graph_data
            # print"------graph kpis------"
            # print result[kpi_name]
            # print"------graph kpis------"
        return {'records': result}

    def update_kpi(self):
        _logger.info("Updating KPI's in My Performance")
        page_ids = self.env['my.performance'].search([('id','in',self.ids)])
        for record in self.env['my.performance'].browse(page_ids):
            field = self.env['track.kpi'].search([('name', '=', record.field)])
            if len(field) == 0:
                kpi = self.env['track.kpi'].create({'name': record.field})
                self.env['my.performance'].write([record.id], {'kpi': kpi})
            else:
                for f in self.env['track.kpi'].browse(field[0]):
                    # print f.name
                    self.env['my.performance'].write([record.id], {'kpi': f.id})
        _logger.info("Updating KPI's in My business")
        page_ids = self.env['my.business'].search([('id','in',self.ids)])
        for record in self.env['my.business'].browse(page_ids):
            field = self.env['track.kpi'].search([('name', '=', record.field)])
            if len(field) == 0:
                kpi = self.env['track.kpi'].create({'name': record.field})
                self.env['my.business'].write([record.id], {'kpi': kpi})
            else:
                for f in self.env['track.kpi'].browse(field[0]):
                    self.env['my.business'].write([record.id], {'kpi': f.id})
        _logger.info("Updating KPI's in My Team Performance")
        page_ids = self.env['my.team.performance'].search([])
        for record in self.env['my.team.performance'].browse(page_ids):
            if record:
                field = self.env['track.kpi'].search([('name', '=', record.field)])
                if len(field) == 0:
                    kpi = self.env['track.kpi'].create({'name': record.field})
                    self.env['my.team.performance'].write([record.id], {'kpi': kpi})
                else:
                    for f in self.env['track.kpi'].browse(field[0]):
                        self.env['my.team.performance'].write([record.id], {'kpi': f.id})
        return True


class track_kpi(models.Model):
    _name = 'track.kpi'
    _description = 'KPI\'s'

    def _create_name(self):
        res = {}
        for rec in self.env['track.kpi'].search([('id','in', self.ids)]):
            if rec.user_id and rec.kpi and rec.date:
                res[rec.id] = "Performance of %s in %s on %s" % (rec.user_id.name, rec.kpi.name, rec.date)
            else:
                res[rec.id] = "New Performance"
        return res


    name = fields.Char('Name', required=True)
    description = fields.Char('Description')



class PerformanceGraph(models.Model):
    _name = 'performance.graph'
    _description = 'Performance Graph'

    def onchange_graph_col(self, graph_col, graph_row):
        if graph_col == graph_row:
            raise osv.except_osv(('ERROR!'), ("Graph Column and Graph Row cannot be same!!"))
        return True

    def onchange_graph_row(self, graph_col, graph_row):
        if graph_row == graph_col:
            raise osv.except_osv(('Error!'), ("Graph Column and Graph Row cannot be same!!"))
        return True
    
    @api.multi
    def _create_name(self):
        res = {}
        for rec in self.env['performance.graph'].search('id','in', self.ids):
            if rec.kpi and rec.type:
                if rec.type == 'my_business':
                    res[rec.id] = " %s for My Business " % rec.kpi.name
                elif rec.type == 'employee_performance':
                    res[rec.id] = " %s for Employee Performance " % rec.kpi.name
                else:
                    res[rec.id] = " %s for Team Performance " % rec.kpi.name
            else:
                res[rec.id] = "New Performance"
        return res


    name = fields.Char(compute='_create_name', string='Name', store=True)
    type = fields.Selection([('employee_performance', 'Employee Performance'),
                              ('team_performance', 'Team Performance')], 'Type', required=True)
    kpi = fields.Many2one('track.kpi', 'KPI', required=True)
    graph_col = fields.Selection([('date', 'Date'), ('parameter', 'Parameter')], 'Graph Columns', required=True, default='date')
    graph_row = fields.Selection([('date', 'Date'), ('parameter', 'Parameter')], 'Graph Rows', required=True, default='parameter')
    graph_type = fields.Selection([('line', 'Line'), ('bar', 'Bar'),
                                    ('pie', 'PieChart'), ('bubble', 'Bubble'),
                                    ('radar', 'Radar'), ('polar_area', 'Polar Area'),
                                    ('doughnut', 'Doughnut')], 'Graph Type', required=True)
    graph_style = fields.Selection([('horizontal', 'Horizontal'), ('vertical', 'Vertical')], 'Graph Style')
    row_limit = fields.Integer('Row Limit')
    col_limit = fields.Integer('Column Limit')


#     _defaults = {
#         'graph_row': 'parameter',
#         'graph_col': 'date',
#     }

    @api.model
    def create(self,vals):
        ids = super(PerformanceGraph, self).create(vals)
        for record in self.env['performance.graph'].search([('id','in', self.ids)]):
            if record.graph_row == record.graph_col:
                raise osv.except_osv(('ERROR!'), ("Graph Column and Graph Row cannot be same!!"))
            if record.graph_row == 'Parameter':
                if record.row_limit:
                    if record.row_limit < 1:
                        raise osv.except_osv(('ERROR!'), ("Minimum Row Limit is 1!!"))
            else:
                if record.row_limit:
                    if record.row_limit < 1 or record.row_limit > 6:
                        raise osv.except_osv(('ERROR!'), ("Row Limit should be between 1-6!!"))
            if record.graph_col == 'Parameter':
                if record.col_limit:
                    if record.col_limit < 1:
                        raise osv.except_osv(('ERROR!'), ("Minimum Column Limit is 1!!"))
            else:
                if record['col_limit']:
                    if record.col_limit < 1 or record.col_limit > 6:
                        raise osv.except_osv(('ERROR!'), ("Column Limit should be between 1-6!!"))
        return ids

    def write(self, vals):
        res = super(PerformanceGraph, self).write(vals)
        for record in self.env['performance.graph'].search([('id','in', self.ids)]):
            if record.graph_row == record.graph_col:
                raise osv.except_osv(('ERROR!'), ("Graph Column and Graph Row cannot be same!!"))
            if 'graph_row' in vals and vals['graph_row'] == 'parameter':
                if record.row_limit:
                    if record.row_limit < 1:
                        raise osv.except_osv(('ERROR!'), ("Minimum Row Limit is 1!!"))
            else:
                if record.row_limit:
                    if record.row_limit < 1 or record.row_limit > 6:
                        raise osv.except_osv(('ERROR!'), ("Row Limit should be between 1-6!!"))
            if record.graph_col == 'Parameter':
                if record.col_limit:
                    if record.col_limit < 1:
                        raise osv.except_osv(('ERROR!'), ("Minimum Column Limit is 1!!"))
            else:
                if record.col_limit:
                    if record.col_limit < 1 or record.col_limit > 6:
                        raise osv.except_osv(('ERROR!'), ("Column Limit should be between 1-6!!"))
        return res


class MyBusiness(models.Model):
    _name = 'my.business'
    _description = 'Business'

    def _create_name(self):
        res = {}
        for rec in self.env['my.business'].search([('id','in', self.ids)]):
            if rec.user_id and rec.kpi and rec.date:
                res[rec.id] = "Performance of %s in %s on %s" % (rec.user_id.name, rec.kpi.name, rec.date)
            else:
                res[rec.id] = "New Performance"
        return res


    name = fields.Char(compute='_create_name', string='Name', store=True)
    user_id = fields.Many2one('res.users', 'User', required=True, default=lambda self: self.env.user)
    date = fields.Date('Date', required=True, default=fields.datetime.now())
    type = fields.Selection([('image', 'Image'), ('text', 'Text'),
                              ('integer', 'Integer'), ('boolean', 'Boolean')], 'Type', required=True, default='text')
    kpi = fields.Many2one('track.kpi', 'KPI', required=True)
    field = fields.Char('Field')
    value_text = fields.Char('Value')
    value_int = fields.Integer('Value')
    value_boolean = fields.Boolean('Value')
    parameter = fields.Char('Parameter')
    manager_id = fields.Many2one('res.users', related='user_id.manager_id', string="Manager", readonly=True,
                                  store=True)

    image = odoo.fields.Binary("Image", attachment=True,
                                  help="This field holds the image used as avatar for this contact, limited to 1024x1024px")

#     _defaults = {
#         'type': 'text',
#         'user_id': lambda obj, cr, uid, context: uid,
#         'date': lambda obj, cr, uid, context: fields.datetime.now(),
#     }

    @api.model
    def write(self, vals):
        latest_pool = self.env['my.latest.business']
        if 'user_id' in vals or 'kpi' in vals:
            business_write = super(MyBusiness, self).write(vals)
            business_id = self.ids[0]
            business = self.env['my.latest.business'].browse(business_id)
            latest_business = latest_pool.search([('user_id', '=', business.user_id.id),
                                                           ('kpi', '=', business.kpi.id)])
            if latest_business:
                lp = latest_pool.browse(latest_business[0])
                # print lp.kpi, 'lp.kpi'
                if lp.date < business.date and lp.kpi != False:
                    latest_pool.write([lp.id], {'date': business.date, 'business_id': business_id})
            else:
                # print business.kpi, 'business_id', business_id
                latest_pool.create({'user_id': business.user_id.id,
                                             'kpi': business.kpi.id,
                                             'business_id': business_id})
            return business_id
        else:
            return super(MyBusiness, self).write(vals)
    
    @api.model    
    def create(self, data):
        latest_pool = self.env['my.latest.business']
        if data['user_id'] and data['kpi']:
            business_id = super(MyBusiness, self).create(data)
            latest_performance = latest_pool.search([('user_id', '=', data['user_id']),
                                                              ('kpi', '=', data['kpi'])])
            if latest_performance:
                lp = latest_pool.browse(latest_performance[0])
                if lp.date < data['date'] and lp.kpi != False:
                    latest_pool.write([lp.id], {'date': data['date'], 'business_id': business_id.id})
            else:
                latest_pool.create({'user_id': data['user_id'],
                                             'kpi': data['kpi'],
                                             'business_id': business_id.id})
            return business_id
        else:
            return super(MyBusiness, self).create(data)


class MyLatestBusiness(models.Model):
    _name = 'my.latest.business'
    _description = 'My Latest Business'

    @api.multi
    def _create_name(self):
        res = {}
        for rec in self.env['my.latest.business'].search([('id','in', self.ids)]):
            if rec.user_id and rec.kpi and rec.date:
                res[rec.id] = "Performance of %s in %s on %s" % (rec.user_id.name, rec.kpi.name, rec.date)
            else:
                res[rec.id] = "New Performance"
        return res

    def _update_value(self):
        res = {}
        for rec in self.env['my.latest.business'].search([('id','in', self.ids)]):
            if rec.business_id:
                business = rec.business_id
                if business.type == 'image':
                    value = "image"
                elif business.type == 'text':
                    value = business.value_text
                elif business.type == 'integer':
                    value = business.value_int
                elif business.type == 'boolean':
                    value = business.value_boolean
                else:
                    value = "-"
                res[rec.id] = value
            else:
                res[rec.id] = "-"
        return res


    name = fields.Char(compute='_create_name', string='Name', store=True)
    user_id = fields.Many2one('res.users', 'User', required=True)
    kpi = fields.Many2one('track.kpi', 'KPI', required=True)
    field = fields.Char('Field')
    business_id = fields.Many2one('my.business', 'My Business')
    value = fields.Char(compute='_update_value', string='Value')
    date = fields.Date(related='business_id.date', string='Date', readonly=True)
    image = fields.Binary(related='business_id.image', string='Image', readonly=True)



class MyTeamLatestPerformance(models.Model):
    _name = 'my.team.latest.performance'
    _description = 'Team Latest Performance'

    @api.multi
    def _update_value(self):
        res = {}
        for rec in self.env['my.team.latest.performance'].search([('id','in',self.ids)]):
            if rec.performance_id:
                performance = rec.performance_id
                if performance.type == 'text':
                    value = performance.value_text
                elif performance.type == 'integer':
                    value = performance.value_int
                elif performance.type == 'boolean':
                    value = performance.value_boolean
                else:
                    value = "-"
                res[rec.id] = value
            else:
                res[rec.id] = "-"
        return res
    
    @api.multi
    def _create_name(self):
        res = {}
        for rec in self.env['my.team.latest.performance'].search([('id','in', self.ids)]):
            if rec.user_id and rec.kpi and rec.parameter:
                res[rec.id] = "Performance of %s in %s with parameter %s" % (
                    rec.user_id.name, rec.kpi.name, rec.parameter)
            else:
                res[rec.id] = "New Performance"
        return res


    name = fields.Char(compute='_create_name', string='Name', store=True)
    user_id = fields.Many2one('res.users', 'User', required=True)
    kpi = fields.Many2one('track.kpi', 'KPI', required=True)
    field = fields.Char('Field')
    parameter = fields.Char('Parameter')
    performance_id = fields.Many2one('my.team.performance', 'Performance')
    value = fields.Char(compute='_update_value', string='Value', store=True)
    date = fields.Date(related='performance_id.date', string='Date')



class MyTeamPerformance(models.Model):
    _name = 'my.team.performance'
    _description = 'Team Performance'

    @api.multi
    def _create_name(self):
        res = {}
        for rec in self.env['my.team.performance'].search([('id','in', self.ids)]):
            if rec.team_id and rec.kpi and rec.date:
                res[rec.id] = "Performance of %s in %s on %s" % (rec.team_id.name, rec.kpi.name, rec.date)
            else:
                res[rec.id] = "New Performance"
        return res


    name = fields.Char(compute='_create_name', string='Name', store=True)
    user_id = fields.Many2one('res.users', 'User', required=True,default=lambda self: self.env.user)
    team_id = fields.Many2one('res.team', 'Team', required=True)
    date = fields.Date('Date', required=True, default=fields.datetime.now())
    type = fields.Selection([('image', 'Image'), ('text', 'Text'),
                              ('integer', 'Integer'), ('boolean', 'Boolean')], 'Type', required=True, default='text')
    kpi = fields.Many2one('track.kpi', 'KPI', required=True)
    field = fields.Char('Field')
    value_text = fields.Char('Value')
    value_int = fields.Integer('Value')
    value_boolean = fields.Boolean('Value')
    parameter = fields.Char('Parameter')

    image = odoo.fields.Binary("Image", attachment=True,
                                  help="This field holds the image used as avatar for this contact, limited to 1024x1024px")

    # _defaults = {
    #     'type': 'text',
    #     'user_id': lambda obj, cr, uid, context: uid,
    #     'date': lambda obj, cr, uid, context: fields.datetime.now(),
    # }

    @api.model
    def write(self, vals):
        latest_pool = self.env['my.team.latest.performance']
        if 'user_id' in vals or 'kpi' in vals or 'parameter' in vals:
            performance_write = super(MyTeamPerformance, self).write(vals)
            performance_id = self.env['my.team.performance'].search([('id','in',self.ids)])[0]
            performance = self.env['my.team.performance'].browse(performance_id)
            latest_performance = latest_pool.search([('user_id', '=', performance.user_id.id),
                                                              ('kpi', '=', performance.kpi.id),
                                                              ('parameter', '=', performance.parameter)])
            if latest_performance:
                lp = latest_pool.browse(latest_performance[0])
                if lp.date < performance.date and lp.kpi:
                    latest_pool.write([lp.id], {'date': performance.date, 'performance_id': performance_id})
            else:
                latest_pool.create({'user_id': performance.user_id.id,
                                             'kpi': performance.kpi.id,
                                             'parameter': performance.parameter,
                                             'performance_id': performance_id})
            return performance_id
        else:
            return super(MyTeamPerformance, self).write(vals)

    @api.model
    def create(self, data):
        latest_pool = self.env['my.team.latest.performance']
        if (data['user_id'] and data['kpi']) or data['parameter']:
            performance_id = super(MyTeamPerformance, self).create(data)
            latest_performance = latest_pool.search([('user_id', '=', data['user_id']),
                                                              ('kpi', '=', data['kpi']),
                                                              ('parameter', '=', data['parameter'])])
            if latest_performance:
                lp = latest_pool.browse(latest_performance[0])
                if lp.date < data['date'] and lp.kpi:
                    latest_pool.write([lp.id], {'date': data['date'], 'performance_id': performance_id.id})
            else:
                # print "user, kpi, parameter,performanceid--", data['user_id'], data['kpi'], data[
                    # 'parameter'], performance_id
                latest_pool.create({'user_id': data['user_id'],
                                             'kpi': data['kpi'],
                                             'parameter': data['parameter'],
                                             'performance_id': performance_id.id, })
            return performance_id
        else:
            return super(MyTeamPerformance, self).create(data)

    @api.model
    def unlink(self):
        latest_pool = self.env['my.team.latest.performance']
        for team_per in self.env['my.team.performance'].search([('id','in', self.ids)]):
            latest_ids = latest_pool.search([('performance_id', '=', team_per.id)])
            latest_pool.unlink(latest_ids)
        return super(MyTeamPerformance, self).unlink(self.ids)

    def get_params(self, kpi, limit):
        params = []
        per_data = self.env['my.team.performance'].search_read([('kpi', '=', kpi), ('user_id', '=', self.id),
                                                                              ('type', 'in', ['integer', 'boolean'])],
                                                                    order="date DESC")
        for dat in per_data:
            params.append(dat['parameter'])
        return params

    def get_dates(self, kpi, limit):
        dates = []
        per_data = self.env['my.team.performance'].search_read([('kpi', '=', kpi), ('user_id', '=', self.id),
                                                                              ('type', 'in', ['integer', 'boolean'])],
                                                                    order="date DESC", limit=limit)
        for dat in per_data:
            dates.append(dat['date'])
        return dates

    def get_param_values(self, kpi, date):
        params = {}
        per_data = self.env['my.team.performance'].search_read([('kpi', '=', kpi), ('user_id', '=', self.id),
                                                                              ('date', '=', date),
                                                                              ('type', 'in', ['integer', 'boolean'])],
                                                                    order="date DESC")
        for dat in per_data:
            params_dat = self.env['my.team.performance'].search_read([('kpi', '=', kpi),
                                                                           ('user_id', '=', self.id),
                                                                           ('date', '=', date),
                                                                           ('type', 'in', ['integer', 'boolean']),
                                                                           ('parameter', '=', dat['parameter'])],
                                                                          order="date DESC")
            vals = {}
            for param_dat in params_dat:
                if param_dat['type'] == 'integer':
                    val = param_dat['value_int']
                elif param_dat['type'] == 'boolean':
                    if param_dat['value_boolean']:
                        param_dat['value_boolean'] = 1
                    elif not param_dat['value_boolean']:
                        param_dat['value_boolean'] = 0
                    val = param_dat['value_boolean']
                vals[date] = val
            params[dat['parameter']] = vals
        return params

    def get_date_values(self, kpi, parameter):
        dates = {}
        per_data = self.env['my.team.performance'].search_read([('kpi', '=', kpi), ('user_id', '=', self.id),
                                                                              ('parameter', '=', parameter),
                                                                              ('type', 'in', ['integer', 'boolean'])],
                                                                    order="date DESC")
        for dat in per_data:
            dates_dat = self.env['my.team.performance'].search_read([('kpi', '=', kpi), ('user_id', '=', self.id),
                                                                          ('parameter', '=', parameter),
                                                                          ('type', 'in', ['integer', 'boolean']),
                                                                          ('date', '=', dat['date'])],
                                                                         order="date DESC")
            vals = {}
            for date_dat in dates_dat:
                if date_dat['type'] == 'integer':
                    val = date_dat['value_int']
                elif date_dat['type'] == 'boolean':
                    if date_dat['value_boolean']:
                        date_dat['value_boolean'] = 1
                    elif not date_dat['value_boolean']:
                        date_dat['value_boolean'] = 0
                    val = date_dat['value_boolean']
                vals[parameter] = val
            dates[dat['date']] = vals
        return dates

    # Need to removed after every app implemented with get_team_performance_list
    # Last updated on Tuesday, 26 February 2018
    def get_team_permormance_list(self):
        _logger.info("In get Team performance List")
        member_arr = []
        team_pool_ids = self.env['res.team'].search([])
        company_ids = self.env['res.company'].search([])
        # print company_ids, 'company_ids'
        for company in self.env['res.company'].browse(company_ids):
            # print company.name
            if company.team_performance:
                for teams in self.env['res.team'].browse(team_pool_ids):
                    for id in teams.members_ids.ids:
                        member_arr.append(id)
                    member_arr.append(self.id)
                    my_teams = self.env['res.team'].search_read([('members_ids', 'in', member_arr)])
            else:
                my_teams = self.env['res.team'].search_read([('manager_id', '=', self.id)])
        _logger.info("Team Members")
        _logger.info(my_teams)
        team_user_ids = []
        team_user_names = []
        team_ids = []
        if len(my_teams) > 0:
            for team_users in my_teams:
                if team_users['id'] not in team_ids:
                    team_ids.append(team_users['id'])
                for team_user in self.env['res.users'].browse(team_users['members_ids']):
                    team_user_ids.append(team_user.id)
                    team_user_names.append(team_user.name)
        all_performances = {}
        my_per = self.env['my.team.latest.performance'].search_read([('user_id', 'in', team_user_ids)],
                                                                         order="date DESC")
        all_per = self.env['my.team.performance'].search_read([('team_id', 'in', team_ids)],
                                                                   order="date DESC")
        # print all_per
        # print "------"
        sorted(all_per, key=itemgetter('date'))
        # print all_per
        for tper in all_per:
            _logger.info("In all per loop---")
            _logger.info(tper)
            if tper['kpi'] != False:
                kpi = tper['kpi'][1]
                usr = tper['user_id'][1]
                if 'parameter' in tper:
                    param = tper['parameter']
                else:
                    param = False
                if 'value' in tper:
                    val = tper['value']
                else:
                    if tper['type'] == 'text':
                        val = tper['value_text']
                    elif tper['type'] == 'integer':
                        val = tper['value_int']
                    elif tper['type'] == 'boolean':
                        val = tper['value_boolean']
                    elif tper['type'] == 'image':
                        val = tper['image']
                if kpi in all_performances:
                    if param:
                        if param in all_performances[kpi]:
                            all_performances[kpi][param][usr] = val
                        else:
                            all_performances[kpi][param] = {usr: val}
                    else:
                        all_performances[kpi]['team_performance'] = {'user_name': usr,
                                                                     'type': tper['type'],
                                                                     'id': tper['id'],
                                                                     'value': val}
                else:
                    if param:
                        all_performances[kpi] = {param: {usr: val}}
                    else:
                        all_performances[kpi] = {'team_performance': {'user_name': usr,
                                                                      'type': tper['type'],
                                                                      'id': tper['id'],
                                                                      'value': val}}
        _logger.info(all_performances)
        result = {}
        for kpi, params in all_performances.iteritems():
            result[kpi] = {}
            header = []
            for p, v in params.iteritems():
                header.append(p)
            result[kpi]['header'] = header
            for usr_name in team_user_names:
                user_list = []
                found = False
                i = 0
                for heading in header:
                    if heading == 'team_performance':
                        if heading in params.keys():
                            if 'id' in params[heading].keys():
                                result[kpi]['id'] = params[heading]['id']
                            if 'user_name' in params[heading].keys():
                                result[kpi]['user_name'] = params[heading]['user_name']
                            if 'type' in params[heading].keys():
                                result[kpi]['type'] = params[heading]['type']
                            if 'value' in params[heading].keys():
                                result[kpi]['value'] = params[heading]['value']
                    else:
                        i = i + 1
                        if i < 5:
                            if heading in params.keys():
                                if usr_name in params[heading].keys():
                                    user_list.append(params[heading][usr_name])
                                    found = True
                                else:
                                    user_list.append('-')
                            else:
                                user_list.append('-')
                        if found:
                            result[kpi][usr_name] = user_list
        graphs_ids = self.env['performance.graph'].search([('type', '=', 'team_performance')])
        data = []
        all_cols = {}
        rows = []
        performance_pool = self.env['my.team.performance']
        for each_graph in self.env['performance.graph'].browse(graphs_ids):
            kpi = each_graph.kpi.id
            kpi_name = each_graph.kpi.name
            # print "graph KIP: %s" % kpi
            if each_graph.graph_row == 'date' or each_graph.graph_col == 'parameter':
                if each_graph.row_limit:
                    row_limit = each_graph.row_limit
                else:
                    row_limit = 6
                rows = self.get_dates(kpi, row_limit)
                for each_date in rows:
                    column = self.get_param_values(kpi, each_date)
                    data.append(column)
            if each_graph.graph_row == 'parameter' or each_graph.graph_col == 'date':
                if each_graph.row_limit:
                    row_limit = each_graph.row_limit
                else:
                    row_limit = 6
                rows = self.get_params(kpi, row_limit)
                for each_param in rows:
                    column = self.get_date_values(kpi, each_param)
                    data.append(column)
            ret_data = {}
            for dat in data:
                for col, obj in dat.iteritems():
                    ret_obj = {}
                    if col in ret_data:
                        ret_obj = ret_data[col]
                    for row, val in obj.iteritems():
                        ret_obj[row] = val
                    ret_data[col] = ret_obj
                for key in dat.keys():
                    all_cols[key] = 1
            # print "------dat------"
            # print ret_data
            graph_data = {'header': ['graph'],
                          'graph_type': each_graph.graph_type,
                          'data': ret_data,
                          'rows': list(set(rows)),
                          'columns': all_cols.keys()}
            result[kpi_name] = graph_data
            # print"------graph kpis------"
            # print result[kpi_name]
            # print"------graph kpis------"
        # print result, 'result'
        return {'records': result}

    @api.multi
    def get_team_performance_list(self):
        _logger.info("In get Team performance List")
        member_arr = []
        team_pool_ids = self.env['res.team'].search([])
        company_ids = self.env['res.company'].search([])
        # print company_ids, 'company_ids'
        for company in self.env['res.company'].browse(company_ids):
            # print company.name
            if company.team_performance:
                for teams in self.env['res.team'].browse(team_pool_ids):
                    for id in teams.members_ids.ids:
                        member_arr.append(id)
                    member_arr.append(self.id)
                    my_teams = self.env['res.team'].search_read([('members_ids', 'in', member_arr)])
            else:
                my_teams = self.env['res.team'].search_read([('manager_id', '=', self.id)])
            _logger.info("Team Members")
            _logger.info(my_teams)
        team_user_ids = []
        team_user_names = []
        team_ids = []
        if len(my_teams) > 0:
            for team_users in my_teams:
                if team_users['id'] not in team_ids:
                    team_ids.append(team_users['id'])
                for team_user in self.env['res.users'].browse(team_users['members_ids']):
                    team_user_ids.append(team_user.id)
                    team_user_names.append(team_user.name)
        all_performances = {}
        my_per = self.env['my.team.latest.performance'].search_read([('user_id', 'in', team_user_ids)],
                                                                         order="date DESC")
        all_per = self.env['my.team.performance'].search_read([('team_id', 'in', team_ids)],
                                                                   order="date DESC")
        # print all_per
        # print "------"
        sorted(all_per, key=itemgetter('date'))
        # print all_per
        for tper in all_per:
            _logger.info("In all per loop---")
            _logger.info(tper)
            if tper['kpi'] != False:
                kpi = tper['kpi'][1]
                usr = tper['user_id'][1]
                if 'parameter' in tper:
                    param = tper['parameter']
                else:
                    param = False
                if 'value' in tper:
                    val = tper['value']
                else:
                    if tper['type'] == 'text':
                        val = tper['value_text']
                    elif tper['type'] == 'integer':
                        val = tper['value_int']
                    elif tper['type'] == 'boolean':
                        val = tper['value_boolean']
                    elif tper['type'] == 'image':
                        val = tper['image']
                if kpi in all_performances:
                    if param:
                        if param in all_performances[kpi]:
                            all_performances[kpi][param][usr] = val
                        else:
                            all_performances[kpi][param] = {usr: val}
                    else:
                        all_performances[kpi]['team_performance'] = {'user_name': usr,
                                                                     'type': tper['type'],
                                                                     'id': tper['id'],
                                                                     'value': val}
                else:
                    if param:
                        all_performances[kpi] = {param: {usr: val}}
                    else:
                        all_performances[kpi] = {'team_performance': {'user_name': usr,
                                                                      'type': tper['type'],
                                                                      'id': tper['id'],
                                                                      'value': val}}
        _logger.info(all_performances)
        result = {}
        for kpi, params in all_performances.iteritems():
            result[kpi] = {}
            header = []
            for p, v in params.iteritems():
                header.append(p)
            result[kpi]['header'] = header
            for usr_name in team_user_names:
                user_list = []
                found = False
                i = 0
                for heading in header:
                    if heading == 'team_performance':
                        if heading in params.keys():
                            if 'id' in params[heading].keys():
                                result[kpi]['id'] = params[heading]['id']
                            if 'user_name' in params[heading].keys():
                                result[kpi]['user_name'] = params[heading]['user_name']
                            if 'type' in params[heading].keys():
                                result[kpi]['type'] = params[heading]['type']
                            if 'value' in params[heading].keys():
                                result[kpi]['value'] = params[heading]['value']
                    else:
                        i = i + 1
                        if i < 5:
                            if heading in params.keys():
                                if usr_name in params[heading].keys():
                                    user_list.append(params[heading][usr_name])
                                    found = True
                                else:
                                    user_list.append('-')
                            else:
                                user_list.append('-')
                        if found:
                            result[kpi][usr_name] = user_list
        graphs_ids = self.env['performance.graph'].search([('type', '=', 'team_performance')])
        data = []
        all_cols = {}
        rows = []
        performance_pool = self.env['my.team.performance']
        for each_graph in self.env['performance.graph'].browse(graphs_ids):
            kpi = each_graph.kpi.id
            kpi_name = each_graph.kpi.name
            # print "graph KIP: %s" % kpi
            if each_graph.graph_row == 'date' or each_graph.graph_col == 'parameter':
                if each_graph.row_limit:
                    row_limit = each_graph.row_limit
                else:
                    row_limit = 6
                rows = self.get_dates(kpi, row_limit)
                for each_date in rows:
                    column = self.get_param_values(kpi, each_date)
                    data.append(column)
            if each_graph.graph_row == 'parameter' or each_graph.graph_col == 'date':
                if each_graph.row_limit:
                    row_limit = each_graph.row_limit
                else:
                    row_limit = 6
                rows = self.get_params(kpi, row_limit)
                for each_param in rows:
                    column = self.get_date_values(kpi, each_param)
                    data.append(column)
            ret_data = {}
            for dat in data:
                for col, obj in dat.iteritems():
                    ret_obj = {}
                    if col in ret_data:
                        ret_obj = ret_data[col]
                    for row, val in obj.iteritems():
                        ret_obj[row] = val
                    ret_data[col] = ret_obj

                for key in dat.keys():
                    all_cols[key] = 1
            # print "------dat------"
            # print ret_data
            graph_data = {'header': ['graph'],
                          'graph_type': each_graph.graph_type,
                          'data': ret_data,
                          'rows': list(set(rows)),
                          'columns': all_cols.keys()}
            result[kpi_name] = graph_data
            # print"------graph kpis------"
            # print result[kpi_name]
            # print"------graph kpis------"
        # print result, 'result'
        return {'records': result}


class ImportToPerformance(models.Model):
    _name = 'import.to.performance'
    _description = 'Import to Performance'


    name = fields.Char("Name")
    import_to = fields.Selection([('employee_performance', 'Employee Performance'),
                                   ("team_performance", "Team Performance")], "Import to", required=True)

    upload_zip_file = odoo.fields.Binary("Zip", attachment=True, help="This field holds the Zip file")

    def zip_uploads(self):
        # self.write({'upload_date':today_date,'is_circulated':'yes'})
        company_obj = self.env['res.users'].browse(self.id).company_id
        temp_files_path = company_obj.temp_files_path
        if not temp_files_path:
            company_write = self.env['res.company'].write([company_obj.id],
                                                               {"temp_files_path": "/home/parrot/"})
            temp_files_path = "/home/parrot/"
        if os.path.exists(temp_files_path + 'zip'):
            shutil.rmtree(temp_files_path + 'zip')
        os.makedirs(temp_files_path + 'zip')
        os.makedirs(temp_files_path + 'zip/Performance')
        for record in self.env['import.to.performance'].search('id','in', self.ids):
            with open(temp_files_path + 'zip/performance.zip', 'w') as f:
                f.write(base64.b64decode(record.upload_zip_file))
                f.close()
            try:
                with contextlib.closing(zipfile.ZipFile(temp_files_path + 'zip/performance.zip', "r")) as z:
                    print("checking zip")
            except:
                raise osv.except_osv(('ERROR!'), (
                    """Incorrect File Format. Please upload .zip File with the name 'performance' """))
            try:
                with contextlib.closing(zipfile.ZipFile(temp_files_path + 'zip/performance.zip', "r")) as z:
                    z.extractall(temp_files_path + 'zip/Performance')
                    for filename in os.listdir(temp_files_path + 'zip/Performance/'):
                        extensions = ('.xls', '.xlsx')
                        file_extension = os.path.splitext(filename)
                        # print
                        # file_extension[1], 'sss'
                        if file_extension[1] in extensions:
                            book = open_workbook(temp_files_path + 'zip/Performance/' + filename)
                            sheet = book.sheet_by_index(0)
                            # read header values into the list
                            keys = [sheet.cell(0, col_index).value for col_index in xrange(sheet.ncols)]
                            dict_key = dict([(x, []) for x in keys])
                            dict_list = []
                            dl = []
                            for row_index in xrange(1, sheet.nrows):
                                d = {keys[col_index]: sheet.cell(row_index, col_index).value.strip()
                                     for col_index in xrange(sheet.ncols)}
                                dict_list.append(d)
                            for key in dict_key:
                                for row in dict_list:
                                    for k, v in row.iteritems():
                                        if key == k:
                                            dict_key[key].append(v)
                            # print
                            # dict_list, 'dict_list'
                            for d in dict_list:
                                try:
                                    if os.path.exists(temp_files_path + 'zip/Performance/' + d['Image']):
                                        with open(temp_files_path + 'zip/Performance/' + d['Image'], 'rb') as img:
                                            encoded_data = base64.b64encode(img.read())
                                            track_ids = self.env['track.kpi'].search([
                                                ('name', 'ilike', d['KPI'])])
                                            # print
                                            # track_ids, 'track_ids'
                                            if not track_ids:
                                                kpi_id = self.env['track.kpi'].create({'name': d['KPI']})
                                            else:
                                                kpi_id = track_ids[0]
                                            if record.import_to == "employee_performance":
                                                user_ids = self.env['res.users'].search([
                                                    ('name', '=', d['User/Team'])])
                                                if len(user_ids) > 1:
                                                    emp_ids = self.env['res.users'].search([
                                                        ('emp_id', '=', d['Employee ID'])])
                                                    if len(emp_ids) == 0:
                                                        document_vals = {'user_id': user_ids[0],
                                                                         'date': d['Date'],
                                                                         'image': encoded_data,
                                                                         'type': "image",
                                                                         'kpi': kpi_id,
                                                                         'parameter': ""}
                                                    else:
                                                        document_vals = {'user_id': emp_ids,
                                                                         'date': d['Date'],
                                                                         'image': encoded_data,
                                                                         'type': "image",
                                                                         'kpi': kpi_id,
                                                                         'parameter': ""}
                                                    self.env['my.performance'].create(document_vals)
                                                else:
                                                    document_vals = {'user_id': user_ids[0],
                                                                     'date': d['Date'],
                                                                     'image': encoded_data,
                                                                     'type': "image",
                                                                     'kpi': kpi_id,
                                                                     'parameter': ""}
                                                    # print
                                                    # document_vals, 'document_vals'
                                                    self.env['my.performance'].create(document_vals)

                                            else:
                                                team_ids = self.env['res.team'].search([('name', '=', d['User/Team'])])
                                                if team_ids:
                                                    document_team_vals = {'team_id': team_ids[0],
                                                                          'date': d['Date'],
                                                                          'image': encoded_data,
                                                                          'type': 'image',
                                                                          'kpi': kpi_id,
                                                                          'parameter': ""}
                                                    self.env['my.team.performance'].create(document_team_vals)

                                except(OSError):
                                    raise osv.except_osv(_('Incorrect Image Name'),
                                                         _('We didn\'t find any image with the name - %s') % (
                                                             d['Image']))
            # except(OSError):
            #     raise osv.except_osv(_('Incorrect File Hierarchy'), _(
            #         """Please add all files to a folder called Performance and then compress to Zip and Upload that Zip file"""))
            except extensions as e:
                print(str(e))
                # except(IndexError):
                #     filename_error.append(filename)


class UserFeedback(models.Model):
    _name = 'user.feedback'
    _description = 'User Feedback'


    name = fields.Text('Feedback', required=True)
    user_id = fields.Many2one('res.users', 'User', required=True,default=lambda self: self.env.user)
    date = fields.Date('Date', required=True, default= fields.date.today())


#     _defaults = {
#         'user_id': lambda obj, cr, uid, context: uid,
#         'date': lambda obj, cr, uid, context: fields.datetime.now(),
#     }


class ChatRoom(models.Model):
    _name = 'chat.room'
    _description = 'Chat Rooms'
    _order = 'date desc'

    @api.multi
    def _get_particpantNames(self):
        participants_pool = self.env['room.participants']
        if not self.ids: return {False}
        res = {}
        for room in self.env['chat.room'].search([('id','in', self.ids)]):
            res[room.id] = ''
            if room.participant_ids is not False:
                for participant in room.participant_ids:
                    if res[room.id] == '':
                        res[room.id] = "%s" % participant.name.name
                    else:
                        res[room.id] = "%s,%s" % (res[room.id], participant.name.name)
        return res
    
    @api.multi
    def update_room_status(self):
        _logger.info("Updating Room Status")
        rooms = self.env['chat.room'].search([('status', 'in', ['active', 'scheduled', '', ' ', False])])
        for room in self.env['chat.room'].browse(rooms):
            now = datetime.datetime.now()
            room_date = datetime.datetime.strptime(room.date, "%Y-%m-%d %H:%M:%S")
            if room_date < (now - timedelta(hours=1)):
                self.write([room.id], {'status': 'closed'})
        return True

    @api.multi
    def inactive_chat_room(self):
        _logger.info("Closing Inactive Chat Room (1 Mins)")
        rooms = self.env['chat.room'].search([('status', 'in', ['active', 'scheduled', '', ' ', False])])
        now = datetime.datetime.now()
        count = 0
        for room in self.env['chat.room'].browse(rooms):
            for participant in room.participant_ids:
                if participant.status == 'active':
                    count += 1
        for room in self.env['chat.room'].browse(rooms):
            if count < 2:
                room_date = datetime.datetime.strptime(room.date, "%Y-%m-%d %H:%M:%S")
                if room_date < (now - timedelta(seconds=30)):
                    self.write([room.id], {'status': 'closed'})
                    _logger.info("'%s' Room is Closed now" % room.name)
                    for participant in room.participant_ids:
                        participant.write({'status': 'closed'})
        return True

    @api.model
    def create(self, data):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        context = dict(self._context or {})
        partner_ids = []
        participant_names = ''
        user_pool = self.env['res.users']
        if context.get("mail_broadcast"):
            context['mail_create_nolog'] = True
        channel_pool = self.env['mail.channel']
        if 'participant_ids' in data:
            if data['participant_ids'] is not False:
                for participant in data['participant_ids']:
                    participant_name = user_pool.browse(participant[2]['name']).name
                    partner_id = user_pool.browse(participant[2]['name']).partner_id.id
                    partner_ids.append(partner_id)
                    if participant_names == '':
                        participant_names = "%s" % participant_name
                    else:
                        participant_names = "%s,%s" % (participant_names, participant_name)
        vals = {'name': 'Chat Room of ' + participant_names,
                'channel_partner_ids': [(6, 0, partner_ids)],
                'public': 'private', }
        chan_id = channel_pool.create(vals)
        data['room_group_channel'] = chan_id.id
        roomId = super(ChatRoom, self).create(data)
        if 'status' in data and data['status'] == 'scheduled':
            room = self.env['chat.room'].browse(roomId)
            video_call = ""
            participant_count = 0
            groupChatUsers = []
            many2manytokens = []
            for participant in room.participant_ids:
                groupChatUsers.append({"id": participant.name.id, "name": participant.name.name})
                participant_count = participant_count + 1
                for token in participant.name.user_tokens:
                    if self.id != participant.name.id:
                        many2manytokens.append(token)
            msg_body = {"channeltype": "many2many", "type": room.type, "calltype": "scheduled", "date": data['date'],
                        "roomName": room.name, "roomId": room.id, "users": groupChatUsers}
            many2many_body = "You have a call scheduled with %s others." % (participant_count - 1)
            if not ('type' in msg_body):
                msg_body['type'] = 'video'
                title = "Video Call"
            else:
                if msg_body['type'] == 'audio':
                    title = "Audio Call"
                else:
                    title = "Video Call"
            data = {'notification': {'title': title,
                                     'body': many2many_body,
                                     'sound': 'notification_sound.mp3',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': msg_body,
                    'registration_ids': many2manytokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            ios_tokens = []
            android_tokens = []
            _logger.info(data['registration_ids'])
            for token in data['registration_ids']:
                try:
                    device_type = token.device_type
                except KeyError as e:
                    device_type = 'Android'
                if device_type == 'iOS':
                    ios_tokens.append(token.name)
                else:
                    android_tokens.append(token.name)
            _logger.info("Android Tokens------ %s" % android_tokens)
            _logger.info("IOs Tokens------ %s" % ios_tokens)
            if len(ios_tokens) >= 1:
                ios_data = data
                if 'channeltype' in ios_data['data'] and ios_data['data']['channeltype'] == 'many2many':
                    ios_data['notification']['sound'] = 'www/notification_ring.caf'
                else:
                    ios_data['notification']['sound'] = 'notification_sound.caf'
                ios_data['registration_ids'] = ios_tokens
                _logger.info("Input Data for iOS------ %s" % ios_data)
                ios_data = json.dumps(ios_data)
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=ios_data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
            if len(android_tokens) >= 1:
                if 'channeltype' in data['data'] and data['data']['channeltype'] == 'many2many':
                    data['notification']['sound'] = 'notification_sound.mp3'
                else:
                    data['notification']['sound'] = 'notification_sound.mp3'
                data['registration_ids'] = android_tokens
                _logger.info("Input Data for android------ %s" % data)
                data = json.dumps(data)
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        return roomId
    
    @api.model
    def write(self, vals):
        user_pool = self.env['res.users']
        mail_pool = self.env['mail.channel']
        if 'participant_ids' in vals:
            if vals['participant_ids'] is not False:
                for participant in vals['participant_ids']:
                    for room in self.env['chat.room'].search([('id','in', self.ids)]):
                        if participant[2]:
                            current_channel_name = mail_pool.browse(room.room_group_channel.id).name
                            if 'name' in participant[2] and participant[2]['name']:
                                user = user_pool.browse(participant[2]['name'])
                            else:
                                user = self.env['room.participants'].browse(participant[1]).name
                            participant_name = user.name
                            mail_pool.write(room.room_group_channel.id,
                                            {'channel_partner_ids': [(4, user.partner_id.id)],
                                             'name': "%s,%s" % (current_channel_name, participant_name)})
        return super(ChatRoom, self).write(vals)


    name = fields.Char('Room Name', required=True)
    participant_ids = fields.One2many('room.participants', 'room_id', 'Participants')
    status = fields.Selection([('active', 'Active'), ('closed', 'Closed'), ('scheduled', 'Scheduled')], 'Status')
    room_group_channel = fields.Many2one('mail.channel', 'Room Channel')
    participant_names = fields.Char(compute='_get_particpantNames', string="Participant Names")
    date = fields.Datetime('Date',default=lambda self: fields.datetime.now())
    type = fields.Char('Type', readonly=True, default='video')


#     _defaults = {
#         'date': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
#         'type': 'video',
#     }

    @api.multi
    def invite_participant(self, roomId, initiator, selectedUserId):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        many2manytokens = []
        video_call = ''
        participant_count = 0
        groupChatUsers = []
        participant_exists = False
        for room in self.env['chat.room'].search([('id','=', int(roomId))]):
            for participant in room['participant_ids']:
                if participant.name.id == selectedUserId:
                    participant_exists = True
            if participant_exists:
                participant_ids = self.env['room.participants'].search([('name', '=', selectedUserId),
                                                                                      ('room_id', '=', int(roomId))])
                self.env['room.participants'].write(participant_ids, {"status": "invite"})
            else:
                self.write([int(roomId)],
                           {'participant_ids': [(0, False, {'name': selectedUserId, 'status': 'invite'})]})
            _logger.info("------Invite Participant")
            for participant in room['participant_ids']:
                groupChatUsers.append({"id": participant.name.id, "name": participant.name.name})
                if int(initiator) == participant.name.id:
                    video_call = '%s,%s' % (participant.name.name, video_call)
                else:
                    participant_count = participant_count + 1
                for token in participant.name.user_tokens:
                    # print "token if ---%s---%s" % (selectedUserId, participant.name.id)
                    if int(selectedUserId) == participant.name.id:
                        many2manytokens.append(token)
            msg_body = {"channeltype": "many2many", "initiator": initiator, "type": room['type'],
                        "roomName": room['name'], "roomId": roomId, "users": groupChatUsers}
        video_call = video_call.rstrip(',')
        if participant_count == 1:
            many2many_body = "%s is calling you.." % video_call
        else:
            many2many_body = "%s and %s others are calling you.." % (video_call, participant_count - 1)
        if not ('type' in msg_body):
            msg_body['type'] = 'video'
            title = "Video Call"
        else:
            if msg_body['type'] == 'audio':
                title = "Audio Call"
            else:
                title = "Video Call"
        data = {'notification': {'title': title,
                                 'body': many2many_body,
                                 'sound': 'notification_ring.mp3',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': msg_body,
                'registration_ids': many2manytokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        ios_tokens = []
        android_tokens = []
        _logger.info(data['registration_ids'])
        for token in data['registration_ids']:
            try:
                device_type = token.device_type
            except KeyError as e:
                device_type = 'Android'
            if device_type == 'iOS':
                ios_tokens.append(token.name)
            else:
                android_tokens.append(token.name)
        _logger.info("Android Tokens------ %s" % android_tokens)
        _logger.info("IOs Tokens------ %s" % ios_tokens)
        if len(ios_tokens) >= 1:
            ios_data = data
            if 'channeltype' in ios_data['data'] and ios_data['data']['channeltype'] == 'many2many':
                ios_data['notification']['sound'] = 'www/notification_ring.caf'
            else:
                ios_data['notification']['sound'] = 'notification_sound.caf'
            ios_data['registration_ids'] = ios_tokens
            _logger.info("Input Data for iOS------ %s" % ios_data)
            ios_data = json.dumps(ios_data)
            res = requests.post("https://fcm.googleapis.com/fcm/send", data=ios_data, headers=headers)
            _logger.info("FCM Response ------ %s" % res.text)
        if len(android_tokens) >= 1:
            if 'channeltype' in data['data'] and data['data']['channeltype'] == 'many2many':
                data['notification']['sound'] = 'notification_ring.mp3'
            else:
                data['notification']['sound'] = 'notification_sound.mp3'
            data['registration_ids'] = android_tokens
            _logger.info("Input Data for android------ %s" % data)
            data = json.dumps(data)
            res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
            _logger.info("FCM Response ------ %s" % res.text)
        return True

    def my_recent_calls(self):
        context = dict(self._context or {})
        if context.get("limit"):
            limit = context['limit']
        else:
            limit = False
        participants_pool = self.env['room.participants']
        my_rooms = self.env['chat.room'].search_read([], order='date DESC')
        finalrooms = []
        for room in my_rooms:
            room['participant_details'] = []
            add = False
            for participant in participants_pool.read(room['participant_ids']):
                if participant['name'][0] == self.id and (
                        (len(finalrooms) < limit or not limit) or room['status'] == "scheduled"):
                    add = True
                room['participant_details'].append({"id": participant['id'], "name": participant['name'][1],
                                                    "calendar_entry": participant['calendar_entry']})
            if add:
                finalrooms.append(room)
        return {'records': finalrooms}

    def get_users_from_partners(self, partner_ids):
        users = []
        _logger.info("Called------")
        for partner_id in partner_ids:
            user_id = self.env["res.users"].search([('partner_id', '=', partner_id)])[0]
            user_obj = self.env["res.users"].browse([user_id])
            _logger.info("USERID--%s, PARTNER_ID--%s" % (user_id, partner_id))
            users.append({'id': user_id, 'name': user_obj.name})
        return {'data': users}

    def scheduled_call_reminder(self):
        _logger.info("Scheduled call Reminder Cron  ------ ")
        from datetime import datetime
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        search_ids = self.env['chat.room'].search([("status", "=", "scheduled")])
        send = True
        today = datetime.today()
        video_call = ""
        participant_count = 0
        groupChatUsers = []
        many2manytokens = []
        for room in self.env['chat.room'].browse(search_ids):
            if room.date:
                scheduled_time = datetime.strptime(room.date, DATETIME_FORMAT)
                two_mins_before = datetime.strptime(room.date, DATETIME_FORMAT) + timedelta(minutes=-2)
                _logger.info("2 Mins before:%s, Scheduled Time;%s, Today:%s" % (two_mins_before, scheduled_time, today))
                if two_mins_before <= today <= scheduled_time:
                    for participant in room.participant_ids:
                        groupChatUsers.append({"id": participant.name.id, "name": participant.name.name})
                        participant_count = participant_count + 1
                        for token in participant.name.user_tokens:
                            many2manytokens.append(token)
                    msg_body = {"channeltype": "many2many",
                                "type": room.type,
                                "roomName": room.name,
                                "roomId": room.id,
                                "users": groupChatUsers}
                    many2many_body = "You have a call scheduled with %s others." % (participant_count - 1)
                    if not ('type' in msg_body):
                        msg_body['type'] = 'video'
                        title = "Video Call"
                    else:
                        if msg_body['type'] == 'audio':
                            title = "Audio Call"
                        else:
                            title = "Video Call"
                    data = {'notification': {'title': title,
                                             'body': many2many_body,
                                             'sound': 'notification_ring.mp3',
                                             'click_action': 'FCM_PLUGIN_ACTIVITY',
                                             'icon': 'fcm_push_icon',
                                             'color': '#F8880D'},
                            'data': msg_body,
                            'registration_ids': many2manytokens,
                            'priority': 'high',
                            'forceShow': 'true',
                            'restricted_package_name': ''}
                    headers = {'Authorization': fcm_authorization_key,
                               'Content-Type': 'application/json'}
                    ios_tokens = []
                    android_tokens = []
                    _logger.info(data['registration_ids'])
                    for token in data['registration_ids']:
                        try:
                            device_type = token.device_type
                        except KeyError as e:
                            device_type = 'Android'
                        if device_type == 'iOS':
                            ios_tokens.append(token.name)
                        else:
                            android_tokens.append(token.name)
                    _logger.info("Android Tokens------ %s" % android_tokens)
                    _logger.info("IOs Tokens------ %s" % ios_tokens)
                    if len(ios_tokens) >= 1:
                        ios_data = data
                        if 'channeltype' in ios_data['data'] and ios_data['data']['channeltype'] == 'many2many':
                            ios_data['notification']['sound'] = 'www/notification_ring.caf'
                        else:
                            ios_data['notification']['sound'] = 'notification_sound.caf'
                        ios_data['registration_ids'] = ios_tokens
                        _logger.info("Input Data for iOS------ %s" % ios_data)
                        ios_data = json.dumps(ios_data)
                        res = requests.post("https://fcm.googleapis.com/fcm/send", data=ios_data, headers=headers)
                        _logger.info("FCM Response ------ %s" % res.text)
                    if len(android_tokens) >= 1:
                        if 'channeltype' in data['data'] and data['data']['channeltype'] == 'many2many':
                            data['notification']['sound'] = 'notification_ring.mp3'
                        else:
                            data['notification']['sound'] = 'notification_sound.mp3'
                        data['registration_ids'] = android_tokens
                        _logger.info("Input Data for android------ %s" % data)
                        data = json.dumps(data)
                        res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                        _logger.info("FCM Response ------ %s" % res.text)
        return True


class PriceList(models.Model):
    _name = 'price.list'
    _description = 'Price List'
    _order = 'id desc'


    category = fields.Char('Category')
    brand = fields.Char('Brand')
    model = fields.Char('Model')
    variant = fields.Char('Variant')
    price = fields.Float('Price')



class RoomParticipants(models.Model):
    _name = 'room.participants'
    _description = 'Room Participants'


    name = fields.Many2one('res.users', 'Participant', required=True, default=lambda self: self.env.user)
    room_id = fields.Many2one('chat.room', 'Room Name')
    status = fields.Selection([('invite', 'Invite'), ('active', 'Active'), ('closed', 'Closed')], 'Status')
    calendar_entry = fields.Boolean('Star')


#     _defaults = {
#         'name': lambda obj, cr, uid, context: uid,
#     }

    @api.model
    def write(self, vals):
        room_id = False
        chat_room = self.env['chat.room']
        if 'status' in vals and vals['status'] == 'closed':
            for participant in self.env['room.participants'].search([('id','in', self.ids)]):
                if 'room_id' in vals and vals['room_id']:
                    room_id = chat_room.browse(vals['room_id'])
                else:
                    room_id = participant.room_id
                status = room_id.status
                close_room = True
                for p in room_id.participant_ids:
                    if p.id != participant.id:
                        if p.status in ['invite', 'active']:
                            close_room = False
                if close_room:
                    chat_room.write(room_id.id, {'status': 'closed'})
        return super(RoomParticipants, self).write(vals)
