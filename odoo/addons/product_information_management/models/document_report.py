import base64
import logging
import os
from datetime import datetime, date, time, timedelta
import xlsxwriter
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo import api, tools
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DATETIME_FORMAT
import xlsxwriter
from odoo import api, models, fields,_
from datetime import datetime, date, time, timedelta
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning
_logger = logging.getLogger(__name__)


class DocumentReport(models.Model):
    _name = 'document.report'

    start_date = fields.Date(string='Start Date')
    end_date = fields.Date(string="End Date")
    doc_report_download_xl = fields.Many2many('ir.attachment', 'user_report_xl_down',
                                              'user_report_xl_down_id',
                                              'attachment_id', 'Download Report', readonly=True)

    @api.multi
    def doc_report_download(self,login):
        _logger.info("Document - Report Generated, Count ")
        today_start = datetime.now().replace(hour=1, minute=00, second=00, microsecond=0).strftime(
            tools.DEFAULT_SERVER_DATETIME_FORMAT)
        today_end = datetime.now().replace(hour=18, minute=5, second=00, microsecond=0).strftime(
            tools.DEFAULT_SERVER_DATETIME_FORMAT)
        yesterday_start = (datetime.now()-timedelta(days=1)).replace(hour=00, minute=00, second=0, microsecond=0).strftime(
            tools.DEFAULT_SERVER_DATETIME_FORMAT)
        yesterday_end = (datetime.now()- timedelta(days=1)).replace(hour=23, minute=59, second=59, microsecond=59).strftime(
            tools.DEFAULT_SERVER_DATETIME_FORMAT)
        nowstrf = datetime.now().strftime(tools.DEFAULT_SERVER_DATETIME_FORMAT)
        if today_start < nowstrf < today_end:
            company_obj = self.env['res.users'].browse(self._uid).company_id
            temp = company_obj.temp_files_path
            if not temp:
                company_write = self.env['res.company'].write(self._uid, [company_obj.id],
                                                                                     {"temp_files_path": "/home/parrot/"})
                temp = "/home/parrot/"
            if not os.path.exists(temp + 'Samples'):
                os.makedirs(temp + 'Samples')
            temp = temp + 'Samples/'
            if not os.path.exists(temp + 'Images'):
                os.makedirs(temp + 'Images')
            temp_img = temp + 'Images/'
            sample_xls = xlsxwriter.Workbook(temp + 'document_report.xls')
            border_format = sample_xls.add_format({'border': 2, 'bold': True, 'bg_color': '#90af48'})
            sheet = sample_xls.add_worksheet()
            header_list = ['Created By', 'Owner', 'Type Of Document', 'Document Name', 'Create Date', 'Region',
                           'Designation', 'Module', 'Status', 'Expiry Date', ]
            module_name = 'Parrot Knowledge'
            row = 0
            col = 0
            for heads in header_list:
                sheet.write(row, col, heads, border_format)
                col += 1
            row += 1
            col = 0
            doc_search_obj = self.env['parrot.knowledge'].search(
                [('create_date', '>=', today_start), ('create_date', '<=',today_end)])
            for doc in doc_search_obj:
                sheet.write(row, col, doc.create_uid.name)
                col += 1
                sheet.write(row, col, doc.user_id.name)
                col += 1
                sheet.write(row, col, doc.type)
                col += 1
                if doc.type == 'video':
                    if doc.video_ids:
                        file_path = os.path.join(temp_img, doc.video_ids.name)
                        with open(file_path, 'wb') as f:
                            f.write(base64.b64decode(doc.video_ids.datas))
                        sheet.write_url(row, col, file_path)
                        col += 1
                    else:
                        sheet.write(row, col, '')
                        col += 1
                elif doc.type == 'attachment':
                    if doc.document_ids:
                        file_path = os.path.join(temp_img, doc.document_ids.name)
                        with open(file_path, 'wb') as f:
                            f.write(base64.b64decode(doc.document_ids.datas))
                        sheet.write_url(row, col, file_path)
                        col += 1
                    else:
                        sheet.write(row, col, '')
                        col += 1
                elif doc.type == 'audio':
                    if doc.audio_ids:
                        file_path = os.path.join(temp_img, doc.audio_ids.name)
                        with open(file_path, 'wb') as f:
                            f.write(base64.b64decode(doc.audio_ids.datas))
                        sheet.write_url(row, col, file_path)
                        col += 1
                    else:
                        sheet.write(row, col, '')
                        col += 1
                elif doc.type == 'link':
                    if doc.link:
                        sheet.write_url(row, col, doc.link)
                        col += 1
                    else:
                        sheet.write(row, col, '')
                        col += 1
                sheet.write(row, col, doc.create_date)
                col += 1
                sheet.write(row, col, doc.user_id.region_id)
                col += 1
                group_name = ""
                for group in doc.user_id.groups_id:
                    search_group_obj = self.env["res.groups"].search([('id', '=', group.id)])
                    if search_group_obj.name:
                        if search_group_obj.name == "BA":
                            if group_name != "AM" and group_name != "RA" and group_name != "RM" and group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                                group_name = "BA"
                        elif search_group_obj.name == "Account Manager":
                            if group_name != "GM" and group_name != "CSO" and group_name != "CCO":
                                group_name = "AM"
                        elif search_group_obj.name == "Regional Admin":
                            group_name = "RA"
                        elif search_group_obj.name == "Regional Manager" and group_name != "AM" and group_name != "GM":
                            group_name = "RM"
                        elif search_group_obj.name == "General Manager":
                            if group_name != "CSO" and group_name != "CCO":
                                group_name = "GM"
                        elif search_group_obj.name == "CSO":
                            if group_name != "CCO":
                                group_name = "CSO"
                        elif search_group_obj.name == "CCO":
                            group_name = "CCO"
                sheet.write(row, col, group_name)
                col += 1
                sheet.write(row, col, module_name)
                col += 1
                sheet.write(row, col, 'Created')
                col += 1
                sheet.write(row, col, doc.due_date)
                col += 1
                row += 1
                col = 0
            doc_delete_obj = self.env['document.delete'].search(
                [('delete_date', '>=', yesterday_start), ('delete_date', '<=', yesterday_end),('status', '=', 'Deleted')])
            for del_doc in doc_delete_obj:
                sheet.write(row, col, del_doc.delete_by.name)
                col += 1
                sheet.write(row, col, del_doc.user_id.name)
                col += 1
                sheet.write(row, col, del_doc.type)
                col += 1
                sheet.write(row, col, del_doc.document_name)
                col += 1
                sheet.write(row, col, del_doc.delete_date)
                col += 1
                sheet.write(row, col, del_doc.region)
                col += 1
                sheet.write(row, col, del_doc.designation)
                col += 1
                sheet.write(row, col, del_doc.module)
                col += 1
                sheet.write(row, col, del_doc.status)
                col += 1
                sheet.write(row, col, del_doc.due_date)
                col += 1
                row += 1
                col = 0
            doc_write_obj = self.env['document.delete'].search(
                [('delete_date', '>=',yesterday_start), ('delete_date', '<=',yesterday_end),('status', '=', 'Updated')])
            for del_doc in doc_write_obj:
                sheet.write(row, col, del_doc.delete_by.name)
                col += 1
                sheet.write(row, col, del_doc.user_id.name)
                col += 1
                sheet.write(row, col, del_doc.type)
                col += 1
                sheet.write(row, col, del_doc.document_name)
                col += 1
                sheet.write(row, col, del_doc.delete_date)
                col += 1
                sheet.write(row, col, del_doc.region)
                col += 1
                sheet.write(row, col, del_doc.designation)
                col += 1
                sheet.write(row, col, del_doc.module)
                col += 1
                sheet.write(row, col, del_doc.status)
                col += 1
                sheet.write(row, col, del_doc.due_date)
                col += 1
                row += 1
                col = 0
            sample_xls.close()
            with open(temp + 'document_report.xls', 'rb') as doc:
                encoded_data = base64.b64encode(doc.read())
                document_vals = {'name': 'document_report_file.xls',
                                 'datas': encoded_data,
                                 'datas_fname': 'document_report_file.xls',
                                 'res_model': 'document.report',
                                 'res_id': self.id,
                                 'type': 'binary'}
            ir_id = self.env['ir.attachment'].create(document_vals)
            self.write({'doc_report_download_xl': [(6, 0, [ir_id.id])]})
            self.mail_document_report(ir_id, login)
        return True

    def mail_document_report(self, attachment_id, login):
        res_user = self.env['res.users'].search([('login', '=', login)])
        mail = self.env['mail.mail'].create({'body_html': '<div><p>Hi Sir/Madam,</p>'
                                                          '<br/><p>Please download the document report.</p></div>',
                                             'subject': 'Document Report',
                                             'email_to': res_user.email,
                                             'attachment_ids': [(4, attachment_id.id)],
                                             'auto_delete': False})
        mail_br = self.env['mail.mail'].browse(mail.id)
        return mail_br.send(auto_commit=False)


class DocumentDelete(models.Model):
    _name = 'document.delete'

    name = fields.Char(String='Record Name')
    delete_by = fields.Many2one('res.users', String='Deleted By')
    user_id = fields.Many2one('res.users', String='Owner')
    delete_date = fields.Date(String='Deleted Date')
    type = fields.Selection([('link', 'Youtube Link'), ('video', 'Video Attachment'),
                             ('attachment', 'Attachment'), ('audio', 'Audio Attachment')], String='Type of Document')
    region = fields.Char(String='Region')
    designation = fields.Char(String='Designation')
    module = fields.Char(String='Module')
    status = fields.Char(String='Status')
    due_date = fields.Date(String='Expiry Date')
    document_name = fields.Char(String='Document Name')


    @api.model
    def create(self, vals):
        result = super(DocumentDelete, self).create(vals)
        return result
