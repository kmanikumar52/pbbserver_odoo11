/* globals Erizo */
'use strict';
var serverUrl = '/';
var localStream, room, recording, recordingId;
var onlySubscribe = false;

function getParameterByName(name) {
    name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
        results = regex.exec(location.search);
    return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function testConnection() { // jshint ignore:line
    window.location = '/connection_test.html';
}

function startRecording() { // jshint ignore:line
    if (room !== undefined) {
        if (!recording) {
            room.startRecording(localStream, function(id) {
                recording = true;
                recordingId = id;
            });
        } else {
            room.stopRecording(recordingId);
            recording = false;
        }
    }
}

var slideShowMode = false;

function toggleSlideShowMode() { // jshint ignore:line
    var streams = room.remoteStreams;
    var cb = function(evt) {
        console.log('SlideShowMode changed', evt);
    };
    slideShowMode = !slideShowMode;
    for (var index in streams) {
        var stream = streams[index];
        if (localStream.getID() !== stream.getID()) {
            console.log('Updating config');
            stream.updateConfiguration({
                slideShowMode: slideShowMode
            }, cb);
        }
    }
}

window.onload = function() {
    //console = new Console();
    //video = document.getElementById('video');
    var broadcastId = window.top.location.hash.split('#id=')[1].split('&')[0];
    console.info(broadcastId);
    document.getElementById('call').addEventListener('click', function() {
        console.log("========Record Start========");
        console.info($("#record").prop('checked'));
        // document.getElementById("record").disabled = true;
        var options = {
            metadata: {
                type: 'publisher'
            }
        };
        room.publish(localStream, options);
    });
    console.log('Viewer ', document.getElementById('viewer'))
    var viewer = document.getElementById('viewer');
    if (viewer != null) {
        document.getElementById('viewer').addEventListener('click', function() {});
        onlySubscribe = true;
    }

    document.getElementById('terminate').addEventListener('click', function() {
        // document.getElementById("record").disabled = false;
        console.log("========Record Stop========");
        console.info($("#record").prop('checked'));
        // stop();
        room.unpublish(localStream);
        // room = null;
        // localStream = null;
        // presenter(broadcastId);
        // document.location.reload();
    });
    console.log('Only Subscribe ', onlySubscribe);
    presenter(broadcastId);
}

function sendMessage(message) {
    var jsonMessage = JSON.stringify(message);
    console.log('Sending JSON message from Presenter: ' + jsonMessage);
}
var presenter = function(broadcastId) {
    console.log("Presenter started");
    recording = false;
    var screen = getParameterByName('screen');
    var roomName = getParameterByName('room') ||  'basicExampleRoom';
    var roomType = getParameterByName('type') ||  'erizo';
    //var onlySubscribe = getParameterByName('onlySubscribe');
    console.log('Selected Room', roomName, 'of type', roomType);
    var message = {
        id: 'presenter',
        className: broadcastId,
        record: $("#record").prop('checked'),
    };
    console.log('Message', message);
    sendMessage(message);
    var config = {
        audio: true,
        video: true,
        data: true,
        screen: screen,
        videoSize: [640, 480, 640, 480],
        videoFrameRate: [10, 20]
    };
    // If we want screen sharing we have to put our Chrome extension id.
    // The default one only works in our Lynckia test servers.
    // If we are not using chrome, the creation of the stream will fail regardless.
    if (screen) {
        config.extensionId = 'okeephmleflklcdebijnponpabbmmgeo';
    }
    localStream = Erizo.Stream(config);
    /*var createToken = function(userName, role, roomName, callback) {
      var req = new XMLHttpRequest();
      var url = serverUrl + 'createToken/';
      var body = {username: userName, role: role, room:roomName};
      req.onreadystatechange = function () {
        if (req.readyState === 4) {
          callback(req.responseText);
        }
      };

      req.open('POST', url, true);
      req.setRequestHeader('Content-Type', 'application/json');
      req.send(JSON.stringify(body));
    };

    createToken("user", "presenter", roomName, function (response) {
      var token = response;*/
    var token = {
        room: parseInt(broadcastId),
        name: '',
        host: 'https://rooms.parrot.solutions:8080'
    };
    console.log(token);
    room = Erizo.Room({
        token: token
    });

    var subscribeToStreams = function(streams) {
        var cb = function(evt) {
            console.log('Bandwidth Alert', evt.msg, evt.bandwidth);
        };
        for (var index in streams) {
            var stream = streams[index];
            if (localStream.getID() !== stream.getID()) {
                room.subscribe(stream, {
                    slideShowMode: slideShowMode,
                    metadata: {
                        type: 'subscriber'
                    }
                });
                stream.addEventListener('bandwidth-alert', cb);
            }
        }
    };

    room.addEventListener('room-connected', function(roomEvent) {
        var options = {
            metadata: {
                type: 'publisher'
            }
        };
        var enableSimulcast = getParameterByName('simulcast');
        if (enableSimulcast) options.simulcast = {
            numSpatialLayers: 2
        };
        if (onlySubscribe) subscribeToStreams(roomEvent.streams);
    });

    room.addEventListener('stream-subscribed', function(streamEvent) {
        var stream = streamEvent.stream;
        var div = document.createElement('div');
        div.setAttribute('style', 'width: 940px; height: 480px;float:left;');
        div.setAttribute('id', 'test' + stream.getID());
        document.getElementById('videoContainer').appendChild(div);
        stream.show('test' + stream.getID());
    });

    room.addEventListener('stream-added', function(streamEvent) {
        var streams = [];
        streams.push(streamEvent.stream);
        if (onlySubscribe) subscribeToStreams(streams);
        //document.getElementById('recordButton').disabled = false;
    });

    room.addEventListener('stream-removed', function(streamEvent) {
        // Remove stream from DOM
        var stream = streamEvent.stream;
        if (stream.elementID !== undefined) {
            var element = document.getElementById(stream.elementID);
            document.getElementById('videoContainer').removeChild(element);
        }
    });

    room.addEventListener('stream-failed', function() {
        console.log('Stream Failed, act accordingly');
    });

    if (onlySubscribe) {
        room.connect();
    } else {
        var div = document.createElement('div');
        div.setAttribute('style', 'width: 940px; height: 480px; float:left');
        localStream.addEventListener('access-accepted', function() {
            room.connect();
            localStream.show('myVideo', {bar: false});
        });
        localStream.init();
        div.setAttribute('id', 'myVideo');
        document.getElementById('videoContainer').appendChild(div);
    }
};
