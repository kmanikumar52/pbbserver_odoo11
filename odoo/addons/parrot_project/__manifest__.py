# -*- coding: utf-8 -*-
{
    'name': "Parrot Project",
    'summary': """Customization of Project for Parrot""",
    'description': """Customization of Project for Parrot
            * User Defined Progress Bar Colour Configuration.
            * Automatic Colour Change of Lifeline Bar.
            * Default Colour for Forbidden.
        Holiday Module customization for Parrot has 'Travel'.""",
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'category': 'Project Management',
    'version': '0.1',
    'sequence': 5,
    'depends': [
        'project',
        'hr_holidays',
        'parrot_notification',
        'mail',
    ],
    'data': [
        'security/parrot_project_security.xml',
        'security/ir.model.access.csv',
        # 'data/parrot_project_data.xml',
        'views/hr_holidays_view.xml',
        'views/project_view.xml',
        'views/progress_bar_view.xml',
        'views/res_team_view.xml',
    ],
    'application': True,
}
