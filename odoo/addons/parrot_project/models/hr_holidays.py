# -*- coding: utf-8 -*-

import json
import logging
import requests
from datetime import datetime
from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class HrHolidays(models.Model):
    _inherit = "hr.holidays"

    @api.one
    def _check_date(self):
        return super(HrHolidays, self)._check_date()

    request_type = fields.Selection([('leave', 'Leave'), ('travel', 'Travel')], string='Type')
    holiday_status_id = fields.Many2one("hr.holidays.status", "Leave Type", required=False, readonly=False, default=4,
                                        states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})

    # _defaults = {
    #     'holiday_status_id': 4,
    #     'state': 'draft',
    # }

    _constraints = [(_check_date, '', []), ]

    @api.model
    def create(self, values):
        _logger.info("Created Travel")
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        hr_holidays_id = super(HrHolidays, self).create(values)
        for team_id in self.env['res.team'].search([('members_ids', 'in', hr_holidays_id.user_id.id)]):
            if team_id.manager_id.user_tokens:
                for token in self.env['user.token'].browse(team_id.manager_id.user_tokens.ids):
                    tokens.append(token.name)
        from_date = datetime.strptime(hr_holidays_id.date_from, "%Y-%m-%d %H:%M:%S")
        to_date = datetime.strptime(hr_holidays_id.date_to, "%Y-%m-%d %H:%M:%S")
        data_json = {"channeltype": "requestTravelForm",
                     "name": hr_holidays_id.name,
                     "author_id": hr_holidays_id.create_uid.id,
                     "author_name": hr_holidays_id.create_uid.name,
                     "employee_id": hr_holidays_id.employee_id.id,
                     "hr_holidays_id": hr_holidays_id.id,
                     "message": "'%s' has requested %s for '%s' from %s to %s" % (
                         hr_holidays_id.employee_id.name, hr_holidays_id.request_type, hr_holidays_id.name,
                         from_date.strftime("%Y-%m-%d"), to_date.strftime("%Y-%m-%d"))}
        data = {'notification': {'title': hr_holidays_id.name,
                                 "body": "'%s' has requested %s for '%s' from %s to %s" % (
                                     hr_holidays_id.employee_id.name, hr_holidays_id.request_type, hr_holidays_id.name,
                                     from_date.strftime("%Y-%m-%d"), to_date.strftime("%Y-%m-%d")),
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': hr_holidays_id.user_id.id,
                                                'model_name': 'hr.holidays',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'create',
                                                'record_id': hr_holidays_id.id,
                                                'message': data_json["message"],
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info("Notification not sent")
        return hr_holidays_id

    @api.multi
    def unlink(self):
        _logger.info("Deleted Travel")
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        user_id = self.env['res.users'].search([('id', '=', self._uid)])
        for team_id in self.env['res.team'].search([('members_ids', 'in', self.user_id.id)]):
            if team_id.manager_id.user_tokens:
                for token in self.env['user.token'].browse(team_id.manager_id.user_tokens.ids):
                    tokens.append(token.name)
        data_json = {"channeltype": "deleteTravelForm",
                     "name": self.name,
                     "author_id": self.create_uid.id,
                     "author_name": self.create_uid.name,
                     "employee_id": self.employee_id.id,
                     "hr_holidays_id": self.id,
                     "message": "'%s' has deleted %s for '%s' " % (user_id.name, self.request_type, self.name)}
        data = {'notification': {'title': self.name,
                                 "body": "'%s' has deleted %s for '%s' " % (user_id.name, self.request_type, self.name),
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': self.user_id.id,
                                                'model_name': 'hr.holidays',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'unlink',
                                                'record_id': self.id,
                                                'message': data_json["message"],
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info("Notification not sent")
        return super(HrHolidays, self).unlink()

    @api.multi
    def accept_travel(self, values):
        _logger.info("Accept Travel")
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        self.state = 'confirm'
        if self.employee_id.user_id.user_tokens:
            for token in self.env['user.token'].browse(self.employee_id.user_id.user_tokens.ids):
                tokens.append(token.name)
        data_json = {"channeltype": "acceptTravel",
                     "name": self.name,
                     "author_id": self.create_uid.id,
                     "author_name": self.create_uid.name,
                     "employee_id": self.employee_id.id,
                     "hr_holidays_id": self.id,
                     "message": "Your %s request has been accepted" % self.request_type}
        data = {'notification': {'title': self.name,
                                 "body": "Your %s request has been accepted" % self.request_type,
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': self.employee_id.id,
                                                'model_name': 'hr.holidays',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'accept_travel',
                                                'record_id': self.id,
                                                'message': data_json["message"],
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info("Notification not sent")

    @api.multi
    def reject_travel(self, values):
        _logger.info("Reject Travel")
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        self.state = 'refuse'
        if self.employee_id.user_id.user_tokens:
            for token in self.env['user.token'].browse(self.employee_id.user_id.user_tokens.ids):
                tokens.append(token.name)
        data_json = {"channeltype": "rejectTravel",
                     "name": self.name,
                     "author_id": self.create_uid.id,
                     "author_name": self.create_uid.name,
                     "employee_id": self.employee_id.id,
                     "hr_holidays_id": self.id,
                     "message": "Your %s request has been rejected" % self.request_type}
        data = {'notification': {'title': self.name,
                                 "body": "Your %s request has been rejected" % self.request_type,
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': self.employee_id.id,
                                                'model_name': 'hr.holidays',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'reject_travel',
                                                'record_id': self.id,
                                                'message': data_json["message"],
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info("Notification not sent")
