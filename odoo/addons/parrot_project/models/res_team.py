# -*- coding: utf-8 -*-
import logging
from odoo import models, fields, api


_logger = logging.getLogger(__name__)


class ResTeam(models.Model):
    _inherit = 'res.team'

    parent_team_id = fields.Many2one('res.team', 'Parent Team')

    @api.multi
    def get_parent_team(self, project_id):
        team_dict = {}
        if project_id:
            project_obj = self.env['project.project'].search([('id', '=', project_id)])
            for project in project_obj:
                parent_team_obj = self.env['res.team'].search([('id', '=', project.team_id.id)])
                for team in parent_team_obj:
                    if team.parent_team_id:
                        team_dict.update({'team_id': team.parent_team_id.id, 'team_name': team.parent_team_id.name})
                    else:
                        team_dict.update({'team_id': team.id, 'team_name': team.name})
        return team_dict
