# -*- coding: utf-8 -*-

import json
import logging
import requests
from datetime import datetime, timedelta

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.osv import osv

_logger = logging.getLogger(__name__)


class ProjectProject(models.Model):
    _inherit = 'project.project'

    business_process_id = fields.Many2one('project.business.process', 'Business Process')
    team_id = fields.Many2one('res.team', 'Team', required=True)
    team_member_ids = fields.One2many('res.users', compute='_compute_team_member_ids', string='Team Members')
    start_date = fields.Date(string='Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    description = fields.Text('Description')

    @api.one
    @api.depends('team_id')
    def _compute_team_member_ids(self):
        member_list = []
        for teams in self.team_id.members_ids:
            member_list += [teams.id]
        self.team_member_ids = member_list

    @api.onchange('team_id')
    def onchange_team_id(self):
        """ Onchange to Check team_id"""
        if self.user_id != self.team_id.manager_id:
            self.user_id = self.team_id.manager_id.id

    @api.one
    @api.constrains('team_id')
    def _constrains_team_id(self):
        """ Constrains to Check team_id"""
        if self.user_id != self.team_id.manager_id:
            self.write({'user_id': self.team_id.manager_id.id})

    @api.multi
    def company_customer(self):
        partner_list = []
        partner_dict = {}
        partner_sr = self.env['res.partner'].search([('company_type', '=', 'company')])
        for partner in partner_sr:
            customer_dict = {}
            customer_dict.update({'id': partner.id, 'name': partner.name})
            partner_list.append(customer_dict)
        partner_dict.update({'company_customer': partner_list})
        return partner_dict

    @api.multi
    def res_team_members(self):
        member_dict_update = {}
        if self.id:
            team_sr = self.env['res.team'].search([('id', '=', self.id)])
            member_list = []
            for usr in team_sr.members_ids:
                member_dict = {}
                member_dict.update({'id': usr.id, 'name': usr.name})
                member_list.append(member_dict)
            member_dict_update.update({'members': member_list})
        return member_dict_update

    @api.multi
    def res_user(self):
        user_list = []
        user_dict_update = {}
        user_sr = self.env['res.users'].search([('login', '!=', 'admin')])
        for usr in user_sr:
            user_dict = {}
            user_dict.update({'id': usr.id, 'name': usr.name})
            user_list.append(user_dict)
        user_dict_update.update({'users': user_list})
        return user_dict_update

    @api.model
    def create(self, values):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        project_id = super(ProjectProject, self).create(values)
        print(project_id,'vvvvvvvvvvvvvvvvvvvv')
        for member in project_id.team_id.members_ids:
            print(member,'gggg')
            print(member.user_tokens,'dddddddd')
            if member.user_tokens:
                for token in self.env['user.token'].browse(member.user_tokens.ids):
                    tokens.append(token.name)
        data_json = {"channeltype": "newProject",
                     "name": project_id.name,
                     "author_id": project_id.create_uid.id,
                     "author_name": project_id.create_uid.name,
                     "project_id": project_id.id,
                     "date_start": project_id.start_date,
                     "date_end": project_id.end_date,
                     "message": "You have been allocated to'%s' project from %s to %s" % (
                         project_id.name, project_id.start_date, project_id.end_date)}
        data = {'notification': {'title': project_id.name,
                                 'body': "You have been allocated to'%s' project from %s to %s" % (
                                     project_id.name, project_id.start_date, project_id.end_date),
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        print(project_id.create_uid.id, 'ddnnnnnn')
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                # 'user_id': member.id,
                                                'user_id': project_id.create_uid.id,
                                                'model_name': 'project.project',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'create',
                                                'record_id': project_id.id,
                                                'message': data_json["message"],
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info('Notification not sent')
        return project_id


class ProjectTask(models.Model):
    _inherit = 'project.task'

    state = fields.Selection([('draft', 'Draft'), ('accept', 'Accept'), ('cancel', 'Cancel')], string='Status',
                             default="accept")
    percent = fields.Float(string="Percent")
    date_deadline = fields.Datetime('Deadline', required=True)
    reassign_id = fields.Many2one('res.users')
    task_allotted_percentage = fields.Float('Task Allotted %')
    # super_task = fields.Many2one('project.task', 'Super Tasks')
    # sub_task = fields.One2many('project.task', 'super_task', 'Sub Tasks')

    @api.one
    def cancel_project(self):
        self.state = 'cancel'

    @api.one
    def accept_project(self):
        self.state = 'accept'

    @api.one
    def draft_project(self):
        self.state = 'draft'

    @api.model
    def create(self, values):
        if 'task_allotted_percentage' in values:
            if 'user_id' in values:
                task_obj = self.env["project.task"].search(
                    [('user_id', '=', values['user_id'])])
                total_percentage = 0
                for task in task_obj:
                    total_percentage += task.task_allotted_percentage
                total_percentage += values['task_allotted_percentage']
                if total_percentage > 100:
                    raise UserError(_("You can't utilize a resource more than 100% "))
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        project_task_id = super(ProjectTask, self).create(values)
        if project_task_id.user_id.user_tokens:
            for token in self.env['user.token'].browse(project_task_id.user_id.user_tokens.ids):
                tokens.append(token.name)
        data_json = {"channeltype": "newProjectTask",
                     "name": project_task_id.name,
                     "author_id": project_task_id.create_uid.id,
                     "author_name": project_task_id.create_uid.name,
                     "project_id": project_task_id.project_id.id,
                     "date_deadline": project_task_id.date_deadline,
                     "date_start": project_task_id.date_start,
                     "date_end": project_task_id.date_end,
                     "date_assign": project_task_id.date_assign,
                     "message": "Project Task '%s' has been created" % project_task_id.name,
                     "project_task_id": project_task_id.id}
        data = {'notification': {'title': project_task_id.name,
                                 'body': "Project Task '%s' has been created" % project_task_id.name,
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': project_task_id.user_id.id,
                                                'model_name': 'project.task',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'create',
                                                'record_id': project_task_id.id,
                                                'message': data_json["message"],
                                                'document_reference_id': project_task_id.project_id.id,
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info('Notification not sent')
        return project_task_id

    @api.model
    def company_customer(self):
        partner_list = []
        partner_dict = {}
        partner_sr = self.env['res.partner'].search([('company_type', '=', 'company')])
        for partner in partner_sr:
            partner_dict.update({'id': partner, 'name': partner.name})
            partner_list.append(partner_dict)
        return partner_list

    def create_mobile(self, cr, uid, values, context=None):
        task_id = super(ProjectTask, self).create_mobile(cr, uid, values, context=context)
        fcm_authorization_key = self.pool['ir.config_parameter'].get_param(cr, uid, 'fcm.authorization.key')
        send = True
        tokens = []
        project_id = 0
        if 'project_id' in values and values['project_id']:
            project_id = values['project_id']
        date_assign = 0
        if 'date_assign' in values and values['date_assign']:
            date_assign = values['date_assign']
        if 'user_id' in values and values['user_id']:
            user_id = self.pool['res.users'].browse(cr, uid, values['user_id'])
            author_name = self.pool['res.users'].browse(cr, uid, uid).name
            if user_id.user_tokens:
                for token in self.pool['user.token'].browse(cr, uid, user_id.user_tokens.ids):
                    tokens.append(token.name)
            data_json = {"channeltype": "newProjectTask",
                         "name": values['name'],
                         "author_id": uid,
                         "author_name": author_name,
                         "project_id": project_id,
                         "date_assign": date_assign,
                         "message": "Project Task '%s' has been created" % values['name']}
            data = {'notification': {'title': values['name'],
                                     'body': "Project Task '%s' has been created" % values['name'],
                                     'sound': 'default',
                                     'click_action': 'FCM_PLUGIN_ACTIVITY',
                                     'icon': 'fcm_push_icon',
                                     'color': '#F8880D'},
                    'data': data_json,
                    'registration_ids': tokens,
                    'priority': 'high',
                    'forceShow': 'true',
                    'restricted_package_name': ''}
            headers = {'Authorization': fcm_authorization_key,
                       'Content-Type': 'application/json'}
            _logger.info("Input Data ------ %s" % data)
            self.pool['parrot.notification'].create(cr, uid, {'name': data_json["name"],
                                                              'user_id': user_id.id,
                                                              'model_name': 'project.task',
                                                              'channel_name': data_json["channeltype"],
                                                              'function_name': 'create_mobile',
                                                              # 'record_id': task_id.id,
                                                              'document_reference_id': project_id,
                                                              'message': data_json["message"],
                                                              'notification_title': data['notification']["title"],
                                                              'notification_body': data['notification']["body"]})
            data = json.dumps(data)
            try:
                if send:
                    res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                    _logger.info("FCM Response ------ %s" % res.text)
            except:
                _logger.info('Notification not sent')
        return task_id

    @api.one
    def write(self, values):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        uid = self.env['res.users'].browse(self._uid)
        total_percentage = 0
        msg_end = "has been edited"
        user_ids = []
        if 'user_id' in values:
            current_task_obj = self.env["project.task"].search(
                [('id', '=', self.id), ('user_id', '=', values['user_id'])])
            task_obj = self.env["project.task"].search(
                [('user_id', '=', values['user_id'])])
        else:
            current_task_obj = self.env["project.task"].search(
                [('id', '=', self.id), ('user_id', '=', self.user_id.id)])
            task_obj = self.env["project.task"].search(
                [('user_id', '=', self.user_id.id)])
        if current_task_obj:
            current_percentage = current_task_obj[0].task_allotted_percentage
        else:
            current_percentage = 0
        for task in task_obj:
            total_percentage += task.task_allotted_percentage
        if 'task_allotted_percentage' in values:
            total_percentage += values['task_allotted_percentage'] - current_percentage
        if total_percentage > 100:
            raise UserError(_("You can't utilize a resource more than 100% "))
        if 'user_id' in values and values['user_id']:
            user = self.env['res.users'].browse([values['user_id']])
            msg_end = "has been assigned to you!"
            user_ids.append(user.id)
            if user.user_tokens:
                for token in self.env['user.token'].browse(user.user_tokens.ids):
                    tokens.append(token.name)
        if 'name' in values and values['name']:
            msg_end = "has been renamed from '%s' to '%s'" % (self.name, values['name'])
            user_ids.append(self.project_id.user_id.id)
            if self.user_id.user_tokens:
                for token in self.env['user.token'].browse(self.user_id.user_tokens.ids):
                    tokens.append(token.name)
        if 'state' in values and values['state']:
            user_ids.append(self.project_id.user_id.id)
            if values['state'] == "cancel":
                msg_end = " is rejected by '%s'" % uid.name
                self.user_id = self.project_id.user_id.id
            if values['state'] == "accept":
                msg_end = " is accepted by '%s'" % uid.name
            if self.project_id.user_id.user_tokens:
                for token in self.env['user.token'].browse(self.project_id.user_id.user_tokens.ids):
                    tokens.append(token.name)
        if 'date_deadline' in values and values['date_deadline']:
            msg_end = "deadline date is changed to '%s'" % values['date_deadline']
            user_ids.append(self.project_id.user_id.id)
            user_ids.append(uid.id)
            if self.user_id.user_tokens:
                for token in self.env['user.token'].browse(self.user_id.user_tokens.ids):
                    tokens.append(token.name)
        percentage = self.percent
        if 'percent' in values and values['percent']:
            msg_end = "%s Percentage Completed by '%s'" % (values['percent'], self.user_id.name)
            percentage = values['percent']
            if 'description' in values and values['description']:
                description = values['description']
            else:
                description = ""
            if 'task_attachment' in values and values['task_attachment']:
                task_attachment = values['task_attachment']
            else:
                task_attachment = ""
            self.env['mail.message'].create({'task_attachment': task_attachment, 'description': description,
                                             'body': "%s Percentage completed : %s" % (description, values['percent']),
                                             "model": 'project.task', "res_id": self.id})
            if self.project_id.user_id.user_tokens:
                for token in self.env['user.token'].browse(self.project_id.user_id.user_tokens.ids):
                    tokens.append(token.name)
        data_json = {"channeltype": "editProjectTask",
                     "name": self.name,
                     "project_id": self.project_id.id,
                     "project_task_id": self.id,
                     "assigned_to": self.user_id.name,
                     "state": self.state,
                     "percentage": percentage,
                     "message": "'%s' task %s" % (self.name, msg_end)}
        data = {'notification': {'title': self.name,
                                 'body': "Project Task '%s' %s" % (self.name, msg_end),
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        for user_id in user_ids:
            self.env['parrot.notification'].create({'name': data_json["name"],
                                                    'user_id': user_id,
                                                    'model_name': 'project.task',
                                                    'channel_name': data_json["channeltype"],
                                                    'function_name': 'write',
                                                    'record_id': self.id,
                                                    'document_reference_id': self.project_id.id,
                                                    'message': data_json["message"],
                                                    'notification_title': data['notification']["title"],
                                                    'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info('Notification not sent')
        return super(ProjectTask, self).write(values)

    @api.one
    def write_mobile(self, values):
        _logger.info(values)
        return super(ProjectTask, self).write(values)

    @api.one
    def reassign_task(self, values):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        uid = self.env['res.users'].browse(self._uid)
        self.reassign_id = values['reassign_id']
        reassign_id = self.env['res.users'].browse(values['reassign_id'])
        project_task_id = self.env['project.task'].browse(values['project_task_id'])
        if reassign_id.user_tokens:
            for token in self.env['user.token'].browse(reassign_id.user_tokens.ids):
                tokens.append(token.name)
        data_json = {"channeltype": "reassignProjectTask",
                     "name": project_task_id.name,
                     "author_id": project_task_id.create_uid.id,
                     "user_id": self.user_id.id,
                     "reassign_id": reassign_id.id,
                     "author_name": project_task_id.create_uid.name,
                     "project_id": project_task_id.project_id.id,
                     "project_task_id": project_task_id.id,
                     "message": "'%s' reassigned '%s' task to You!" % (uid.name, project_task_id.name)}
        data = {'notification': {'title': project_task_id.name,
                                 'body': "'%s' reassigned '%s' task to You!" % (uid.name, project_task_id.name),
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': project_task_id.user_id.id,
                                                'model_name': 'project.task',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'reassign_task',
                                                'record_id': project_task_id.id,
                                                'message': data_json["message"],
                                                'document_reference_id': project_task_id.project_id.id,
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info('Notification not sent')
        return project_task_id

    @api.one
    def accept_reassign_task(self, values):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        uid = self.env['res.users'].browse(self._uid)
        self.user_id = values['user_id']
        user_id = self.env['res.users'].browse(values['user_id'])
        project_task_id = self.env['project.task'].browse(values['project_task_id'])
        if project_task_id.user_id.user_tokens:
            for token in self.env['user.token'].browse(project_task_id.user_id.user_tokens.ids):
                tokens.append(token.name)
        message = "'%s' has accepted & reassigned '%s' task to You!" % (uid.name, project_task_id.name)
        data_json = {"channeltype": "acceptReassignTask",
                     "name": project_task_id.name,
                     "author_id": project_task_id.create_uid.id,
                     "author_name": project_task_id.create_uid.name,
                     "reassign_id": user_id.id,
                     "user_id": self.user_id.id,
                     "project_id": project_task_id.project_id.id,
                     "project_task_id": project_task_id.id,
                     "message": message}
        data = {'notification': {'title': project_task_id.name,
                                 "body": message,
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': project_task_id.user_id.id,
                                                'model_name': 'project.task',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'accept_reassign_task',
                                                'record_id': project_task_id.id,
                                                'message': message,
                                                'document_reference_id': project_task_id.project_id.id,
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info('Notification not sent')
        return project_task_id

    @api.one
    def reject_reassign_task(self, values):
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        uid = self.env['res.users'].browse(self._uid)
        user_id = self.env['res.users'].browse(values['user_id'])
        project_task_id = self.env['project.task'].browse(values['project_task_id'])
        if project_task_id.project_id.user_id.user_tokens:
            for token in self.env['user.token'].browse(project_task_id.project_id.user_id.user_tokens.ids):
                tokens.append(token.name)
        message = "'%s' has rejected & reassigned '%s' task to You!" % (uid.name, project_task_id.name)
        data_json = {"channeltype": "rejectReassignTask",
                     "name": project_task_id.name,
                     "author_id": project_task_id.create_uid.id,
                     "author_name": project_task_id.create_uid.name,
                     "user_id": self.user_id.id,
                     "reassign_id": user_id.id,
                     "project_id": project_task_id.project_id.id,
                     "project_task_id": project_task_id.id,
                     "message": message}
        data = {'notification': {'title': project_task_id.name,
                                 "body": message,
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true',
                'restricted_package_name': ''}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        self.env['parrot.notification'].create({'name': data_json["name"],
                                                'user_id': project_task_id.project_id.user_id.id,
                                                'model_name': 'project.task',
                                                'channel_name': data_json["channeltype"],
                                                'function_name': 'reject_reassign_task',
                                                'record_id': project_task_id.id,
                                                'message': message,
                                                'document_reference_id': project_task_id.project_id.id,
                                                'notification_title': data['notification']["title"],
                                                'notification_body': data['notification']["body"]})
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            _logger.info('Notification not sent')
        return project_task_id

    @api.multi
    def _archive_old_task(self):
        for task in self.search([]):
            if task.percent == 100 and task.active and task.write_date < str(datetime.now() - timedelta(days=2)):
                _logger.info("Archive Old Task")
                task.active = False


class ResUsers(models.Model):
    _inherit = 'res.users'

    total_allotted_percentage = fields.Float('Total Allotted %',compute='_compute_total_percentage')
    # total_allotted_percentage = fields.Float('Total Allotted %', compute='_compute_total_percentage')

    @api.one
    def _compute_total_percentage(self):
        for rec in self:
            task_obj = rec.env["project.task"].search([('user_id', '=', rec.id)])
            total_percentage = 0
            for task in task_obj:
                total_percentage += task.task_allotted_percentage
                rec.total_allotted_percentage = total_percentage


class MailMessage(models.Model):
    _inherit = "mail.message"

    task_attachment = fields.Binary("Attachment", attachment=True)
    description = fields.Text("Description")

    @api.multi
    def get_mail_message_datas(self):
        cr = self._cr
        data_dict = {}
        message_obj = self.env["mail.message"].search([('res_id', '=', self.id)])
        message_list = []
        for message_data in message_obj:
            message_dict = {}
            message_dict.update({'description': message_data.description, 'body': message_data.body,
                                 'author_name': message_data.author_id.name, 'date': message_data.create_date})
            if message_data.id:
                cr.execute('''select attachment.name as attachment_name,
                                    attachment.id as attachment_id,
                                    attachment.res_id as attachment_res_id
                                    from ir_attachment attachment
                                    where attachment.res_id=%s''', (message_data.id,))
                ir_attachment_data = cr.dictfetchall()
                if ir_attachment_data:
                    message_dict.update({'attachment_dict': ir_attachment_data})
            message_list.append(message_dict)
            data_dict.update({'message_list': message_list})
        return data_dict
