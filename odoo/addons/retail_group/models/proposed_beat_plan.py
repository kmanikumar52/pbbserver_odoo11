import datetime
import json
import logging
import requests
from datetime import datetime

from odoo import models, fields, api
from odoo.osv import osv
from odoo.tools.translate import _

_logger = logging.getLogger(__name__)


class ProposedBeatPlan(models.Model):
    _name = 'proposed.beat.plan'

    # pos_id = fields.Many2one('retail.customer', 'POS ID')
    pos_id = fields.Many2many('retail.customer', 'proposed_beat_plan_rel', 'beat_plan_id', 'retail_id', 'POS')
    beat_id = fields.Many2one('retail.group', 'Beat ID')
    user_id = fields.Many2one('res.users', string="User ID")
    approver1 = fields.Many2one('res.users', string="Approver1")
    approver2 = fields.Many2one('res.users', string="Approver2")
    date = fields.Datetime("Date")
    beat_name = fields.Char("Beat Name")
    state = fields.Selection([('draft', 'Draft'),
                              ('submitted', 'Submitted'),
                              ('approved', 'Approved'),
                              ('rejected', 'Rejected')], 'State', default='draft')

    @api.multi
    def create(self, values):
        if 'user_id' in values:
            search_user = self.env['res.users'].search([('id', '=', values['user_id'])])
            if search_user:
                values['approver1'] = search_user.manager_id.id
        beat_plan_id = super(ProposedBeatPlan, self).create(values)
        return beat_plan_id

    @api.multi
    def mobile_beat_create(self, values):
        if 'user_id' in values:
            search_user = self.env['res.users'].search([('id', '=', values['user_id'])])
            if search_user:
                values['approver1'] = search_user.manager_id.id
        beat_plan_id = super(ProposedBeatPlan, self).create(values)
        fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
        send = True
        tokens = []
        if search_user.manager_id.user_tokens:
            for token in search_user.manager_id.user_tokens:
                tokens.append(token.name)
        message = "'%s' has been created by '%s'" % (values['beat_name'], search_user.name)
        data_json = {"channeltype": "proposedBeatCreate",
                     "name": search_user.manager_id.name,
                     "author_id": search_user.manager_id.id,
                     "message": message}
        data = {'data': {'title': "Proposed Beat Create",
                         'body': "Proposed Beat Create",
                         'sound': 'default',
                         'click_action': 'FCM_PLUGIN_ACTIVITY',
                         'icon': 'fcm_push_icon',
                         'color': '#F8880D'},
                'data': data_json,
                'registration_ids': tokens,
                'priority': 'high',
                'forceShow': 'true'}
        headers = {'Authorization': fcm_authorization_key,
                   'Content-Type': 'application/json'}
        _logger.info("Input Data ------ %s" % data)
        data = json.dumps(data)
        try:
            if send:
                res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                _logger.info("FCM Response ------ %s" % res.text)
        except:
            raise osv.except_osv(_('Notification not sent'))
        return beat_plan_id

    @api.one
    def button_approve(self):
        return self.write({'state': 'approved'})

    @api.one
    def button_reject(self):
        return self.write({'state': 'rejected'})

    @api.one
    def button_submit(self):
        return self.write({'state': 'submitted'})

    @api.one
    def button_draft(self):
        return self.write({'state': 'draft'})

    @api.multi
    def get_proposed_beat_plan(self):
        result = {}
        result_list = []
        search_user = self.env['res.users'].search([('id', '=', self.id)])
        if search_user.employee_list:
            lists = []
            for user_id in search_user.employee_list:
                lists.append(user_id.id)
                for usr_id in user_id.employee_list:
                    lists.append(usr_id.id)
                    for us_id in usr_id.employee_list:
                        lists.append(us_id.id)
                        for u_id in us_id.employee_list:
                            lists.append(u_id.id)
        if len(lists) > 0:
            for user in lists:
                proposed_obj = self.search([('user_id', '=', user)],order ='date desc')
                for beat in proposed_obj:
                    if beat.approver2.id != self.id:
                        beat_res = {'beat_id': beat.id, 'user_id': beat.user_id.id, 'user_name': beat.user_id.name,
                                    'proposed_date': beat.date, 'beat_name': beat.beat_name, 'state': beat.state}
                        beat_res_list = []
                        for pos in beat.pos_id:
                            beat_detail_res = {}
                            if user == beat.user_id.id:
                                beat_detail_res.update({'id': pos.id,
                                                        'pos_name': pos.name,
                                                        'pos_id': pos.pos_id,
                                                        'Latitude': pos.latitude,
                                                        'Longitude': pos.longitude,
                                                        'created_user': pos.create_uid.name,
                                                        'create_uid': pos.create_uid.id,
                                                        'create_date': pos.create_date,
                                                        'location': pos.location or '', 'date': beat.date}
                                                       )
                                beat_res_list.append(beat_detail_res)
                        beat_res.update({'detail': beat_res_list})
                        result_list.append(beat_res)
                    result.update({'result': result_list})
        return result

    @api.multi
    def view_proposed_beat_plan(self, date):
        cr = self._cr
        result = {}
        result_list1 = []
        result_list2 = []
        date_strpformat = datetime.strptime(date, "%Y-%m-%d")
        date_strfformat = date_strpformat.strftime('%Y-%m-%d')
        cr.execute("""SELECT id from proposed_beat_plan where create_uid = %s and date::date = %s""",
                   (self.id, date_strfformat))
        proposed_sr = cr.dictfetchall()
        proposed_obj = []
        for key in proposed_sr:
            sr_obj = self.env['proposed.beat.plan'].search([('id', '=', key['id'])])
            proposed_obj.append(sr_obj)
        assigned_beat = self.env['retail.group'].search([('assigned_to', '=', self.id), ('allotted_date', '=', date)],order ='create_date desc')
        for beat1 in assigned_beat:
            beat_res1 = {'beat_id': beat1.id, 'user_id': beat1.assigned_to.id, 'user_name': beat1.assigned_to.name,
                         'proposed_date': beat1.create_date, 'beat_name': beat1.name, 'state': 'approved'}
            beat_res_list1 = []
            for pos in beat1.retail_id:
                beat_detail_res1 = {}
                if self.id == beat1.assigned_to.id:
                    beat_detail_res1.update({'id': pos.id,
                                             'pos_name': pos.name,
                                             'pos_id': pos.pos_id,
                                             'Latitude': pos.latitude,
                                             'Longitude': pos.longitude,
                                             'created_user': pos.create_uid.name,
                                             'create_uid': pos.create_uid.id,
                                             'create_date': pos.create_date,
                                             'location': pos.location or '', 'date': beat1.create_date,
                                             'assigned_to': pos.assigned_to.id, }
                                            )
                    beat_res_list1.append(beat_detail_res1)
            beat_res1.update({'detail': beat_res_list1})
            result_list1.append(beat_res1)
        for beat in proposed_obj:
            beat_res = {'beat_id': beat.id, 'user_id': beat.user_id.id, 'user_name': beat.user_id.name,
                        'proposed_date': beat.date, 'beat_name': beat.beat_name, 'state': beat.state}
            beat_res_list = []
            for pos in beat.pos_id:
                beat_detail_res = {}
                if self.id == beat.user_id.id:
                    beat_detail_res.update({'id': pos.id,
                                            'pos_name': pos.name,
                                            'pos_id': pos.pos_id,
                                            'Latitude': pos.latitude,
                                            'Longitude': pos.longitude,
                                            'created_user': pos.create_uid.name,
                                            'create_uid': pos.create_uid.id,
                                            'create_date': pos.create_date,
                                            'location': pos.location or '', 'date': beat.date,
                                            'assigned_to': pos.assigned_to.id, }
                                           )
                    beat_res_list.append(beat_detail_res)
            beat_res.update({'detail': beat_res_list})
            result_list2.append(beat_res)
        result_list = result_list1 + result_list2
        result.update({'result': result_list})
        return result

    @api.multi
    def update_proposed_beat_plan(self, vals):
        cr = self._cr
        if 'beat_id' in vals:
            proposed_obj = self.search([('id', '=', vals['beat_id'])])
            user_sr = self.env['res.users'].search([('id', '=', vals['user_id'])])
            if 'state' in vals and vals['state']:
                proposed_obj.write({'state': vals['state']})
                fcm_authorization_key = self.env['ir.config_parameter'].get_param('fcm.authorization.key')
                send = True
                tokens = []
                if proposed_obj.user_id.user_tokens:
                    for token in proposed_obj.user_id.user_tokens:
                        tokens.append(token.name)
                message = "'%s' has been '%s' by '%s'" % (proposed_obj.beat_name, vals['state'], user_sr.name)
                data_json = {"channeltype": "proposedBeatUpdate",
                             "name": proposed_obj.user_id.name,
                             "author_id": proposed_obj.user_id.id,
                             "message": message}
                data = {'data': {'title': "Proposed Beat Create",
                                 'body': "Proposed Beat Create",
                                 'sound': 'default',
                                 'click_action': 'FCM_PLUGIN_ACTIVITY',
                                 'icon': 'fcm_push_icon',
                                 'color': '#F8880D'},
                        'data': data_json,
                        'registration_ids': tokens,
                        'priority': 'high',
                        'forceShow': 'true'}
                headers = {'Authorization': fcm_authorization_key,
                           'Content-Type': 'application/json'}
                _logger.info("Input Data ------ %s" % data)
                data = json.dumps(data)
                try:
                    if send:
                        res = requests.post("https://fcm.googleapis.com/fcm/send", data=data, headers=headers)
                        _logger.info("FCM Response ------ %s" % res.text)
                except:
                    raise osv.except_osv(_('Notification not sent'))
            if 'reject_pos' in vals:
                if len(vals['reject_pos']) > 0:
                    cr.execute('''delete from proposed_beat_plan_rel where beat_plan_id= %s and retail_id in %s''',
                               (vals['beat_id'], tuple(vals['reject_pos'])))
            if 'new_pos_id' in vals:
                if len(vals['new_pos_id']) > 0:
                    value = {'pos_id': [(4, vals['new_pos_id'])]}
                    res = proposed_obj.write(value)

        return True
