import datetime
import logging
from datetime import datetime, timedelta

from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class RetailCustomer(models.Model):
    _inherit = 'retail.customer'

    retail_ids = fields.Many2many('retail.group', 'retail_customer_rel', 'retail_id', 'group_id', 'Beat Management')


class RetailGroup(models.Model):
    _name = 'retail.group'
    _rec_name = 'name'

    name = fields.Char('Beat Plan', required=True)
    beat_id = fields.Char('Beat Id')
    assigned_to = fields.Many2one('res.users', string="Assigned To")
    start_date = fields.Date('Start Date', default= fields.datetime.today())
    end_date = fields.Date('End Date', default= fields.datetime.today() + timedelta(days=365))
    allotted_date = fields.Date('Allotted Date', default= fields.datetime.today())
    frequency = fields.Integer('Frequency', default=1)
    next_date = fields.Date('Next Allotted Date')
    retail_id = fields.Many2many('retail.customer', 'retail_customer_rel', 'group_id', 'retail_id', 'Retail')
    cron = fields.Boolean('Cron', default=False)
    location_id = fields.One2many('retail.location','retail_group_id', string='Location')


    # _defaults = {
    #         'start_date': datetime.today(),
    #         'end_date': datetime.today() + timedelta(days=365),
    #         'allotted_date': datetime.today(),
    #         'frequency': 1,
    #         'cron': False,
    #     }

    @api.one
    @api.constrains('allotted_date')
    def _constrains_allotted_date(self):
        """ Constrains to Check Allotted date"""
        if self.start_date > self.allotted_date:
            raise NameError("The 'Allotted date' must be posterior to the 'Start date'.")
        if self.end_date < self.allotted_date:
            raise NameError("The 'Allotted date' must be anterior to the 'End date'.")
        self.next_date = datetime.strptime(self.allotted_date, '%Y-%m-%d') + timedelta(days=self.frequency)

    @api.one
    @api.constrains('start_date', 'end_date')
    def _constrains_date(self):
        """ Constrains to Check Start and End date"""
        if self.start_date > self.end_date:
            raise NameError("The start date must be anterior to the end date.")

    @api.one
    @api.constrains('frequency')
    def _constrains_frequency(self):
        """ Set Next date using frequency"""
        self.next_date = datetime.strptime(self.allotted_date, '%Y-%m-%d') + timedelta(days=self.frequency)

    @api.multi
    def write(self, values):
        """ To Change assigned_to, start, end, allotted date and frequency"""
        for retail in self.retail_id:
            if 'assigned_to' in values and values['assigned_to'] != retail.assigned_to.id:
                retail.write({'assigned_to': values['assigned_to']})
            if 'start_date' in values and values['start_date'] != retail.start_date:
                retail.write({'start_date': values['start_date']})
            if 'end_date' in values and values['end_date'] != retail.end_date:
                retail.write({'end_date': values['end_date']})
            if 'allotted_date' in values and values['allotted_date'] != retail.allotted_date:
                if not self.cron:
                    retail.write({'allotted_date': values['allotted_date']})
                elif retail.allotted_date < str(values['allotted_date']):
                    retail.write({'allotted_date': values['allotted_date']})
                    self.write({'cron': False})
            if 'frequency' in values and values['frequency'] != retail.frequency:
                retail.write({'frequency': values['frequency']})
        return super(RetailGroup, self).write(values)

    @api.multi
    def _frequency_cron(self):
        _logger.info("Cron job for Updating Next date in Retail using frequency(Beat Management)")
        for retail in self.search([]):
            if retail.frequency and str(datetime.today() - timedelta(days=1)) > retail.allotted_date < retail.end_date:
                _logger.info(retail)
                next_date = datetime.strptime(retail.allotted_date, '%Y-%m-%d') + timedelta(days=retail.frequency)
                retail.write({'cron': True})
                retail.write({'allotted_date': next_date})
        return True

    @api.multi
    def _frequency_extend(self, days):
        _logger.info("Cron job for Extending %s days in Retail End Date for all(Beat Management)" % days)
        for retail in self.search([]):
            if retail.end_date < str(datetime.now()):
                next_date = datetime.strptime(retail.end_date, '%Y-%m-%d') + timedelta(days=days)
                retail.write({'end_date': next_date})
        return True


    @api.multi
    def get_beat_details(self,user_id,date):
        pos_list = []
        result = {}
        # pos_search_obj=self.search([('allotted_date','=',date),('assigned_to','=',user_id)])
        pos_search_obj=self.search([('allotted_date','=',date),('assigned_to','=',user_id)])
        for pos in pos_search_obj.retail_id:
            pos_dict = {}
            pos_dict.update({'id': pos.id,
                               'pos_name': pos.name,
                               'pos_id': pos.pos_id,
                               'Latitude': pos.latitude,
                                'Longitude': pos.longitude,
                               'created_user': pos.create_uid.name,
                               'create_uid': pos.create_uid.id,
                               'create_date': pos.create_date,
                                'location': pos.location or ''})
            pos_list.append(pos_dict)
        result.update({"result": pos_list})
        _logger.info(result)
        return result

    @api.multi
    def get_beat_dropdown(self,user_id):
        beat_list = []
        result = {}
        pos_search_obj=self.search([('assigned_to','=',user_id)])
        for beat in pos_search_obj:
            beat_dict = {}
            beat_dict.update({'id': beat.id,
                               'beat_name': beat.name,
                              'beat_name': beat.beat_id,
                               })
            beat_list.append(beat_dict)
            result.update({"result": beat_list})
            _logger.info(result)
        return result

class RetailLocation(models.Model):
    _name = 'retail.location'

    retail_group_id = fields.Many2one('retail.group', string='Retail Group Reference')
    latitude=fields.Float('Latitude', digits=(6, 10))
    longitude= fields.Float('Longitude', digits=(6, 10))
