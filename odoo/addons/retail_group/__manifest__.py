# -*- coding: utf-8 -*-
{
    'name': "Retail Management",
    'summary': """Set Start, End, Allotted Date and Frequency for group of Retail at a time""",
    'description': """
        This module helps to set Start, End, Allotted Date and Frequency for group of Retail at a time
    """,
    'author': "Parrot Solutions",
    'website': "parrot.solutions",
    'version': '0.1',
    'depends': [
        'retail_parrot',
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/retail_security.xml',
        'views/retail_view.xml',
        'views/proposed_beat_plan_view.xml',
        # 'data/retail_group_cron.xml',
    ],
    'application': True,
}
